#!/usr/bin/perl
# -*- fundamental -*-

# handy rune for grepping net orders etc:
# <reversers.net perl -ne 'next unless m/\bPOINTS-(\d+)\b/; printf "%02d\t%s",$1,$_' | sort

use IO::Handle;

die unless @ARGV==1;
$ARGV[0] =~ m,\b(reversers|detectors)(?:\.net-spec)?$, or die;
$$1= 1;

@indiv= qw(
		icsp_pcq
		l_gnd
		led_red
		led_green
	);

if ($reversers) {
	push @indiv, qw(
		cdu_enable
		booster_shutdown
		booster_overload
		userfault
		booster_dirn
		booster_pwm
		spare0
		spare1
		spare2
		rs232_rxin
		rs232_txout
		rs232_fcin
		rs232_fcout
		);
	print "INDIV1 26 @indiv\n"; # 25-way D
	%val = qw(
		q	5
		p	17
		x	qmid=
		o	8
		);
	$val{'m'}= '';
	$val{'M'}= '';
	$val{'n'}= '';
} else {
	push @indiv, qw(
		spare0
		analogue
		rs232_rxin
		rs232_txout
		pwmout
		extint0
		extint1
		ptdrain0..10
		);
	%val = qw(
		q	20
		p	9
		x	q=
		o	4
		);
	@sensel= qw(18 15 12 9 6 0 3  19 16 13 10 7 1 4  20 17 14 11 8 2 5);
	$val{'m'}.= "sensei{";
	for ($j=0; $j<16; $j+=8) {
		for ($i=0;$i<4;$i++) {
			$val{'m'} .= sprintf " %d %d", $i+$j, $i+$j+4;
		}
	}
	$val{'m'}.= " sensei19..16 20 }{ @sensel }";
	$val{'M'}= $val{'m'};
	$val{'M'} =~ s/sensei/sense/g;
	@senser= @sensel[0..12];
	push @senser, qw(20 2 -  4 17 - 8 11 14 5 -);
	@senser= map { s/^\d/sense$&/; $_ } @senser;
	print "RAS0 9 l_vcc @senser[0..7]\n";
	print "RAS1 9 l_vcc @senser[8..15]\n";
	print "RAS2 9 l_vcc @senser[16..23]\n";
}

print "INDIV0 25 :13-%12/2 @indiv\n"; # 26-way ribbon

$doing= 1;

while (<DATA>) {
	next if m/^\#/;
	if (m/^\^\=(reversers|detectors)\:\s*$/) {
		$doing= $$1;
		next;
	}
	next unless $doing;
	s/\^(\w)/ die $1 unless exists $val{$1}; $val{$1}; /ge;
	print;
}

die $! if STDOUT->error;

__DATA__

l_gnd	Power
l_vcc	Power
rly_gnd	Power
rly_v12	Power

!PIC-DEFINE 40
		1 mclr
		2 RA0..5 RE0..2
		11 l_vcc l_gnd clock
		14 RA6 RC0..3 RD0 RD1
		21 RD2 RD3 RC4..7 RD4..7
		31 l_gnd l_vcc
		33 RB0..7

!type Fat q0..^q qmid0..^q
!type Power ptdrain0..^p

# 32-way ribbon
BUS 32
		      icsp_pdw 
		l_gnd icsp_pdrany 
		l_gnd icsp_pdrall 
		l_gnd clock 
		l_gnd l_vcc mclr
		l_gnd i2c_clock led_reflow i2c_data led_refhigh
		rly_gnd rly_v12
		cdu_gnd cdu_out

CAC05 2		l_gnd l_vcc
0-1/1 CPIC= 2	l_gnd l_vcc

0-^q/1 BR= 4
		:1+2 sshort=*2
		:2 ^x
		:4 q

0-^q/1 RS= 2
		sensei= ^x

AC05 14
		:8+
		icsp_pdrall icsp_pdrall_mid icsp_pdrall_mid icsp_pd
		icsp_pdrany icsp_pd
		l_vcc
		:7-
		l_gnd
		icsp_pdw_mid icsp_pdw
		ptgate0 icsp_pd
		- l_gnd

RPDR 2		icsp_pdrall_mid l_vcc
RPDWU 2		icsp_pdw_mid l_vcc
RPDWI 2		icsp_pdw_mid icsp_pd
RPT0U 2		ptgate0 l_vcc

RPCI 2		icsp_pc icsp_pcq
RPCL 2		icsp_pcq l_gnd
RPCPT 2		icsp_pc ptgate1

0-^p/1 PTFET= 3	ptgate= ptdrain= cdu_gnd
0-^p/1 PTD= 2	cdu_out ptdrain=
2-^p/8 RAP= 16	:1 pt<..> :-0- ptgate<..>

cdu_out		Power
cdu_gnd		Power

TS912 8
		:1 ledmid_red perpicled led_reflow
		:7- ledmid_green led_refhigh perpicled
		:4 l_gnd
		:8 l_vcc

RLG 2		ledmid_green led_green
RLR 2		ledmid_red led_red
RLL 2		perpicled l_gnd

0-^q/4 OC= 16==^o ^m ^M
		:1+2 sensei<..>
		:2+2 q*&
		:-1-2 l_gnd*&
		:-0-2 sense<..>

^=reversers:

!type Fat t0..5

!PIC-ASSIGN
		1 mclr
		spare0
		reverse0..2
		sense3
		reverse3..5
		sense2
		l_vcc l_gnd clock
		sense1
		pt17,booster_dirn
		pt16,booster_pwm
		sense0
		i2c_clock
		19 sense5..4
		:40-
		pt0,icsp_pd
		pt1,icsp_pc
		pt6,cdu_enable
		pt7,rs232_fcin
		pt2
		pt8,booster_shutdown,
		pt9,booster_overload
		pt10,userfault
		l_vcc
		l_gnd
		pt3
		pt11,spare2
		pt12,spare1
		pt4
		pt13,rs232_rxin
		pt14,rs232_txout
		pt15,rs232_fcout
		i2c_data
		pt5
		21 perpicled

TERM 14
		1 t q
		:3+2 t0..5
		:4+2 q0..5

ULN 18
		:11 rlydrv5..0
		:8- reverse5..0 rly_gnd rly_gnd
		:9 rly_gnd
		:10 rly_v12

CULN 2		rly_gnd rly_v12

RAS 9
		l_vcc
		:2 sense3..0 sense5..4
		- perpicled

POINTS 25
		: 13-1 13 25-1 : ptdrain1..17
		:14 ptdrain0

0-5/1 RLY= 16
		:1 rly_v12
		:4 q=
		:6 qmid=
		:8 t
		:9 qmid=
		:11 t
		:13 t=
		:16 rlydrv=

^=detectors:

#0-^q/8 RAS= 9
#		l_vcc
#		:2 line{ line0..20 - - }{ ^r } line&

RAS2 9
		:9 perpicled

!PIC-ASSIGN
		1 mclr
		pt4,analogue
		pt5,spare0
		sense18
		sense15
		sense12
		sense9
		sense6
		sense0
		sense3
		l_vcc l_gnd clock
		sense19
		sense16
		sense13
		sense10
		i2c_clock
		sense7
		sense1
		:40-
		pt0,icsp_pd
		pt1,icsp_pc
		pt2
		sense4
		sense20
		sense17
		pt6,extint1
		pt7,extint0
		l_vcc
		l_gnd
		sense2
		sense11
		sense14
		pt3,pwmout
		pt8,rs232_rxin
		pt9,rs232_txout
		sense5
		i2c_data
		sense8
		21 perpicled

TERM 22
		q0..20 q
