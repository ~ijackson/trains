#!/usr/bin/perl
while (<>) {
    if (m/^\s*(Pin|Via)\[\-?\d+ \-?\d+ (\d+) (\d+) (\d+) (\d+) .*\]$/) {
	$have{ sprintf "%s %05d %05d %05d %05d",
	      $1,$2,$3,$4,$5 }++;
    } elsif (m/^ \s* (?:
		      PCB | Grid | Cursor | Thermal | DRC |
		      Flags | Groups | Styles |
		      Layer | Line | Text | Arc | Polygon |
		      Symbol | SymbolLine |
		      Element | ElementLine | ElementArc |
		      NetList | Net | Connect ) \s* (?: \( | \[ ) /x ||
	     m/^ \s* [()\#] /x ||
	     !m/\S/ ||
	     m/^ (?: \s* \[ \d+ \s \d+ \] )+ \s* $/x) {
    } else {
	die "$_ ?";
    }
}

@i= qw(Pv Pad Poclr Rsist Drill PaRs DrPa Count);
$ix=0; map { $i{$_}= $ix++; } @i;

printf "%-3s %5s %5s %5s %5s  %6s %6s  %5s\n", @i
    or die $!;

foreach $k (sort keys %have) {
    @v= split / /, $k;
    $v[$i{Count}]= $have{$k};
    $v[$i{PaRs}]= 0.005 * ($v[$i{Rsist}]-$v[$i{Pad}]);
    $v[$i{DrPa}]= 0.005 * ($v[$i{Pad}]-$v[$i{Drill}]);
    printf "%-3s %5d %5d %5d %5d  %6.2f %6.2f  %5d\n", @v
	or die $!;
}
