# recommended programming order
#   one test on PIC#0
#	first time:				erase, write FOO+entire0.hex
#	subsequently if only FOO.asm changed:	update FOO.hex
#   for all pics
#	first time:	for each individually	erase, write perpicNUM.hex
#			then for all		update FOO.hex
#	subsequently:	for all			update FOO.hex
#   other possibilities are not ruled out
#
# For actual program from detpic, we program (with odyssey-train):
#      odyssey-train <n> write program+code.hex
# and then later
#        write/update  program+program/code.hex
# etc.
#
# filename conventions - contents of hex files
#   FOO.hex		FOO.o (only)
#   FOO-withcfg.hex	FOO.o				config.o
#   FOO+entire0.hex	FOO.o		idlocs0.o	config.o
#   perpicNUMBER.hex			idlocsNUMBER.o	config.o

#ASFLAGS=	-Dmclock=20000 -Dsclock=20000
CLOCKS=		-Dmclock=32000 -Dsclock=4000
ASFLAGS=	$(CLOCKS)

HEXMERGER=	$(CEBPIC)merge-hex
MERGEHEX=	./$^ $o

PIC=		18f458
LINK=		gplink -m -o $@ $^
ASSEMBLE=	gpasm -p$(PIC)  $(ASFLAGS)
DOASSEMBLE=	$(ASSEMBLE) -c $< && mv $*.lst $*+asm.lst

PIC_HEADER=	/usr/share/gputils/header/p$(PIC).inc

%.o:		%.asm $(INCLUDES)
		$(DOASSEMBLE)

TIA_DIR=	../iwjpictest
TIA_TIA=	$(TIA_DIR)/to-insn-aliases
TIA_ALIASES=	$(TIA_DIR)/insn-aliases.inc
TIA=		$(TIA_TIA) -A $(TIA_ALIASES) -H $(PIC_HEADER)

%.disasm:	%.hex %.map $(TIA_TIA) $(TIA_ALIASES)
		gpdasm -p$(PIC) $(word 1,$+) >$@.tmp
		$(TIA) -M $(word 2,$+) <$@.tmp $o
		@rm $@.tmp

.PRECIOUS:	%.o

pic-clean:
		-rm -f -- *~ *.new *.tmp *.disasm
		-rm -f *.hex *.cod *.lst *.o *.map
