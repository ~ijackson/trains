/**/

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/kd.h>

int main(int argc, char **argv) {
  uint8_t l;
  int r;
  r= ioctl(0, KDGETLED, &l);
  if (r<0) { perror("KDGETLED"); exit(-1); }
  printf("%02x\n", l);
  return 0;
}
