/*
 * realtime
 * helpers for event handling
 */

#include <assert.h>
#include <limits.h>

#include "realtime.h"

const char toev_fast_pclass[]= "fast";

void real_mgettimeofday(struct timeval *tv) {
  int r;
  r= gettimeofday(tv,0);
  if (r) diee("gettimeofday failed");
}

static int toev_sim_show(TimeoutEvent *toev) {
  return simlog_full || toev->pclass!=toev_fast_pclass;
}

void *toev_callback(oop_source *source, struct timeval tv, void *t_v) {
  TimeoutEvent *toev= t_v;
  if (toev_sim_show(toev)) {
    simlog("timer-event %s.%s\n",toev->pclass,toev->pinst);
    simlog_flush();
  }
  toev->running= 0;
  toev->callback(toev);
  return OOP_CONTINUE;
}

void toev_init(TimeoutEvent *toev) { toev->running= 0; }

void toev_start(TimeoutEvent *toev) {
  toev_stop(toev);
  if (toev->duration==-1) return;
  toev->running= 1;
  if (toev_sim_show(toev)) mgettimeofday(&toev->abs);
  else real_mgettimeofday(&toev->abs);
  assert(toev->duration < INT_MAX/1000);
  toev->abs.tv_usec += toev->duration * 1000;
  toev->abs.tv_sec += toev->abs.tv_usec / 1000000;
  toev->abs.tv_usec %= 1000000;
  if (simulate) sim_toev_start(toev);
  else events->on_time(events, toev->abs, toev_callback, toev);
}

void toev_stop(TimeoutEvent *toev) {
  if (!toev->running) return;
  toev->running= 0;
  if (simulate) sim_toev_stop(toev);
  else events->cancel_time(events, toev->abs, toev_callback, toev);
}

void mgettimeofday(struct timeval *tv) {
  if (simulate) {
    sim_mgettimeofday(tv);
  } else {
    real_mgettimeofday(tv);
  }
  simlog("timestamp %ld.%06ld\n",tv->tv_sec,tv->tv_usec);
  simlog_flush();
}
