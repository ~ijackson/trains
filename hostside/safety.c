/*
 * realtime
 * safety core algorithm
 */

#include <stdio.h>
#include <assert.h>

#include "realtime.h"

int n_trains;
Train *trains;
Segment *segments;



/*
 * New signalling/safety algorithm:
 *
 * When we detect a train at the relevant det_expected we firstly
 * update our knowledge of its current position.  We don't change
 * any ownerships just yet, but we do set pred_present for the parts
 * where the train is right now.
 *
 * We then check to see that when we reach the _next_ block boundary,
 * we'll still be able to stop safely (unless we're already doing an
 * emergency stop).  If this fails we issue the emergency stop command
 * now.  This prediction is done as follows:
 *
 * We use three tracklocs which move roughly together - one for the
 * foredetect and one for each end of our train.  This represents a
 * predicted train position.  (We use `predicted' to refer to these
 * future situations and `current' to refer to that prevailing at the
 * time we are making the prediction.)  The advancement is driven
 * using the foredetect; when it enters a new segment we advance first
 * the head and then the tail by the same distance.
 *
 * We advance foredetect by the total of the uncertainty (which takes
 * it into the next segment) and the current stopping distance.  At
 * each advancement we record the earliest time from now at which this
 * prediction will happen (computed from the current speed).  (We
 * don't take into account any already commanded but not necessarily
 * happened deceleration; however if we are attempting to increase our
 * speed, current speed refers to the new speed.)  If we're already
 * stopping then we advance by the stopping distance only (since we
 * don't need to worry about not being told to stop until the next
 * detection); also we stop advancing when we run out of segments we
 * own (this deals with segment-sized rounding errors which might
 * otherwise make us think we will overrun).
 *
 * At each new track location for our front end we hope to find a
 * segment which no-one currently owns and which is currently set for
 * us.  (We check this before updating our tail trackloc as below.)
 * In that case we update the segment's until.)  (We use the value
 * `elapsed' which is the minimum time until the current nose
 * prediction.)
 *
 * However it may be a segment which we currently own, or perhaps it
 * may be set against us.  (If it's owned by anyone else or is not
 * owned but moving then we are definitely doomed and must stop.)
 *
 * If it's owned by us but we haven't claimed it yet in this
 * prediction then that's fine we can just claim it.
 *
 * If we _have_ claimed it then it had better be predicted to be
 * vacant by the time we get there (pred_present clear, see below).
 *
 * Regarding moveable features, we continuously develop our plan,
 * as represented in route_plan and route_reservation.
 *
 * If the feature is currently moving and owned by us then that must
 * be OK because we checked it before (except that if we're trying to
 * increase our speed that may not be OK and we must replan the speed
 * increase to check whether the movement will happen in time).
 *
 * If it is not currently moving and is set against us, then we plan
 * to set it.  If there is already a plan (ie, a reservation) and
 * we're not accelerating then that's fine.  Otherwise we make one: we
 * determine the desired movposcomb make a reservation to move it in
 * time.  (If the segment is already owned by us that means a
 * reservation to move it between when the tail leaves and the nose
 * enters.)
 *
 * When predicted foredetect enters a new segment we add the new
 * segment to the tally of segments of the particular kind and
 * train-relative orientation (for polarity purposes), and check that
 * a solution is possible.  For the first such solution (ie, the one
 * for the immediately-forthcoming detection) we actually implement
 * it.
 *
 * And, when predicted foredetect enters a new segment we advance the
 * predicted tail (as described above).  We set pred_vacated and
 * until, and remove the departing segments from tallies.
 *
 * If all of this works then pred_present and pred_vacated constitute
 * the new ownership and motion_newplan constitutes the new plan.  We
 * commit to it by rewriting owner and det_* and actually requesting
 * any movfeat changes which we can do now, and replacing motion with
 * motion_newplan.
 *
 * If not then we issue an signal stop command (we'd better not be
 * doing that already!) or countermand the speed increase or whatever.  */
/*
 * Here is how we use the stuff in the Segment:
 *
 *  owner		train which owns this for signalling purposes
 * 				no other train may interfere
 *
 *  physical		in an unowned segment, this has no particular
 *  feature			relationship to anything
 *   &
 *  movposcomb		in an owned segment, if moving, means that
 *   &				we have a promise that the feature
 *  moving			will be OK by the time we get there
 *   &				at our current speed
 *  motion		if not moving then motion is our reservation which
 * 				allows us to know that we are going to
 * 				be able to change this segment between
 * 				the tail leaving it and the head arriving
 *			in each case movpos_change_intent tells us the plan
 *
 *  motion_newplan	while we are making a plan this is the new value
 *				for motion.  If identical to motion it
 *				means we should keep it; if 0 we cancel
 *				or unreserve; if a different value we
 *				commit to the new plan instead
 *
 *			during prediction, motion_newplan can be non-0 for
 *				unowned segments as we make our plan.
 *				If we succeed we will end up
 *				owning the segment, and if we fail
 *				we garbage collect it
 *
 *  det_ignore		this is part of the body of a train which we've
 * 				already detected here
 *
 *  det_expected	this is the next place we expect owner train
 *				to arrive
 * 			(it is not valid to have both det_ignore
 *			 and _expected)
 *
 * VALID DURING PREDICTION ETC. ONLY
 *
 *  pred_present	in our prediction, the train physically occupies this
 * 				until_here tells us when will be
 *
 *  pred_vacated 	in our prediction, the train does not occupy this
 * 				although it did in previous predictions
 * 			until_here tells us when it will have been vacated
 * 			if set together with pred_present, means was vacated
 * 				and then reentered; do not record vacation
 *				again
 *
 *			iff either of these is set, this train is going
 *				to own the segment when we're finished
 *
 *  now_present		train is in this segment now (in which we include
 *				the segment just before foredetect)
 *
 *  until               time until the we will arrive (pred_present), or
 *				depart (pred_vacated), valid only if
 *				one of those is set
 *				value is as for u->elapsed
 *
 * Valid combinations during prediction:
 *
 *					   now_present
 *					     	 | pred_present
 *					     	 |/ vacated
 *				     		 ||/
 *				     		 |||
 *						 NPV  until
 *
 *   has never been here as far as we know	 ---  undef
 *   here now, prediction has it still here	 NP-  arrival = 0
 *   here now, but predicted to have departed	 N-V  departure
 *   pred. to have arrived here			 -P-  arrival
 *   pred. to have arrived and departed		 --V  departure
 *   here now, pred. to have dep'd and ret'd	 NPV  re-arrival
 *   now absent, pred. to have arr, dep & ret'd	 -PV  re-arrival
 *
 *  seg->tr_backwards records the sense of the _first_ passage of the train.
 *
 *  invalid/impossible combination:
 *   train here now but never in prediction	 N--
 *
 * Meanings of moving, motion and motion_newplan:
 * For segments NPV==000, no plan made yet:
 *
 *     moving  motion  _newplan
 *	0	0	0	 segment is entirely stable
 *	0	non-0	0	 existing plan has deferred motion
 *  	1	non-0	0	 currently moving - as planned if owned
 *
 *	1	0	?	 always forbidden
 *	?	?	non-0	 forbidden for not-yet-planned segments
 *
 * For segments with NPV!=0, some kind of planning done:
 * 
 *     moving  motion  _newplan	 new plan involves
 *	0	0	0	  no motion, segment is entirely stable
 *	0	non-0	0	  abandoning planned deferred motion
 *	0	0	non-0	  new motion, perhaps deferred
 *	any	non-0	=motion	  same motion as before
 *	any	non-0  !=motion	  different motion
 *
 *	1	0	?	  always forbidden
 *	1	non-0	0	  forbidden: need explicit motion to cancel
 *
 * (These are the values when not in the middle of nose_nextseg.)
 */
/*
 * Simulation:
 *
 *     	       	  	       	  DETECT
 *  real stop dist             	  >>>>>
 *  simul. stop dist                   	      >>>>>
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  set_exactinto     	      	  FNT
 *  adv. tail back        T    	  FN
 *  adv. nose find        T    	  F N
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  adv. fdet 1 seg       T    	    N         F                
 *   adv. nose            T    	              F N
 *   adv. tail                 	      T       F N
 *  adv. fdet stopdist         	      T         N  F
 *   adv. nose                 	      T            F N
 *   adv. tail                 	           T       F N
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *
 * At next detection:
 *     	       	       	      	       	      DETECT
 *  real stop dist             	       	      >>>>>
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  set_exactinto      	      	              FNT
 *  adv. tail back            	      T       FN
 *  adv. nose find            	      T       F N
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  adv. fdet 1 seg            	      T         N        F     
 *   adv. nose                	      T                  N     ProblemPredicted
 *
 * Oops, so stop:
 *     	       	       	      	       	      DETECT
 *  real stop dist             	       	      >>>>>
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  set_exactinto      	      	              FNT
 *   adv. tail back            	      T       FN
 *   adv. nose find            	      T       F N
 *    	      	      +++++++++++|+++++++++++|++++++++++|XXXXXX
 *  adv. fdet stopdist         	      T         N  F        } may give
 *   adv. nose                	      T            F N      }  HorizonReached
 *   adv. tail                 	           T       F N             instead
 *
 */

/*========== prediction machinery ==========*/

#define now_present mark0
#define pred_present mark1
#define pred_vacated mark2
#define will_polarise mark3

typedef struct {
  Train *train;
  unsigned
    speed_info_specified:1,
    neednewplan:1,
    stopping:1,
    done_first_new_fdet:1,
    usecurrentposn:1, /* for {pred,report}_getmovpos */
    optimistic:1, /* for autopoint */ 
    walk_compute_polarise:1, /* nose_nextseg still needs to worry */
    need_polarise:1, /* when we commit */
    train_polarity_inverted:1, /* right now, or if know_best, the best */
    know_best_polarity:1, /* longest-lasting into the future */
    done_fdet:1; /* for report_nextseg */
  TrackLocation nose, fdet, tail;
  TrackAdvanceContext nosec, tailc, fdetc;
  TimeInterval elapsed; /* from now, minimum */
  Distance was_distance, autopoint_distance, stopping_distance;
  double maxspeed;
  Segment *hindmost, *furthest, *desire_move;
  int desire_movposcomb;
  int lookahead, reportcount;

  PredictionProblemCallback *problem_callback;
  void *problem_callback_u;

  /* tally counts segments which have been entered in fdet_nextseg but
   * not left in tail_nextseg (ie not the same ones as pred_present),
   * in each direction (according to tr_backwards) */
  int noninv_tally[2];
} PredictUserContext;

static int tail_length(Train *tra) {
  return tra->backwards ? tra->head : tra->tail;
}
static int nose_length(Train *tra) {
  return tra->backwards ? tra->tail : tra->head;
}
static void advance_elapsed(PredictUserContext *u, int advance_distance) {
  u->elapsed += advance_distance / u->maxspeed;
}
static int calc_advanced(TrackAdvanceContext *c) {
  PredictUserContext *u= c->u;
  return u->was_distance - c->distance;
}
static void qprintf_position(Train *tra,
   void (*qp)(const char *fmt,...) __attribute__((format(printf,1,2)))) {
  qp("%s%s:%d+-%d",
     tra->foredetect->tr_backwards?"-":"",
     tra->foredetect->i->pname, tra->maxinto, tra->uncertainty);
}

#define SKELETON_PREDICT_USER_CONTEXT(u,tra)	\
  (memset(&(u),0,sizeof((u))),			\
   (u).problem_callback= ppc,			\
   (u).problem_callback_u= ppcu,		\
   (u).train= tra)
 /* for callers of predict_problem rather than predict */

/*---------- prediction problem reporting ----------*/

static ErrorCode predict_vproblem(PredictUserContext *u, Segment *seg,
				  const char *fmt, va_list al) {
  int l;
  char *message;

  l= vasprintf(&message, fmt, al);  if (l <= 0) diem();

  if (u->optimistic)
    DPRINTF(safety,predict,"   optimistic problem %s\n", message);
  else if (!u->problem_callback)
    safety_panic(u->train, seg, "unexpected problem predicted"
		 " (context: %s): %s", (char*)u->problem_callback_u, message);
  else
    u->problem_callback(u->train, seg, u->problem_callback_u, message);

  free(message);
  return EC_SignallingPredictedProblem;
}

static ErrorCode predict_problem(PredictUserContext *u, Segment *seg,
				  const char *fmt, ...) {
  ErrorCode ec;
  va_list al;
  va_start(al,fmt);
  ec= predict_vproblem(u,seg,fmt,al);
  va_end(al);
  return ec;
}

static const char *motion_pname(Segment *seg, MovPosChange *motion) {
  if (!motion) return "";
  return movpos_pname(seg, movpos_change_intent(motion));
}

static void pred_callback_debug(const char *what, TrackLocation *t,
				struct TrackAdvanceContext *c,
				const TrackLocation *before) {
  PredictUserContext *u= c->u;
  
  DPRINTF(safety,predictseg,"   %s"
	  " %c%s dist=%-4d until=%-4ld %c%c%c.%c%c (was %s%s..%d dist=%-4d)"
	  "  %c%c%c.%c%c%c%c elapsed=%ld la#%d"
	  " %s/%s %c%s >%s"
	  " nit=%d,%d\n",
	  what,

	  " -"[t->backwards],
	  t->seg->i->pname,
	  c->distance,
	  (long)t->seg->until,

	  "-N"[ t->seg->now_present ],
	  "-P"[ t->seg->pred_present ],
	  "-V"[ t->seg->pred_vacated ],
	  
	  "-p"[ t->seg->will_polarise ],
	  "-i"[ t->seg->seg_inverted ],

	  before && before->backwards?"-":"",
	  before ? before->seg->i->pname : "-",
	  before ? before->remain : -1,

	  u->was_distance,
	  
	  "-2"[ u->done_first_new_fdet ],
	  "-c"[ u->usecurrentposn ],
	  "-o"[ u->optimistic ],
	  
	  "-w"[ u->walk_compute_polarise ],
	  "-n"[ u->need_polarise ],
	  "-i"[ u->train_polarity_inverted ],
	  "-b"[ u->know_best_polarity ],
	  (long)u->elapsed,
	  u->lookahead,

	  t->seg->i->pname,
	  movpos_pname(t->seg, t->seg->movposcomb),
	  "~!"[ t->seg->moving ],
	  motion_pname(t->seg, t->seg->motion),
	  motion_pname(t->seg, t->seg->motion_newplan),

	  u->noninv_tally[0], u->noninv_tally[1]);
}

/*---------- prediction trivial callbacks ----------*/

static int pred_getmovpos(TrackLocation *t, TrackAdvanceContext *c,
			  MovPosComb *use_io) {
  PredictUserContext *u= c->u;
  if (!u->usecurrentposn &&
      !t->seg->now_present &&
      t->seg->motion_newplan)
    *use_io= movpos_change_intent(t->seg->motion_newplan);
  if (*use_io<0)
    return predict_problem(u, t->seg, "track route not known");
  return 0;
}

static int pred_trackend(TrackLocation *t, TrackAdvanceContext *c) {
  PredictUserContext *u= c->u;
  return predict_problem(u, t->seg, "end of track");
}

static int pred_trackend_panic(TrackLocation *t, TrackAdvanceContext *c) {
  PredictUserContext *u= c->u;
  safety_panic(u->train,t->seg,"unexpected end of track");
}

static int initpresent_nextseg(TrackLocation *t, TrackAdvanceContext *c,
			       MovPosComb *mpc_io,
			       const TrackLocation *before) {
  PredictUserContext *u= c->u;
  pred_callback_debug(" initpresent_nextseg",t,c,before);

  /* it had better be set for where we came from */
  if (before) {
    assert(SOMEP(*mpc_io));
    const SegPosCombInfo *pci= &t->seg->i->poscombs[*mpc_io];
    const SegmentLinkInfo *link= &pci->link[!t->backwards];
    if (!SOMEP(link->next) || &segments[link->next] != before->seg)
      return predict_problem(u, t->seg, "route set against proposed location");
  }

  t->seg->now_present= t->seg->pred_present= t->seg->will_polarise= 1;
  t->seg->tr_backwards= !t->backwards;
  t->seg->until= 0;
  if (!t->seg->i->invertible)
    u->noninv_tally[!t->backwards]++; /* ! since going backwards along train */
  return 0;
}

/*---------- prediction nose advancement ----------*/

static int nose_nextseg(TrackLocation *t, TrackAdvanceContext *c,
			MovPosComb *mpc_io, const TrackLocation *before) {
  PredictUserContext *u= c->u;
  MovPosComb route_plan;
  TimeInterval max_ms;
  ErrorCode ec;

  pred_callback_debug(" nose_nextseg",t,c,before);
  if (!before) return 0;

  if (u->optimistic)
    advance_elapsed(u,calc_advanced(c));

  /* Is it empty ? */

  if (u->stopping && u->lookahead == u->train->plan_lookahead_nsegs)
    return EC_SignallingHorizonReached;

  if (t->seg->owner) {
    if (t->seg->owner != u->train)
      return predict_problem(u, t->seg, "impeded by %s",
			     t->seg->owner->pname);
  }
  if (t->seg->pred_present)
    return predict_problem(u, t->seg, "will collide with itself!");

  if (!u->optimistic) {
    Segment *interferer= segment_interferer(t->seg);
    /* We want to avoid calling segment_interferer_does unless we have
     * to, as it uses getmovpos which, if the movposcomb is unknown,
     * will predict_problem even if the interferer is empty. */
    if (interferer &&
	(interferer->owner || interferer->pred_present)) {
      int does;
      ec= segment_interferer_does(c,t->seg,interferer, &does);
      if (ec) return ec;

      if (does) {
	if (interferer->owner && interferer->owner != u->train)
	  return predict_problem(u, t->seg, "impeded by %s at %s",
			 interferer->owner->pname, interferer->i->pname);
	if (interferer->pred_present)
	  return predict_problem(u, t->seg, "will collide with itself at %s!",
				 interferer->i->pname);
      }
    }
  }

  /* What about the route ? */

  if (t->seg->i->n_poscombs==1)
    goto movement_ok;

  if (t->seg->moving) {
    assert(!t->seg->now_present);
    if (!t->seg->owner)
      return predict_problem(u, t->seg, "route not yet set");
    if (!u->neednewplan) {
      t->seg->motion_newplan= t->seg->motion;
      *mpc_io= movpos_change_intent(t->seg->motion);
      goto movement_ok;
    }
  }

  int autopoint= u->train->autopoint && t->seg->autopoint;

  if (t->seg->motion_newplan) {
    route_plan= movpos_change_intent(t->seg->motion);
    *mpc_io= route_plan;
    goto movement_ok;
  } else if (t->seg == u->desire_move && !t->seg->now_present) {
    ec= movpos_findcomb_bysegs(before->seg,t->seg,0,
			       u->desire_movposcomb, &route_plan);
    assert(!ec);
    if (route_plan != u->desire_movposcomb)
      return predict_problem(u, t->seg, " proposed route would be against"
			     " approaching train");
    autopoint= 1;
  } else if (t->seg->motion) {
    /* We already have a plan. */
    route_plan= movpos_change_intent(t->seg->motion);
    if (!u->neednewplan) {
      t->seg->motion_newplan= t->seg->motion;
      *mpc_io= route_plan;
      goto movement_ok;
    }
  } else {
    /* Extend the plan. */
    ec= movpos_findcomb_bysegs(before->seg,t->seg,0,*mpc_io, &route_plan);
    assert(!ec); /* there must be _some_ route since
		    we're moving into t->seg */
  }
  if (route_plan == *mpc_io && !t->seg->moving) {
    /* Well, that's all fine then. */
    goto movement_ok;
  }
  /* We can make it work but have to plan some motion: */
  if (u->elapsed < 0)
    return predict_problem(u, t->seg, "train arriving but route not yet set");
  if (t->seg->pred_vacated) {
    if (!t->seg->now_present)
      /* And we're not even there yet!  This is too hard because we
       * are too stupid not to change it straight away. */
      return predict_problem(u, t->seg, "dynamic route too complicated"
			     " - would need multiple queued changes");
    max_ms= u->elapsed - t->seg->until;
  } else {
    max_ms= u->elapsed;
  }

  if (!autopoint)
    return predict_problem(u,t->seg,"will not automatically set route");

  ec= movpos_reserve(t->seg, max_ms, &t->seg->motion_newplan,
		     route_plan, *mpc_io);
  if (ec) return predict_problem(u, t->seg, "insufficient time to"
				 " set route: %s", ec2str(ec));
  *mpc_io= route_plan;

 movement_ok:
  /* Now we definitely have a plan which sets a good route at t->seg. */
  if (!t->seg->pred_vacated)
    t->seg->tr_backwards= t->backwards;
  t->seg->pred_present= 1;
  t->seg->until= u->elapsed < 0 ? 0 : u->elapsed;
  u->lookahead++;
  return 0; /* yay! */
}

/*---------- prediction tail advancement ----------*/

static int tail_nextseg(TrackLocation *t, TrackAdvanceContext *c,
			MovPosComb *mpc_io, const TrackLocation *before) {
  PredictUserContext *u= c->u;

  pred_callback_debug(" tail_nextseg",t,c,before);
  if (!before) return 0;

  if (!before->seg->i->invertible)
    u->noninv_tally[before->backwards]--;

  if (before->seg->pred_vacated) return 0; /* only vacate once */
  before->seg->pred_present= 0;
  before->seg->pred_vacated= 1;
  before->seg->until= u->elapsed < 0 ? 0 : u->elapsed;

  return 0;
}

/*---------- prediction foredetect advancement ----------*/

static int fdet_nextseg(TrackLocation *t, TrackAdvanceContext *c,
			MovPosComb *mpc_io, const TrackLocation *before) {
  PredictUserContext *u= c->u;
  int advanced, r;

  /* NB, on entry the movposcomb of the segment we're entering (and
   * other similar details) may not be known or sufficient because
   * when fdet has only just entered a new segment, nose is still in
   * the previous segment.
   */

  pred_callback_debug("fdet_nextseg",t,c,before);
  if (!before) return 0;

  advanced= calc_advanced(c);
  advance_elapsed(u,advanced);

  u->nosec.distance= advanced;
  r= trackloc_advance(&u->nose,&u->nosec);
  if (r) {
    DPRINTF(safety,predictseg,"   fdet_nextseg  r=%s\n",ec2str(r));
    return r;
  }

  /* Now we have advanced the nose and have recorded any appropriate
   * motion(s).  Advancing the nose has updated the segment's
   * notion of the motion but not trackloc_advance's cached version of
   * the movposcomb in *mpc_io - but trackloc_advance will call
   * pred_getmovpos right after we return so we do not need to worry.
   */

  /* Check polarity */

  if (!t->seg->i->invertible) {
    u->noninv_tally[t->backwards]++;
    if (u->noninv_tally[0] && u->noninv_tally[1])
      return predict_problem(u, t->seg, "cannot set track polarity");
  }

  if (!u->done_first_new_fdet) {
    t->seg->det_expected= 1;
    u->done_first_new_fdet= 1;
  }

  if (u->elapsed < MARGIN_POLARISETIME) {
    u->need_polarise |= (t->seg->seg_inverted ^ t->backwards ^
			 u->train_polarity_inverted);
  } else {
    u->walk_compute_polarise &= u->need_polarise;
  }

  if (u->train->autopoint && !u->autopoint_distance) {
    const SegPosCombInfo *pci;
    r= trackloc_getlink(t,c,&pci,0,-1);  assert(!r);
    u->autopoint_distance= pci->dist;
  }    

  if (u->walk_compute_polarise) {
    if (t->seg->will_polarise)
      /* this is our 2nd visit, stop now */
      u->walk_compute_polarise= 0;
  }
  if (u->walk_compute_polarise) {
    if (!u->know_best_polarity) {
      if (u->noninv_tally[0]) {
	u->train_polarity_inverted= 0;
	u->know_best_polarity= 1;
      } else if (u->noninv_tally[1]) {
	u->train_polarity_inverted= 1;
	u->know_best_polarity= 1;
      }
    }
    if (u->know_best_polarity &&
	!t->seg->i->invertible &&
	u->train_polarity_inverted != t->backwards) {
      /* incompatible, stop now */
      u->walk_compute_polarise= 0;
    }
  }
  if (u->walk_compute_polarise) {
    t->seg->will_polarise= 1;
  }

  /* Advance the tail */

  u->tailc.distance += advanced;
  if (u->tailc.distance > 0) {
    r= trackloc_advance(&u->tail,&u->tailc);
    assert(!r);
    assert(!u->tailc.distance);
  }

  /* Final adjustments, prepare for next iteration */

  u->was_distance= c->distance;
  return 0;
}

/*---------- backtracking the optimistic nose ----------*/

static int optunwind_nextseg(TrackLocation *t, TrackAdvanceContext *c,
			     MovPosComb *mpc_io, const TrackLocation *before) {
  PredictUserContext *u= c->u;

  pred_callback_debug("optunwind_nextseg",t,c,before);

  if (t->seg == u->furthest ||
      t->seg->motion_newplan)
    return EC_SignallingHorizonReached;

  t->seg->pred_present= 0;
  t->seg->until= 0;
  u->lookahead--;
  return 0;
}

/*========== prediction entrypoint ==========*/

ErrorCode predict(Train *tra, struct timeval tnow, unsigned flags,
		  Segment *desire_move, int desire_movposcomb,
		  const SpeedInfo *speed_info,
		  PredictionProblemCallback *ppc, void *ppcu) {
  PredictUserContext u;
  SpeedInfo speed_info_buf;
  Segment *foredet;
  SEG_IV;
  ErrorCode ec;
  int DP;

  memset(&u,0,sizeof(u));
  u.train= tra;
  u.problem_callback= ppc;
  u.problem_callback_u= ppcu;
  u.neednewplan= !(flags & PREDF_OLDPLAN);
  u.speed_info_specified= !!speed_info;

  if (!speed_info) {
    speedmanager_getinfo(tra,tnow,&speed_info_buf);
    speed_info= &speed_info_buf;
  }
  u.stopping= (flags & PREDF_OLDPLAN) && speed_info->stopping;
  u.maxspeed= speed_info->max_speed_estimate;
  u.stopping_distance= speed_info->stopping_distance;

  u.desire_move= desire_move;
  u.desire_movposcomb= desire_movposcomb;

  Distance detectable= ceil(tra->detectable * MARGIN_TRAINLENGTH);

  if (DEBUGP(safety,core)) {
    DPRINTF(safety,core," ***predicting*** %s%s ",
	    tra->backwards?"-":"",tra->pname);
    qprintf_position(tra,DPRINTFA);
    DPRINTFA(" det.able=%d %smaxspeed=%f%s stopdist=%d (speed %f, step %d%s)"
	     " flags=%c%c%s desire=%s/%s\n",
	     detectable,
	     u.speed_info_specified ? "specified " : "",
	     u.maxspeed, u.stopping ? "(stopping)" : "",
	     u.stopping_distance,
	     tra->speed.speed, tra->speed.step,
	     tra->speed.decel.running ? " decel" : "",
	     "-j"[ !!(flags & PREDF_JUSTDET) ],
	     "no"[ !!(flags & PREDF_OLDPLAN) ],
	     u.stopping ? " stopping" : "",
	     u.desire_move ? u.desire_move->i->pname : "",
	     u.desire_move ? u.desire_move->i->poscombs[u.desire_movposcomb]
	                    .pname : "");
  }

  FOR_SEG {
    if (!seg->owner || seg->owner == u.train)
      seg->det_expected= 0;
    seg->now_present= seg->pred_present=
      seg->pred_vacated= seg->will_polarise= 0;
  }

  foredet= tra->foredetect;
  assert(foredet);

  u.walk_compute_polarise= 1;
  u.usecurrentposn= 1;
  u.train_polarity_inverted= foredet->seg_inverted ^ foredet->tr_backwards;

  /*----- find the train's foredetect (rearmost possibility) -----*/
  /*
   * If tra->uncertainty > tra->maxinto then in theory the foredetect
   * might be in the previous segment.  We don't take that into account,
   * so we must instead extend our idea of the length of the train.
   */

  int effective_uncertainty= tra->uncertainty;
  int effective_length= MARGIN_NOSE + tail_length(tra) + detectable;

  if (effective_uncertainty > tra->maxinto) {
    effective_length += tra->uncertainty - tra->maxinto;
    effective_uncertainty= tra->maxinto;
  }

  int effective_into_fdet= tra->maxinto - effective_uncertainty;
  int tail_advance_delaydist= MARGIN_TAIL;

  DPRINTF(safety,core,"  predict eff.uncert=%d eff.len=%d"
	  " eff.into_fdet=%d tail.adv.delay=%d\n",
	  effective_uncertainty, effective_length, 
	  effective_into_fdet, tail_advance_delaydist);

  u.fdetc.u= &u;
  u.fdetc.getmovpos= pred_getmovpos;

  trackloc_set_exactinto(&u.fdet, &u.fdetc, foredet, foredet->tr_backwards,
			 effective_into_fdet);

  /*----- find the train's tail (rearmost possibility) -----*/
  /*
   * We walk backwards, also marking the train present.
   */

  u.tailc.u= &u;
  u.tailc.getmovpos= pred_getmovpos;
  u.tailc.nextseg= initpresent_nextseg;
  u.tailc.trackend= pred_trackend_panic;
  u.tailc.distance= effective_length;

  u.tail= u.fdet;

  ec= trackloc_reverse_exact(&u.tail,&u.tailc);  assert(!ec);

  ec= trackloc_advance(&u.tail,&u.tailc);
  if (ec) { assert(flags & PREDF_INITQUEST); return ec; }

  ec= trackloc_reverse_exact(&u.tail,&u.tailc);  assert(!ec);

  u.hindmost= u.tail.seg;

  /*----- find the train's nose (rearmost possibility) -----*/

  u.usecurrentposn= 0;
  u.nosec.u= &u;
  u.nosec.getmovpos= pred_getmovpos;
  u.nosec.nextseg= nose_nextseg;
  u.nosec.trackend= pred_trackend;
  u.nosec.distance= nose_length(tra);

  u.nose= u.fdet;

  ec= trackloc_advance(&u.nose,&u.nosec);
  if (ec && ec != EC_SignallingHorizonReached) goto xproblem;

  /*----- predict the future - (including the present uncertainty) -----*/

  u.fdetc.nextseg= fdet_nextseg;
  u.fdetc.getmovpos= pred_getmovpos;
  u.fdetc.trackend= pred_trackend;
  u.tailc.nextseg= tail_nextseg;
  u.tailc.distance= -tail_advance_delaydist;

  if (!u.stopping) {
    /* we're carrying on until the next segment */
    u.fdetc.distance= u.fdet.remain;
  } else if (flags & PREDF_JUSTDET) {
    /* we actually know exactly where we are */
    u.fdetc.distance= 0;
  } else { /* stopping, but we're mid-segment */
    u.fdetc.distance= effective_uncertainty;
  }
  u.elapsed= -u.fdetc.distance / u.maxspeed; /* imagine we're already there */
  u.fdetc.distance += u.stopping_distance + MARGIN_NOSE;
  u.was_distance= u.fdetc.distance;

  ec= trackloc_advance(&u.fdet,&u.fdetc);
  if (ec && ec != EC_SignallingHorizonReached) goto xproblem;

  /*----- look ahead for autopoint -----*/

  if (tra->autopoint) {
    Distance min_autopoint_dist= MARGIN_AUTOPOINTTIME * u.maxspeed;

    DPRINTF(safety,predict,"  optimism %d (min %d) nose %s\n",
	    u.autopoint_distance, min_autopoint_dist, u.nose.seg->i->pname);

    if (u.autopoint_distance < min_autopoint_dist)
      u.autopoint_distance= min_autopoint_dist;

    u.furthest= u.nose.seg;
    u.optimistic= 1;
    u.was_distance= u.nosec.distance= u.autopoint_distance;
    trackloc_advance(&u.nose,&u.nosec);

    /* But actually we don't want to end up owning
     * segments unless we have to for motions. */

    u.nose.backwards ^= 1;
    u.nose.remain= 0;
    u.nosec.nextseg= optunwind_nextseg;
    u.nosec.distance= TL_DIST_INF;

    ec= trackloc_advance(&u.nose,&u.nosec);
    assert(ec == EC_SignallingHorizonReached);
  }

  /*----- commit to the plan -----*/

  DPRINTF(safety,predict,"  committing la#%d %c%c%c\n",
	  u.lookahead,
	  "-n"[ u.need_polarise ],
	  "-i"[ u.train_polarity_inverted ],
	  "-b"[ u.know_best_polarity ]);

  tra->plan_lookahead_nsegs= u.lookahead;

  if (u.need_polarise) {
    DPRINTF(safety,predict,"  polarising train_polarity=%d\n",
	    u.train_polarity_inverted),
    actual_inversions_start();
  }

  DPRINTF1(safety,predictplan,"  ");
  FOR_SEG {
    DPRINTF2(" %s%s%s%c%c%c%c/%s%s%s%s%s",
	     seg->tr_backwards?"-":"", seg->i->pname,
	     seg->owner == tra ? "=" : seg->owner ? "#" : "-",
	     "-N"[ seg->now_present ],
	     "-P"[ seg->pred_present ],
	     "-V"[ seg->pred_vacated ],
	     "-p"[ seg->will_polarise ],
	     movpos_pname(seg,seg->movposcomb),
	     seg->motion ? (seg->moving ? "!" : "~") : "",
	     seg->motion ? motion_pname(seg,seg->motion) : "",
	     seg->motion_newplan ? ">" : "",
	     seg->motion && seg->motion_newplan==seg->motion ? "=" :
	     motion_pname(seg,seg->motion_newplan));

    /* set ownership and det_ignore */
    if (seg->pred_present || seg->pred_vacated) {
      seg->owner= tra;
      seg->det_ignore= seg->now_present;
    } else if (seg->owner == tra) {
      seg->det_ignore= 0;
      seg->owner= 0;
      /* garbage collect our old plan for now-irrelevant segments */
      assert(!seg->motion_newplan);
      if (!seg->moving) {
	movpos_unreserve(seg->motion);
	seg->motion= 0;
      }
      continue;
    } else {
      /* segment not relevant to us at all */
      continue;
    }

    /* now it's our segment: */

    /* clear any irrelevant old plan */
    if (seg->motion && !seg->moving) {
      if (seg->motion != seg->motion_newplan)
	movpos_unreserve(seg->motion);
      seg->motion= 0;
    }
    /* now motion!=0 only if seg->moving */

    /* install the new plan, if any */
    if (seg->motion_newplan) {
      if (seg->motion_newplan == seg->motion) {
	/* excellent, already doing it */
      } else if (!seg->now_present) {
	MovPosComb target= movpos_change_intent(seg->motion_newplan);

	DPRINTF2(" ...\n");
	ec= movpos_change(seg, target, -1, seg->motion_newplan);
	assert(!ec);
	DPRINTF1(safety,predictplan, " ... ");
	/* motion is updated by movpos_change */
      } else {
	/* we'll do it later, then */
	seg->motion= seg->motion_newplan;
      }
      seg->motion_newplan= 0;
    }
    /* now the new plan is in motion and moving */

    if (u.need_polarise && seg->will_polarise) {
      seg->seg_inverted= seg->tr_backwards ^ u.train_polarity_inverted;
      if (seg->i->invertible) {
	actual_inversions_segment(seg);
	Segment *interferer= segment_interferes_simple(0,seg);
	if (interferer && !interferer->will_polarise &&
	    interferer->i->invertible) {
	  interferer->seg_inverted= seg->seg_inverted
	    ^ seg->i->interferes_polarity_opposed;
	  actual_inversions_segment(interferer);
	}
      } else {
	assert(!seg->seg_inverted);
      }
    }
    seg->now_present= seg->pred_present=
      seg->pred_vacated= seg->will_polarise= 0;
  }
  DPRINTF2("\n");

  if (u.need_polarise)
    actual_inversions_done();

  report_train_ownerships(tra,u.hindmost,0);

  if (u.desire_move && !u.desire_move->owner)
    /* Oh!  Train must be slower and less far advanced than expected.
     * Well, then we can move it right away. */
    return movpos_change(u.desire_move,u.desire_movposcomb,-1,0);

  return 0;

 xproblem:
  DPRINTF(safety,predict,"  returning %s\n", ec2str(ec));

  FOR_SEG {
    seg->now_present= seg->pred_present=
      seg->pred_vacated= seg->will_polarise= 0;

    if (seg->motion_newplan==seg->motion) {
      seg->motion_newplan= 0;
    }
    if (seg->motion_newplan) {
      movpos_unreserve(seg->motion_newplan);
      seg->motion_newplan= 0;
    }
    if (!seg->owner && !seg->moving && seg->motion) {
      movpos_unreserve(seg->motion);
      seg->motion= 0;
    }
  }
  return ec;
}

/*========== reporting position and ownership ==========*/

void report_train_position(Train *tra) {
  ouprintf("train %s at ",tra->pname);
  qprintf_position(tra,ouprintf);
  ouprintf(" %s\n", tra->backwards ? "backwards" : "forwards");
}  
  
static int report_getmovpos(TrackLocation *t, TrackAdvanceContext *c,
			    MovPosComb *use_io) {
  PredictUserContext *u= c->u;
  if (u->done_fdet && t->seg != u->train->foredetect)
    /* we must use current posn for foredetect itself */
    u->usecurrentposn= 0;
  if (!u->usecurrentposn && t->seg->motion)
    *use_io= movpos_change_intent(t->seg->motion);
  assert(*use_io>=0);
  return 0;
}

static int report_nextseg(TrackLocation *t, struct TrackAdvanceContext *c,
			  MovPosComb *mpc, const TrackLocation *before) {
  PredictUserContext *u= c->u;
  char flags[6];
  int r;

  u->reportcount++;
  if (u->reportcount > NUM_SEGMENTS * 2) {
    ouprintf(" [... infinite loop!]\n");
    abort();
  }

  if (u->done_fdet) /* we've had foredetect */
    if (++u->lookahead > u->train->plan_lookahead_nsegs)
      return -1;

  flags[0]= 0;

  if (t->seg->det_expected)
    strcat(flags,"*");
  if (t->seg->det_ignore)
    strcat(flags,".");
  if (t->seg == u->train->foredetect && !u->done_fdet) {
    strcat(flags,"!");
    u->done_fdet= 1;
  }

  ouprintf(" %s%s", t->backwards?"-":"", t->seg->i->pname);

  if (t->seg->i->n_poscombs > 1) {
    r= report_getmovpos(t,c,mpc);  assert(!r);  assert(*mpc>=0);
    ouprintf("/%s", t->seg->i->poscombs[*mpc].pname);
  }
  ouprintf("%s",flags);

  return 0;
}

void report_train_ownerships(Train *tra, Segment *hindmost,
			     int always_use_motions) {
  PredictUserContext u;

  memset(&u,0,sizeof(u));
  u.train= tra;
  u.hindmost= 0;
  u.usecurrentposn= !always_use_motions;
  u.done_fdet= 0;
  
  /* Walk along the train printing its segments: */
  ouprintf("train %s has", tra->pname);
  
  u.nose.seg= hindmost;
  u.nose.remain= 0;
  u.nose.backwards= hindmost->tr_backwards;

  u.nosec.distance= TL_DIST_INF;;
  u.nosec.nextseg= report_nextseg;
  u.nosec.getmovpos= report_getmovpos;
  u.nosec.u= &u;
  u.hindmost= hindmost;

  trackloc_advance(&u.nose,&u.nosec);

  ouprintf("\n");
}

/*========== reversing a train ==========*/

static int reverse_nextseg(TrackLocation *t, struct TrackAdvanceContext *c,
			   MovPosComb *mpc_io, const TrackLocation *before) {
  PredictUserContext *u= c->u;

  DPRINTF(safety,predictseg,"   reverse_nextseg "
	  " %c%s dist=%-4d\n",

	  " -"[t->backwards],
	  t->seg->i->pname,
	  c->distance);

  if (*mpc_io==-1 || t->seg->motion)
    return predict_problem(u,t->seg, "segment under train is not stable");
  return 0;
}

static int reverse_trackend(TrackLocation *t, struct TrackAdvanceContext *c) {
  abort();
}

ErrorCode safety_setdirection(Train *tra, int backwards,
			      PredictionProblemCallback *ppc, void *ppcu) {
  /* We adjust:
   *   tra->foredetect, maxinto, uncertainty
   *   tra->backwards
   *   tra->plan_lookahead_nsegs
   *   seg->tr_backwards
   * The following remain unchanged as all their users have
   * regard to the train's direction:
   *   tra->head, tail
   */
  TrackLocation newfdet;
  TrackAdvanceContext c;
  PredictUserContext u;
  const SegPosCombInfo *pci;
  Segment *oldfdet;
  int oldmaxinto, oldfdet_trbackwards, old_planlookaheadnsegs;
  ErrorCode ec, ec2;
  int r;
  struct timeval tnow;

  SKELETON_PREDICT_USER_CONTEXT(u,tra);

  MUSTECR( safety_checktrain(tra,ppc,ppcu) );

  if (tra->backwards == backwards)
    return 0;

  if (!speedmanager_stopped(tra))
    return predict_problem(&u,0, "train is not stopped");
  
  newfdet.seg= tra->foredetect;
  newfdet.remain= tra->maxinto;
  newfdet.backwards= !tra->foredetect->tr_backwards;

  c.distance= ceil(tra->detectable * MARGIN_TRAINLENGTH) + tra->uncertainty;
  c.nextseg= reverse_nextseg;
  c.getmovpos= 0;
  c.trackend= reverse_trackend;
  c.u= &u;

  MUSTECR( trackloc_advance(&newfdet,&c) );

  mgettimeofday(&tnow);

  /* right, now let's start fiddling */
  oldfdet= tra->foredetect;
  oldmaxinto= tra->maxinto;
  oldfdet_trbackwards= tra->foredetect->tr_backwards;
  old_planlookaheadnsegs= tra->plan_lookahead_nsegs;

  r= trackloc_getlink(&newfdet,&c,&pci,0,-1);  assert(!r);

  tra->foredetect= newfdet.seg;
  tra->maxinto= pci->dist - newfdet.remain;
  tra->backwards ^= 1;
  newfdet.seg->tr_backwards ^= 1;
  tra->plan_lookahead_nsegs= INT_MAX;
  
  ec= predict(tra,tnow, PREDF_OLDPLAN|PREDF_INITQUEST, 0,0, 0,ppc,ppcu);
  if (!ec) {
    /* yay! */
    report_train_position(tra);
    return 0;
  }

  /* It can happen eg that the uncertainty is more troublesome when
   * considered one way than the other.  In which case just put it back now. */
  assert(ec == EC_SignallingPredictedProblem);

  tra->foredetect= oldfdet;
  tra->maxinto= oldmaxinto;
  tra->backwards ^= 1;
  tra->foredetect->tr_backwards= oldfdet_trbackwards;
  tra->plan_lookahead_nsegs= old_planlookaheadnsegs;

  ec2= predict(tra,tnow, PREDF_OLDPLAN,0,0, 0,0,(char*)"abandon reverse");
  assert(!ec2);

  return ec;
}

/*========== entrypoints from rest of the program ==========*/

static void detection_report_problem(Train *tra, Segment *seg,
				     void *pu, const char *message) {
  ouprintf("train %s signalling-problem %s : %s\n",
	   tra->pname, seg ? seg->i->pname : "-", message);
}

void safety_notify_detection(Segment *seg) {
  Train *tra;
  ErrorCode ec;
  int maxinto;
  struct timeval tnow;
  SpeedInfo speed_info;

  if (seg->det_ignore) return;

  debug_count_event("detection");

  if (!seg->det_expected) {
    Segment *interferer= segment_interferes_simple(0,seg);
    if (!interferer) safety_panic(0,seg, "unexpected detection");
    if (interferer->det_ignore) return;
    if (!interferer->det_expected)
      safety_panic(0,seg, "unexpected detection, perhaps really at %s",
		   interferer->i->pname);
    DPRINTF(safety,core, " detection %s using interferer %s",
	    seg->i->pname, interferer->i->pname);
    seg= interferer;
  }

  tra= seg->owner;
  if (seg->movposcomb < 0)
    safety_panic(tra,seg, "track route not set and train has arrived");

  mgettimeofday(&tnow);

  speedmanager_getinfo(tra,tnow,&speed_info);
  maxinto= seg->i->poscombs[seg->movposcomb].dist;
  if (speed_info.stopping && maxinto > speed_info.stopping_distance)
    maxinto= speed_info.stopping_distance;

  tra->foredetect= seg;
  tra->uncertainty= tra->maxinto= maxinto;
  tra->plan_lookahead_nsegs--;
  report_train_position(tra);

  ec= predict(tra,tnow, PREDF_JUSTDET|PREDF_OLDPLAN,0,0,
	      &speed_info, detection_report_problem,0);
  if (!ec) return;

  assert(ec == EC_SignallingPredictedProblem);

  if (maxinto > speed_info.stopping_distance)
    maxinto= speed_info.stopping_distance;
  tra->maxinto= tra->uncertainty= maxinto;

  report_train_position(tra);
  speedmanager_safety_stop(tra,tnow, PREDF_JUSTDET|PREDF_OLDPLAN);
}

ErrorCode safety_movposchange(Segment *seg, MovPosComb comb,
     int allow_queueing, PredictionProblemCallback *ppc, void *ppcu) {
  PredictUserContext u;
  struct timeval tnow;
  ErrorCode ec, ec2;

  SKELETON_PREDICT_USER_CONTEXT(u,seg->owner);

  if (!seg->owner)
    return movpos_change(seg,comb,-1,0);

  if (allow_queueing<2) {
    if (seg->det_ignore)
      return predict_problem(&u,seg, "route set for train");
    if (seg->det_expected)
      return predict_problem(&u,seg, "route set for immiment train");
  }
  if (allow_queueing<1)
    return predict_problem(&u,seg, "route set for approaching train");

  mgettimeofday(&tnow);
  ec= predict(seg->owner,tnow, 0, seg,comb, 0, ppc,ppcu);
  if (!ec) return 0;

  ec2= predict(seg->owner,tnow, PREDF_OLDPLAN, 0,0, 0,
	       0,(char*)"abandon movposchange");
  assert(!ec2);

  return ec;
}

ErrorCode safety_checktrain(Train *tra,
			    PredictionProblemCallback *ppc, void *ppcu) {
  if (!tra->foredetect) {
    ppc(tra,0,ppcu,"train is not on layout");
    return EC_SignallingPredictedProblem;
  }
  return 0;
}

void safety_abandon_run(void) {
  SEG_IV;

  FOR_SEG {
    if (seg->moving) continue;
    movpos_unreserve(seg->motion);
    seg->motion= 0;
  }
}
