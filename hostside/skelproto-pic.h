/* @skeleton@ @1@ @L@ */
/*
 * arranges for the declarations of
 *  enco_pic_WHATEVER
 *  on_pic_WHATEVER
 * and the tables
 * and related functions
 */

#ifndef AUPROTO_PIC_H
#define AUPROTO_PIC_H

typedef struct PicInsnInfo PicInsnInfo;
typedef void PicInputFn(const PicInsnInfo *pii, const PicInsn *pi, int objnum);

void enco_pic_@cnameyn@(PicInsn *out);                   @h2p@ @nargs=0@
void enco_pic_@cnameyn@(PicInsn *out, int objum);        @h2p@ @nargs=1@
void enco_pic_@cnameyn@(PicInsn *out, int objum, int v); @h2p@ @nargs=2@
PicInputFn on_pic_@cnameyn@;                    @p2h@
#define PICMSG_@cnameynu@       @opcodeyn@
#define PICMSG_@cnameynu@_M     @opcodemaskyn@
#define PICMSG_@cnameynu@_P(v)  (((v) & @opcodemaskyn@) == @opcodeyn@)

extern void enco_pic_polarity_begin(PicInsn *out);
extern void enco_pic_polarity_setbit(PicInsn *out, int objnum);

extern void enco_pic_pii(PicInsn *out, const PicInsnInfo *pii, int obj, int v);

const PicInsnInfo *lookup_byopcode(Byte byte0, const PicInsnInfo *table);
void picinsn_decode(const PicInsn *pi, const PicInsnInfo *table,
		    const PicInsnInfo **pii_r, int *objnum_r, int *v_r);
void oopicio(const char *dirn, const PicInsnInfo *pii, int objnum);

struct PicInsnInfo {
  const char *name;
  Byte opcode, mask;
  int argsbits, vbits, noiselevel;
  PicInputFn *input_fn;
};

extern const PicInsnInfo pic_command_infos[], pic_reply_infos[];

#define PING_PONG_PATTERN 0x5a

#endif /*AUPROTO_PIC_H*/
