/*
 * arranges for the declarations of
 *  enco_nmra_WHATEVER
 */

#ifndef NMRA_H
#define NMRA_H

#define Aint(v,i)    , int v
#define Abitmap(v,i) , unsigned v
#define Abyte(v,i)   , Byte v
#define Anone
#define NMRA(n,al,body) void enco_nmra_##n(Nmra *packet_r al);
#include "nmra-packets.h"

#endif /*NMRA_H*/
