/*
 * common
 * general serial i/o and system interface etc.
 */

#include <unistd.h>
#include <assert.h>
#include <errno.h>

#include <fcntl.h>

#include "common.h"

int serial_fudge_delay= 0;
int serial_fd= -1;

void serial_open(const char *device) {
  assert(serial_fd==-1);

  serial_fd= open(device,O_RDWR);
  if (serial_fd<0) diee(device);
}

void serial_transmit_now(const Byte *command, int length) {
  int r;
  assert(length <= COMMAND_ENCODED_MAX);

  while (length > 0) {
    r= write(serial_fd, command,
	     serial_fudge_delay ? 1 : length);
    if (r==-1) {
      if (errno == EINTR) continue;
      diee("command_transmit");
    }
    assert(r<=length);
    command += r;
    length -= r;
    if (r>0 && serial_fudge_delay)
      usleep(serial_fudge_delay);
  }
}
