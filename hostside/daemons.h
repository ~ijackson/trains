/*
 * declarations common to
 *  - realtime daemon
 *  - gui-plan-* displayers
 */

#ifndef DAEMONS_H
#define DAEMONS_H

#include <oop.h>
#include <oop-read.h>

#include "common.h"

typedef struct CommandInput CommandInput;

/*---------- from obc.c ----------*/

typedef struct OutBuffer OutBuffer;
typedef struct OutBufferChain OutBufferChain;
typedef void OutBufferError(OutBufferChain*, const char *e1, const char *e2
			    /* on error: both e1 and e2 non-0. say `$e1: $e2'
			     * on eof: both e1 and e2 =0. */);
typedef void OutBufferEmpty(OutBufferChain*);

typedef void CopyCallBack(char *m, size_t l, void *u);

struct OutBufferChain {
  /* set by user: */
  char *desc;
  int fd;
  int limit; /* 0 means obc_init will set a default */
  OutBufferError *error;
  OutBufferEmpty *empty; /* may be 0 */
  /* set/used by obc_... but may be read by user */
  int total; /* amount buffered */
  /* set/used by obc_..., oprintf, etc., only */
  int done_of_head; /* -1 = empty and fileevent not registered with liboop */
  struct { OutBuffer *head, *tail; } obs;
};

void obc_init(OutBufferChain *ch);
void obc_init_core(OutBufferChain *ch); /* doesn't mess with fd */
int obc_tryflush(OutBufferChain *ch);
 /* returns 0 for all flushed or errno, including particularly EWOULDBLOCK */

void ovprintf_ccb(OutBufferChain *ch, CopyCallBack *ccb, void *ccbu,
		   const char *fmt, va_list al)
     __attribute__((format(printf,4,0)));
void ovprintf(OutBufferChain *ch, const char *fmt, va_list al)
     __attribute__((format(printf,2,0)));
void oprintf(OutBufferChain *ch, const char *msg, ...)
     __attribute__((format(printf,2,3)));
void owrite(OutBufferChain *ch, const char *data, int l);
void voerror(OutBufferChain *ch, const char *fmt, va_list al)
     __attribute__((format(printf,2,0)));

/*---------- from cmdinput.c ----------*/

extern oop_source *events;

typedef void CommandInputCallback(ParseState *ps, CommandInput *cmdi);

struct CommandInput {
  OutBufferChain out;
  CommandInputCallback *doline;
  oop_read *rd;
};

void cmdin_new(CommandInput *cmdi, int readfd);
  /* fill in cmdi->out's `set by user' fields before calling cmdin_new,
   * as cmdin_new will call obc_init and will use cmdi->out->fd. */

#endif /*DAEMONS_H*/
