/*
 * topology dumper
 */

#include "common.h"
#include "../layout/layout-data.h"

static const char *mfkpnames[/*MovFeatKind*/]=
  { 0,"point","relay" };

static void plink(const SegmentLinkInfo *p) {
  putchar(' ');
  if (!SOMEP(p->next)) { putchar('-'); return; }
  if (p->next_backwards) putchar('-');
  fputs(info_segments[p->next].pname,stdout);
}

const char *progname= "topology-dump";

void die_hook(void) { }
void die_vprintf_hook(const char *fmt, va_list al) { }

int main(int argc, char **argv) {
  const SegmentInfo *segi;
  const MovFeatInfo *mfi;
  const SegPosCombInfo *spci;
  int segn, mfn, posn;

  if (argc!=1) badusage("no arguments permitted");

  for (segn=0, segi=info_segments; segn<info_nsegments; segn++, segi++) {
    printf("topology segment %s %d %d %c\n", segi->pname,
	   segi->n_movfeats, segi->n_poscombs, "ni"[segi->invertible]);
    for (mfn=0, mfi=segi->movfeats; mfn<segi->n_movfeats; mfn++, mfi++) {
      MovFeatKind k= mfi->kind;
      printf("topology movfeat %s %s %d %d %s\n", segi->pname, mfi->pname,
	     mfi->posns, mfi->weight, mfkpnames[k]);
    }
    for (mfn=0; mfn<segi->n_fixedmovfeats; mfn++, mfi++) {
      printf("topology movfeatfixed %s %s %d\n", segi->pname, mfi->pname,
	     mfi->posns);
    }
    for (posn=0, spci=segi->poscombs; posn<segi->n_poscombs; posn++, spci++) {
      printf("topology movposcomb %s %s %d %d", segi->pname,
	     segi->n_poscombs>1 ? spci->pname : "-",
	     posn, spci->dist);
      plink(&spci->link[1]);
      plink(&spci->link[0]);
      putchar('\n');
    }
  }
  mflushstdout();
  return 0;
}
