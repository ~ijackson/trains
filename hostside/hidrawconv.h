#ifndef HIDRAWCONV_H
#define HIDRAWCONV_H

#include <stdio.h>
#include <stdint.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/types.h>

#include "hidraw.h"

#include "common.h"

#define MAXREPORTLEN 256
#define MAXREPORTS 256

typedef struct { int len; uint8_t *msg; } Last;
typedef struct { Last lasts[MAXREPORTS]; } LastReports;
typedef void ProcessReport(const uint8_t *msg, int msglen, const uint8_t *last);

extern const char *const descriptor;
extern ProcessReport *const report_processors[MAXREPORTS];

void dispatch(LastReports *lasts, const char *message_prefix,
	      ProcessReport *const report_processors[MAXREPORTS],
	      const uint8_t *msg, int l);

/*---------- specific reporters ----------*/

/*----- "bits", ie keys and buttons -----*/

typedef struct {
  const char *str;
  int pos;
  uint8_t mask;
} KeyBit;

void reportbits(const uint8_t msg[], const uint8_t last[],
		int len, const KeyBit keybits[]);

/*----- "vals", ie absolute values -----*/

typedef struct {
  const char *str;
  int pos, rshift, sign; /* mask is applied before rshift */
  uint8_t mask, zero;
} ValLoc;

void reportvals(const uint8_t msg[], const uint8_t last[],
		int len, const ValLoc vallocs[]);


/*----- "hats", ie hatswitches -----*/

typedef struct {
  int x, y;
} HatPosn;

typedef struct {
  const char *str; /* base; reporter will append "X" and "Y" to this */
  int pos, rshift, max; /* mask is applied before rshift */
  uint8_t mask;
  const HatPosn *posns;
} HatLoc;

void reporthats(const uint8_t msg[], const uint8_t last[],
		int len, const HatLoc hatlocs[]);

#endif /*HIDRAWCONV_H*/
