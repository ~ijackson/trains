/*
 * realtime
 * simulation harness
 */

#include "realtime.h"

typedef struct SimTimeout {
  struct { struct SimTimeout *back, *next; } l;
  TimeoutEvent *toev;
} SimTimeout;

typedef void SimEventFn(void);

int simlog_full;
const char *simulate;

static SimEventFn se_timestamp, se_timerevent, se_command;
static SimEventFn se_serial, se_serial_detcompr, se_samedet, se_eof;

static SimTimeout *simtimeouts;

#define DETSAVESLN2 4
#define DETSAVES (1<<DETSAVESLN2) /* encoded in letters so must be <=26 ! */

static FILE *simoutput;
static FILE *siminput;
static char *sevent_buf;
static size_t sevent_buflen;
static int sevent_lno;
static Byte sevent_lastdet[DETSAVES][2];

static SimEventFn *sevent_type;
static char *sevent_data;
static struct timeval sevent_abst;

#define PICMSG_DETECT_01 (PICMSG_DETECT0^PICMSG_DETECT1)

/*---------- writing simulation log ----------*/

static Byte simlog_lastdet[DETSAVES][2];
static uint32_t simlog_detcompr_lcongx;

void simlog_ccb(char *m, size_t l, void *u) {
  size_t r;
  if (!simoutput || simoutput==stdout) return;

  r= fwrite(m,1,l,simoutput);
  if (r!=l) {
    assert(ferror(simoutput));
    diee("copy output to simulation log");
  }
}
void simlogv(const char *fmt, va_list al) {
  if (simoutput==stdout) ouvprintf(fmt,al);
  else if (simoutput) vfprintf(simoutput,fmt,al);
}
void simlog(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  simlogv(fmt,al);
  va_end(al);
}
void simlog_flush(void) {
  if (simoutput==stdout) obc_tryflush(&cmdi.out);
  else if (simoutput)
    if (ferror(simoutput) || fflush(simoutput))
      diee("write simulation log");
}

void simlog_serial(const Byte *data, int length) {
  static Byte db0, db1;
  int is_det;

  if (!length) return;

  db0= data[0] & ~PICMSG_DETECT_01;

  is_det= PICMSG_DETECT0_P(db0) && length==2 && !((db1=data[1]) & 0x80);
  if (is_det) {
    int i;
    for (i=0; i<DETSAVES; i++)
      if (simlog_lastdet[i][0]==db0 && simlog_lastdet[i][1]==db1) {
	simlog("%c\n", ((data[0] & PICMSG_DETECT_01) ? 'A' : 'a') + i);
	simlog_flush();
	return;
      }
    /* not found, choose a slot to replace */
    simlog_detcompr_lcongx += 5;     /* WP Linear_congruential_generator */
    simlog_detcompr_lcongx *= 69069; /* values are those also used by GCC */
    i= (simlog_detcompr_lcongx >> (30 - DETSAVESLN2)) &
       ((1UL << DETSAVESLN2)-1);
    simlog_lastdet[i][0]= db0;
    simlog_lastdet[i][1]= db1;
    simlog("picioh in detcompr%c", 'A' + i);
  } else {
    simlog("picioh in suppressed");
  }
  while (length--) simlog(" %02x",*data++);
  simlog("\n");
  simlog_flush();
}
void simlog_open(const char *fn) {
  int r;
  
  if (!fn) fn= "+realtime.log";
  if (!strcmp(fn,"-")) {
    simoutput= stdout; /* we don't really use this - see vsimlog */
  } else if (fn[0]) {
    r= unlink(fn);
    if (r && errno!=ENOENT && errno!=EPERM)
      diee("unlink old simulation log %s",fn);
    simoutput= fopen(fn,"w");
    if (!simoutput) diee("open simulation log %s",fn);
  }
}

/*---------- simulation input stream parser ----------*/

static void simbad(const char *how) __attribute__((noreturn));
static void simbad(const char *how) {
  die("simulation failed (line %d, in `%s'): %s", sevent_lno, sevent_buf, how);
}

static void sevent(void) {
  ssize_t gr;
  char *p;

  if (sevent_type)
    return;

  for (;;) {
    sevent_lno++;

    gr= getline(&sevent_buf,&sevent_buflen,siminput);
    if (feof(siminput)) { sevent_type= se_eof; return; }
    if (ferror(siminput)) diee("read simulation input failed");
    assert(gr>0);
    assert(sevent_buf[gr-1]=='\n');
    sevent_buf[gr-1]= 0;

    if (gr==2 && CTYPE(isalpha,sevent_buf[0])) {
      sevent_type= se_samedet;
      sevent_data= &sevent_buf[0];
      return;
    }

#define IF_ET(pfx, et)							\
    if (!strncmp(pfx, sevent_buf, sizeof(pfx)-1)			\
	&& (sevent_type=(et), p=sevent_buf+sizeof(pfx)-1))

    IF_ET("command-in ", se_command) {
      sevent_data= p;
      return;
    }
    IF_ET("picioh in detcompr", se_serial_detcompr) {
      sevent_data= p;
      return;
    }
    IF_ET("picioh in ", se_serial) {
      p= strchr(p,' ');
      if (!p) simbad("missing space after `in'");
      sevent_data= p+1;
      return;
    }
    IF_ET("timer-event ", se_timerevent) {
      sevent_data= p;
      return;
    }
    IF_ET("timestamp ", se_timestamp) {
      int n, r;
      n=-1;
      r= sscanf(p,"%ld.%ld%n", &sevent_abst.tv_sec, &sevent_abst.tv_usec, &n);
      if (r<2 || !(n==-1 || !p[n])) simbad("unparseable timestamp");
      return;
    }
  }
}

/*---------- hooks for eventhelp ----------*/

void sim_toev_start(TimeoutEvent *toev) {
  SimTimeout *s= mmalloc(sizeof(*s));
  s->toev= toev;
  DLIST1_PREPEND(simtimeouts,s,l);
}

static void sim_toev_remove(SimTimeout *s) {
  DLIST1_REMOVE(simtimeouts,s,l);
  free(s);
}

void sim_toev_stop(TimeoutEvent *toev) {
  SimTimeout *s;
  for (s=simtimeouts; !(s->toev==toev); s=s->l.next);
  sim_toev_remove(s);
}

void sim_mgettimeofday(struct timeval *tv) {
  sevent();
  *tv= sevent_abst;
  if (sevent_type==se_timestamp)
    sevent_type= 0;
  else
    fprintf(stderr,"simulation warning -"
	    " mgettimeofday desynched at line %d\n", sevent_lno);
}

/*---------- simulation events ----------*/

static void se_timestamp(void) { }

static void se_timerevent(void) {
  TimeoutEvent *toev;
  SimTimeout *s;
  char *delim;

  delim= strchr(sevent_data,'.');
  if (!delim) simbad("missing `.' in timer event name");
  *delim= 0;

  for (s=simtimeouts; s; s=simtimeouts->l.next) {
    toev= s->toev;
    if (!strcmp(toev->pclass, sevent_data) &&
	!strcmp(toev->pinst, delim+1))
      goto found;
  }
  *delim='.';  simbad("timeout event not found");
  
 found:
  sim_toev_remove(s);
  toev_callback(0, sevent_abst, toev);
}

static int unhex(const char *str, Byte *buf, int buf_len) {
  char c[3], *ep;
  int buf_used;
  const char *p;

  c[2]= 0;
  for (p=str, buf_used=0;
       ;
       ) {
    if (*p==' ') p++;
    if (!(c[0]= *p++)) break;
    if (!(c[1]= *p++)) simbad("odd number of hex digits");

    if (buf_used==buf_len) simbad("serial_buf overrun");

    buf[buf_used++]= strtoul(c,&ep,16);
    if (*ep) simbad("bad hex");
  }
  return buf_used;
}
 
static void se_serial(void) {
  int buf_used;

  buf_used= serial_buf.l;
  buf_used += unhex(sevent_data, serial_buf.d + buf_used,
		    sizeof(serial_buf.d) - buf_used);
  serial_indata_process(buf_used);
}

static void serial_detevent(Byte m[2], Byte or_m0) {
  int buf_used;
  buf_used= serial_buf.l+2;
  if (buf_used > sizeof(serial_buf.d)) simbad("serial_buf det overrun");
  serial_buf.d[serial_buf.l]=   m[0] | or_m0;
  serial_buf.d[serial_buf.l+1]= m[1];
  serial_indata_process(buf_used);
}

static void se_serial_detcompr(void) {
  int i, l;

  i= *sevent_data++ - 'A';
  if (i<0 || i>=DETSAVES) simbad("detcompr bad slot specifier");
  l= unhex(sevent_data, sevent_lastdet[i], 2);
  if (l!=2) simbad("detcompr wrong message length");
  serial_detevent(sevent_lastdet[i],0);
  sevent_lastdet[i][0] &= ~PICMSG_DETECT_01;
}
static void se_samedet(void) {
  int i, d;
  i= *sevent_data - 'a';
  d= *sevent_data < 'a'; /* A is before a */
  if (d) i -= 'A'-'a';
  if (i<0 || i>=DETSAVES || !sevent_lastdet[i][0]) simbad("detsame bad slot");
  serial_detevent(sevent_lastdet[i], d ? PICMSG_DETECT_01 : 0);
}

static void se_command(void) {
  ParseState ps;
  ps.remain= sevent_data;
  ps.thisword= 0;
  ps.lthisword= 0;
  cmdi.doline(&ps,&cmdi);
}

static void se_eof(void) {
  exit(0);
}

/*---------- core ----------*/

void sim_initialise(const char *logduplicate) {
  obc_init_core(&cmdi.out);
  serial_fd= open("/dev/null",O_WRONLY);
  if (serial_fd<0) diee("open /dev/null for dummy serial");
  siminput= fopen(simulate,"r");
  if (!siminput) diee("open simulation input %s",simulate);
  if (logduplicate)
    simlog_open(logduplicate);
}

void sim_run(void) {
  SimEventFn *fn;
  for (;;) {
    sevent();
    fn= sevent_type;
    sevent_type= 0;
    fn();
    obc_tryflush(&cmdi.out);
  }
}
