/*
 * usage: .../gui-plan [<windowid>]
 * protocol on stdin:
 *  series of lines
 *   off <segname>[/[<movfeat>]
 *   [t|f][i]on <segname>[[/<movfeat>] <movpos>]
 *   [t|f][i]det <segname>[[/<movfeat>] <movpos>]
 */

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <X11/Xlib.h>
#include <X11/xpm.h>
#include <X11/Xproto.h>

typedef int ErrorCode;

#include "daemons.h"
#include "dliste.h"
#include "../layout/plan-data-format.h"
#include "../layout/layout-data.h"
#include "stastate.h"

#include <oop.h>
#include <oop-read.h>

#define NSEGMENTS (ui_plan_data.n_segments)

typedef struct MaskState MaskState;
struct MaskState {
  int x, y, width, height;
  GC gc;
};

typedef struct PosnState PosnState;
struct PosnState {
  int x, y, width, height;
  Pixmap pm;
  MaskState edge;
};

typedef struct {
  unsigned on:1, inv:1, det:1, trainown:2, updated_tmp:1,
    resolution_problem:1; /* for multiplexer client only */
} SegFlags;

typedef struct SegmovfeatState SegmovfeatState;
struct SegmovfeatState {
  SegmovfeatState *next;
  SegFlags flags;
  unsigned redraw_needed:1;
  int posn;
  MaskState whole;
  PosnState (*posns)[2/*i*/][2/*det*/];
    /* posns[n_posns]=unknown if n_posns>1 */
};

oop_source *events;
const char *progname= "gui-plan";

typedef struct TrainState TrainState;

typedef struct {
  SegmovfeatState *mfs;
  SegFlags flags; /* used by multiplexer client */
  TrainState *owner;
} SegState;

static FILE *debug;
static SegState *state;
static SegmovfeatState *states_head;
static StartupState stastate;
static Display *disp;
static oop_source_sys *sys_events;
static Window win;
static int redraw_needed_count, expose_count;
static Pixmap bg_pixmap;
static unsigned long train_pixel, owned_pixel;

static const char *stastate_names[]= DEFINE_STASTATE_DATA;

static const char *badcmdreport_data;
static size_t badcmdreport_recsz;

void die_hook(void) { }
void die_vprintf_hook(const char *fmt, va_list al) { }

static void xlib_process(void);

static void diex(const char *fn, const char *w) __attribute__((noreturn));
static void diex(const char *fn, const char *w) {
  die("Xlib call failed: %s%s%s%s", fn,
      (w)?" (":"", (w), (w)?")":"");
}

#define XCALL(f,w,al) do{			\
    if (!( (f) al ))				\
      diex(#f, (w));				\
  }while(0)

static void diexpm(const char *fn, const char *w, int xpmst)
     __attribute__((noreturn));
static void diexpm(const char *fn, const char *w, int xpmst) {
  die("Xpm call failed: %s%s%s%s: %s", fn,
      (w)?" (":"", (w), (w)?")":"", XpmGetErrorString(xpmst));
}

#define XPMCALL(f,w,al) do{			\
    int xpmcall_xpmst;				\
    xpmcall_xpmst= ((f) al);			\
    if (xpmcall_xpmst != XpmSuccess)		\
      diexpm(#f, (w), xpmcall_xpmst);		\
  }while(0)

static void die_graphicsexpose(const XEvent *ev) {
  die("GraphicsExpose %lx(%s) x=%d y=%d w=%d h=%d count=%d"
      " major=%d(%s)",
      (unsigned long)ev->xgraphicsexpose.drawable,
      ev->xgraphicsexpose.drawable==win ? "w" : "?",
      ev->xgraphicsexpose.x,
      ev->xgraphicsexpose.y,
      ev->xgraphicsexpose.width,
      ev->xgraphicsexpose.height,
      ev->xgraphicsexpose.count,
      ev->xgraphicsexpose.major_code,
      ev->xgraphicsexpose.major_code==X_CopyArea ? "CA" :
      ev->xgraphicsexpose.major_code==X_CopyPlane ? "CP" : "?");
}

/*---------- input handling ----------*/

typedef struct {
  const char *name;
  void (*on_eof)(void);
  void (*on_input_line)(ParseState *ps);
  int (*on_badcmd)(void);
} InputStream;

static const InputStream *instream;

static void *input_iferr(oop_source *evts, oop_read *stdin_read,
			  oop_rd_event evt, const char *errmsg, int errnoval,
			  const char *data, size_t recsz, void *u_v) {
  const char *emsg;
  emsg= oop_rd_errmsg(stdin_read, evt, errnoval, OOP_RD_STYLE_GETLINE);
  die("%s: %s", instream->name, emsg);
}

static void *input_ifok(oop_source *evts, oop_read *cl_read,
			oop_rd_event evt, const char *errmsg, int errnoval,
			const char *data, size_t recsz, void *u_v) {
  ParseState ps;

  if (evt == OOP_RD_EOF) {
    instream->on_eof();
    abort();
  }
  if (evt != OOP_RD_OK) {
    input_iferr(evts,cl_read,evt,errmsg,errnoval,data,recsz,0);
    return OOP_CONTINUE;
  }

  badcmdreport_data= data;
  badcmdreport_recsz= recsz;

  ps.remain= data;
  instream->on_input_line(&ps);
  xlib_process();
  return OOP_CONTINUE;
}

static void *some_exception(oop_source *evts, int fd,
			    oop_event evt, void *name_v) {
  const char *name= name_v;
  die("poll exception on %s (fd %d)",name,fd);
}

int vbadcmd(ParseState *ps, const char *fmt, va_list al) {
  fprintf(stderr,"gui-plan: incorrect input: `%.*s': ",
	  (int)badcmdreport_recsz, badcmdreport_data);
  vfprintf(stderr,fmt,al);
  putc('\n',stderr);
  return instream->on_badcmd();
}

static int lstrpdbsearch(const char *str, int l,
			 const char *what,
			 const void *items, int n_items,
			 int itemsz) {
  int min, maxe, try, cmp;
  const void *try_item;
  const char *try_name;
  
  min= 0;
  maxe= n_items;
  for (;;) {
    if (min >= maxe) { badcmd(0,"unknown %s `%.*s'",what,l,str); return -1; }
    try= min + (maxe - min) / 2;
    try_item= (const char*)items + itemsz * try;
    try_name= *(const char *const *)try_item;
    cmp= lstrstrcmp(str, l, try_name ? try_name : "");
    if (!cmp) return try;
    if (cmp < 0) maxe= try;
    else min= try + 1;
  }
}

static int lstrpdbsearch_movfeat(const char *str, int l,
				 const PlanSegmentData *sd) {
  return lstrpdbsearch(str, l, "movfeat", sd->movfeats, sd->n_movfeats,
		       sizeof(*sd->movfeats));
}
static int lstrpdbsearch_segment(const char *str, int l) {
  return lstrpdbsearch(str, l, "segment",
		       ui_plan_data.segments, ui_plan_data.n_segments,
		       sizeof(*ui_plan_data.segments));
}

/*---------- drawing etc. ----------*/

static int range_overlap(int x1, int width1, int x2, int width2) {
  /* works for y's and heights too, obviously. */
  int rhs1, rhs2;
  rhs1= x1 + width1;
  rhs2= x2 + width2;
  if (rhs1 <= x2 || rhs2 <= x1) return 0;
  return 1;
}

static void redraw_mark(SegmovfeatState *fs) {
  if (fs->redraw_needed) return;
  fs->redraw_needed= 1;
  redraw_needed_count++;
}

static void xlib_expose(XExposeEvent *ev) {
  SegmovfeatState *fs;
  
  expose_count= ev->count;
  if (!ev->width || !ev->height) return;

  for (fs= states_head;
       fs;
       fs= fs->next) {
    if (!range_overlap(fs->whole.x, fs->whole.width,
		       ev->x, ev->width)) continue;
    if (!range_overlap(fs->whole.y, fs->whole.height,
		       ev->y, ev->height)) continue;
    redraw_mark(fs);
  }
}

static void copyarea(const char *what,
		     Drawable src, GC gc, int src_x, int src_y,
		     int w, int h, int dest_x, int dest_y) {
  XCALL( XCopyArea, what,
	 (disp, src, win, gc, src_x,src_y, w,h, dest_x,dest_y) );
//  XEvent ev;
//  XSync(disp,0);
//  if (XCheckTypedEvent(disp,GraphicsExpose,&ev))
//    die_graphicsexpose(&ev);
}

static void redraw(SegmovfeatState *fs) {
  PosnState *src;
  XGCValues gcv;
  
  if (fs->redraw_needed) {
    fs->redraw_needed= 0;
    redraw_needed_count--;
  }
  src= 0;
  copyarea("redraw", bg_pixmap, fs->whole.gc,
	   fs->whole.x, fs->whole.y,
	   fs->whole.width, fs->whole.height,
	   fs->whole.x, fs->whole.y);
   
  if (fs->flags.on) {
    src= &fs->posns[fs->posn][fs->flags.inv][fs->flags.det];
    copyarea("redraw-on", src->pm, fs->whole.gc,
	     0,0, src->width, src->height,
	     src->x, src->y);
  }
  if (fs->flags.trainown && src && src->edge.x >= 0) {
    gcv.foreground= fs->flags.trainown>1 ? train_pixel : owned_pixel;
    XCALL( XChangeGC, "train/own",
	   (disp, src->edge.gc, GCForeground, &gcv) );
    XCALL( XFillRectangle, "train/own",
	   (disp, win, src->edge.gc,
	    src->edge.x, src->edge.y,
	    src->edge.width, src->edge.height) );
  }
}

static void redraw_as_needed(void) {
  SegmovfeatState *fs;

  for (fs= states_head;
       fs;
       fs= fs->next)
    if (fs->redraw_needed)
      redraw(fs);
  assert(!redraw_needed_count);
}
  
static Bool evpredicate_always(Display *d, XEvent *ev, XPointer a) {
  return True;
}

static void xlib_process(void) {
  XEvent ev;
  Status xst;
  
  for (;;) {
    xst= XCheckIfEvent(disp,&ev,evpredicate_always,0);
    if (!xst) {
      if (!redraw_needed_count || expose_count)
	return;
      redraw_as_needed();
      continue;
    }

    switch (ev.type) {
    case Expose: xlib_expose(&ev.xexpose); break;
    case NoExpose: break;
    case MappingNotify: break;
    case GraphicsExpose: die_graphicsexpose(&ev);
    default: die("unrequested event type %d\n",ev.type);
    }
  }
}

static void *xlib_readable(oop_source *evts, int fd,
			   oop_event evt, void *cl_v) {
  xlib_process();
  return OOP_CONTINUE;
}

static int thiswordeatonechar(ParseState *ps, int c) {
  if (ps->thisword[0] == c) {
    ps->thisword++; ps->lthisword--;
    return 1;
  }
  return 0;
}

static void loadmask(MaskState *out, const PlanPixmapDataRef *ppd,
		     XGCValues *gcv, long gcv_mask) {
  static XpmColorSymbol coloursymbols[2]= {
    { (char*)"space", 0, 0 },
    { (char*)"mark",  0, 1 }
  };
  
  XpmAttributes mattribs;
  Pixmap pm;

  out->x= ppd->x;
  out->y= ppd->y;
  mattribs.valuemask= XpmDepth | XpmColorSymbols;
  mattribs.depth= 1;
  mattribs.colorsymbols= coloursymbols;
  mattribs.numsymbols= sizeof(coloursymbols) / sizeof(*coloursymbols);
  XPMCALL( XpmCreatePixmapFromData, "mask",
	   (disp,win, (char**)ppd->d, &pm,0, &mattribs) );
  out->width= mattribs.width;
  out->height= mattribs.height;

  gcv->clip_x_origin= out->x;
  gcv->clip_y_origin= out->y;
  gcv->clip_mask= pm;
  out->gc= XCreateGC(disp,win,
		     gcv_mask | GCClipXOrigin | GCClipYOrigin | GCClipMask,
		     gcv);
  XCALL( XFreePixmap, "mask", (disp,pm) );
}

/*---------- stdin input handling ----------*/

static void stdin_eof(void) { exit(0); }
static int stdin_badcmd(void) { exit(8); }

static void stdin_input_line(ParseState *ps) {
  const char *slash, *movfeatname;
  int invert, det, trainown, segment_ix, movfeat_ix, lmovfeatname;
  long posn;
  const PlanSegmentData *segment_d;
  const PlanSegmovfeatData *movfeat_d;
  SegmovfeatState *fs;

  ps_needword(ps);

  if (!thiswordstrcmp(ps,"off")) {
    invert= -1;
    trainown= 0;
    det= 0;
  } else {
    trainown=
      thiswordeatonechar(ps,'t') ? 2 :
      thiswordeatonechar(ps,'f') ? 1 : 0;
    invert= thiswordeatonechar(ps,'i');
    det= (!thiswordstrcmp(ps,"on") ? 0 :
	  !thiswordstrcmp(ps,"det") ? 1 :
	  (badcmd(ps,"unknown command"),-1));
  }
  ps_needword(ps);
  slash= memchr(ps->thisword, '/', ps->lthisword);
  if (slash) {
    movfeatname= slash + 1;
    lmovfeatname= (ps->thisword + ps->lthisword) - movfeatname;
    ps->lthisword= slash - ps->thisword;
  } else {
    movfeatname= 0;
    lmovfeatname= 0;
  }
  segment_ix= lstrpdbsearch_segment(ps->thisword,ps->lthisword);
  segment_d= &ui_plan_data.segments[segment_ix];

  movfeat_ix= lstrpdbsearch_movfeat(movfeatname, lmovfeatname, segment_d);
  movfeat_d= &segment_d->movfeats[movfeat_ix];

  if (ps->remain) {
    if (invert<0) badcmd(0,"off may not take movfeatpos");
    ps_neednumber(ps, &posn, 0, movfeat_d->n_posns-1, "movfeatpos");
  } else {
    if (invert>=0 && movfeat_d->n_posns > 1) {
      posn= movfeat_d->n_posns;
    } else {
      posn= 0;
    }
  }

  ps_neednoargs(ps);

  fs= &state[segment_ix].mfs[movfeat_ix];
  if (invert>=0) {
    fs->flags.on= 1;
    fs->flags.inv= invert;
  } else {
    fs->flags.on= 0;
    fs->flags.inv= 0;
  }
  fs->flags.det= det;
  fs->flags.trainown= trainown;
  fs->posn= posn;

  redraw(fs);
}

static const InputStream stdin_is= {
  "stdin", stdin_eof, stdin_input_line, stdin_badcmd
};

/*---------- multiplexer client ----------*/

static OutBufferChain sock= { (char*)"multiplexer socket", -1 };

static void sockvprintf(const char *fmt, va_list al)
  __attribute__((format(printf,1,0)));
static void sockprintf(const char *fmt, ...)
  __attribute__((format(printf,1,2)));

static void sockvprintf(const char *fmt, va_list al) {
  ovprintf(&sock,fmt,al);
}
static void sockprintf(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  sockvprintf(fmt,al);
  va_end(al);
}

static int sock_clientconnect(const char *node, const char *service) {
  struct addrinfo *aires, *try, hints;
  char niaddrbuf[256], niportbuf[64];
  int r, sock;
  
  memset(&hints,0,sizeof(hints));
  hints.ai_family= PF_UNSPEC;
  hints.ai_socktype= SOCK_STREAM;
  r= getaddrinfo(node,service,&hints,&aires);
  if (r) die("getaddrinfo node `%s' service `%s' failed: %s\n",
	     node,service,gai_strerror(r));

  for (try=aires; try; try=try->ai_next) {
    assert(try->ai_socktype == SOCK_STREAM);

    r= getnameinfo(try->ai_addr, try->ai_addrlen,
		   niaddrbuf, sizeof(niaddrbuf),
		   niportbuf, sizeof(niportbuf),
		   NI_NUMERICHOST|NI_NUMERICSERV);
    assert(!r);

    printf("trying %s,%s... ",niaddrbuf,niportbuf);
    mflushstdout();

    sock= socket(try->ai_family, SOCK_STREAM, try->ai_protocol);
    if (sock<0) {
      printf("couldn't create socket: %s\n",strerror(errno));
      continue;
    }

    r= connect(sock, try->ai_addr, try->ai_addrlen);
    if (!r) {
      printf("connected\n");
      return sock;
    }

    printf("connect failed: %s\n",strerror(errno));
    close(sock);
  }
  mflushstdout();
  return -1;
}

static void sock_eof(void) { die("EOF on multiplexer connection"); }
static int sock_badcmd(void) { return -1; }

/*---------- multiplexer protocol ----------*/

struct TrainState {
  struct { struct TrainState *next, *back; } others;
  char *name;
};

static int poweron;
static struct { TrainState *head, *tail; } trains;

#define FOR_S for (s=0; s<NSEGMENTS; s++)

static void mx_clear_updated(void) {
  int s;
  FOR_S
    state[s].flags.updated_tmp= 0;
}

static void mx_redraw_feat(int s, int f) {
  SegState *ss= &state[s];
  SegmovfeatState *fs= &ss->mfs[f];
  fs->flags= ss->flags;
  fs->flags.on= stastate > Sta_Off && poweron;
  if (stastate==Sta_Resolving)
    fs->flags.trainown= ss->flags.resolution_problem ? 2 : 0;
  redraw_mark(fs);
}

static void mx_redraw_seg(int s) {
  int f;
  assert(!!state[s].flags.trainown == !!state[s].owner);
  for (f=0; f < ui_plan_data.segments[s].n_movfeats; f++)
    mx_redraw_feat(s,f);
}

static void mx_redraw_all(void) {
  int s;
  FOR_S
    mx_redraw_seg(s);
}

static int ps_needsegment(ParseState *ps) {
  int r= ps_needword(ps);                                if (r) return -1;
  return lstrpdbsearch_segment(ps->thisword,ps->lthisword);
}

static void si_detect(ParseState *ps) {
  long dl;
  int r, s;

  s= ps_needsegment(ps);                                 if (s<0) return;
  r= ps_neednumber(ps,&dl,0,1,"detection flag");         if (r) return;
  state[s].flags.det= dl;
  mx_redraw_seg(s);
}

static void si_polarity(ParseState *ps) {
  char *delim, *end;
  int s;

  mx_clear_updated();
  if (*ps->remain++ != '<') { badcmd(ps,"missing <"); return; }

  end= strchr(ps->remain,'>');
  if (!end) { badcmd(ps,"missing >"); return; }

  while (ps->remain < end) {
    delim= memchr(ps->remain, ',', end - ps->remain);
    if (!delim) delim= end;

    ps->thisword= ps->remain;
    ps->lthisword= delim - ps->remain;
    ps->remain= delim+1;

    s= lstrpdbsearch_segment(ps->thisword,ps->lthisword);   if (s<0) continue;
    state[s].flags.updated_tmp= 1;
  }
  FOR_S {
    if (state[s].flags.inv == state[s].flags.updated_tmp)
      continue;
    state[s].flags.inv= state[s].flags.updated_tmp;
    mx_redraw_seg(s);
  }
}

static void si_movpos(ParseState *ps) {
  int r,s,f;
  s= ps_needsegment(ps);                                 if (s<0) return;
  r= ps_needword(ps);                                    if (r) return;
  if (thiswordstrcmp(ps,"position")) { badcmd(ps,"weird movpos"); return; }

  r= ps_needword(ps);                                    if (r) return;
  if (!thiswordstrcmp(ps,"?")) {
    int f;
    for (f=0; f < ui_plan_data.segments[s].n_movfeats; f++) {
      int n_posns= ui_plan_data.segments[s].movfeats[f].n_posns;
      if (n_posns > 1) state[s].mfs[f].posn= n_posns;
    }
    mx_redraw_seg(s);
    return;
  }

  const char *feat= ps->thisword;
  const char *endword= ps->thisword + ps->lthisword;
  while (feat < endword) {
    int featl;
    for (featl=0;
	 featl < ps->lthisword && CTYPE(isalpha, feat[featl]);
	 featl++);

    char *ep;
    errno=0; unsigned long pos= strtoul(feat+featl,&ep,10);

    if (!featl || errno || ep==feat+featl || ep > endword) {
      badcmd(ps,"bad movfeatpos `%.*s'", (int)(endword - feat), feat);
      return;
    }

    f= lstrpdbsearch_movfeat(feat, featl, &ui_plan_data.segments[s]);
    if (f<0) continue;

    if (pos >= ui_plan_data.segments[s].movfeats[f].n_posns)
      { badcmd(ps,"out of range movfeat %.*s%lu", featl,feat, pos); }

    state[s].mfs[f].posn= pos;
    mx_redraw_seg(s);

    feat= ep;
  }
}

static void si_on(ParseState *ps) {
  poweron= 1;
  mx_redraw_all();
}

static void si_off(ParseState *ps) {
  poweron= 0;
  mx_redraw_all();
}

static void si_train(ParseState *ps) {
  int r,sl,s;
  int lastchar;
  TrainState *train;
  const char *seg;

  mx_clear_updated();
  r= ps_needword(ps); /* <train> */                if (r) return;

  /* atomise the train name */
  for (train=trains.head;
       train;
       train= train->others.next)
    if (!thiswordstrcmp(ps,train->name))
      goto found;
  /* not found */
  train= mmalloc(sizeof(*train));
  train->name= mmalloc(ps->lthisword+1);
  memcpy(train->name,ps->thisword,ps->lthisword);
  train->name[ps->lthisword]= 0;
  DLIST2_APPEND(trains,train,others);
found:

  r= ps_needword(ps);                              if (r) return;
  if (thiswordstrcmp(ps,"has")) { badcmd(ps,"weird train"); return; }
  while (ps_word(ps) >= 0) {
    sl= strcspn(ps->thisword, "/.!*~#+");
    if (sl > ps->lthisword) sl= ps->lthisword;
    seg= ps->thisword;
    if (*seg=='-') { seg++; sl--; }
    s= lstrpdbsearch_segment(seg,sl);              if (s<0) continue;
    lastchar= ps->thisword[ps->lthisword-1];
    state[s].owner= train;
    state[s].flags.updated_tmp= 1;
    state[s].flags.trainown= 1 + (lastchar=='!' || lastchar=='*');
  }
  FOR_S {
    if (state[s].flags.updated_tmp) {
      mx_redraw_seg(s);
    } else if (state[s].owner==train) {
      state[s].owner= 0;
      state[s].flags.trainown= 0;
      mx_redraw_seg(s);
    }
  }
}

typedef struct MuxEventInfo MuxEventInfo;
typedef void MuxEventFn(ParseState *ps);

struct MuxEventInfo {
  const char *prefix; /* 0: sentinel */
  const char *remainpat; /* 0: no pattern in select needed */
  MuxEventFn *fn; /* 0: just ignore matching messages */
};
static const MuxEventInfo muxeventinfos[];

static void si_stastate(ParseState *ps) {
  int s;
  const char *const *new_stastate;

  new_stastate= some_needword_lookup(ps, stastate_names, "stastate");
  stastate= new_stastate ? new_stastate - stastate_names : 0;
  
  FOR_S {
    state[s].flags.resolution_problem= 0;
  }
  if (stastate <= Sta_Resolving) {
    FOR_S {
      state[s].flags.det= 0;
      state[s].flags.trainown= 0;
      state[s].owner= 0;
    }
  }
  mx_redraw_all();
}

static void mx_resolution_problem(int s) {
  state[s].flags.resolution_problem= 1;
  mx_redraw_seg(s);
}
static void si_resolution_inexplicable(ParseState *ps) {
  int s;
  s= ps_needsegment(ps);                                 if (s<0) return;
  mx_resolution_problem(s);
}
static void si_resolution_mispositioned(ParseState *ps) {
  int r, s;
  r= ps_needword(ps);  /* head|tail */                   if (r) return;
  r= ps_needword(ps);  /* <train> */                     if (r) return;
  s= ps_needsegment(ps);                                 if (s<0) return;
  mx_resolution_problem(s);
}

static void si_connected(ParseState *ps) {
  const MuxEventInfo *mxi;
  const char *p;
  int c;
  
  sockprintf("select-replay");
  for (mxi=muxeventinfos; mxi->prefix; mxi++) {
    if (!mxi->remainpat)
      continue;
    sockprintf(" ");
    for (p=mxi->prefix; (c=*p); p++) {
      switch (c) {
      case ' ': c= '_';
      }
      sockprintf("%c",c);
    }
    sockprintf("%s",mxi->remainpat);
  }
  sockprintf(" ~|*\n");

  /* set the fixed moveable features */
  const SegmentInfo *segi;
  const MovFeatInfo *mfi;
  const PlanSegmentData *planseg;
  const PlanSegmovfeatData *planfeat;
  int segn, s, mfn, f;
  
  for (segn=0; segn<info_nsegments; segn++) {
    segi= &info_segments[segn];
    if (!segi->n_fixedmovfeats) continue;
    for (s=0; s<NSEGMENTS; s++)
      if (!strcmp(segi->pname, (planseg=&ui_plan_data.segments[s])->segname))
	goto found_ld_seg;
    die("layout data segment %s not found\n",segi->pname);
  found_ld_seg:
    for (mfn=0, mfi= segi->movfeats + segi->n_movfeats;
	 mfn < segi->n_fixedmovfeats;
	 mfn++, mfi++) {
      for (f=1; f<planseg->n_movfeats; f++)
	if (!strcmp(mfi->pname, (planfeat=&planseg->movfeats[f])->movfeatname))
	  goto found_ld_feat;
      die("layout data movfeat %s/%s not found\n",segi->pname,mfi->pname);
    found_ld_feat:
      state[s].mfs[f].posn= mfi->posns;
      if (debug) fprintf(debug,"fixed %s/%s=%d\n",
			 segi->pname,mfi->pname,mfi->posns);
    }
  }
}

static void si_fatal(ParseState *ps) {
  die("multiplexer reports problem: %.*s\n",
      (int)badcmdreport_recsz, badcmdreport_data);
}
static void si_ack(ParseState *ps) {
  ps_needword(ps); /* command */
  ps_needword(ps); /* status */
  if (thiswordstrcmp(ps,"ok")) si_fatal(0);
}

static const MuxEventInfo muxeventinfos[]= {
  { "?detect", "",                          si_detect                     },
  { "?picio out polarity", "",              si_polarity                   },
  { "?movpos", "_*_position",               si_movpos                     },
  { "?picio out on", "",                    si_on                         },
  { "?picio out off", "",                   si_off                        },
  { "?train", "_*_has",                     si_train                      },
			                                               
  { "?stastate", "",                        si_stastate                   },
			                        
  { "?resolution inexplicable", "",         si_resolution_inexplicable    },
  { "?resolution mispositioned", "",        si_resolution_mispositioned   },
  			                        
  { "=connected", "",                       si_connected                  },
  { "=permission", "",                      0                             },
			                    		                
  { "+executing", 0,                        0                             },
  { "+ack", 0,                              si_ack                        },
  { "+nak", 0,                              si_fatal                      },
  { "=failed", 0,                           si_fatal                      },
  { "=denied", 0,                           si_fatal                      },
  { 0 }
};

static void sock_input_line(ParseState *ps) {
  const MuxEventInfo *mxi;
  const char *got, *expected;
  int l, c;
  if (!ps->remain || !ps->remain[0])
    return;
  ps->thisword= ps->remain;
  for (mxi=muxeventinfos; mxi->prefix; mxi++) {
    got= ps->remain;
    expected= mxi->prefix;
    l= ps->lthisword= strlen(expected);
    if (*expected=='?') { got++; expected++; l--; }
    if (memcmp(got, expected, l)) continue;
    if (!got[l] || got[l]==' ') goto found;
  }
  return;
found:
  if (!mxi->fn) return;
  ps->remain= got + l;
  if ((c= *ps->remain)) { assert(c==' '); ps->remain++; }
  if (debug) fprintf(debug,"calling <%s> with <%.*s|%s>\n",
		     mxi->prefix, ps->lthisword,ps->thisword, ps->remain);
  mxi->fn(ps);
}

static const InputStream sock_is= {
  "multiplexer connection",
  sock_eof,
  sock_input_line,
  sock_badcmd
};

/*---------- main program including much of the initialisation ----------*/

int main(int argc, const char *const *argv) {
  oop_read *rd;
  const char *arg;
  XpmAttributes mattribs;
  XWindowAttributes wattribs;
  int segment_ix, movfeat_ix, posn, invert, det, oor, infd;
  Window wspec=None;
  XGCValues gcv;
  XColor colour;
  SegmovfeatState *fs;
  const PlanSegmentData *segment_d;
  const PlanSegmovfeatData *movfeat_d;
  char *ep;
  
  sys_events= oop_sys_new();  if (!sys_events) diee("oop_sys_new");
  events= oop_sys_source(sys_events);  assert(events);

  while ((arg= *++argv)) {
    if (!strcmp(arg,"--sizes")) {
      printf("%d\n%d\n", ui_plan_data.xsz, ui_plan_data.ysz);
      if (ferror(stdout) || fflush(stdout)) diee("print stdout");
      exit(0);
    } else if (!strcmp(arg,"--debug")) {
      debug= stderr;
    } else if (arg[0]=='@') {
      char *node, *comma;
      const char *service;
      node= (char*)arg+1;
      comma= strchr(node,',');
      if (comma) {
	*comma= 0;
	service= comma+1;
      } else {
	service= STR(TRAINMX_PORT);
      }
      sock.fd= sock_clientconnect(node,service);
      if (sock.fd<0) die("unable to connect to multiplexer");
      obc_init(&sock);
      if (comma) *comma= ',';
    } else if (arg[0]=='-') {
      die("invalid option(s)");
    } else {
      errno=0; wspec= strtoul(arg,&ep,0);
      if (errno || ep==arg || *ep || wspec==None) die("bad windowid");
    }
  }

  disp= XOpenDisplay(0);  if (!disp) die("XOpenDisplay failed");

  if (wspec==None) {
    win= XCreateSimpleWindow(disp, DefaultRootWindow(disp),
			     0,0, ui_plan_data.xsz, ui_plan_data.ysz,
			     0,0, 0);
    if (win == None) diex("XCreateSimpleWindow", "initial");
  } else {
    win= wspec;
  }

  XCALL( XGetWindowAttributes, 0, (disp,win,&wattribs) );

  XPMCALL( XpmCreatePixmapFromData, "background",
	   (disp,win, (char**)ui_plan_data.background, &bg_pixmap,0,0) );

  XCALL( XSetWindowBackgroundPixmap, 0, (disp,win,bg_pixmap) );

  XCALL( XAllocNamedColor, "white",
         (disp, wattribs.colormap, "#ffffff",
          &colour, &colour) );
  train_pixel= colour.pixel;

  XCALL( XAllocNamedColor, "owned",
         (disp, wattribs.colormap, "#a0a0a0",
          &colour, &colour) );
  owned_pixel= colour.pixel;

  state= mmalloc(sizeof(*state) * NSEGMENTS);
  for (segment_ix= 0, segment_d= ui_plan_data.segments;
       segment_ix < ui_plan_data.n_segments;
       segment_ix++, segment_d++) {
    state[segment_ix].flags.on= 0;
    state[segment_ix].flags.inv= 0;
    state[segment_ix].flags.det= 0;
    state[segment_ix].flags.trainown= 0;
    state[segment_ix].flags.updated_tmp= 0;
    state[segment_ix].owner= 0;
    state[segment_ix].mfs= fs=
      mmalloc(sizeof(*state[segment_ix].mfs) * segment_d->n_movfeats);
    for (movfeat_ix= 0, movfeat_d= segment_d->movfeats;
	 movfeat_ix < segment_d->n_movfeats;
	 movfeat_ix++, movfeat_d++, fs++) {
      fs->next= states_head;  states_head= fs;
      fs->flags= state[segment_ix].flags;
      fs->posn= movfeat_d->n_posns;
      if (fs->posn==1) fs->posn= 0;
      fs->redraw_needed= 0;

      loadmask(&fs->whole, &movfeat_d->mask, &gcv, 0);

      fs->posns= mmalloc(sizeof(*fs->posns)*(fs->posn+1));
      for (posn= 0; posn <= fs->posn; posn++)
	for (invert=0; invert<2; invert++)
	  for (det=0; det<2; det++) {
	    PosnState *ps= &fs->posns[posn][invert][det];
	    const PlanPixmapOnData *ppod=
	      posn < movfeat_d->n_posns
	      ? &movfeat_d->posns[posn] : 0;
	    const PlanPixmapDataRef *ppdr= ppod
	      ? &ppod->on[invert][det]
	      : &movfeat_d->unknown[invert][det];
	    ps->x= ppdr->x;
	    ps->y= ppdr->y;
	    mattribs.valuemask= 0;
	    XPMCALL( XpmCreatePixmapFromData, "main",
		     (disp,win,
		      (char**)ppdr->d,
		      &ps->pm,
		      0, &mattribs) );
	    ps->width= mattribs.width;
	    ps->height= mattribs.height;
	    if (ppod) {
	      loadmask(&ps->edge, &ppod->pedge, &gcv, 0);
	    } else {
	      ps->edge.x= -1;
	      ps->edge.gc= None;
	    }
	  }
    }
  }

  if (sock.fd<0) {
    infd= 0;
    instream= &stdin_is;
  } else {
    infd= sock.fd;
    instream= &sock_is;
  }
  
  events->on_fd(events, infd, OOP_EXCEPTION, some_exception,
		(void*)instream->name);

  rd= oop_rd_new_fd(events, infd, 0,0);
  if (!rd) diee("oop_rd_new_fd");

  oor= oop_rd_read(rd, OOP_RD_STYLE_GETLINE, 1024,
		   input_ifok,0, input_iferr,0);
  if (oor) diee("oop_rd_read");

  int fd= ConnectionNumber(disp);
  events->on_fd(events, fd, OOP_READ,      xlib_readable,  0);
  events->on_fd(events, fd, OOP_EXCEPTION, some_exception,
		(void*)"xserver connection");

  XCALL( XSelectInput, 0, (disp,win, ExposureMask) );

  if (wspec!=None) {
    XCALL( XClearArea, "initial", (disp,win, 0,0,0,0, True) );
  } else {
    XCALL( XMapWindow, 0, (disp,win) );
  }

  xlib_process();

  oop_sys_run(sys_events);
  abort();
}
