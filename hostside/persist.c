/*
 * persist
 * persistent state management.
 */

/*
 * We use these files:
 *      persist.lock		  - protocol is as for with-lock-ex
 *      persist.data[.new,.old]	  - mmap, see record.c alloc
 *      persist.conv[.new,.old]	  - copy of our own convutable
 *
 *      persist.record     generated and updated automatically
 *
 * we mark the data files as executable, and then later insist on that,
 * because we map them into our own address space and trust them completely
 *
 * Update protocol:
 *
 *   interpretation of the states      data conv
 *		   		         .o  .o
 *
 *	case A				y ? y ?
 *	case B				? y ? y		but not A
 *	case C				y ? ? y		but not A or B
 *
 *      (y means file exists, use this version; ? existence irrelevant)
 *      (update protocol ignores .new files, which are used only for
 *       atomic creation of actual files)
 *
 *    				       data conv
 *   procedure for updating	         .o  .o
 *
 *	normal state			1 0 1 0		case A, 1
 *	delete old converter		1 0 1 -		case A, 1
 *	delete data.old			1 - 1 -		case A, 1
 *	rename converter -> .old	1 - 1 1		case A, 1
 *	 rename completes		1 - - 1		case C, 1
 *	rename data -> .old		1 1 - 1		case B, 1
 *	 rename completes		- 1 - 1		case B, 1
 *	create new data			2 1 - 1		case B, 1
 *	create new converter		2 1 2 1		case A, 2
 *
 *     (0, 1, 2 are successive versions; - is ENOENT)
 */

#include "realtime.h"

const char *persist_fn= "+persist";
char *persist_record_converted;

static int fd= -1;
static void *mapbase;
static int datalen;

/*---------- filename handling ----------*/

#define FN(dcl,suffix) persist_fn_ephemeral("." #dcl "." #suffix)
#define FN1(dcl)       persist_fn_ephemeral("." #dcl)

#define PFES 50
static const char *persist_fn_ephemeral(const char *suffix) {
  static char *rr[PFES];
  static int i;

  i++;
  i %= PFES;

  free(rr[i]);
  if (asprintf(&rr[i], "%s%s", persist_fn, suffix) <= 0)
    diee("vasprintf failed for persist_fn");
  return rr[i];
}

/*---------- utilities ----------*/

static void unlink_or_enoent(const char *filename) {
  int r;

  r= unlink(filename);
  if (r && errno != ENOENT) diee("unlink `%s'", filename);
}

static int fe(const char *fn) {
  struct stat stab;
  int r;

  r= stat(fn,&stab);
  if (r) {
    if (errno==ENOENT) return 0;
    else diee("failed stat to check for existence of `%s'", fn);
  }

  if (!S_ISREG(stab.st_mode))
    die("checking for existence of `%s' but it is not a plain file", fn);

  return 1;
}

/*---------- finding and interpreting of old persistent data ----------*/

static int persist_convert(const char *data, const char *conv) {
  int data_fd, newrecord_fd;
  pid_t child;

  if (!fe(conv)) return 0;

  data_fd= open(data, O_RDONLY);
  if (data_fd<0) {
    if (errno==ENOENT) return 0;
    else diee("persist data failed to check/open `%s'",data);
  }

  newrecord_fd= open(persist_record_converted, O_WRONLY|O_CREAT|O_TRUNC, 0666);
  if (newrecord_fd<0)
    diee("persist data failed to create new record");

  child= fork();
  if (child<0) diee("persist conversion: failed to fork");

  if (!child) {
    if (dup2(data_fd,0)) diee("persist child: failed to dup2 0");
    if (dup2(newrecord_fd,1)!=1) diee("persist child: failed to dup2 1");
    execl(conv, conv, PERSIST_CONVERT_OPTION, (char*)0);
    diee("persist child: failed to exec `%s'", conv);
  }

  mwaitpid(child, "persist conversion");

  if (close(newrecord_fd)) diee("persist data close new record");
  close(data_fd);

  return 1;
}

static int try(const char *data, const char *conv) {
  if (!persist_convert(data,conv)) return 0;
  ouprintf("info : converted %s using %s\n",data,conv);
  return 1;
}

void persist_entrails_interpret(void) {
  /* creates persist_record_converted */
  if (!persist_fn[0]) return;
  assert(!persist_record_converted);
  persist_record_converted= mstrdup(FN1(record));

  if (!simulate &&
      try(FN1(data),    FN1(conv))) return;
  if (try(FN(data,old), FN(conv,old))) return;
  
  if (simulate &&
      try(FN1(data),    FN1(conv))) return;
  
  if (try(FN1(data),    FN(conv,old))) return;

  free(persist_record_converted);
  persist_record_converted= 0;
}

/*---------- stupid mmap workaround ----------*/

static Byte resaddrbuf[1024*1024];
static long pagesize;
static unsigned long resaddruse;

#define RESADDR_DIEFMT   "(persist mapping parameters:" \
                         " %p+0x%lx%%0x%lx->%p+0x%lx)"
#define RESADDR_DIEARGS  resaddrbuf,(unsigned long)sizeof(resaddrbuf), \
                         pagesize, mapbase,resaddruse

static void resaddrdiee(const char *why) {
  diee("%s " RESADDR_DIEFMT, why, RESADDR_DIEARGS);
}

void persist_map_veryearly(void) {
  int r;
	 
  errno= 0; pagesize= sysconf(_SC_PAGE_SIZE);
  if (pagesize<=0) diee("could not find pagesize");

  if (pagesize & (pagesize-1)) return;
  if (pagesize > sizeof(resaddrbuf)/2) return;

  mapbase= (void*)(((unsigned long)resaddrbuf + pagesize - 1) &
		   ~(pagesize - 1UL));
  resaddruse= sizeof(resaddrbuf) - pagesize;

  r= mprotect(mapbase,resaddruse,PROT_READ);
  if (r) resaddrdiee("mprotect reserve buffer");
}

static void mapmem(int fd, int datalen, int prot) {
  void *rv;
  int r;

  if (!resaddruse)
    die("inappropriate combination of sizes " RESADDR_DIEFMT, RESADDR_DIEARGS);
  
  if (datalen > resaddruse)
    die("data length %d too large " RESADDR_DIEFMT, datalen, RESADDR_DIEARGS);
  
  r= munmap(mapbase, resaddruse);
  if (r) resaddrdiee("munmap reserve buffer");
    
  rv= mmap(mapbase, datalen, prot, MAP_SHARED|MAP_FIXED, fd, 0);
  if (rv == MAP_FAILED)
    resaddrdiee(prot==(PROT_READ|PROT_WRITE) ? "map data rw" :
		prot==PROT_READ ? "map data ro" : "map data badly");
	 
  assert(rv == mapbase);
}

/*---------- installing of our data as the current one ----------*/

void persist_install(void) {
  const char *devnull= "/dev/null";
  FILE *src, *dst;
  DIR *dir;
  const char *dirname;
  char *dirname_buf, *slash;
  int c, dst_fd;

  if (fd==-1) return;
  if (!persist_fn[0]) return;
  
  src= fopen("/proc/self/exe","rb");  if (!src) diee("open /proc/self/exe");

  unlink_or_enoent(FN(conv,new));
  dst_fd= open(FN(conv,new), O_WRONLY|O_CREAT|O_TRUNC, 0777);
  if (dst_fd<0) diee("create persist new conv");
  dst= fdopen(dst_fd,"wb");  if (!dst) diee("fdopen persist new conv");

  while ((c= getc(src)) != EOF)
    if (putc(c,dst) == EOF) diee("write persist new conv");

  if (ferror(src) || fclose(src)) diee("read /proc/self/exe");
  if (ferror(dst) || fflush(dst) || fsync(fileno(dst)) || fclose(dst))
    diee("finish writing persist new conv");

  if (fsync(fd) || msync(mapbase,datalen,MS_SYNC) || fsync(fd))
    diee("sync persist new data");

  /* Now we have the .new's, but let's just check ... */
  persist_record_converted= (char*)devnull;
  if (!persist_convert(FN(data,new),FN(conv,new)))
    die("persist conversion claims .new's do not exist ?!");
  assert(persist_record_converted == devnull);
  persist_record_converted= 0;

  dirname_buf= mstrdup(persist_fn);
  slash= strrchr(dirname_buf, '/');
  if (slash) do { *slash=0; } while (slash>dirname_buf && *--slash=='/');
  dirname= slash ? dirname_buf : ".";
  dir= opendir(dirname);
  if (!dir) diee("opendir persist directory `%s'", dirname);

  if (fe(FN1(data)) && fe(FN1(conv))) {        /* 1 ? 1 ?   A               */
    unlink_or_enoent(FN(conv,old));            /* 1 ? 1 -   A               */
    unlink_or_enoent(FN(data,old));            /* 1 - 1 -   A               */
    mrename(FN1(conv),FN(conv,old));           /* 1 - 1 1   A               */
                       /* rename completes        1 - - 1   C               */
  }
  /* we've converted A to C, so only B and C remain: */
  if (fe(FN(data,old)) && fe(FN(conv,old))) {  /* ? 1 ? 1   B               */
    unlink_or_enoent(FN1(data));               /* - 1 ? 1   B unlike C      */
  }
  /* B has been made not to look like C, so now only
   * genuine C and unmistakeable B remains: */
  if (fe(FN1(data)) && fe(FN(conv,old))) {     /* 1 ? ? 1   C               */
    mrename(FN1(data),FN(data,old));           /* 1 1 ? 1   B               */
                       /* rename completes        - 1 ? 1   B unlike A or C */
  }
  /* Just B now, ie we have */                 /* - 1 ? 1   B               */
  				        		   
  unlink_or_enoent(FN1(conv));                 /* - 1 - 1   B               */
				        		   
  mrename(FN(data,new),FN1(data));             /* 2 1 - 1   B               */
  mrename(FN(conv,new),FN1(conv));             /* 2 1 2 1   A               */

  if (fsync(dirfd(dir))) diee("sync persist directory `%s'", dirname);

  free(dirname_buf);
  fd= -1; /* do not install it again */
}

/*---------- creation (and mmapping) of new persistent data ----------*/

void *record_allocate(int datalen_spec) {
  /* claims lock, allocates space for new data file */
  int lockfd, r, i;
  FILE *data;
  struct flock fl;
  struct stat buf_stat, buf_fstat;

  assert(fd==-1);
  datalen= datalen_spec;
  
  for (;;) {
    lockfd= open(FN1(lock), O_RDWR|O_CREAT|O_TRUNC, 0660);
    if (lockfd<0) diee("open new persist lock file");

    memset(&fl,0,sizeof(fl));
    fl.l_type= F_WRLCK;
    fl.l_whence= SEEK_SET;
    r= fcntl(lockfd, F_SETLK, &fl);
    if (r<0) diee("claim persistent lock file");

    r= stat(FN1(lock), &buf_stat);  if (r) diee("stat persistent lock");
    r= fstat(lockfd, &buf_fstat);  if (r) diee("fstat persistent lock");
    if (!(buf_stat.st_dev != buf_fstat.st_dev ||
	  buf_stat.st_ino != buf_fstat.st_ino))
      break;

    close(lockfd);
  }

  
  unlink_or_enoent(FN(data,new));

  fd= open(FN(data,new), O_RDWR|O_CREAT|O_TRUNC, 0777);
  if (fd<0) diee("open new persist data file");
  data= fdopen(fd, "w+");  if (!data) diee("fdopen new persist data file");

  for (i=0; i<datalen; i++) putc(0x55,data);
  if (ferror(data) || fflush(data)) diee("clear new persist data file");

  fd= fileno(data);

  mapmem(fd, datalen, PROT_READ|PROT_WRITE);

  return mapbase;
}

/*---------- reading and mapping of existing persistent data ----------*/

static void phi_load(void *object, size_t sz, int *offset) {
  size_t r;

  while (*offset % sz) { getchar(); (*offset)++; }

  r= fread(object,1,sz,stdin);
  if (feof(stdin))  die("truncated persistent data header");
  if (ferror(stdin)) diee("read persistent data header");
  assert (r==sz);

  *offset += sz;
}

static void phi_check(const void *expected, size_t sz,
		      int *offset, const char *what) {
  Byte actual[sz];

  phi_load(actual, sz, offset);
  if (memcmp(actual, expected, sz))
    die("header magic check failed, in `%s'", what);
}
  
static void persist_mapread(void) {
  struct stat stab;
  int offset=0, r;

  r= fstat(0, &stab);  if (r) diee("could not fstat persist data file");
  if (!(stab.st_mode & 0111)) die("persist data file is not executable");

#define PHI_CHECK(x) phi_check(&(x), sizeof(x), &offset, STR(x));
#define PHI_LOAD(x) phi_load(&(x), sizeof(x), &offset);
  DO_PERSIST_HEADER_ITEMS(PHI_CHECK, PHI_LOAD, PHI_LOAD)

  mapmem(0, datalen, PROT_READ);
}

#define SANE_SEGMENT(seg)

void persist_entrails_run_converter(void) {
  SEG_IV;
  MovPosComb report;

  printf("# %s\n", autoversion);

  persist_mapread();

  FOR_SEG {
    if (seg->i != segi || !segi->pname ||
	!seg->owner || !seg->owner->pname)
      continue;
    printf("seg %s has %s%s%s\n", segi->pname,
	   (seg->tr_backwards ^ seg->owner->backwards) ? "-" : "",
	   seg->owner->pname,
	   seg->seg_inverted ? " inverted" : "");

    if (segi->n_poscombs>1 &&
	!seg->moving &&
	(report= seg->movposcomb) >=0 &&
	report < segi->n_poscombs)
      printf("seg %s at %s\n", segi->pname, segi->poscombs[report].pname);
  }
  if (ferror(stdout) || fflush(stdout))
    diee("entrails converter: stdout write error");

  printf("end\n");
  
  if (ferror(stdout) || fclose(stdout))
    diee("entrails converter: stdout write/close error");
  exit(0);
}
