/*
 * simple test harness for now
 * asserts on usage errors
 * see README.adhoc-test for usage info
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>

#include "common.h"

const char *progname= "adhoc-test";
static FILE *dump_stream= 0;

static int repeat_delay= -1, iterations= -1;
static const char *serial_port= "/dev/railway";

static Nmra buf;

void die_hook(void) { }
void die_vprintf_hook(const char *fmt, va_list al) { }

static void pahex(const char **argv) {
  const char *data_string;
  char hbuf[3], *ep;
  int i, l;
  Byte *c;

  c= buf.d;

  while ((data_string= *argv++)) {
    l= strlen(data_string);
    if (l&1) badusage("hex argument odd length");
    l >>= 1;
  
    for (i=0; i<l; i++) {
      if (c >= buf.d + sizeof(buf.d)) badusage("too much hex");

      hbuf[0]= data_string[i*2];
      hbuf[1]= data_string[i*2+1];
      hbuf[2]= 0;
      *c++= strtoul(hbuf,&ep,16); 
      if (ep!=&hbuf[2]) badusage("bad hex in hex argument");
    }
  }
  buf.l= c - buf.d;
}

struct NmraParseEncodeCaller {
  const char *const *argv;
};

unsigned long nmra_argnumber(NmraParseEncodeCaller *pec, int argi) {
  const char *arg;
  char *ep;
  unsigned long l;

  arg= pec->argv[argi];
  errno=0; l= strtoul(arg,&ep,0);
  if (errno || *ep || ep==arg) badusage("bad numeric arg value");
  return l;
}

void nmra_problem(NmraParseEncodeCaller *pec, const char *problem) {
  badusage(problem);
}

static void dump(const char *what, const Byte *data, int length) {
  if (!dump_stream) return;
  fprintf(dump_stream,"%-25s %3d  ",what,length);
  while (length>0) {
    fprintf(dump_stream," %02x", *data);
    data++;
    length--;
  }
  fprintf(dump_stream,"\n");
}

static void xmit_command(void) {
  dump("xmit_command", buf.d, buf.l);
  serial_transmit_now(buf.d, buf.l);
}

static void xmit_nmra_raw(void) {
  PicInsn pi;
  
  dump("xmit_nmra_raw", buf.d, buf.l);
  nmra_encodeforpic(&buf, &pi);
  serial_transmit_now(pi.d, pi.l);
}

static void xmit_nmra_bytes(void) {
  int l;
  l= buf.l;
  nmra_addchecksum(&buf);
  xmit_nmra_raw();
  buf.l= l;
}

int main(int argc, const char **argv) {
  void (*xmitter)(void);
  const char *arg;

  if (!*argv++) badusage("need argv[0]");
  
  while ((arg=*argv++) && *arg=='-') {
    arg++;
    switch (*arg++) {
    case 's': serial_port=             arg;  break;
    case 'i': iterations=         atoi(arg); break;
    case 'f': serial_fudge_delay= atoi(arg); break;
    case 'w': repeat_delay=       atoi(arg); break;
    case 'd': dump_stream= stdout; goto noarg;
    default: badusage("uknown option");
    noarg: if (*arg) badusage("option with value where forbidden");
    }
  }

  if (!arg) badusage("need arg");

  #define BARE(f)				\
  if (!strcmp(arg,#f)) {			\
    pahex(argv);				\
    xmitter= xmit_##f;				\
  } else
  BARE(command)
  BARE(nmra_raw)
  BARE(nmra_bytes)
  #undef BARE
  {
    NmraParseEncodeCaller pec;
    pec.argv= argv;
    for (argc=0; argv[argc]; argc++);
    nmra_parse_encode(&buf, arg, strlen(arg), argc, &pec);
    xmitter= xmit_nmra_bytes;
  }

  serial_open(serial_port);

  for (;;) {
    xmitter();
    
    if (repeat_delay < 0) break;
    if (iterations >= 0 && !iterations--) break;
    if (repeat_delay > 0) usleep(repeat_delay);
  }

  return 0;
}
