/*
 * realtime
 * implementation of safety algorithm's decisions
 */

#include "realtime.h"
#include "nmra.h"

/*---------- polarity ----------*/

static PicInsn polarityinsn= { { 0x90 }, 0 };

#define OPONBIT_BYTE (polarityinsn.d[bytenum])
#define OPONBIT(body) do{				\
    BoardObject bo;					\
    int bytenum, bitnum;				\
    Byte bitv;						\
							\
    bo= segi->invert;					\
    assert(bo);						\
    bytenum= (bo + 3) / 7;				\
    bitnum= bytenum ? 6 - (bo + 3) % 7 : 3 - bo;	\
    bitv= 1u << bitnum;					\
							\
    { body }						\
  }while(0)

int picinsn_polarity_testbit(const PicInsn *pi, const SegmentInfo *segi) {
  OPONBIT({
    return !!(OPONBIT_BYTE & bitv);
  });
}

void actual_inversions_start(void) {
  int i;
  polarityinsn.l= (info_maxreverse + 4 + 6) / 7;
  for (i=1; i<polarityinsn.l-1; i++)
    polarityinsn.d[i] |= 0x80;
}

void actual_inversions_segment(Segment *seg) {
  const SegmentInfo *segi= seg->i;
  
  OPONBIT({
    Byte *insnbyte= &OPONBIT_BYTE;
    if (seg->seg_inverted) *insnbyte |= bitv;
    else *insnbyte &= ~bitv;
  });
}

void actual_inversions_done(void) {
  serial_transmit(&polarityinsn);
}

/*---------- adjucts ----------*/

static void adj_updated_cmd(AdjunctsAddr *a, int ix, Nmra *n /*destroyed*/) {
  if (a->rn[ix].pi.l)
    retransmit_relaxed_cancel(&a->rn[ix]);
  retransmit_relaxed_queue(&a->rn[ix], n);
  assert(a->rn[ix].pi.l);
}

static void adj_updated_funcs(AdjunctsAddr *a, int ix,
			      void (*enco)(Nmra *n, int addr, unsigned map),
			      unsigned thiscmd) {
  Nmra n;
  if (!(a->all & thiscmd)) return;
  enco(&n,a->addr,a->current);
  adj_updated_cmd(a,ix,&n);
}

void adjuncts_updated(AdjunctsAddr *a) {
  /* idempotent; call this after updating a->current */

  adj_updated_funcs(a,0, enco_nmra_funcs0to4,  0x0001f);
  adj_updated_funcs(a,1, enco_nmra_funcs5to8,  0x001e0);
  adj_updated_funcs(a,2, enco_nmra_funcs9to12, 0x01e00);

  if (a->all & ADJS_SPEEDSTEP_BIT) {
    Nmra n;
    enco_nmra_speed126(&n, a->addr,
		       a->current & ADJS_SPEEDSTEP_BIT
		       ? a->speedstep : 0,
		       !!(a->all & ADJS_SPEEDSTEP_REVERSE));
    adj_updated_cmd(a,3, &n);
  }
}

void adjuncts_start_xmit(void) {
  AdjunctsAddr **ap;
  int ai;

  for (ai=0,ap=adjaddrs; ai<n_adjaddrs; ai++,ap++) {
    AdjunctsAddr *a= *ap;
    adjuncts_updated(a);
  }
}
