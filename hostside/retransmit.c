/*
 * realtime
 * nmra retransmission
 */

#include "realtime.h"

/*
 * There are two kinds of messages: urgent and relaxed.
 *
 * Urgent messages may be due for speedy transmission.  The due urgent
 * message with the highest priority is always transmitted as soon as
 * a slot is available.  This is known as `speedy transmission'.
 * 
 * When no urgent messages are due for speedy transmission, messages
 * (urgent or relaxed) are transmitted in a round-robin fashion.  This
 * round-robin transmission is completely independent of the speedy
 * transmission.
 *
 * Each urgent message is due for speedy transmission as follows:
 *   - When it is first queued and has not yet been
 *     speedily transmitted:
 *         immediately due, with maximum priority
 *   - Following each speedy transmission:
 *         due after an interval since the previous
 *           transmission; this interval increases exponentially
 *           for each transmission
 *         with priority which decreases with later transmissions
 *         until it has been speedily transmitted a certain number of times
 *   - Thereafter:
 *         no longer ever due; subject to round-robin transmission only.
 * Intervals are measured in messages regardless of length.
 */
/*
 * Implementation:
 *  All messages are on one of the relaxed queues; urgent messages
 *  are on the relaxed[0] queue and messages which are explicitly
 *  relaxed are on the relaxed[1] queue.  This gives the previously-
 *  urgent messages 50% of the bandwidth.
 *  There is one queue for each speedy retransmission; messages are on
 *  it in chronological order of dueness.
 */

#include "realtime.h"
#include "nmra.h"
#include "retransmit-table.h"

#define RETRANSMIT_TIME_SIGNED_MAX ((~(Retransmit__Time)0) >> 1)

static PicInsn linefill;

static DLIST2_HEAD(RetransmitRelaxedNode) relaxed[2];
static int last_relaxed_qn;
static Retransmit__Time elapsed;
static PerSpeedyTrans speedies[] = SPEEDIESINIT;

static void debug_hex(const Byte *d, int l) {
  int i;
  for (i=0; i<l; i++) DPRINTFA(" %02x", d[i]);
}

static void debug_message(const PicInsn *pi, const Nmra *n) {
  DPRINTFA(" :"); debug_hex(pi->d, pi->l);
  if (n) { DPRINTFA(" <="); debug_hex(n->d, n->l); }
  DPRINTFA(".\n");
}

static void retransmit_this(const PicInsn *pi) {
  if (DEBUGP(retransmit,message)) debug_message(pi, 0);
  serial_transmit(pi);
  elapsed += pi->l;
}

void retransmit_start(void) {
  Nmra n;

  enco_nmra_idle(&n);
  nmra_addchecksum(&n);
  nmra_encodeforpic(&n, &linefill);

  retransmit_something();
  retransmit_something();
  retransmit_something();
}

void retransmit_something(void) {
  PerSpeedyTrans *spd;
  RetransmitUrgentNode *urg;
  RetransmitRelaxedNode *rlx;
  int ix, DP;

  DPRINTF1(retransmit,message,"xmit ");

  for (ix=0, spd=speedies;
       ix < SPEEDYCOUNT;
       ix++, spd++) {
    urg= spd->queue.head;
    if (!urg) { DPRINTF2("-"); continue; }
    if (elapsed - urg->u.when > RETRANSMIT_TIME_SIGNED_MAX)
      { DPRINTF2("<"); continue; }

    DPRINTF2("%*s spd%-2d", SPEEDYCOUNT-ix,"", ix);

    /* found one to transmit: */
    DLIST2_REMOVE(spd->queue,urg,u.queue);
    if (++ix < SPEEDYCOUNT) {
      DPRINTF2(" ->spd%-2d =%p",ix, urg);
      urg->u.ix= ix;
      urg->u.when= elapsed + spd->interval;
      spd++;
      DLIST2_APPEND(spd->queue,urg,u.queue);
    } else {
      DPRINTF2(" ->rlx   =%p", urg);
      urg->u.ix= -1;
    }
    retransmit_this(&urg->pi);
    return;
  }

  for (ix=0; ix<2; ix++) {
    int qn= last_relaxed_qn ^ ix ^ 1;
    rlx= relaxed[qn].head;
    if (!rlx) { DPRINTF2("%d",qn); continue; }
    
    DPRINTF2("%-*s rlx%d        =%p", 2-ix,"", qn, rlx);

    DLIST2_REMOVE(relaxed[qn],rlx,rr);
    DLIST2_APPEND(relaxed[qn],rlx,rr);

    last_relaxed_qn= qn;
    retransmit_this(&rlx->pi);
    return;
  }

  serial_transmit(&linefill);
}

static void relaxed_queue(int qn, RetransmitRelaxedNode *rn, Nmra *n,
			  const char *debugwhat) {
  int DP;
  if (n) { nmra_addchecksum(n); nmra_encodeforpic(n, &rn->pi); }
  DPRINTF1(retransmit,queue, "%s %p", debugwhat, rn);
  if (DP) debug_message(&rn->pi, n);
  DLIST2_PREPEND(relaxed[qn],rn,rr);
}
static void relaxed_cancel(int qn, RetransmitRelaxedNode *rlx,
			   const char *debugwhat) {
  if (debugwhat) DPRINTF(retransmit,queue, "%s %p\n", debugwhat, &rlx->pi);
  DLIST2_REMOVE(relaxed[qn],rlx,rr);
}

void retransmit_relaxed_queue(RetransmitRelaxedNode *rn, Nmra *n) {
  relaxed_queue(1,rn,n, "retransmit_relaxed_queue");
}
void retransmit_relaxed_cancel(RetransmitRelaxedNode *rlx) {
  relaxed_cancel(1,rlx, "retransmit_relaxed_cancel");
}

static void urgent_queue(RetransmitUrgentNode *urg, Nmra *n,
			 const char *debugwhat) {
  relaxed_queue(0, &urg->u.relaxed, n, debugwhat);
  urg->u.ix= 0;
  urg->u.when= elapsed;
  DLIST2_APPEND(speedies[0].queue,urg,u.queue);
}
static void urgent_cancel(RetransmitUrgentNode *urg,
			  const char *debugwhat) {
  if (urg->u.ix >= 0)
    DLIST2_REMOVE(speedies[urg->u.ix].queue,urg,u.queue);
  relaxed_cancel(0, &urg->u.relaxed, debugwhat);
}

void retransmit_urgent_queue(RetransmitUrgentNode *urg, Nmra *n) {
  urgent_queue(urg,n, "retransmit_urgent_queue");
}
void retransmit_urgent_queue_relaxed(RetransmitUrgentNode *urg, Nmra *n){
  relaxed_queue(0, &urg->u.relaxed, n, "retransmit_urgent_queue_relaxed");
  urg->u.ix= -1;
  urg->u.when= elapsed;
}
void retransmit_urgent_requeue(RetransmitUrgentNode *rn, Nmra *n) {
  urgent_cancel(rn, 0);
  urgent_queue(rn, n, "retransmit_urgent_requeue");
}
void retransmit_urgent_cancel(RetransmitUrgentNode *urg) {
  urgent_cancel(urg, "retransmit_urgent_cancel");
}


void on_pic_nmradone(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  assert(objnum);
  if (sta_state == Sta_Run || sta_state == Sta_Manual)
    while (objnum--)
      retransmit_something();
  else
    if (sta_state > Sta_Settling)
      die("PIC sent NMRADONE in %s", stastatelist[sta_state]);
}
