/*
 * realtime
 * realtime priority
 */

#include "realtime.h"

#include <sys/resource.h>
#include <signal.h>
#include <time.h>

#include <sched.h>

/*---------- !ALL(MEM): prevent us eating all RAM ----------*/

static void rtf_limit_MEM(void) {
  int r;
  struct rlimit64 rl;
  r= getrlimit64(RLIMIT_MEMLOCK,&rl);  if (r) diee("getrlimit RLIMIT_MEMLOCK");
  rl.rlim_cur= 4*1024*1024;
  r= setrlimit64(RLIMIT_MEMLOCK,&rl);  if (r) diee("setrlimit RLIMIT_MEMLOCK");
}

/*---------- MEM: lock us into memory ----------*/

static void rtf_acquire_MEM(void) {
  int r;
  r= mlockall(MCL_CURRENT|MCL_FUTURE);  if (r) diee("mlockall");
}

/*---------- !ALL(CPU): use SIGXCPU to detect us spinning ----------*/

#define MAX_CPU_CREDIT 5 /* max burst of 5 seconds */
#define MAX_CPU_NTH 2 /* allow up to 50% usage, sustained */

static time_t now, last;
static struct rlimit rlcpu;
static struct sigaction sa_xcpu;

static void write_stderr(const char *m, int l) { write(2,m,l); }

static void cpulim_bomb(const char *what) __attribute__((noreturn));
static void cpulim_bomb(const char *what) {
  static const char m[]= "realtime: cpu limitation failure: ";
  int e;
  const char *s;

  e= errno;
  write_stderr(m,sizeof(m)-1);
  write_stderr(what,strlen(what));
  write_stderr(": ",2);
  
  s= strerror(e);
  write_stderr(s,strlen(s));
  write_stderr("\n",1);
  exit(-127);
}

static const char *cpulim_gnow(void) { /* returns what failed */
  if (time(&now) == (time_t)-1) return "time(2)";
  return 0;
}

static const char *cpulim_makepending(void) { /* returns what failed */
  int r;
  last= now;
  r= sigaction(SIGXCPU, &sa_xcpu, 0);  if (r) return "sigaction SIGXCPU";
  r= setrlimit(RLIMIT_CPU, &rlcpu);    if (r) return "setrlimit RLIMIT_CPU";
  return 0;
}

static void cpulim_exceeded(void) {
  static const char m[]= "realtime: cpu limit exceeded\n";
  write_stderr(m,sizeof(m)-1);
  raise(SIGXCPU);
  abort();
}

static void xcpuhandler(int ignored) {
  int credit;
  const char *whatfailed;

  whatfailed= cpulim_gnow();
  if (whatfailed) goto fail;

  credit= (now - last)/MAX_CPU_NTH;
  if (credit <= 0) cpulim_exceeded();
  if (credit > MAX_CPU_CREDIT) credit= MAX_CPU_CREDIT;
  rlcpu.rlim_cur += credit;

  whatfailed= cpulim_makepending();
  if (whatfailed) goto fail;
  return;

fail:
  cpulim_bomb(whatfailed);
}

static void rtf_limit_CPU(void) {
  int r;
  const char *whatfailed;

  memset(&sa_xcpu,0,sizeof(sa_xcpu));
  sa_xcpu.sa_handler= xcpuhandler;
  sigemptyset(&sa_xcpu.sa_mask);
  sa_xcpu.sa_flags= SA_RESETHAND | SA_RESTART | SA_NODEFER;
  
  r= getrlimit(RLIMIT_CPU, &rlcpu);
  if (r) { whatfailed= "getrlimit RLIMIT_CPU"; goto fail; }

  whatfailed= cpulim_gnow();
  if (whatfailed) goto fail;

  rlcpu.rlim_cur= MAX_CPU_CREDIT;

  whatfailed= cpulim_makepending();
  if (whatfailed) goto fail;
  return;

fail:
  diee("prepare cpu limiter: %s",whatfailed);
}

/*---------- CPU: set hard scheduling priority ----------*/

static void rtf_acquire_CPU(void) {
  pid_t child;

  child= fork();
  if (child<0) diee("fork for auth-sched-setscheduler");

  if (!child) {
    execlp("auth-sched-setscheduler",
	   "auth-sched-setscheduler","fifo","54",(char*)0);
    diee("exec auth-sched-setscheduler");
  }
  mwaitpid(child, "auth-sched-setscheduler");
}

/*---------- core ----------*/

unsigned rtfeats_use= RTFEAT_DEFAULTS;

void realtime_priority(void) {
#define RESOURCE(NAME)						\
  if (rtfeats_use & RTFEAT_##NAME) {				\
    if (!(rtfeats_use & RTFEAT_ALL(NAME))) rtf_limit_##NAME();	\
    rtf_acquire_##NAME();					\
  }
  RESOURCE(MEM);
  RESOURCE(CPU);
}
