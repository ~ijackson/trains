/*
 * realtime & multiplexer
 * reading and parsing commands from some fd
 */

#include "daemons.h"

oop_source *events;

static void *cmdi_exception(oop_source *evts, int fd,
			    oop_event evt, void *cmdi_v) {
  CommandInput *cmdi= cmdi_v;
  cmdi->out.error(&cmdi->out, "error by command source",
		  "exceptional condition");
  return OOP_CONTINUE;
}

static void *cmdi_iferr(oop_source *evts, oop_read *cl_read,
			oop_rd_event evt, const char *errmsg, int errnoval,
			const char *data, size_t recsz, void *cmdi_v) {
  CommandInput *cmdi= cmdi_v;

  cmdi->out.error(&cmdi->out, "read",
		oop_rd_errmsg(cl_read, evt,
			      errnoval, OOP_RD_STYLE_GETLINE));
  return OOP_CONTINUE;
}

static void *cmdi_ifok(oop_source *evts, oop_read *cl_read,
		       oop_rd_event evt, const char *errmsg, int errnoval,
		       const char *data, size_t recsz, void *cmdi_v) {
  CommandInput *cmdi= cmdi_v;
  ParseState ps;

  if (evt == OOP_RD_EOF) {
    cmdi->out.error(&cmdi->out,0,0);
    return OOP_CONTINUE;
  }
  
  if (evt != OOP_RD_OK)
    return cmdi_iferr(evts,cl_read,evt,errmsg,errnoval,data,recsz,cmdi_v);

  ps.remain= data;
  cmdi->doline(&ps, cmdi);
  return OOP_CONTINUE;
}

void cmdin_new(CommandInput *cmdi, int readfd) {
  int r;

  obc_init(&cmdi->out);

  events->on_fd(events, readfd, OOP_EXCEPTION, cmdi_exception, cmdi);

  cmdi->rd= oop_rd_new_fd(events, readfd, 0,0);
  if (!cmdi->rd) diee("oop_rd_new_fd");
  r= oop_rd_read(cmdi->rd, OOP_RD_STYLE_GETLINE, 1024,
		 cmdi_ifok, cmdi,
		 cmdi_iferr, cmdi);
  if (r) diee("oop_rd_read");  
}
