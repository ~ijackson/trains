/*
 * evdev-manip [<options> <device> ...]
 *
 *  modes:
 *     --dump-raw      default
 *     --redact        print redacted version
 *                      output is   <path|label> <value> <str> <str>...
 *                      where <str>... identifies the button, axis, etc.
 *
 *  global options:
 *     --stdin-monitor      quit if stdin becomes readable
 *
 *  redaction mode options:
 *
 *     --redaction <str>... --suppress  do not print these
 *     --redaction <str>... --show      print these normally
 *
 *        Longest matching <str>... applies.  Later identical
 *        <str>... paths override earlier ones.
 *
 *     --new-redactions     resets everything to default (which is --show)
 *
 *        Every device specified after --new-redactions gets the new
 *        set of redactions, which includes even redactions specified
 *        _after_ the device but _before_ the next --new-redactions.
 *        It is not possible to selectively edit the redactions list;
 *        devices which need different redaction lists need them
 *        respecifying.
 *
 *  per-device options (applies only to next device):
 *    --label LABEL         use LABEL instead of path in redacted output
 *
 *  per-device options (apply to all subsequent):
 *     --evdev                     subsequent devices are evdev
 *     --hiddev                    subsequent devices are hiddev
 *
 *  per-evdev options (apply to all subsequent):
 *     --[no-]grab                 --nograb is default
 *     --expect-sysfs /sys/class/input/inputX/eventY/dev
 *                        ^^^^^^^^^^^^^^^^^^^^
 *                          this part is in /proc/bus/usb/devices
 *                           and can thus be specified by caller
 *                          for evdev devices
 *
 *     evdev <str>... are type and code, each split up into
 *     prefix and tail, eg EV_KEY KEY_EQUAL becomes EV KEY KEY EQUAL.
 *     If the type or code is not found in our number-to-string
 *     tables, the respective two <strs> are 0x and the value in hex.
 *
 *  per-hiddev options (apply to all subsequent):
 *     --elide|show-unchanged      --elide-unchanged is default
 *
 *     hiddev <str>... are application and field.  In each case, page
 *     and usage, both in hex, both at least two digits with 0x in
 *     from of page only.  This matches what is shown in the USB HID
 *     usage tables specification which can be found at
 *     http://www.usb.org/developers/hidpage/
 */

#include "common.h"

#include <oop-read.h>
#include <poll.h>
#include <sys/fcntl.h>
#include <search.h>
#include <inttypes.h>

#include <linux/input.h>
#include <linux/hiddev.h>

typedef struct InputEventState InputEventState;
#include "input-codes.h" /* not really a header */

typedef struct Device Device;
typedef struct KindInfo KindInfo;
typedef struct ModeInfo ModeInfo;
typedef const ModeInfo *Mode;
typedef const KindInfo *Kind;

typedef enum { RA_Show, RA_IntermediateNode, RA_Suppress } RedactionAction;

typedef struct RedactionNode {
  const char *str;
  void *children; /* tsearch tree of RedactionNodes */
  RedactionAction act;
} RedactionNode;

struct ModeInfo {
  void (*evdev_event)(Device *d, const struct input_event *ie);
  void (*evdev_readable)(Device *d);
  void (*evdev_synch)(Device *d, struct timeval tv);
  void (*hiddev_event)(Device *d, const struct hiddev_usage_ref *ur);
  int (*hiddev_xflags)(void);
  void (*opened)(Device *d);
  void (*redacted)(Device *d, int nstrs, const char *strs[nstrs], int value);
  void (*died)(Device *d, int revents, int readr, int readc, int e)
       __attribute__((noreturn));
  void (*mainloop)(void);
};

struct KindInfo {
  void (*prepare)(Device*);
  void (*readable)(Device*);
};

typedef struct {
  int elide;
  RedactionNode redactions; /* root; .str is 0 and irrelevant */
} DeviceOptions;

typedef struct {
  struct hiddev_field_info fi;
  uint32_t maxusage;
  int *lastvalues;
} HiddevField;

struct Device {
  char *path;
  const char *label;
  int fd;
  const KindInfo *kind;
  DeviceOptions opts;
  union {
    struct {
      void *froot;
      HiddevField *fbuf;
    } hiddev;
    struct {
      int nabsinfos;
      struct input_absinfo **absinfos;
    } evdev;
  } forkind;
};

/*---------- globals ----------*/

/* command line options */
static Mode mode;
static Kind kind;
static int grab, stdinmonitor;
static const char *expect_sysfs, *label;
static DeviceOptions dopts;

static int ndevices;
static Device *devices;

/*---------- generally useful ----------*/

static void pr_hex(unsigned long value) { printf("%#lx",value); }

#define PR_TABLE_STR(tab, val) (pr_table_str(iesis_##tab, ien_##tab, (val)))
static void pr_table_str(const InputEventStringInfo *strings, int nstrings,
			 unsigned long value) {
  const InputEventStringInfo *string;
  if (value > nstrings) { pr_hex(value); return; }
  string= &strings[value];
  if (!string->prefix) { pr_hex(value); return; }
  printf("%s_%s", string->prefix, string->main);
}

static void pr_time(struct timeval tv) {
  printf("%ju.%06d", (uintmax_t)tv.tv_sec, (int)tv.tv_usec);
}

static void mread(Device *d, void *buf, size_t l) {
  char *p;
  int r, remain;

  for (p=buf, remain=l;
       remain;
       p+=r, remain-=r) {
    r= read(d->fd, p, remain);
    if (r<=0) { mode->died(d, POLLIN, r, -1, errno); abort(); }
    assert(r <= remain);
  }
}

static void dump_vpv(unsigned vendor, unsigned product, unsigned version) {
  printf(" vendor %#x product %#x version %#x",
	 vendor, product, version);
}
#define DUMP_VPV(t) (dump_vpv((t).vendor, (t).product, (t).version))

/*---------- evdev kind ----------*/

static const struct input_absinfo *evdev_getabsinfo(Device *d, uint16_t code) {
  struct input_absinfo **aip, *ai;
  int i, r;

  if (code >= 0x80) return 0; /* absurd ioctl scheme! */

  if (code >= d->forkind.evdev.nabsinfos) {
    int newsize= code+10;
    d->forkind.evdev.absinfos= mrealloc(d->forkind.evdev.absinfos,
           newsize * sizeof(*d->forkind.evdev.absinfos));
    for (i=d->forkind.evdev.nabsinfos; i<newsize; i++)
      d->forkind.evdev.absinfos[i]= 0;
  }
  aip= &d->forkind.evdev.absinfos[code];
  ai= *aip;
  if (ai) return ai;

  *aip= ai= mmalloc(sizeof(*ai));
  r= ioctl(d->fd, EVIOCGABS(code), ai);
  ai->value= r ? errno : 0;
  return ai;
}

static void evdev_dump(Device *d, const struct input_event *ie) {
  const InputEventTypeInfo *t;
  const struct input_absinfo *ai;
  printf("evdev ");
  pr_time(ie->time);

  printf(" ");
  PR_TABLE_STR(ev, ie->type);

  printf(" ");
  if (ie->type >= IETIN) {
    t= 0;
  } else {
    t= &ietis[ie->type];
    if (!t->strings) t= 0;
  }
  if (t) pr_table_str(t->strings, t->nstrings, ie->code);
  else pr_hex(ie->code);

  printf(" ");
  switch (ie->type) {
  case EV_ABS:
    ai= evdev_getabsinfo(d, ie->code);
    if (!ai) {
      printf("?");
    } else if (ai->value) {
      printf("?%ld [%s]", (long)ie->value, strerror(ai->value));
      break;
    } else if ((ai->minimum==-1 || ai->minimum== 0) &&
	(ai->maximum== 0 || ai->maximum==+1)) {
    } else if (ai) {
      printf("%.7f",
	     (double)(ie->value - ai->minimum)/(ai->maximum - ai->minimum));
      break;
    }
    /* fall through */
  case EV_REL:
    printf("%ld",(long)ie->value);
    break;
  default:
    printf("%lx",(unsigned long)ie->value);
    break;
  }
  printf("\n");
}

#define MAXTABSTRH 20
static void tab_redact(const InputEventStringInfo *strings, int nstrings,
		       unsigned long value, char hexbuf[MAXTABSTRH],
		       const char *sb[2]) {
  const InputEventStringInfo *string;
  if (value < nstrings &&
      (string= &strings[value],
       string->prefix)) {
    sb[0]= string->prefix;
    sb[1]= string->main;
  } else {
    snprintf(hexbuf,sizeof(hexbuf),"%lx",value);
    sb[0]= "0x";
    sb[1]= hexbuf;
  }
}

static void evdev_redact(Device *d, const struct input_event *ie) {
  const InputEventTypeInfo *t;
  char sbh_type[MAXTABSTRH];
  char sbh_code[MAXTABSTRH];
  const char *strs[4];
  
  tab_redact(iesis_ev, ien_ev, ie->type, sbh_type, &strs[0]);

  if (ie->type >= IETIN) {
    t= 0;
  } else {
    t= &ietis[ie->type];
    if (!t->strings) t= 0;
  }
  tab_redact(t ? t->strings : 0,
	     t ? t->nstrings : 0,
	     ie->code, sbh_code, &strs[2]);

  mode->redacted(d, 4,strs, ie->value);
}

static void evdev_readable(Device *d) {
  struct input_event ie;

  if (mode->evdev_readable) mode->evdev_readable(d);

  for (;;) {
    mread(d, &ie, sizeof(ie));
    if (ie.type == EV_SYN) {
      if (mode->evdev_synch) mode->evdev_synch(d, ie.time);
      break;
    }

    mode->evdev_event(d, &ie);
  }
}

static void evdev_readable_dump(Device *d) {
  printf("report-from device %s\n",d->path);
}
static void evdev_synch_dump(Device *d, struct timeval tv) {
  printf("synch ");
  pr_time(tv);
  printf("\n");
}

static void check_expect_sysfs(int fd, const char *path, const char *efn) {
  char buf[50], *ep;
  unsigned long maj, min;
  struct stat stab;
  FILE *sysfs;
  int r;
    
  r= fstat(fd, &stab);  if (r) diee("%s: fstat failed", path);
  if (!S_ISCHR(stab.st_mode)) die("%s: not a character device", path);
    
  sysfs= fopen(efn,"r");
  if (!sysfs) diee("%s: failed to open sysfs %s", path, efn);
  if (!fgets(buf,sizeof(buf)-1,sysfs)) {
    if (ferror(sysfs)) diee("%s: failed to read sysfs %s", path, efn);
    assert(feof(sysfs)); die("%s: eof on sysfs %s", path, efn);
  }
  buf[sizeof(buf)-1]= 0;
  errno=0; maj=strtoul(buf,&ep,0);
  if (errno || *ep!=':') die("%s: bad major number or no colon in sysfs"
			     " dev file %s", path, efn);
  errno=0; min=strtoul(ep+1,&ep,0);
  if (errno || *ep!='\n') die("%s: bad minor number or no colon in sysfs"
			      " dev file %s", path, efn);

  if (maj != major(stab.st_rdev) || min != minor(stab.st_rdev))
    die("%s: is %lu:%lu, expected %lu:%lu", path,
	(unsigned long)major(stab.st_rdev),
	(unsigned long)minor(stab.st_rdev),
	maj, min);

  if (fclose(sysfs)) die("%s: failed to close sysfs %s", path, efn);
}

static void evdev_prepare(Device *d) {
  int r;
  struct input_id iid;
  
  if (expect_sysfs) {
    check_expect_sysfs(d->fd, d->path, expect_sysfs);
    expect_sysfs= 0;
  }

  r= ioctl(d->fd, EVIOCGID, &iid);
  if (r) diee("%s: failed to get id",d->path);

  mode->opened(d);
  printf(" bustype ");
  PR_TABLE_STR(bus, iid.bustype);
  DUMP_VPV(iid);
  putchar('\n');
  mflushstdout();

  if (grab) {
    r= ioctl(d->fd, EVIOCGRAB, 1);
    if (r) diee("%s: failed to grab",d->path);
  }

  d->forkind.evdev.nabsinfos= 0;
  d->forkind.evdev.absinfos= 0;
}

static const KindInfo kind_evdev= { evdev_prepare, evdev_readable };

/*---------- hiddev kind ----------*/

static int hiddev_f_compar(const void *a_v, const void *b_v) {
  const HiddevField *a=a_v, *b=b_v;
  /* these are all unsigned 0..0xffff so the differences fit nicely */
  return (int)a->fi.report_type - (int)b->fi.report_type ? :
         (int)a->fi.report_id   - (int)b->fi.report_id   ? :
         (int)a->fi.field_index - (int)b->fi.field_index;
}

static HiddevField *hiddev_get_f(Device *d,
				 const struct hiddev_usage_ref *ur) {
  HiddevField *f;
  void **fvp;
  int r;
  
  if (ur->field_index == HID_FIELD_INDEX_NONE)
    return 0;

  if (!d->forkind.hiddev.fbuf) {
    d->forkind.hiddev.fbuf= mmalloc(sizeof(*d->forkind.hiddev.fbuf));
  }

  memset(&d->forkind.hiddev.fbuf->fi,0x55,sizeof(d->forkind.hiddev.fbuf->fi));
  d->forkind.hiddev.fbuf->fi.report_type= ur->report_type;
  d->forkind.hiddev.fbuf->fi.report_id=   ur->report_id;
  d->forkind.hiddev.fbuf->fi.field_index= ur->field_index;
  fvp= tsearch(d->forkind.hiddev.fbuf,
	       &d->forkind.hiddev.froot,
	       hiddev_f_compar);
  if (!fvp) diee("tsearch hiddev type/id/index");
  f= *fvp;

  if (f == d->forkind.hiddev.fbuf) {
    d->forkind.hiddev.fbuf= 0;
    
    r= ioctl(d->fd, HIDIOCGFIELDINFO, &f->fi);
    if (r) diee("%s: ioctl HIDIOCGFIELDINFO %#x %#x %#x", d->path,
		f->fi.report_type, f->fi.report_id, f->fi.field_index);

    f->maxusage= f->fi.maxusage;

    size_t sz= sizeof(*f->lastvalues) * f->maxusage;
    f->lastvalues= mmalloc(sz);
    memset(f->lastvalues,0,sz);
  }
  if (ur->usage_index >= f->maxusage) {
    uint32_t newmax= ur->usage_index + 1;
    fprintf(stderr,"%s: usage_index %"PRIu32" >= maxusage %"PRIu32"\n",
	    d->path, ur->usage_index, f->maxusage);
    f->lastvalues= mrealloc(f->lastvalues, sizeof(*f->lastvalues) * newmax);
    memset(f->lastvalues + f->maxusage, 0,
	   sizeof(*f->lastvalues) * (newmax - f->maxusage));
    f->maxusage= newmax;
  }

  return f;
}

static int hiddev_elide(Device *d, const struct hiddev_usage_ref *ur,
			HiddevField *f) {
  if (!f)
    return 0;
  
  if (!d->opts.elide)
    return 0;

  unsigned relevant_flags= f->fi.flags &
    (HID_FIELD_RELATIVE|HID_FIELD_BUFFERED_BYTE);

  if (relevant_flags == HID_FIELD_RELATIVE &&
      !ur->value) {
    return 1;
  }
  if (relevant_flags == 0) {
    if (ur->value == f->lastvalues[ur->usage_index])
      return 1;
    f->lastvalues[ur->usage_index]= ur->value;
  }
  return 0;
}

static void hiddev_dump(Device *d, const struct hiddev_usage_ref *ur) {
  HiddevField *f;

  f= hiddev_get_f(d, ur);

  if (hiddev_elide(d, ur, f))
    return;

  printf("hiddev type %04x id %04x", ur->report_type, ur->report_id);
  if (ur->field_index == HID_FIELD_INDEX_NONE) {
    printf(" field index NONE\n");
    return;
  }

  printf(" field index %04x"
	 " usage index %04x code %04x value %08lx ",
	 ur->field_index,
	 ur->usage_index, ur->usage_code,
	 (unsigned long)ur->value);

  printf(" maxusage %04x flags %04x"
	 " physical %04x %08lx..%08lx"
	 " logical %04x %08lx..%08lx"
	 " application %04x"
	 " unit %04x exponent %04x",
	 f->fi.maxusage, f->fi.flags,
	 f->fi.physical, (unsigned long)f->fi.physical_minimum,
	 (unsigned long)f->fi.physical_maximum,
	 f->fi.logical, (unsigned long)f->fi.logical_minimum,
	 (unsigned long)f->fi.logical_maximum,
	 f->fi.application,
	 f->fi.unit, f->fi.unit_exponent);

  putchar('\n');
}

static void hiddev_redact_pageusage(unsigned long code,
				    char sb_buf[2][7],
				    const char *strs[2]) {
  assert(code <= 0xffffffffUL);
  sprintf(sb_buf[0], "%#04lx", code >> 16);
  sprintf(sb_buf[1], "%02lx", code & 0xffff);
  strs[0]= sb_buf[0];
  strs[1]= sb_buf[1];
}

static void hiddev_redact(Device *d, const struct hiddev_usage_ref *ur) {
  HiddevField *f;
  char sb_app[2][7], sb_usage[2][7];
  const char *strs[4];

  if (ur->field_index == HID_FIELD_INDEX_NONE)
    return;

  f= hiddev_get_f(d, ur);
  if (hiddev_elide(d, ur, f))
    return;

  hiddev_redact_pageusage(f->fi.application, sb_app,   &strs[0]);
  hiddev_redact_pageusage(ur->usage_code,    sb_usage, &strs[2]);

  mode->redacted(d, 4,strs, ur->value);
}

static void hiddev_readable(Device *d) {
  struct hiddev_usage_ref ur;
  mread(d, &ur, sizeof(ur));
  mode->hiddev_event(d, &ur);
}

static void hiddev_prepare(Device *d) {
  int r, flags;
  struct hiddev_devinfo di;

  flags= HIDDEV_FLAG_UREF;
  if (mode->hiddev_xflags) flags |= mode->hiddev_xflags();
  r= ioctl(d->fd, HIDIOCSFLAG, &flags);
  if (r) diee("hiddev %s: ioctl HIDIOCSFLAG", d->path);

  r= ioctl(d->fd, HIDIOCGDEVINFO, &di);
  if (r) diee("hiddev %s: ioctl HIDIOCGDEVINFO", d->path);

  mode->opened(d);
  printf(" bustype ");
  PR_TABLE_STR(bus, di.bustype);
  printf(" bus %d dev %d if %d", di.busnum, di.devnum, di.ifnum);
  DUMP_VPV(di);
  printf(" napplications %d\n", di.num_applications);

  d->forkind.hiddev.froot= 0;
  d->forkind.hiddev.fbuf= 0;
}

static int hiddev_xflags_dump(void) { return HIDDEV_FLAG_REPORT; }

static const KindInfo kind_hiddev= { hiddev_prepare, hiddev_readable };

/*---------- mode dump ----------*/

static void dump_died(Device *d, int revents, int readr, int readc, int e)
     __attribute__((noreturn));
static void dump_died(Device *d, int revents, int readr, int readc, int e) {
  printf("device-terminated %s %#x ", d->path, revents);
  if (readr<0) printf("err %s", strerror(e));
  else if (readr==0) printf("eof");
  else printf("%#x",readc);
  printf("\n");
  mflushstdout();
  exit(0);
}

static void dump_opened(Device *d) {
  printf("device %s", d->path);
}

static void mainloop(void) {
  struct pollfd *polls;
  int i, r, npolls;

  npolls= ndevices + stdinmonitor;
  polls= mmalloc(sizeof(*polls)*npolls);
  for (i=0; i<ndevices; i++) {
    polls[i].fd= devices[i].fd;
    polls[i].events= POLLIN;
  }
  if (stdinmonitor) {
    polls[ndevices].fd= 0;
    polls[ndevices].events= POLLIN;
  }

  for (;;) {
    for (i=0; i<npolls; i++)
      polls[i].revents= 0;

    r= poll(polls,npolls,-1);
    if (r==-1) {
      if (errno==EINTR) continue;
      diee("poll failed");
    }
    assert(r>0);

    if (stdinmonitor) {
      if (polls[ndevices].revents) {
	printf("quitting-stdin-polled %#x\n", polls[ndevices].revents);
	exit(0);
      }
    }

    for (i=0; i<ndevices; i++) {
      if (polls[i].revents & ~POLLIN) {
	unsigned char dummy;
	r= oop_fd_nonblock(polls[i].fd, 1);
	if (r) diee("nonblock %s during poll unepxected %#x",
		    devices[i].path, polls[i].revents);
	r= read(polls[i].fd, &dummy,1);
	mode->died(&devices[i], polls[i].revents, r, dummy, errno);
	abort();
      }
      if (polls[i].revents) {
	Device *d= &devices[i];
	d->kind->readable(d);
	mflushstdout();
      }
    }
  }
}

static const ModeInfo mode_dump= {
  evdev_dump, evdev_readable_dump, evdev_synch_dump,
  hiddev_dump, hiddev_xflags_dump,
  dump_opened, 0, dump_died, mainloop
};

/*---------- mode redact ----------*/

static int redaction_node_compar(const void *a_v, const void *b_v) {
  const RedactionNode *a=a_v, *b=b_v;
  return strcmp(a->str, b->str);
}

static RedactionNode *redact_find_node(int nstrs, const char *strs[nstrs]) {
  RedactionNode key, *lastfound, *search;
  void **rn_vp;

  lastfound= search= &dopts.redactions;
  for (;;) {
    if (!nstrs) return lastfound;
    key.str= strs[0];
    rn_vp= tfind(&key, &search->children, redaction_node_compar);
    if (!rn_vp) return lastfound;
    search= *rn_vp;
    if (search->act != RA_IntermediateNode)
      lastfound= search;
    strs++;
    nstrs--;
  }
}

static void redact_redacted(Device *d, int nstrs, const char *strs[nstrs],
			    int value) {
  int i;
  RedactionNode *rn;

  rn= redact_find_node(nstrs, strs);
  switch (rn->act) {
  case RA_Show: break;
  case RA_Suppress: return;
  default: abort();
  }

  printf("%s %d", d->label, value);
  for (i=0; i<nstrs; i++)
    printf(" %s", strs[i]);
  putchar('\n');
}

static void redact_opened(Device *d) {
  printf("%s opened", d->label);
}

static const ModeInfo mode_redact= {
  evdev_redact, 0, 0,
  hiddev_redact, 0,
  redact_opened, redact_redacted, dump_died, mainloop
};

static void redaction(const char ***argv) {
  RedactionNode *path, *newnode;
  void **searched;
  const char *arg;

  path= &dopts.redactions;
  for (;;) {
    arg= *++(*argv);
    if (!arg) badusage("missing str or action for --redaction");
    if (arg[0]=='-') break;
    newnode= mmalloc(sizeof(*newnode));
    newnode->str= arg;
    searched= tsearch(newnode, &path->children, redaction_node_compar);
    if (!searched) diee("allocate new redaction node");
    if (*searched == newnode) {
      newnode->children= 0;
      newnode->act= RA_IntermediateNode;
      path= newnode;
    } else {
      free(newnode);
      path= *searched;
    }
  }
  if (!strcmp(arg,"--suppress")) {
    path->act= RA_Suppress;
  } else if (!strcmp(arg,"--show")) {
    path->act= RA_Show;
  } else {
    badusage("unknown or missing action for --redaction");
  }
}

/*---------- main program ----------*/

static void getdevice(const char *path) {
  Device *d;
  ndevices++;
  devices= mrealloc(devices, sizeof(*devices)*ndevices);
  d= &devices[ndevices-1];

  d->path= mstrdup(path);
  d->fd= open(path, O_RDONLY);  if (d->fd<0) diee("%s: failed to open",path);
  d->kind= kind;
  d->opts= dopts;

  if (label) {
    d->label= label;
    label= 0;
  } else {
    d->label= d->path;
  }

  kind->prepare(d);
}

int main(int argc, const char **argv) {
  const char *arg;

  mode= &mode_dump;
  kind= &kind_evdev;
  dopts.elide= 1;

  while ((arg= *++argv)) {
    if (arg[0] != '-') {
      getdevice(arg);
    }
    else if (!strcmp(arg,"--expect-sysfs")) {
      if (!(expect_sysfs= *++argv)) badusage("missing arg for --expect-sysfs");
    }
    else if (!strcmp(arg,"--label")) {
      if (!(label= *++argv)) badusage("missing arg for --expect-sysfs");
    }
    else if (!strcmp(arg,"--redaction")) {
      redaction(&argv);
    }
    else if (!strcmp(arg,"--new-redactions")) {
      dopts.redactions.children= 0;
      dopts.redactions.act= RA_Show;
    }
    else if (!strcmp(arg,"--dump-raw")) { mode= &mode_dump; }
    else if (!strcmp(arg,"--redact")) { mode= &mode_redact; }
    else if (!strcmp(arg,"--evdev")) { kind= &kind_evdev; }
    else if (!strcmp(arg,"--hiddev")) { kind= &kind_hiddev; }
    else if (!strcmp(arg,"--grab")) { grab= 1; }
    else if (!strcmp(arg,"--no-grab")) { grab= 0; }
    else if (!strcmp(arg,"--show-unchanged")) { dopts.elide= 0; }
    else if (!strcmp(arg,"--elide-unchanged")) { dopts.elide= 1; }
    else if (!strcmp(arg,"--stdin-monitor")) { stdinmonitor= 1; }
    else badusage("unknown option");
  }
  mode->mainloop();
  return 0;
}

const char *progname= "evdev-manip";
void die_hook(void) { }
void die_vprintf_hook(const char *fmt, va_list al) { }
