/*
 * Handling of points and other moveable features.
 */

#include "realtime.h"

/*========== declarations ==========*/

typedef struct {
  const MovFeatInfo *i;
  Small posn;
} Motion;

typedef struct Method Method;
typedef struct Change Change;

#define PRunkx PRIx32
typedef uint32_t UnkMap;

typedef struct MovPosChange {
  Segment *move;
  /* everything beyond here is private for indep */
  MovPosComb actualpos, target;
  UnkMap actualunk; /* bit per movfeat, set iff actualpos contains dummy 0 */
  int refcount;
  int n_changes;
  Change *changes[];
} Indep;

static void method_update_feature(Method*, MovPosChange*, const Motion *mo);
  /* Called from methods' execution logic when an individual feature
   * has been moved.  This is used by the method-independent code to
   * compute the correct delta set of movements from the current
   * actual position, when thinking about new plans.  It is also sent
   * to clients and ultimately used by the UI.
   */

static void method_change_done(Method *m, Change *chg);
  /* Called from methods' execution logic when a whole change
   * has been done.  The method-independent code will take care of
   * updating move->movposcomb. etc.
   *
   * REENTRANCY: May be called from within a call to the method's
   * execute().  Of course cannot legally be called from within
   * prepare, consider, check or dispose.
   */


/* Kind-independent code is responsible for determining
 * the method, doing a bit of cleanup, and adjusting the flow
 * slightly.  Per-kind code does the actual work and is mostly in
 * charge - it is also responsible for updating seg->moving and ->motion.
 */
/*
 * The following states exist for each Method
 *   S  Starting     corresponds to global states other than Sta_Run
 *   T  Tentative    changes have been made but may yet be undone
 *   Y  Yes          proposed changes have been checked and are OK
 *   E  Executing    the method is executing
 *
 * The following states exist for each Change
 * at points when control flow  passes between kind and indep:
 *   U  Unallocated  no memory allocated (Change does not exist)
 *   P  Prepared     memory allocation done, basics filled in
 *   I  Installed    reservation or confirmation successful
 *   D  Done         motion is complete and callback is being entered
 *   G  Garbage      motion is complete and callback is being exited
 *
 * Changes may be for:
 *   R  Reservation
 *   C  Confirmation
 *
 * No Changes remain Prepared while the Method is Executing.
 */

struct Change {                    /* valid in:  filled in by and when:      */
  Method *meth;                    /*  PIDG       indep after prepare()      */
  MovPosChange *indep;             /*  PID        indep after prepare()      */
  unsigned installed:1; /* private for indep */
  /* kind-specific data follows */ /*  varies     kind-specific code, varies */
};

struct Method {
  const char *pname;

  ErrorCode (*prepare)(Method *m,                                 /* TYE->T */
		       const Segment *move,
		       int n_motions, const Motion *motions,
		       int ms, int confirmation,
		       Change **chg_r,          /* 0->P; err: 0->0; may be 0 */
		       int *cost_r /* may be 0 */);
  void (*dispose)(Method *m,                                       /* TY->TY */
		  Change*);                                          /* P->U */

  ErrorCode (*install)(Method *m,                   /* TYE->T; err: TYE->TYY */
		       Change *inst);                     /* P->I; err: P->P */
      /* error: ET->T, no change to Changes */
  void (*remove)(Method *m,                                      /* TYE->TYY */
		 Change *remove);                                    /* I->P */
      /* error: ET->T, no change to Changes */

  ErrorCode (*check)(Method *m);                    /* TYE->Y; err: TYE->TYE */
  void (*execute)(Method *m);                                       /* EY->E */
  void (*all_abandon)(Method *m);                                  /* TYE->S */

  unsigned needcheck, needexec; /* used by indep to track T/Y/E */
};

/*========== general utility functions ==========*/

static UnkMap unkfeatbit(int featix) { return (UnkMap)1 << featix; }
static UnkMap unkallfeatbits(int nfeats) { return ~(~(UnkMap)0 << nfeats); }

const char *movpos_pname(const Segment *move, MovPosComb poscomb) {
  return !SOMEP(poscomb) ? "?" : move->i->poscombs[poscomb].pname;
}

static void ouposn_moving(const MovPosChange *indep) {
  Segment *move= indep->move;
  ouprintf("movpos %s position %s moving\n",
	   move->i->pname,
	   indep->actualunk ? "?" : movpos_pname(move, indep->actualpos));
}
static void ouposn_stable(const Segment *move) {
  ouprintf("movpos %s position %s stable\n",
	   move->i->pname, movpos_pname(move, move->movposcomb));
}
static void ouposn_feat(const Segment *move, const MovFeatInfo *feati,
			MovPosComb posn, const Method *m) {
  ouprintf("movpos %s feat %s %d %s\n", move->i->pname,
	   feati->pname, posn, m->pname);
}

int movposcomb_feature_posn(const MovFeatInfo *feati, MovPosComb comb) {
  /* Returns position of individual feature. */
  return (comb / feati->weight) % feati->posns;
}

MovPosComb movposcomb_feature_update(const MovFeatInfo *mfi,
				     MovPosComb startpoint, int featpos) {
  MovPosComb above_weight= mfi->weight * mfi->posns;
  MovPosComb above= startpoint / above_weight;
  MovPosComb below= startpoint % mfi->weight;
  return above*above_weight + featpos*mfi->weight + below;
}

static void ignore_all_abandon(Method *m) { }

/*========== points and other fixed timeslot movfeats ==========*/

/*
 * We maintain two queues, one for reserved one for actually confirmed
 *  requests where we know what we're doing.
 *
 * We divide time into discrete slots, numbered with clock arithmetic.
 *
 *     cslot         cslot+1      cslot+2
 *
 *     currently     next in      after
 *     changing      line         that
 *
 * We increment cslot when we consider the movfeat to move;
 * for points and relays this is when we issue the command to the PIC.
 * In a request, the deadline represents the latest allowable value
 * of cslot just before that increment.
 */

typedef unsigned FsqSlot;
typedef int FsqSlotSigned;

#define FSQDN (~(FsqSlot)0)

typedef struct {   /* state      Prep Resv  Inst Resv  Prep Conf  Inst Conf   */
  Change h;        /* in queue?  absent     reserved   absent     confirmed   */
  FsqSlot deadline; /*           relative   relative   absolute   absolute    */
  int n_motions;    /*            1          1         num undone num undone  */
  Motion motions[]; /*  [0].i:    0          0         non-0      non-0       */
                    /*  .posn:    undef      undef     defined    defined     */
} FsqReq;

#define FSQ_MAX_QUEUE  15

typedef struct {
  int n;
  FsqReq *l[FSQ_MAX_QUEUE];
} FsqQueue;

typedef struct FsqMethod FsqMethod;

typedef struct {
  /* set by fsq's client before fsq_init */
  int slot_ms, lag_ms;
  void (*move)(FsqMethod *m, const MovFeatInfo *mfi, int movfeatposn);
  /* shared */
  int ready; /* >0 means ready; set to 0 by fsq just before move */
  /* fsq's client must arrange that these start out at all-bits-zero */
  FsqSlot cslot;
  FsqQueue confirmed, reserved;
} FsqState;

struct FsqMethod {
  Method m;
  FsqState f;
  /* client may put things here */
};

static void fsq_check_action(FsqMethod *m);
static ErrorCode fsq_check_plan(FsqMethod *m);

static FsqSlotSigned fsq_maxdelay_reldeadline(FsqMethod *m,
					      int maxdelay_ms, int n_motions) {
  if (maxdelay_ms==-1) return FSQ_MAX_QUEUE*16;
  return (maxdelay_ms - m->f.lag_ms) / m->f.slot_ms - n_motions;
}

static void fsq_queue_remove_index(FsqQueue *q, int index) {
  q->n--;
  memmove(&q->l[index], &q->l[index+1], sizeof(q->l[0]) * (q->n - index));
}

static int fsq_req_compar_approx(const void *av, const void *bv) {
  FsqReq *const *a= av;
  FsqReq *const *b= av;
  FsqSlotSigned dldiff= (*b)->deadline - (*a)->deadline;
  return dldiff;
}

static void fsq_queue_remove_item(FsqQueue *q, FsqReq *r) {
  FsqReq **entry;
  int i;
  entry= bsearch(r, q->l, q->n, sizeof(q->l[0]), fsq_req_compar_approx);
  assert(entry);

#define SEARCH_CHECK do{				\
    if (q->l[i] == r) goto found;			\
    if (q->l[i]->deadline != r->deadline) break;	\
}while(0)
  for(i= entry - q->l;     i >= 0;   i--)  SEARCH_CHECK;
  for(i= entry - q->l + 1; i < q->n; i++)  SEARCH_CHECK;
  abort();

 found:
  fsq_queue_remove_index(q, i);
}

static FsqQueue *fsq_item_queue(FsqMethod *m, FsqReq *r)
  { return r->motions[0].i ? &m->f.confirmed : &m->f.reserved; }

#define WHICH(wh)				\
  (whichr= wh##r,				\
   whichwhen= wh##when,				\
   wh++)

static ErrorCode fsq_check_plan(FsqMethod *m) {
  /* Checks whether we can meet the currently queued commitments */
  /* if this fails, indep machinery calls fsq_prepare to dequeue */
  int future, conf, resv, whichwhen, DP;
  FsqReq *whichr;

  conf=resv=0;
  future=0;

  DPRINTF1(movpos,fsq, "%s   plan", m->m.pname);

  /* If CDU isn't charged we can't do one right away */
  if (m->f.ready<0) {
    DPRINTF2(" +");
    future++;
  }

  for (;;) {
    FsqReq *confr= conf < m->f.confirmed.n ? m->f.confirmed.l[conf] : 0;
    FsqReq *resvr= resv < m->f.reserved .n ? m->f.reserved .l[resv] : 0;
    if (!confr && !resvr) break;
    DPRINTF2(" %d:",future);
    int confwhen= confr ? confr->deadline - m->f.cslot : INT_MAX;
    int resvwhen= resvr ? resvr->deadline              : INT_MAX;
    if (future && resvwhen < confwhen) {
      WHICH(resv);  DPRINTF2("~");
    } else if (confr) {
      WHICH(conf);
    } else {
      future++;     DPRINTF2("-");
      continue;
    }
    DPRINTF2("%s/%s[%d@t+%d]", whichr->h.indep->move->i->pname,
	    movpos_pname(whichr->h.indep->move, whichr->h.indep->target),
	    whichr->n_motions, whichwhen);
    if (future > whichwhen) {
      DPRINTF2("!...bad\n");
      return EC_MovFeatTooLate;
    }
    future += whichr->n_motions;
  }
  DPRINTF2(" ok\n");
  return 0;
}

#undef WHICH

static ErrorCode fsq_queue_insert_item(FsqMethod *m, FsqQueue *q, FsqReq *r) {
  int insat;

  if (q->n == FSQ_MAX_QUEUE)
    return EC_BufferFull;

  for (insat= q->n;
       insat>0 && (FsqSlotSigned)(r->deadline - q->l[insat-1]->deadline) < 0;
       insat--)
    q->l[insat]= q->l[insat-1];
  q->l[insat]= r;
  q->n++;

  return 0;
}

static void fsq_item_debug(FsqMethod *m, FsqReq *r, const char *opwhat) {
  int DP;
  Segment *move= r->h.indep->move;
  DPRINTF1(movpos,fsq, "%s %s %s", m->m.pname, opwhat, move->i->pname);
  if (r->motions[0].i) {
    int i;
    Motion *mo;
    for (i=0, mo=r->motions; i<r->n_motions; i++, mo++)
      DPRINTF2("/%s%d", mo->i->pname, (int)mo->posn);
    DPRINTF2("\n");
  } else {
    DPRINTF2("(%d)\n", r->n_motions);
  }
}
 
static ErrorCode fsq_enqueue(FsqMethod *m, FsqReq *r) { /* P->I; err: P->P */
  if (!r) return 0;
  fsq_item_debug(m,r,"enqueue");
  return fsq_queue_insert_item(m, fsq_item_queue(m,r), r);
}

static void fsq_dequeue(FsqMethod *m, FsqReq *r) { /* I->P */
  if (!r) return;
  fsq_item_debug(m,r,"dequeue");
  fsq_queue_remove_item(fsq_item_queue(m,r), r);
}

/*---------- method entrypoints ----------*/

static ErrorCode fsq_prepare(Method *mm, const Segment *move,
			     int n_motions, const Motion *motions,
			     int ms, int confirmation,
			     Change **chg_r, int *cost_r) {
  FsqMethod *m= (void*)mm;

  assert(n_motions > 0);

  FsqSlotSigned reldeadline= fsq_maxdelay_reldeadline(m, ms, n_motions);
  if (reldeadline <= 0) return EC_MovFeatTooLate;

  if (chg_r) {
    int alloc_motions= confirmation ? n_motions : 1;
      /* we need at least one motion in the table so we can tell
       *  the difference between the states by looking at motions[0].i */
    FsqReq *r= mmalloc(sizeof(*r) + alloc_motions * sizeof(r->motions[0]));
    r->n_motions= n_motions;
    if (confirmation) {
      r->deadline= reldeadline + m->f.cslot;
      memcpy(r->motions, motions, sizeof(*r->motions)*n_motions);
    } else {
      r->deadline= reldeadline;
      r->motions[0].i= 0;
    }
    *chg_r= &r->h;
  }
  if (cost_r) {
    *cost_r= n_motions * m->f.slot_ms;
  }

  return 0;
}

static void fsq_dispose(Method *mm, Change *chg) {
  FsqReq *r= (FsqReq*)chg;
  free(r);
}

static void fsq_remove(Method *mm, Change *remvchg) {
  FsqMethod *m= (void*)mm;
  FsqReq *remv= (FsqReq*)remvchg;

  fsq_dequeue(m, remv);
}

static ErrorCode fsq_install(Method *mm, Change *instchg) {
  FsqMethod *m= (void*)mm;
  FsqReq *inst= (FsqReq*)instchg;

  return fsq_enqueue(m, inst);
}

static ErrorCode fsq_check(Method *mm) { 
  FsqMethod *m= (void*)mm;
  ErrorCode ec= fsq_check_plan(m);
  return ec;
}

static void fsq_execute(Method *mm) {
  FsqMethod *m= (void*)mm;
  fsq_check_action(m);
}

/*---------- something to do ? ----------*/

static void fsq_check_action(FsqMethod *m) {
  /* client should call this after it sets ready */
  ErrorCode ec;

  if (!m->f.confirmed.n) {
    if (sta_state == Sta_Finalising) resolve_motioncheck();
    return;
  }

  FsqReq *r= m->f.confirmed.l[0];

  if (r->n_motions && m->f.ready>0) {
    /* look for something to move */
    Motion *mo= &r->motions[--r->n_motions];
    assert(mo->posn < mo->i->posns);
    m->f.ready= 0;
    m->f.move(m, mo->i, mo->posn);
    m->f.cslot++;

    method_update_feature(&m->m, r->h.indep, mo);
  }

  if (!r->n_motions) {
    fsq_queue_remove_index(&m->f.confirmed, 0);
    method_change_done(&m->m, &r->h);
    m->m.dispose(&m->m, &r->h);

    ec= fsq_check_plan(m);  assert(!ec);
    fsq_check_action(m);
  }
}

/*========== points ==========*/

/*
 * CDU and point queue states:
 *
 *
 *    ____________		           	conf'd
 *   /   points_  \		    ready  	  .n
 *   |   all_      |
 *   |   abandon   |
 *   |             V
 *   |from       INACTIVE		-1	0
 *   |any       <=Sta_Settling
 *  ^^^^^^^^     (start)
 *                 |
 *     ___________ |turning
 *    /           \| _on
 *   |             V
 *   |           CHARGING		0	any
 *   |          >=Sta_Resolving
 *   |             |
 *   |             |on_pic
 *   |             |_charged
 *   |             V
 *   ^           READY			1	any
 *   |             |
 *   |             |fsq_check_action
 *   |             | calls point_move which fires a point
 *    \___________/
 *
 */

#define CDU_RECHARGE   350 /*ms*/
#define POINT_MOVEMENT  50 /*ms*/

static ErrorCode point_prepare(Method *mm, const Segment *move,
			       int n_motions, const Motion *motions,
			       int ms, int confirmation,
			       Change **chg_r, int *cost_r) {
  FsqMethod *m= (void*)mm;
  assert(m->f.ready>=0);
  return fsq_prepare(mm,move, n_motions,motions,
		     ms,confirmation, chg_r,cost_r);
 }

static void point_move(FsqMethod *m, const MovFeatInfo *mfi, int posn) {
  /* actually firing points, yay! */
  PicInsn piob;
  enco_pic_point(&piob, mfi->boob[posn]);
  serial_transmit(&piob);
}

static void points_all_abandon(Method *mm) {
  FsqMethod *m= (void*)mm;
  m->f.ready= -1;
}

static FsqMethod points_method;

void points_turning_on(void) {
  FsqMethod *m= &points_method;
  m->f.ready= 0;
}
void on_pic_charged(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  FsqMethod *m= &points_method;
  if (m->f.ready<0) return;
  m->f.ready= 1;
  fsq_check_action(m);
}

static FsqMethod points_method= {
  { "point",
    point_prepare, fsq_dispose,
    fsq_install, fsq_remove,
    fsq_check, fsq_execute, points_all_abandon },
  { .lag_ms= POINT_MOVEMENT, .slot_ms= CDU_RECHARGE, .move= point_move }
};

/*========== relays ==========*/

/*
 * Waggler states:
 *
 *    ____________		           	conf'd
 *   /  wagglers_ \		    ready  	  .n
 *   |   all_      |
 *   |   abandon   |
 *   |             V
 *   |from       UNKNOWN		-1	0
 *   |any       <=Sta_Settling
 *  ^^^^^^^^     (start)
 *                 |
 *     ___________ |turning
 *    /           \| _on
 *   |             V
 *   |           CHARGING		0	any
 *   |          >=Sta_Resolving
 *   |             |
 *   |             |on_pic
 *   |             |_charged
 *   |             V
 *   ^           READY			1	any
 *   |             |
 *   |             |fsq_check_action
 *   |             | calls waggle_do which switches a relay
 *    \___________/
 *
 */

static FsqMethod waggle;

static void waggle_do(FsqMethod *m, const MovFeatInfo *mfi, int posn) {
  /* actually setting relays */
  PicInsn piob;
  enco_pic_waggle(&piob, mfi->boob[0], posn);
  serial_transmit(&piob);
}

static SegmentNum waggle_settle_seg;
static int waggle_settle_feat;

static void waggle_settle_check(void) {
  for (;;) {
    if (waggle_settle_seg >= info_nsegments) return;

    Segment *seg= &segments[waggle_settle_seg];
    if (waggle_settle_feat >= seg->i->n_movfeats) {
      waggle_settle_seg++; waggle_settle_feat=0; continue;
    }

    const MovFeatInfo *feati= &seg->i->movfeats[waggle_settle_feat];
    if (feati->kind != mfk_relay) {
      waggle_settle_feat++; continue;
    }

    waggle.f.ready= 0;
    waggle_do(&waggle, feati, (seg->movposcomb / feati->weight) & 1);
    waggle_settle_feat++;
  }
}

void waggle_startup_manual(void) {
  waggle.f.ready= 1;
}

void waggle_settle(void) {
  waggle_settle_seg= 0;
  waggle_settle_feat= 0;
  waggle.f.ready= 1;
  waggle_settle_check();
}

void on_pic_waggled(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  if (sta_state == Sta_Settling) {
    waggle.f.ready= 1;
    waggle_settle_check();
  } else if (sta_state >= Sta_Resolving || sta_state == Sta_Manual) {
    waggle.f.ready= 1;
    fsq_check_action(&waggle);
    return;
  }
}

static FsqMethod waggle= {
  { "relay",
    fsq_prepare, fsq_dispose,
    fsq_install, fsq_remove,
    fsq_check, fsq_execute, ignore_all_abandon },
  { .lag_ms= 5, .slot_ms= 50, .move= waggle_do }
};

/*========== dummy `nomove' kind ==========*/

typedef struct NomoveChange {
  Change h;
  DLIST_NODE(struct NomoveChange) inqueue;
  int n_motions; /* 0 for reservations */
  Motion motions[];
} NomoveChange;

typedef struct {
  Method m;
  NomoveChange *queuehead; /* contains confirmations only */
} NomoveMethod;

static ErrorCode nomove_prepare(Method *meth_in, const Segment *move,
				int n_motions, const Motion *motions,
				int ms, int confirming,
				Change **chg_r, int *cost_r) {
  if (chg_r) {
    NomoveChange *chg;
    assert(n_motions>0);
    if (!confirming) n_motions= 0;
    chg= mmalloc(sizeof(*chg) + sizeof(Motion)*n_motions);
    chg->n_motions= n_motions;
    memcpy(chg->motions, motions, sizeof(Motion)*n_motions);
  }
  if (cost_r) {
    *cost_r= 0;
  }
  return 0;
}
static void nomove_dispose(Method *mm, Change *remvchg) {
  NomoveChange *remv= (void*)remvchg;
  free(remv);
}

static ErrorCode nomove_install(Method *mm, Change *instchg) {
  NomoveMethod *meth= (void*)mm;
  NomoveChange *inst= (void*)instchg;
  if (inst->n_motions)
    DLIST1_PREPEND(meth->queuehead, inst, inqueue);
  return 0;
}
static void nomove_remove(Method *mm, Change *remvchg) {
  NomoveMethod *meth= (void*)mm;
  NomoveChange *remv= (void*)remvchg;
  if (remv->n_motions)
    DLIST1_REMOVE(meth->queuehead, remv, inqueue);
}

static ErrorCode nomove_check(Method *mm) { return 0; }

static void nomove_execute(Method *mm) {
  NomoveMethod *meth= (void*)mm;
  NomoveChange *done;

  while ((done= meth->queuehead)) {
    assert(done->n_motions);
    int i;
    for (i=0; i<done->n_motions; i++)
      method_update_feature(&meth->m, done->h.indep, &done->motions[i]);
    method_change_done(&meth->m, &done->h);
    DLIST1_REMOVE(meth->queuehead, done, inqueue);
    nomove_dispose(&meth->m, &done->h);
  }
}

static Method nomove_method= {
  "nomove",
  nomove_prepare, nomove_dispose,
  nomove_install, nomove_remove,
  nomove_check, nomove_execute, ignore_all_abandon
};

/*========== method-independent machinery ==========*/

#define INDEP_DBG_FMT "<%p:%s/%s[%d]>"
#define INDEP_DBG_ARGS(in) (in),					\
    ((in)->move->i->pname), (movpos_pname((in)->move, (in)->target)),	\
    ((in)->n_changes)

#define INDEP_DPFX_FMT "movpos " INDEP_DBG_FMT " "
#define INDEP_DPFX_ARGS(in) INDEP_DBG_ARGS((in))

#define METH_DPFX_FMT "%s " INDEP_DBG_FMT " "
#define METH_DPFX_ARGS(indep, meth) ((meth).pname), INDEP_DBG_ARGS((indep))

static void indep_indep_done(Indep *indep);

static Method *methods[]= {
  [mfk_none] = (Method*)&nomove_method,
  [mfk_point] = (Method*)&points_method,
  [mfk_relay] = (Method*)&waggle,
  0
};

/*---------- entrypoints from methods ----------*/

static void method_update_feature(Method *m, MovPosChange *indep,
				  const Motion *mo) {
  int featix= mo->i - indep->move->i->movfeats;
  assert(featix >= 0 && featix < indep->move->i->n_movfeats);
  ouposn_feat(indep->move, mo->i, mo->posn, m);
  indep->actualpos=
    movposcomb_feature_update(mo->i, indep->actualpos, mo->posn);
  indep->actualunk &= ~unkfeatbit(featix);
  ouposn_moving(indep);
}

static void method_change_done(Method *m, Change *chg) {
  Indep *indep= chg->indep;
  Change **search;

  DPRINTF(movpos,meth, METH_DPFX_FMT "method_change_done...\n",
	  METH_DPFX_ARGS(indep,*m));

  for (search=indep->changes; *search; search++)
    if ((*search) == chg) goto found;
  assert(!"change in indep");

 found:
  indep->n_changes--;
  if (indep->n_changes) {
    *search= indep->changes[indep->n_changes];
    return;
  }

  if (!indep->refcount)
    indep_indep_done(indep);
}

static void indep_indep_done(Indep *indep) {
  /* all done */
  Segment *move= indep->move;
  move->moving= 0;
  move->motion= 0;
  move->movposcomb= indep->target;
  ouposn_stable(move);
  free(indep);
}

/*---------- internal core machinery ----------*/

static Method *feature_method(const MovFeatInfo *feati) {
  assert(feati->kind >= 0);
  assert(feati->kind < ARRAY_SIZE(methods));
  Method *meth= methods[feati->kind];
  assert(meth);
  return meth;
}

static int change_needed(int featix, const MovFeatInfo *feati,
			 MovPosComb startpointpos, UnkMap startpointunk,
			 MovPosComb target) {
  int r;

  r= (startpointunk & unkfeatbit(featix)) ||
    movposcomb_feature_posn(feati, target) -
    movposcomb_feature_posn(feati, startpointpos);

  if (DEBUGP(movpos,eval))
    DPRINTFA(" { %s:%s(%d*%d) %d..%d => %d }",
	     feature_method(feati)->pname, feati->pname,
	     feati->posns, feati->weight,
	     startpointpos, target, r);
  return r;
}

static void indep_dispose(MovPosChange *indep) {
  if (!indep) return;

  DPRINTF(movpos,intern, INDEP_DPFX_FMT "dispose...\n",
	  INDEP_DPFX_ARGS(indep));

  int changei;
  for (changei=0; changei<indep->n_changes; changei++) {
      Change *chg= indep->changes[changei];
      if (!chg) continue;

      Method *meth= chg->meth;
      DPRINTF(movpos,meth, METH_DPFX_FMT "dispose...\n",
	      METH_DPFX_ARGS(indep,*meth));
      meth->dispose(meth, chg);
  }
  free(indep);
}

#define EVAL_MAX_METHODS 2
#define EVAL_MAX_MOTIONS 2

static ErrorCode indep_prepare(Segment *move, MovPosComb target,
			       MovPosComb startpointpos,
			       int ms, int confirming,
			       MovPosChange **indep_r /* 0 ok */,
			       int *cost_r /* 0 ok */) {
  static int n_meths;
  static Method *meths[EVAL_MAX_METHODS];
  static int n_motions[EVAL_MAX_METHODS];
  static Motion motions[EVAL_MAX_METHODS][EVAL_MAX_MOTIONS];

  const SegmentInfo *movei= move->i;
  int feat, DP;
  UnkMap startpointunk;

  MovPosChange *indep=0;

  DPRINTF1(movpos,eval, "movpos prepare %s/%s <-%s", move->i->pname,
	   movpos_pname(move,target), movpos_pname(move,startpointpos));

  if (SOMEP(startpointpos)) {
    startpointunk= 0;
  } else {
    if (move->moving) {
      startpointpos= move->motion->actualpos;
      startpointunk= move->motion->actualunk;
    } else if (SOMEP(move->movposcomb)) {
      startpointpos= move->movposcomb;
      startpointunk= 0;
    } else {
      startpointpos= 0;
      startpointunk= unkallfeatbits(move->i->n_movfeats);
    }
    if (DP) {
      DPRINTF2(" actual <-%s/", move->i->pname);
      for (feat=0; feat<movei->n_movfeats; feat++) {
	const MovFeatInfo *feati= &movei->movfeats[feat];
	if (startpointunk & unkfeatbit(feat))
	  DPRINTF2("%s?", feati->pname);
	else
	  DPRINTF2("%s%d", feati->pname,
		   movposcomb_feature_posn(feati, startpointpos));
      }
    }
  }

  n_meths= 0;

  for (feat=0; feat<movei->n_movfeats; feat++) {
    const MovFeatInfo *feati= &movei->movfeats[feat];
    if (!change_needed(feat,feati,
		       startpointpos,startpointunk,
		       target))
      continue;
    MovPosComb posn= movposcomb_feature_posn(feati, target);
    Method *meth= feature_method(feati);

    int methi;
    for (methi=0; methi<n_meths; methi++)
      if (meths[methi] == meth) goto found_method;
    /* need a new method */
    methi= n_meths++;
    if (methi >= EVAL_MAX_METHODS) {
      DPRINTF2(" MovFeatTooManyMethods methi=%d\n",methi);
      return EC_MovFeatTooManyMethods;
    }
    meths[methi]= meth;
    n_motions[methi]= 0;
    DPRINTF2(" meths[%d]=%s", methi,meth->pname);

  found_method:;
    int motioni= n_motions[methi]++;
    if (motioni >= EVAL_MAX_MOTIONS) {
      DPRINTF2(" MovFeatTooManyMotions motioni=%d\n",motioni);
      return EC_MovFeatTooManyMotions;
    }
    DPRINTF2(" motion[%d][%d]=%s%d", methi, motioni, feati->pname,posn);
    motions[methi][motioni].i= feati;
    motions[methi][motioni].posn= posn;
  }

  if (indep_r) {
    DPRINTF2(" alloc");
    indep= mmalloc(sizeof(*indep) + sizeof(Change*) * n_meths);
    indep->move= move;
    indep->actualpos= startpointpos;
    indep->actualunk= startpointunk;
    indep->target= target;
    indep->n_changes= n_meths;
    indep->refcount= 0;
    memset(indep->changes, 0, sizeof(Change*) * n_meths);
  }
  DPRINTF2("\n");

  int totalcost= 0;
  int changei;
  ErrorCode ec;

  for (changei=0; changei<n_meths; changei++) {
    Method *meth= meths[changei];
    int thiscost= 0;
    meth->needcheck= 1;
    meth->needexec= 1;

    if (indep_r)
      DPRINTF(movpos,meth, METH_DPFX_FMT "prepare n_motions=%d...\n",
	      METH_DPFX_ARGS(indep,*meth), n_motions[changei]);
    else
      DPRINTF(movpos,meth, "%s prepare (costing) n_motions=%d...\n",
	      meth->pname, n_motions[changei]);

    ec= meth->prepare(meth,move,
		      n_motions[changei],motions[changei],
		      ms, confirming,
		      indep ? &indep->changes[changei] : 0,
		      &thiscost);
    if (ec) goto x;
    if (indep) {
      Change *chg= indep->changes[changei];
      chg->meth= meth;
      chg->indep= indep;
      chg->installed= 0;
    }
    totalcost += thiscost;
  }

  if (indep_r) *indep_r= indep;
  if (cost_r) *cost_r= totalcost;

  if (indep_r)
    DPRINTF(movpos,eval, INDEP_DPFX_FMT "prepare cost=%d ok\n",
	    INDEP_DPFX_ARGS(indep), totalcost);
  else
    DPRINTF(movpos,eval, "movpos prepare %s/%s cost=%d ok\n",
	    move->i->pname, movpos_pname(move,target), totalcost);
  return 0;

 x:
  indep_dispose(indep);
  DPRINTF(movpos,entry, "movpos prepare %s/%s err=%s\n", move->i->pname,
	  movpos_pname(move,target), ec2str(ec));
  return ec;
}

static void indep_remove(MovPosChange *remv) {
  if (!remv) return;
  
  DPRINTF(movpos,intern, INDEP_DPFX_FMT "remove...\n",
	  INDEP_DPFX_ARGS(remv));

  int i;
  for (i=0; i<remv->n_changes; i++) {
    Change *chg= remv->changes[i];
    if (!chg->installed) continue;
    Method *meth= chg->meth;
    meth->needexec= 1;

    DPRINTF(movpos,meth, METH_DPFX_FMT "remove...\n",
	    METH_DPFX_ARGS(remv,*meth));
    meth->remove(meth, chg);
    chg->installed= 0;
  }
}

static ErrorCode
indep_install(MovPosChange *inst, int checknow) {
  /* if this fails, inst may be left partially installed */
  if (!inst) return 0;
  
  DPRINTF(movpos,intern, INDEP_DPFX_FMT "install checknow=%d...\n",
	  INDEP_DPFX_ARGS(inst), checknow);

  int i;
  ErrorCode ec;
  for (i=0; i<inst->n_changes; i++) {
    Change *chg= inst->changes[i];
    assert(!chg->installed);
    Method *meth= chg->meth;

    meth->needexec= 1;
    ec= meth->install(meth, chg);
    DPRINTF(movpos,meth, METH_DPFX_FMT "install=%s\n",
	    METH_DPFX_ARGS(inst,*meth), ec2str(ec));
    if (ec) goto x;
    chg->installed= 1;
    meth->needcheck= 1;

    if (checknow) {
      ec= meth->check(meth);
      DPRINTF(movpos,meth, METH_DPFX_FMT "check=%s\n",
	      METH_DPFX_ARGS(inst,*meth), ec2str(ec));
      if (ec) goto x;
      meth->needcheck= 0;
    }
  }
  return 0;

 x:
  return ec;
}

static void indep_check_execute(void) {
  DPRINTF(movpos,intern, "movpos indep_check_execute\n");

  Method **methwalk, *meth;
  for (methwalk= methods; (meth= *methwalk); methwalk++) {
    if (meth->needcheck) {
      ErrorCode ec= meth->check(meth);
      DPRINTF(movpos,meth, "%s check=%s\n", meth->pname, ec2str(ec));
      assert(!ec);
      meth->needcheck= 0;
    }
    if (meth->needexec) {
      meth->needexec= 0;
      DPRINTF(movpos,meth, "%s execute...\n", meth->pname);
      meth->execute(meth);
    }
  }
}

/*---------- entrypoints from rest of program ----------*/

ErrorCode
movpos_reserve(Segment *move, int maxdelay_ms, MovPosChange **res_r,
	       MovPosComb target, MovPosComb startpoint /*as for findcomb*/) {
  ErrorCode ec;
  MovPosChange *indep= 0;

  DPRINTF(movpos,entry, "movpos reserve %s/%s maxdelay=%dms startpoint=%s\n",
	  move->i->pname, movpos_pname(move,target),
	  maxdelay_ms, movpos_pname(move,startpoint));

  ec= indep_prepare(move,target, startpoint,
		    maxdelay_ms,0,
		    &indep, 0);
  if (ec) return ec;

  ec= indep_install(indep, 1);
  if (ec) goto x;

  indep_check_execute();

  DPRINTF(movpos,entry, "movpos reserve %s/%s ok\n",
	  move->i->pname, movpos_pname(move,target));
  *res_r= indep;
  return 0;

 x:
  indep_remove(indep);
  indep_dispose(indep);
  indep_check_execute();

  DPRINTF(movpos,entry, "movpos reserve %s/%s err=%s\n",
	  move->i->pname, movpos_pname(move,target), ec2str(ec));
  return ec;
}

ErrorCode movpos_findcomb_bysegs(Segment *back, Segment *move, Segment *fwd,
				 MovPosComb startpoint, MovPosComb *chosen_r) {
  const SegmentInfo *movei= move->i;
  MovPosComb tcomb, bestcomb=-1;
  int tcost, bestcost=INT_MAX;
  const SegPosCombInfo *pci;

  DPRINTF(movpos,eval, "movpos_findcomb_bysegs %s-%s-%s <-%s\n",
	  back ? back->i->pname : "*", move->i->pname,
	  fwd  ? fwd ->i->pname : "*", movpos_pname(move, startpoint));

  for (tcomb=0, pci=movei->poscombs;
       tcomb<movei->n_poscombs;
       tcomb++, pci++) {
    /* these next assignments may generate segments[-1] but we don't
     * care because that won't compare equal to back or fwd */
    Segment *tback= &segments[pci->link[1].next];
    Segment *tfwd=  &segments[pci->link[0].next];

    DPRINTF(movpos,intern, "movpos_findcomb_bysegs  ... %s : %s-%s-%s\n",
	    movpos_pname(move, tcomb),
	    SOMEP(pci->link[1].next) ? tback->i->pname : "#",
	    move->i->pname,
	    SOMEP(pci->link[0].next) ? tfwd->i->pname : "#");	    

    if (back && !(back==tback || back==tfwd)) continue;
    if (fwd  && !(fwd ==tback || fwd ==tfwd)) continue;

    /* we have to search for the one which is least effort */
    ErrorCode ec= indep_prepare(move,tcomb,startpoint, -1,0, 0,&tcost);
    if (ec) return ec;

    if (tcost >= bestcost) /* prefer low-numbered movposcombs */
      continue;

    bestcomb= tcomb;
    bestcost= tcost;
  }
  DPRINTF(movpos,entry, "movpos_findcomb_bysegs %s..%s..%s <-%s => %s/%s\n",
	  back ? back->i->pname : "-", move->i->pname,
	  fwd  ? fwd ->i->pname : "-", movpos_pname(move, startpoint),
	  move->i->pname, movpos_pname(move,bestcomb));

  if (chosen_r) *chosen_r= bestcomb;
  return
    bestcost==INT_MAX ? EC_MovFeatRouteNotFound :
    0;
}

ErrorCode movpos_change(Segment *move, MovPosComb target,
			int maxdelay_ms, MovPosChange *resv) {
  int DP;
  ErrorCode ec;

  DPRINTF1(movpos,entry, "movpos change %s/%s maxdelay=%dms",
	   move->i->pname, movpos_pname(move, target), maxdelay_ms);
  if (resv) DPRINTF2(" resv=%s/%s",
		     resv->move->i->pname,
		     movpos_pname(resv->move, resv->target));
  if (move->motion) DPRINTF2(" oldmotion=/%s",
			     movpos_pname(move, move->motion->target));
  DPRINTF2("\n");

  MovPosChange *inst= 0;

  ec= indep_prepare(move,target, NOTA(MovPosComb),
		    maxdelay_ms,1,
		    &inst, 0);
  if (ec) goto x;

  indep_remove(resv);
  indep_remove(move->motion);;

  ec= indep_install(inst, 1);
  if (ec) goto x;

  indep_dispose(resv);
  indep_dispose(move->motion);

  move->motion= inst;
  move->moving= 1;

  inst->refcount++; /* prevents method_change_done from destroying it */

  ouposn_moving(inst);
  indep_check_execute();

  inst->refcount--;
  if (!inst->n_changes)
    /* oh! */
    indep_indep_done(inst);

  DPRINTF(movpos,entry, "movpos change %s/%s ok\n",
	  move->i->pname, movpos_pname(move, target));
  return 0;

 x:
  indep_remove(inst);
  indep_dispose(inst);
  ec= indep_install(move->motion, 0);  assert(!ec);
  ec= indep_install(resv, 0);          assert(!ec);
  indep_check_execute();

  DPRINTF(movpos,entry, "movpos change %s/%s err=%s\n",
	  move->i->pname, movpos_pname(move, target), ec2str(ec));
  return ec;
}

void movpos_unreserve(MovPosChange *resv) {
  if (!resv) return;
  DPRINTF(movpos,entry, "movpos unreserve %s/%s...\n",
	  resv->move->i->pname, movpos_pname(resv->move, resv->target));
  indep_remove(resv);
  indep_dispose(resv);
  indep_check_execute();
}

MovPosComb movpos_change_intent(MovPosChange *indep) {
  return indep->target;
}

void motions_all_abandon(void) {
  Method **meth;
  SEG_IV;
  DPRINTF(movpos,entry, "movpos motions_all_abandon...\n");
  FOR_SEG {
    if (!seg->moving) continue;

    MovPosChange *abandon= seg->motion;
    indep_remove(abandon);
    seg->movposcomb= abandon->actualunk ? NOTA(MovPosComb) : abandon->actualpos;
    seg->moving= 0;
    seg->motion= 0;
    indep_dispose(abandon);
  }
  for (meth=methods; *meth; meth++)
    (*meth)->all_abandon(*meth);
}

void movpos_reportall(void) {
  SEG_IV;
  int feat;
  
  FOR_SEG {
    assert(!seg->moving);
    if (seg->i->n_poscombs <= 1) continue;
    ouposn_stable(seg);
    if (SOMEP(seg->movposcomb)) {
      for (feat=0; feat<seg->i->n_movfeats; feat++) {
	const MovFeatInfo *feati= &seg->i->movfeats[feat];
	MovPosComb posn= movposcomb_feature_posn(feati, seg->movposcomb);
	ouposn_feat(seg, feati, posn, feature_method(feati));
      }
    }
  }
}
