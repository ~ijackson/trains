/*
 */

#ifndef RECORD_I_H
#define RECORD_I_H

#include "record.h"
#include "record-y.h"

void record_train_is(Train *tra, int addr, int head, int det, int tail);
void record_train_home(Train *tra, int backw, Segment *seg);
void record_train_step_speed(Train *tra, int step, double speed);
void record_train_stopregime_count(void);
void record_train_stopregime(Train *tra, int step, int xs, int ts);
void record_seg_has(Segment *seg, int backw, Train *tra, int inverted);
void record_seg_at(Segment *seg, const char *movposcomb_pname);
void record_adjunct_nmrafunc(AdjunctsAdjunct*, AdjunctsAddr*, int num);
void record_adjunct_motor(AdjunctsAdjunct*, AdjunctsAddr*, int speed);

Train *record_pname2train(const char *pname);
Segment *record_pname2seg(const char *pname);
AdjunctsAdjunct *record_pname2adjunct(const char *pname, const char *letter);
AdjunctsAddr *record_adjaddr(int addr);
char *record_tempzone_strdup(const char *s);
void record_yyerror(const char *m);
void record_tempzone_clear(void);

int record_yyparse(void);
int record_yylex(void);
int record_yylex_destroy(void);
extern int record_yylineno;

#endif /*RECORD_I_H*/
