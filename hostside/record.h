/*
 */

#ifndef RECORD_H
#define RECORD_H

#include "realtime.h"

/* record.[ch] can be used in programs using persist.c, for the full
 * works.  Alternatively for use in some other program, just provide
 * a persist_allocate which calls malloc:
 */
void *record_allocate(int length);

#define DO_PERSIST_HEADER_ITEMS(cnst,num,ptr)	\
  cnst("#! /dev/enoent/trains-image\n")		\
  cnst(mapbase)					\
  num(datalen)					\
  ptr(trains)					\
  num(n_trains)					\
  ptr(segments)

#endif /*RECORD_H*/
