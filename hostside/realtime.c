/*
 * main program for realtime control
 */

#include <sys/resource.h>

#include "realtime.h"

const char *progname= "realtime";

/*---------- global variables ----------*/

CommandInput cmdi;
int picio_send_noise= 1;
int disable_watchdog;

static const char *device= "/dev/railway";
static const char *logcopy_fn;

/*---------- general event handling ----------*/

static void comms_error(const char *ch, const char *e1,
			const char *e2 /* may be 0 */) {
  if (!e1 && !e2) e1= "end of file";
  die("communications error: %s: %s%s%s", ch, e1, e2?": ":"", e2?e2:"");
}

static void *read_exception(oop_source *evts, int fd,
			    oop_event evt, void *cl_v) {
  const char *ch;
  char bufc;
  int r;

  ch= (fd==cmdi.out.fd ? cmdi.out.desc :
       fd==serial_fd ? "serial port" :
       0);
  
  r= read(fd, &bufc, 1);
  if (r==-1) comms_error(ch, "read error", strerror(errno));
  else if (r==0) comms_error(ch, "reports exception, reads EOF", 0);
  else comms_error(ch, "reports exception, but readable", 0);

  return OOP_CONTINUE;
}

/*---------- logging and output ----------*/

void safety_vpanic(Train *tra, Segment *seg,const char *fmt,va_list al) {
  char *msg;
  PicInsn piob;

  enco_pic_off(&piob);
  serial_transmit(&piob);

  if (vasprintf(&msg,fmt,al) < 0)
    diee("vasprintf failed in safety_vpanic fmt=\"%s\"", fmt);

  die("fatal signalling problem: %s at %s: %s",
      tra ? tra->pname : "?",
      seg ? seg->i->pname : "?",
      msg);
}

void safety_panic(Train *tra, Segment *seg, const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  safety_vpanic(tra, seg, fmt, al);
}

void ouvprintf(const char *fmt, va_list al) {
  ovprintf_ccb(&cmdi.out, simlog_ccb,0, fmt,al);
}
void ouprintf(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  ouvprintf(fmt,al);
  va_end(al);
}

void ouvprintf_only(const char *fmt, va_list al) {
  ovprintf(&cmdi.out, fmt,al);
}
void ouprintf_only(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  ouvprintf_only(fmt,al);
  va_end(al);
}

/*---------- printing nmra data ----------*/

typedef struct {
  const Byte *i, *o;
  int npending;
  unsigned datapending;
} NmraDecCtx;

static void nmradec_init(NmraDecCtx *d, const PicInsn *pi) {
  d->i= pi->d;
  d->o= pi->d + pi->l;
  d->npending= 0;
  d->datapending= 0;
}

static void nmradec_getbits(NmraDecCtx *d, int *nbits_io, unsigned *bits_r) {
  int use= *nbits_io;
  while (d->npending < use) {
    if (d->i >= d->o) break;
    d->datapending <<= 7;
    d->datapending |= *d->i & 0x7f;
    d->npending += 7;
    if (!(*d->i++ & 0x80)) break;
  }
  if (d->npending < use) use= d->npending;
  d->npending -= use;
  *bits_r= (d->datapending >> d->npending) & ~(~0u << use);
  *nbits_io= use;
}
  
static void nmra_decodeforpic(const PicInsn *pi,
			      void (*on_idle)(void *u, int),
			      void (*on_packet)(void *u, const Nmra*),
			      void (*on_error)(void *u, int reasonchar),
			      void *u) {
  /* reasonchars:
   *    #  - bad checksum
   *    !  - too little idle
   *    $  - truncated
   */
  NmraDecCtx d;
  Nmra n;
  int need_idle=7, got_idle=0;
  unsigned got, csum;
  int nbits;
  
  nmradec_init(&d,pi);
  for (;;) {
    nbits=1; nmradec_getbits(&d,&nbits,&got);
    if (got) { got_idle++; continue; }
    if (got_idle) on_idle(u,got_idle);
    if (!nbits) return;
    if (got_idle < need_idle) on_error(u,'!');
    got_idle= 0;

    n.l=0; csum=0;
    do {
      nbits=8; nmradec_getbits(&d,&nbits,&got);
      n.d[n.l++]= got;
      csum ^= got;
      if (nbits<8) on_error(u,'$');

      nbits=1; nmradec_getbits(&d,&nbits,&got);
      if (nbits<1) on_error(u,'$');
    } while (nbits && !got);

    if (csum) on_error(u,'#');
    else n.l--;

    on_packet(u,&n);
    need_idle= 14;
  }
}

static void opn_idle(void *u, int idle) {
  static const char dots[]= "......."; /* at least base/2 */
  const int base= 14;
  int largers= idle / base;
  int units= idle % base;

  ouprintf(" %.*s%.*s%.*s",
	  units/2, dots,
	  largers, ":::::::" /* enough */,
	  (units+1)/2, dots);
}
static void opn_error(void *u, int rc) { ouprintf(" %c",rc); }
static void opn_packet(void *u, const Nmra *n) {
  int i;
  const char *delim= " <";
  for (i=0; i<n->l; i++) {
    ouprintf("%s%02x",delim,n->d[i]);
    delim=" ";
  }
  ouprintf(">");
}

static void oprint_nmradata(const PicInsn *pi) {
  ouprintf("picio out nmradata");
  nmra_decodeforpic(pi, opn_idle,opn_packet,opn_error, 0);
  ouprintf("\n");
}
  
/*---------- command channel handling (oop_read, obc) ----------*/

int vbadcmd(ParseState *ps, const char *fmt, va_list al) {
  ouprintf("ack %s BadCmd : ", current_cmd?current_cmd->name:"?");
  ouvprintf(fmt,al);
  ouprintf("\n");
  return EC_BadCmd;
}

void oupicio(const char *dirn, const PicInsnInfo *pii, int obj, int v,
	     void (*qprintf)(const char *fmt, ...)) {
  if (!pii->argsbits)
    qprintf("picio %s %s\n", dirn, pii->name);
  else if (!pii->vbits)
    qprintf("picio %s %s %#x\n", dirn, pii->name, obj);
  else
    qprintf("picio %s %s %#x %d\n", dirn, pii->name, obj, v);
}

static void obc_error(OutBufferChain *ch, const char *e1, const char *e2) {
  if (!e1) { assert(!e2); die_hook(); exit(0); }
  fprintf(stderr,"command communication error: %s%s%s\n",
	  e1, e2?": ":"", e2?e2:"");
  die_hook();
  exit(-1);
}

static void qouhex(const char *word, const Byte *command, int length,
		   void (*qprintf)(const char *fmt, ...)) {
  qprintf("%s", word);
  while (length--) qprintf(" %02x", *command++);
  qprintf("\n");
}

void ouhex(const char *word, const Byte *command, int length) {
  qouhex(word,command,length, ouprintf);
}
void ouhex_nosim(const char *word, const Byte *command, int length) {
  qouhex(word,command,length, ouprintf_only);
}

void die_vprintf_hook(const char *fmt, va_list al) {
  static int recursing;
  if (cmdi.out.desc && !recursing++)
    ouvprintf(fmt, al);
  recursing--;
}
 
void die_hook(void) {
  PicInsn off;
  int e;

  enco_pic_off(&off);
  
  write(serial_fd,off.d,off.l);
  e= events ? obc_tryflush(&cmdi.out) : 0;
  if (e) fprintf(stderr,"(unwritten command output: %s)\n",strerror(e));
}

/*---------- serial input (via oop) ----------*/

PicInsn serial_buf;

static void *serial_readable(oop_source *evts, int fd,
			     oop_event evt, void *u0) {
  int r, buf_used;

  r= read(serial_fd, serial_buf.d + serial_buf.l,
	  sizeof(serial_buf.d) - serial_buf.l);

  if (r==0) die("serial port - eof");
  if (r==-1) {
    if (errno == EWOULDBLOCK || errno == EINTR)
      return OOP_CONTINUE;
    diee("serial port - read error");
  }
  assert(r>0);

  buf_used= serial_buf.l + r;
  serial_indata_process(buf_used);
  return OOP_CONTINUE;
}

void serial_indata_process(int buf_used) {
  for (;;) {
    serial_buf.l= buf_used;
    serial_moredata(&serial_buf);
    if (!serial_buf.l) break;
    buf_used -= serial_buf.l;
    memmove(serial_buf.d, serial_buf.d + serial_buf.l, buf_used);
    if (!buf_used) break;
  }
  serial_buf.l= buf_used;
}

/*---------- serial port output (not via liboop) ----------*/

void serial_transmit(const PicInsn *pi) {
  const PicInsnInfo *pii;
  int obj, v, suppress=0;

  if ((pi->d[0] & 0xf8) == 0x90) {
    SEG_IV;
    const char *delim;
    
    ouprintf("picio out polarity <");
    delim="";
    FOR_SEG {
      if (!segi->invertible) continue;
      if (!picinsn_polarity_testbit(pi,segi)) continue;
      ouprintf("%s%s", delim, segi->pname);
      delim= ",";
    }
    ouprintf(">\n");
  } else if (pi->d[0] == 0xff) {
    if (picio_send_noise < 3)
      suppress= 1;
    else 
      oprint_nmradata(pi);
  } else {
    picinsn_decode(pi, pic_command_infos, &pii, &obj, &v);
    if (!pii)
      ouprintf("picio out unknown\n");
    else if (pii->noiselevel > picio_send_noise)
      suppress= 1;
    else
      oupicio("out",pii,obj,v,ouprintf);
  }

  if (!suppress && picio_send_noise >= 2)
    ouhex("picioh out", pi->d, pi->l);

  /* note that the serial port is still in nonblocking mode.  if
   * we ever buffer up far enough that the kernel wants to make us
   * block, we should die! */
  serial_transmit_now(pi->d, pi->l);
}

/*---------- reporting page faults etc. ----------*/

#define CHECK_RUSAGE_FIELDS(F)			\
  F(ru_majflt)					\
  F(ru_nswap)					\

/*  F(ru_minflt)					\
 *  F(ru_nivcsw)
 */

#define CRF_DECL(f) static long check_rusage_last_##f;
CHECK_RUSAGE_FIELDS(CRF_DECL)

static void getru(struct rusage *ru) {
  int r= getrusage(RUSAGE_SELF, ru);  if (r) diee("getrusage");
}

void check_rusage_baseline(void) {
  if (!rtfeats_use & RTFEAT_RUSAGE) return;
  struct rusage ru;
  ouprintf("info rusage : monitoring\n");
  getru(&ru);
  #define CRF_BASE(f) check_rusage_last_##f= ru.f;
  CHECK_RUSAGE_FIELDS(CRF_BASE)
}

static void check_rusage_field(const char *f, long *last, long this, int alw) {
  long diff= this - *last;
  if (alw || diff) {
    ouprintf(" %s+=%ld", f, diff);
    *last= diff;
  }
}

void check_rusage_check(int always_report) {
  if (!rtfeats_use & RTFEAT_RUSAGE) return;
  struct rusage ru;
  getru(&ru);
  #define CRF_CHANGED(f) || ru.f != check_rusage_last_##f
  if (always_report
      CHECK_RUSAGE_FIELDS(CRF_CHANGED)) {
    ouprintf("info rusage :");
    #define CRF_SHOW(f) \
    check_rusage_field(STR(f), &check_rusage_last_##f, ru.f, always_report);
    CHECK_RUSAGE_FIELDS(CRF_SHOW);
    ouprintf(".\n");
  }
}

#define SERINTRPOKE "/var/run/serial-irq-priority-poke"

static void serial_interrupt_priority(void) {
  int fd= open(SERINTRPOKE, O_WRONLY|O_NONBLOCK);
  if (fd<0) {
    if (errno==ENOENT || errno==EAGAIN || errno==EACCES || errno==EWOULDBLOCK)
      return;
    diee("failed to open " SERINTRPOKE);
  }
  static char dummy;
  int r= write(fd,&dummy,1);
  if (r!=1) {
    assert(r<0);
    if (!(errno==EWOULDBLOCK))
      diee("failed to write a byte to " SERINTRPOKE);
  }
  close(fd);
}

/*---------- debugging ----------*/

unsigned long eventcounter;

void debug_count_event(const char *what) {
  DPRINTF(misc,event, "#0x%lx %s\n",eventcounter,what);
  eventcounter++;
}

#define DEFDFLAGS_safety ~(DBIT_safety_predictplan|DBIT_safety_predictseg)
#define DEFDFLAGS_movpos ~(DBIT_movpos_eval|DBIT_movpos_intern|DBIT_movpos_meth|DBIT_movpos_fsq)
#define DEFDFLAGS_speed ~(DBIT_speed_query)
#define DEFDFLAGS_retransmit ~(DBIT_retransmit_message)

#define DEBUG_FLAGS_H_DEFINE
#include "realtime+dflags.h"

static int debug_simulate_exactly;
static int nononblock_stdin;

static void debug_user_set(const DebugSelectorAreaInfo *dsai,
			   int op, unsigned long bits) {
  switch (op) {
  case '+':  *dsai->dflags |=  bits;  break;
  case '-':  *dsai->dflags &= ~bits;  break;
  default: abort();
  }
  *dsai->userset |= bits;
}

static void debug_arg_spec(const char *arg) {
  /* syntaxes:
   *  -D+
   *  -D-
   *  -D+<area>
   *  -D-<area>
   *  -D<area><kinds>
   * eg:
   *  -D<area>+<kind>[<kinds>...]
   *  -D<area>-<kind>[<kinds>...]
   * where <kind> can be `*'
   */
  int wlen, l, alen, op=0;
  const char *delim, *area;
  const DebugSelectorAreaInfo *dsai=0;
  const DebugSelectorKindInfo *dski;
  unsigned long bits;

  if (!strcmp("=",arg)) { debug_simulate_exactly= 1; return; }

  for (;;) {
    /* possibilities, arg points to |   dsai   op
     *  -D|                              0      undef
     *  -D|?                             0      undef
     *  -D|?<area>                       0      undef
     *  -D|<area>?...                    0      undef
     *  -D<area>...?|<kind>            <area>   char `?'
     *  -D<area>...?|<kind>?...        <area>   1st `?'
     */

    wlen= strcspn(arg,"+-");
    delim= &arg[wlen];

    if (!dsai) { /* -D|... */
      if (!*delim) badusage("-D without any + - or =");
      if (wlen) { /* -D|<area>?... */
	area= arg;
	alen= wlen;
      } else { /* -D|?[<area>] */
	area= arg+1;
	alen= strlen(area);
	if (!alen) { /* -D|? */
	  for (dsai=dsais; dsai->name; dsai++)
	    debug_user_set(dsai,*delim,~0UL);
	  return;
	}
      }
      /* -D|<area>?... or -D?<area> */
      for (dsai=dsais; dsai->name; dsai++) {
	l= strlen(dsai->name);
	if (l==alen && !memcmp(dsai->name,area,l)) goto area_found;
      }
      badusage("unknown debug message area");
    area_found:
      if (!wlen) { /* -D|?<area> */
	debug_user_set(dsai,*delim,~0UL);
	return;
      }
      /* -D|<area>?<kind>... */
    } else { /* -D<area>...?|<kind>[?...] */
      if (wlen==1 && arg[0]=='*') {
	bits= ~0UL;
	goto kind_found;
      }
      for (dski=dsai->kinds; dski->name; dski++) {
	l= strlen(dski->name);
	if (l==wlen && !memcmp(dski->name,arg,l)) {
	  bits= dski->bit;
	  goto kind_found;
	}
      }
      badusage("unknown debug message kind");
    kind_found:
      debug_user_set(dsai,op,bits);
      if (!*delim) /* -D<area>...?|<kind> */
	return;
       /* -D<area>...?|<kind>?<kind>... */
    }
    /* -D...?|<something>?<kind>... */
    op= *delim++;
    arg= delim;
    /* -D...?<something>?|<kind>... */
  }
}    

static void debug_setup(void) {
  const DebugSelectorAreaInfo *dsai;
  unsigned long def;

  for (dsai=dsais; dsai->name; dsai++) {
    def= (simulate && !debug_simulate_exactly) ? ~0UL : dsai->defdflags;
    *dsai->dflags |= def & ~*dsai->userset;
  }
}

/*---------- initialisation ----------*/

int main(int argc, const char **argv) {
  oop_source_sys *sys_events;
  const char *arg;
  int r, c;

  persist_map_veryearly();

  if (argv[0] && argv[1] && !strcmp(argv[1],PERSIST_CONVERT_OPTION))
    /* do this before we call malloc so that MAP_FIXED is sure to work */
    persist_entrails_run_converter();

  while ((arg=*++argv) && *arg=='-' && arg[1]) {
    arg++;
    while (arg && *arg) {
      switch (*arg++) {
      case 's': device= arg;                      arg=0; break;
      case 'p': persist_fn= arg;                  arg=0; break;
      case 'v': picio_send_noise= atoi(arg);      arg=0; break;
      case 'm': sta_state= Sta_Manual;                   break;
      case 'V': simlog_full=1;                           break;
      case 'B': nononblock_stdin=1;                      break;
      case 'W': disable_watchdog=1;                      break;
      case 'L': logcopy_fn= arg;                  arg=0; break;
      case 'S': simulate= arg;                    arg=0; break;
      case 'D':	debug_arg_spec(arg);              arg=0; break;
      case 'R':
	rtfeats_use= 0;
	while ((c= *arg++)) {
	  switch (c) {
	  case 'd': rtfeats_use |= RTFEAT_DEFAULTS;     break;
	  case 'p': rtfeats_use |= RTFEAT_CPU;          break;
	  case 'P': rtfeats_use |= RTFEAT_ALL(CPU);     break;
	  case 'm': rtfeats_use |= RTFEAT_MEM;          break;
	  case 'M': rtfeats_use |= RTFEAT_ALL(MEM);     break;
	  case 'r': rtfeats_use |= RTFEAT_RUSAGE;       break;
	  default: badusage("unknown -R suboption");
	  }
	}
	arg= 0;
	break;
      default: badusage("unknown option");
      }
    }
  }

  cmdi.out.desc= (char*)"command";
  cmdi.out.fd= 1;
  cmdi.out.error= obc_error;
  cmdi.doline= command_doline;
  cmdi.out.empty= cmdi_output_bufferempty;

  debug_setup();

  if (!simulate) {
    sys_events= oop_sys_new();  if (!sys_events) diee("oop_sys_new");
    events= oop_sys_source(sys_events);  massert(events);

    simlog_open(logcopy_fn);
    cmdin_new(&cmdi, 0);
    if (nononblock_stdin) oop_fd_nonblock(0,0);

    serial_open(device);
    serial_interrupt_priority();
    r= oop_fd_nonblock(serial_fd, 1);  if (r) diee("nonblock(serial_fd,1)");

    events->on_fd(events, serial_fd, OOP_READ, serial_readable, 0);
    events->on_fd(events, serial_fd, OOP_EXCEPTION, read_exception, 0);

    if (rtfeats_use & RTFEAT_DEFAULTS)
      rtfeats_use |= RTFEAT_CPU | RTFEAT_MEM | RTFEAT_RUSAGE;
  } else {
    sim_initialise(logcopy_fn);
    sys_events= 0;
  }

  ouprintf("info version : %s\n", autoversion);
  persist_entrails_interpret();
  records_parse(argv);
  realtime_priority();
  speedmanager_init();
  sta_startup();

  if (!simulate) oop_sys_run(sys_events);
  else sim_run();

  abort();
}

DEFINE_ERRORCODELIST_DATA
