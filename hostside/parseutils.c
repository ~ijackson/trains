/*
 * common
 * utilities for helping parsing
 */

#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

int badcmd(ParseState *ps, const char *fmt, ...) {
  int r;
  va_list al;
  va_start(al,fmt);
  r= vbadcmd(ps,fmt,al);
  va_end(al);
  return r;
}

int ps_word(ParseState *ps) {
  const char *space;
  if (!ps->remain) { ps->lthisword=0; return -1; }
  space= strchr(ps->remain, ' ');
  ps->thisword= ps->remain;
  if (space) {
    ps->lthisword= space - ps->thisword;
    ps->remain= space + 1;
  } else {
    ps->lthisword= strlen(ps->remain);
    ps->remain= 0;
  }
  return 0;
}

int ps_needword(ParseState *ps) {
  if (ps_word(ps)<0) return badcmd(ps,"too few args");
  return 0;
}

void ps_pushbackword(ParseState *ps) {
  ps->remain= ps->thisword;
}

int ps_neednumber(ParseState *ps, long *r,
			long mi, long mx, const char *wh) {
  char *ep;
  long v;
  
  MUSTECR( ps_needword(ps) );
  errno= 0; v= strtol(ps->thisword,&ep,0);
  if (errno || ep != ps->thisword + ps->lthisword)
    return badcmd(ps,"invalid number for %s",wh);
  if (v < mi || v > mx)
    return badcmd(ps,"%s %ld out of range %ld..%ld",wh,v,mi,mx);
  *r= v;
  return 0;
}

int ps_neednoargs(ParseState *ps) {
  if (ps->remain)
    return badcmd(ps,"too many arguments");
  return 0;
}
 
int ps_needhextoend(ParseState *ps, Byte *d, int *len_io) {
  Byte *d_begin, *d_end;
  char buf[3], *ep;

  d_begin= d;
  d_end= d + *len_io;
  buf[2]= 0;
  
  if (!ps->remain) return badcmd(ps,"need hex data block");
  for (;;) {
    if (ps_word(ps)<0) break;
    while (ps->lthisword > 0) {
      if (ps->lthisword & 1)
	return badcmd(ps,"hex data block with odd number of digits in part");
      buf[0]= ps->thisword[0];
      buf[1]= ps->thisword[1];
      if (d >= d_end) return badcmd(ps,"hex data block too long");
      *d++= strtoul(buf,&ep,16);
      if (*ep) return badcmd(ps,"invalid digit in hex data block");
      ps->lthisword -= 2;
      ps->thisword += 2;
    }
  }

  *len_io= d - d_begin;
  return 0;
}

const void *any_lookup(ParseState *ps, const void *inf, int ninfsmax,
		       size_t sz) {
  const char *tname;
  int i;
  
  for (i=0;
       i<ninfsmax && (tname= *(const char *const*)inf);
       i++, inf= (const char*)inf + sz)
    if (!thiswordstrcmp(ps,tname))
      return inf;
  return 0;
}

const void *any_needword_lookup(ParseState *ps, const void *infs, int ninfsmax,
				size_t sz, const char *what) {
  int ec;
  const void *r;

  ec= ps_needword(ps);  if (ec) return 0;
  r= any_lookup(ps,infs,ninfsmax,sz);
  if (!r) {
    badcmd(ps,"unknown %s %.*s",what, ps->lthisword,ps->thisword);
    return 0;
  }
  return r;
}

int lstrstrcmp(const char *a, int la, const char *b) {
  int lb, minl, r;

  lb= strlen(b);
  minl= la < lb ? la : lb;
  r= memcmp(a,b,minl);
  if (r) return r;

  return (la < lb ? -1 :
	  la > lb ? 1 : 0);
}

int thiswordstrcmp(ParseState *ps, const char *b) {
  return lstrstrcmp(ps->thisword, ps->lthisword, b);
}

