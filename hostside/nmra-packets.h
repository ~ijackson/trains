/*
 * strange macros - do not use directly, usually
 */
  
NMRA(speed28, Aint(addr,0) Aint(speed,1) Aint(reverse,2), {
  /* 0<=speed<=28 or <=126; reverse: 0 forwards, non-0 backwards */
  int adj;
  Byte cmd;
  ADDR;
  nmra_errchk(cn, speed, speed>=0 && speed<=28);
  cmd= 0x40 | (reverse ? 0 : 0x20);
  if (speed) {
    adj= speed + 3;
    cmd |= adj & 1 ? 0x10 : 0;
    cmd |= adj >> 1;
  }
  *c++= cmd;
})
NMRA(estop1, Aint(addr,0), {
  /* Baseline Speed and direction Forwards E-Stop(I) S9.2 B table l.56 */
  ADDR;
  *c++= 0x71;
})
NMRA(speed126, Aint(addr,0) Aint(speed,1) Aint(reverse,2), {
  /* Advanced Operations 128 Speed Step Control
   * (actually speeds 0..126) RP9.2.1 C l.200- */
  ADDR;
  nmra_errchk(cn, speed, speed>=0 && speed<=126);
  *c++= 0x3f;
  *c++= (speed ? speed + 1 : 0) | (reverse ? 0 : 0x80);
})

NMRA(estop, Anone, {
  /* Baseline Broadcast stop Forwards(I) Emergency S9.2 B l.98- */
  CONST(0x00, 0x71);
})
NMRA(bstop, Anone, {
  /* Baseline Broadcast stop Forwards(I) non-Emergency S9.2 B l.98- */
  CONST(0x00, 0x70);
})
NMRA(reset, Anone, {
  /* Baseline Decoder Reset S9.2 B l.77- */
  CONST(0x00, 0x00);
})
NMRA(idle, Anone, {
  /* Baseline Idle S9.2 B l.87- */
  CONST(0xff, 0x00);
})

/* For functions:
 * bit 0 is FL aka F0; bits 1-12 are F1-F12; do not call with bits
 * outside 0-12 set; bits in 0-12 but not relevant for the relevant
 * command are ignored. */
NMRA(funcs0to4, Aint(addr,0) Abitmap(bitmap,1), {
  /* Function Group One RP9.2.1 C l.234- */
  FUNCS(0x80 | ((bitmap >> 1) & 0x0f) | ((bitmap << 4 & 0x10)));
})
NMRA(funcs5to8, Aint(addr,0) Abitmap(bitmap,1), {
  /* Function Group Two RP9.2.1 C l.246- */
  FUNCS(0xb0 | ((bitmap >> 5) & 0x0f));
})
NMRA(funcs9to12, Aint(addr,0) Abitmap(bitmap,1), {
  /* Function Group Two RP9.2.1 C l.246- */
  FUNCS(0xa0 | ((bitmap >> 9) & 0x0f));
})
NMRA(cvwrite, Aint(addr,0) Aint(cv,1) Abyte(value,2), {
  /* Configuration Variable Access Long Form RP9.2.1 C l.286- */
  int adj;
  ADDR;
  nmra_errchk(cn, cv, cv>=1 && cv<=1024);
  adj= cv - 1;
  *c++= 0xec | (adj >> 8);
  *c++= adj;
  *c++= value;
})

/*
 * Service mode.
 */
NMRA(svc_cvwrite, Aint(cv,0) Abyte(value,1), {
  /* Service Mode Instruction for Direct Mode, Write Byte
   * RP 9.2.3 E l.107-
   */
  int adj;
  nmra_errchk(cn, cv, cv>=1 && cv<=1024);
  adj= cv - 1;
  *c++= 0x7c | (adj >> 8);
  *c++= adj;
  *c++= value;
})
NMRA(svc_factoryreset, Anone, {
  /* Service Mode Instruction for Decoder Factory Reset
   * RP 9.2.3 E l.279-
   * (See also Packet Sequence for Physical Register Addressing
   * RP 9.2.3 E l.166-)
   */
  CONST(0x7f, 0x08);
})
NMRA(svc_pagepreset, Anone, {
  /* Service Mode Page Preset Instruction
   * RP 9.2.3 E l.90-91
   * used in various packet sequences.
   */
  CONST(0x7d, 0x01);
})
     
#undef Aint
#undef Abitmap
#undef Abyte
#undef Anone
#undef NMRA
