/**/

#ifndef SAFETY_H
#define SAFETY_H

#include <stdarg.h>

#include "../layout/layout-data.h"
#include "layoutinfo.h"
#include "errorcodes.h"

/*========== more basic types etc. ==========*/

typedef long TimeInterval; /*ms*/

/*---------- units and scales ----------*/

/*
 * Distances are in mm.
 * Times are generally in s.
 */

/*========== state of the layout ==========*/

typedef struct MovPosChange MovPosChange;

#define SPEEDSTEPS 126

typedef struct {
  float speed; /* applies to all speeds up to and including this one */
  Distance xs; /* max stopping distance */
  TimeInterval ts; /* max stopping time */
} SpeedRange;

struct Train {
  /* Configuration (excluding speed curve): */
  char *pname;
  int addr;
  Distance head, detectable, tail;

  /* Location: */
  struct Segment *foredetect;
  Distance maxinto, uncertainty;
    /* Front of train's detectable portion (the `reference') is at
     * most maxinto into foredetect, but may be up to uncertainty
     * further back (which may put it in a previous segment).  If the
     * train is stationary it owns the track forwards from the
     * foremost reference possibility, plus ->head, plus MARGIN_NOSE
     * (and if it is not stationary it may own more).  It owns the
     * track backwards from the rearmost reference possibility for
     * ceil(->detectable * MARGIN_TRAINLENGTH) + ->tail + MARGIN_NOSE.
     */
  unsigned
    backwards:1, /* train is moving backwards wrt its own front and back */
    autopoint:1, /* set points automatically if point has autopoint too */

  /* Startup resolution (resolve.c): */
    resolution:2; /* for use by speedmanager */

  int plan_lookahead_nsegs; /* counts from foredetect exclusive */

  struct {
    int step; /* commanded */
    TimeoutEvent decel; /* running iff decelerating */
    double speed; /* iff decerating, at last change; otherwise accurate */
    RetransmitUrgentNode rn;
  } speed;

  int n_speedregimes;
  SpeedRange *speedregimes;
  float speedcurve[SPEEDSTEPS+1];
};

struct Segment {
  Train *owner;                                  /* or 0 */
  Train *home;                                   /* or 0 */
  unsigned
    tr_backwards:1, /* train's motion is (would be) backwards wrt track */
    ho_backwards:1, /* home train has its front and rear backwards wrt track */
    autopoint:1, /* set points automatically if train has autopoint too */
    seg_inverted:1, /* polarity is inverted */
    det_ignore:1, det_expected:1, /* safety.c */
    moving:1, /* feature(s) have been told to change */
    mark0:1,mark1:1,mark2:1,mark3:1, /* for temporary private uses */
    res_movposset:1, /* we have set the position so it is known good */
    res_detect:1, /* detection noticed here during resolution */
    detect_actual:1, /* last detection state received */
    detect_reported:1; /* detection state reported to parent */
  int detect_flaps;
  MovPosComb movposcomb; /* -1 means not known or moving */
  MovPosChange *motion; /* if ->moving, owned by movpos, otherwise by safety */
  TimeInterval until;            /* } for use by safety.c */
  MovPosChange *motion_newplan;  /* }  during predict     */
  const SegmentInfo *i;
};

/*========== embed.c ==========*/
/* surrounds the algorithms and machinery in a program: logging, error
 * handling, arg parsing, etc.
 */

void safety_vpanic(Train *tra, Segment *seg, const char *fmt, va_list al)
     __attribute__((format(printf,3,0),noreturn));
void safety_panic(Train *tra, Segment *seg, const char *fmt,...)
     __attribute__((format(printf,3,4),noreturn));

/*========== safety.c ==========*/
/*
 * safety.c is responsible for ensuring that things don't go
 * physically wrong (eg, collisions, derailments, short circuits,
 * etc.).
 */

typedef void PredictionProblemCallback(Train *tra, Segment *seg /* may be 0 */,
				       void *pu, const char *message);

ErrorCode safety_checktrain(Train *tra,
			    PredictionProblemCallback *ppc, void *ppcu);

void safety_notify_detection(Segment *seg);
  /* Called by startup.c when new train detection occurs in state Run. */

ErrorCode safety_movposchange(Segment *seg, MovPosComb comb,
     int allow_queueing, PredictionProblemCallback *ppc, void *ppcu);
  /* If this succeeds, caller need not call movpos_change.
   * allow_queueing==0  means we only allow unowned segments to move
   *               ==1  allows changing a segment the train is approaching
   *               ==2  allows queueing a change for when the train has left
   */

ErrorCode safety_setdirection(Train *tra, int backwards,
			      PredictionProblemCallback *ppc, void *ppcu);

#define PREDF_JUSTDET 001u
   /* we have just detected the train entering its foredetect so we
    * can subtract the uncertainty from the stopping distance */
#define PREDF_OLDPLAN 002u
   /* the old plan is necessarily sufficient as we are not intending
    * to accelerate, change a movpos, etc. */
#define PREDF_INITQUEST 004u
   /* the initial location is questionable (ie, the initial present
    * marking is allowed to fail) */

typedef struct {
  unsigned stopping:1;
  double max_speed_estimate;
  Distance stopping_distance;
} SpeedInfo;

ErrorCode predict(Train *tra, struct timeval tnow, unsigned flags,
		  Segment *desire_move, int desire_movposcomb,
		  const SpeedInfo *speed_info /* 0: use speedmgr */,
		  PredictionProblemCallback *ppc, void *ppcu);
  /* Lower-level interface for speedmanager etc.
   * Caller must call this with different situations until it succeeds!
   * Caller may pass ppc=0 and ppcu=(char*)"some context" to
   * cause safety_panic if it fails.
   */

void report_train_position(Train *tra);
void report_train_ownerships(Train *tra, Segment *furthest,
			     int always_use_motions);
  /* ... for use by things which update these only, which
   * is basically safety.c and resolve.c */

void safety_abandon_run(void);

/*========== movpos.c ==========*/
/*
 * movpos.c manages the CDU and points and other moveable features.
 * Only safety.c should request changes.
 */

ErrorCode
movpos_change(Segment *tomove, MovPosComb target,
	      int maxdelay_ms, MovPosChange *reservation);
  /* If segment has already been requested to change, an attempt is
   * made to replace that change with the new request; if this is not
   * successful then the existing change will still happen.
   *
   * reservation should be 0, or the results of movpos_reserve
   * on the same move.  It is always consumed, even on error.
   *
   * One of the following must be true of tomove on entry:
   *   moving==1   motion already owned by movpos, see above
   *   motion==0   motion empty, will become owned by movpos
   *
   * On successful exit, tomove->moving=1 and tomove->motion!=0; from
   * then on until ->moving becomes 0, movposcomb may not reflect the
   * real physical state of the layout; instead it gives only
   * information about the target configuration.  (Choreographers are
   * allowed to know implementation details and may know what
   * movposcomb means for certain segments depending on the movement
   * requested.)
   *
   * If a reservation is supplied, maxdelay_ms may be -1 to use the
   * same value as was given to movpos_reserve, or if no reservation
   * given, to say we don't care.
   */

ErrorCode movpos_findcomb_bysegs(Segment *back, Segment *move, Segment *fwd,
				 MovPosComb startpoint, MovPosComb *chosen_r);
  /* Looks for a movposcomb of move where the adjacent segment in one
   * direction is back and the other fwd.  back and fwd may be 0 if we
   * don't care (and must be if there is no track in that direction.
   * It is immaterial which is back and which fwd.
   *
   * The returned movposcomb is the `best' one which is the one
   * closest to the starting point (and if that is not unique, the
   * lowest-numbered).
   *
   * startpoint=-1 means starting with current state.  chosen_r may be 0.
   */

ErrorCode
movpos_reserve(Segment *move, int maxdelay_ms, MovPosChange **res_r,
	       MovPosComb target, MovPosComb startpoint /*as for findcomb*/);
  /* The resulting MovPosChange is a reservation which is guaranteed
   * to be useable successfully later for any movpos_change for the
   * same move, target and startpoint and a greater or equal
   * maxdelay_ms.  On successful exit *res_r is set to non-0.
   *
   * On transition out of Sta_Run, all unconfirmed reservations must
   * be unreserved before points_all_abandon is called (see
   * startup.c);
   */

void movpos_unreserve(MovPosChange *reservation /* 0 ok */);

MovPosComb movpos_change_intent(MovPosChange *chg);
  /* Returns the target value supplied to _reserve or _change */

 /* Error returns from movpos calls
  *
  *  EC_Invalid         there is no movposcomb of tomove matching back/fwd
  *  EC_MovFeatTooLate  maxdelay_ms could not be met
  *  EC_MovFeatReservationInapplicable
  *                     reservation not compatible with change request
  *                       and/or current situation
  */

const char *movpos_pname(const Segment *move, MovPosComb poscomb);


/* Pure functions for manipulating MovPosComb values: */

int movposcomb_feature_posn(const MovFeatInfo *mfi, MovPosComb comb);
  /* Returns position of individual feature. */

MovPosComb movposcomb_feature_update(const MovFeatInfo *mfi,
				     MovPosComb startpoint,
				     int featpos);
  /* Returns movposcomb representing a changed combination of positions */

/*========== speed.c ==========*/

ErrorCode speedmanager_speedchange_request(Train *tra, int step,
                           PredictionProblemCallback *ppc, void *ppcu);
  /* ppc and ppcu as for predict_confirm */

void speedmanager_safety_stop(Train *tra, struct timeval tnow,
			      unsigned predictflags);
  /* Ignores value of PREDF_OLDPLAN */
void speedmanager_reset_train(Train *tra);

void speedmanager_getinfo(Train *tra, struct timeval tnow, SpeedInfo*);
int speedmanager_stopped(Train *tra);

void actual_speed(Train *tra, int step);

void speedmanager_init(void);

/*========== actual.c ==========*/
/* actual.c should only be called from safety.c.
 * It is responsible for communicating with the PICs, including
 * repeating the NMRA commands and redacting detection information.
 *
 * In general, when State information is shared between actual.c
 * and safety.c, safety.c is responsible for modifying State, and
 * will then call an actual_... function to notify the change.
 */

void actual_setspeed(Train *tra);
void actual_emergencystop(Train *tra);

void actual_inversions_start(void);
void actual_inversions_segment(Segment *seg);
void actual_inversions_done(void);
  /* safety.c will call these in this order: first start, then segment
   * for 0 or more segments (whose s.segments[segn].seg_inverted may
   * or may not have changed), and finally done.  At done, the PICs
   * should be told to (de)invert the segments as specified.  */
  
/*
 * Entrypoints are:                  Called from, and as a result of:
 *   actual_setspeed                    safety.c
 *   actual_emergencystop               safety.c
 */

/*========== utils.c ==========*/

typedef struct TrackLocation TrackLocation;
struct TrackLocation { /* transparent, and manipulable by trackloc_... fns */
  Segment *seg; /* current segment */
  Distance remain; /* distance from end of segment as we look at it */
  unsigned backwards:1; /* if 1, into is positive and measured from end */
};

void trackloc_set_maxinto(TrackLocation *tloc, Segment *seg, int backwards);

typedef struct TrackAdvanceContext {
  /* all set by caller, only distance modified by trackloc: */
  int distance;
  /* All event callbacks are optional; if not supplied we pretend
   * that they were a no-op which returned 0. */
  int (*nextseg)(TrackLocation *t, struct TrackAdvanceContext *c,
		 MovPosComb *mpc_io, const TrackLocation *before);
    /* On entry *mpc_io is from next->movposcomb.  nextseg may modify
     * it if it feels like it in which case the modified value will
     * be used instead.  If on 0-return it is still -1, getmovpos is used.
     * t->remain is not valid on entry and should not be touched.
     * Called once right at the start with before==0. */
  int (*getmovpos)(TrackLocation *t, struct TrackAdvanceContext *c,
		   MovPosComb *use_io);
    /* Will be called                            *use_io on entry is
     *  - at the start of trackloc_advance         value from segment
     *  - after nextseg                            value left by nextseg
     *  - by trackloc_getlink                      getlink's mpc argument
     *  - by interfering_segment                   base->movposcomb
     * May modify *use_io if it wishes.  It is OK to leave it as
     * -1 in which case advance will return whatever getmovpos did
     * (even if 0).  When called by interfering_segment, non-0 returns
     * will be treated as an indication that the movposcomb is unknown.
     * On entry t->remain is invalid; it should not be modified. */
  int (*trackend)(TrackLocation *t, struct TrackAdvanceContext *c);
    /* trackloc_advance stops even if this returns 0. */
  void *u;
} TrackAdvanceContext;

int trackloc_getlink(TrackLocation *t, TrackAdvanceContext *c,
		     const SegPosCombInfo **pci_r /* 0 ok */,
		     const SegmentLinkInfo **link_r /* 0 ok */,
		     MovPosComb mpc /* -1: get from segment */);
  /* Of c, only c->getmovpos is used.  t->remain is not used.
   * c may be 0 which works just like c->getmovpos==0. */

Segment *segment_interferer(Segment *base);
  /* Returns the segment which may interfere with base, if any. */

ErrorCode segment_interferer_does(TrackAdvanceContext *c, Segment *base,
				  Segment *interferer, int *does_r);
  /* Checks whether it interferes with base.
   * Return is 0 or the error value from c->getmovpos.
   * *does_r is set (only) on successful return, 
   * c is used as for trackloc_getlink.  If c->getmovpos is 0
   * the segments are taken to interfere if they would in any movposcomb.
   * interferer may be 0 in which case succeeds, setting *does_r=0.
   */

Segment *segment_interferes_simple(TrackAdvanceContext *c, Segment *base);
  /* Combines segment_interferer and segment_interferer_does.
   * c->getmovpos MUST NOT return errors; we abort if it does. */

int trackloc_advance(TrackLocation *t, TrackAdvanceContext *c);
  /* Advances t by dist until distance becomes 0, some callback
   * returns non-0, or sometimes 0 if we cannot continue and the
   * relevant callback is missing or returned 0.  If we exhausted
   * c->distance then *t is the end of the counted distance, on the
   * earlier of the two segments if there is a choice.  Otherwise *t
   * is the last location on the last sensible segment.  (Ie, nextseg
   * and getmovpos get a putative new segment and if they don't work
   * trackloc_advance rewinds.)  In any case c->distance is updated by
   * the distance successfully traversed. */

int trackloc_reverse_exact(TrackLocation *t, TrackAdvanceContext *c);
int trackloc_set_exactinto(TrackLocation *t, TrackAdvanceContext *c,
			   Segment *seg, int backwards, int into);
  /* c is used just as for trackloc_getlink.
   * If we can't determine the movposcombs we call abort. */

#define TL_DIST_INF (INT_MAX/16)

/*========== useful macros and declarations ==========*/

/*---------- looping over trains and segments ----------*/

#define SEGMENT_ITERVARS(seg)			\
  SegmentNum seg##n;				\
  Segment *seg;					\
  const SegmentInfo *seg##i

#define TRAIN_ITERVARS(tra)			\
  Train *tra;					\
  int tra##n

#define FOR_SEGMENT(seg, initx, stepx)			\
  for (seg##n=0, seg=segments, seg##i=info_segments;	\
       seg##n < NUM_SEGMENTS;				\
       seg##n++, seg++, seg##i++, stepx)

#define FOR_TRAIN(tra, initx, stepx)		\
  for (tra##n=0, tra=trains, initx;		\
       tra##n < n_trains;			\
       tra##n++, tra++, stepx)

#define SEG_IV SEGMENT_ITERVARS(seg)
#define FOR_SEG FOR_SEGMENT(seg,(void)0,(void)0)
  
#define TRA_IV TRAIN_ITERVARS(tra)
#define FOR_TRA FOR_TRAIN(tra,(void)0,(void)0)

/*---------- calculations with fixed point speeds ----------*/

#define SPEED_CALC_TIME(speed,dist,round) ((dist) / ((speed)+1e-6))
#define SPEED_CALC_DIST(speed,time,round) ((time) * (speed))

/*---------- safety margin parameters ----------*/

#define MARGIN_NOSE 6 /*mm*/ /* physical spare at each end of train */
#define MARGIN_TAIL 6 /*mm*/ /* spare when in motion only */
#define MARGIN_SPEED 1.2 /*ratio*/
#define MARGIN_AUTOPOINTTIME 500 /*ms*/
#define UNMARGIN_ROUNDING 1e-4 /* mm/ms; for 1s, leads to max err of 100um */
#define MARGIN_TRAINLENGTH 1.05

#define MARGIN_STOPTIME 240 /*ms*/
#define MARGIN_POLARISETIME 120 /*ms*/

#define UNMARGIN_WATCHDOG_16 7 /* 16ms units */ 
#define UNMARGIN_WATCHDOG (UNMARGIN_WATCHDOG_16*16) /*ms*/
                                  /* should be < MARGIN_POLARISETIME */
#define MARGIN_WATCHDOG 40

#endif /*SAFETY_H*/
