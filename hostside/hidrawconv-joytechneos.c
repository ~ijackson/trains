#include "hidrawconv.h"

static const KeyBit keybits[]= {
  { "EV_KEY BTN_1",          6, 0x01 },
  { "EV_KEY BTN_2",          6, 0x02 },
  { "EV_KEY BTN_3",          6, 0x04 },
  { "EV_KEY BTN_4",          6, 0x08 },
  { "EV_KEY BTN_5",          6, 0x10 },
  { "EV_KEY BTN_6",          6, 0x20 },
  { "EV_KEY BTN_THUMBL",     7, 0x01 },
  { "EV_KEY BTN_THUMBR",     7, 0x02 },
  { "EV_KEY BTN_TL",         6, 0x40 },
  { "EV_KEY BTN_TR",         6, 0x80 },
  { "EV_KEY BTN_TOP2",       7, 0x04 },
  { "EV_KEY BTN_TOP",        7, 0x08 },
  { 0 }
};
static const ValLoc vallocs[]= {
  { "EV_ABS ABS_X",          1,0,+1, 0xff, 0x80 },
  { "EV_ABS ABS_Y",          2,0,-1, 0xff, 0x80 },
  { "EV_ABS ABS_RUDDER",     3,0,+1, 0xff, 0x80 },
  { "EV_ABS ABS_THROTTLE",   4,0,-1, 0xff, 0x80 },
//  { "EV_ABS DPAD",           5,0,+1, 0x0f, 0x00 },
  { 0 }
};

static void pr(const uint8_t *msg, int msglen, const uint8_t *last) {
  reportbits(msg, last, msglen, keybits);
  reportvals(msg, last, msglen, vallocs);
}

ProcessReport *const nested_processors[MAXREPORTS]= {
 [0x4a] = pr
};

const char *progname= "hidrawconv-joytechneos";

const char *const descriptor=
  "05010904a101854a0901a1000501093009311580257f350045ff660000750895028102c0050209ba150026ff00750895018102050209bb150026ff007508950181020501093915012508360000463b016514750895018102950c7501050945013500150025011901290c810295040605ff09018102c005140924a101854d150026ff000931750895079182c005010906a101854b050719e029e71500250175019508810295017508810195057501050819012905910295017503910195057508150025680507190029688100c0";

static LastReports inner_lasts;

static void pr_outer(const uint8_t *msg, int msglen, const uint8_t *last) {
  dispatch(&inner_lasts,"inner message: ",nested_processors, msg+1,msglen-1);
}

ProcessReport *const report_processors[MAXREPORTS]= {
 [0x00] = pr_outer
};
