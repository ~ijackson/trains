/*
 * dlist.h
 * Doubly linked lists
 *
 * This file is part of epithet, a DNS nameserver.
 * epithet is Copyright 2001 Ian Jackson.
 *
 * epithet is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program and documentation is distributed in the hope that it
 * will be useful, but without any warranty; without even the implied
 * warranty of merchantability or fitness for a particular
 * purpose. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with epithet; if not, write to the Free Software Foundation,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA, or email
 * epithet-maint@chiark.greenend.org.uk.
 */

#ifndef DLIST_H
#define DLIST_H


#define DLIST_NNODE(nodetype,linktag) struct linktag { nodetype *next, *back; }
#define DLIST_NODE(nodetype)          DLIST_NNODE(nodetype, )

#define DLIST1_PREPEND(head,node,linkmemb)                                    \
  ((void)((node)->linkmemb.next= (head),				      \
	  (node)->linkmemb.back= 0,					      \
	  ((head) ? (head)->linkmemb.back= (node) : 0),			      \
	  (head)= (node)))

#define DLIST1_REMOVE(head,node,link)					      \
  ((void)								      \
   ((node)->link.next ? ((node)->link.next->link.back= (node)->link.back) :0, \
    (node)->link.back ? ((node)->link.back->link.next= (node)->link.next)     \
                      : ((head)=                       (node)->link.next)))


#define DLIST2_NHEAD(nodetype,listtag) struct listtag { nodetype *head,*tail; }
#define DLIST2_HEAD(nodetype)          DLIST2_NHEAD(nodetype, )

#define DLIST2_INIT(list) ((list).head= (list).tail= 0)

#define DLIST2_REMOVE(list,node,link)					      \
  ((void)								      \
   (((node)->link.back ? ((node)->link.back->link.next= (node)->link.next)    \
                       : (                 (list).head= (node)->link.next)),  \
    ((node)->link.next ? ((node)->link.next->link.back= (node)->link.back)    \
                       : (                 (list).tail= (node)->link.back))))

#define DLIST2_APPEND(list,node,link)			\
  ((void)						\
   ((node)->link.next= 0,				\
    (node)->link.back= (list).tail,			\
    ((list).tail ? ((list).tail->link.next= (node))	\
                 : ((list).head= (node))),		\
    (list).tail= (node)))

#define DLIST2_PREPEND(list,node,link)			\
  ((void)						\
   ((node)->link.next= (list).head,			\
    (node)->link.back= 0,				\
    ((list).head ? ((list).head->link.back= (node))	\
                 : ((list).tail= (node))),		\
    (list).head= (node)))

#define DLIST2_INSERT_BEFORE(list,newnode,link,refnode)	\
  ((void)						\
   ((newnode)->link.next= (refnode),			\
    (newnode)->link.back= (refnode)->link.back,		\
    ((refnode)->link.back				\
       ? ((refnode)->link.back->link.next= (newnode))	\
       : ((list).head= (newnode))),			\
    (refnode)->link.back= (newnode)))

#define DLIST2_INSERT_AFTER(list,newnode,link,refnode)	\
  ((void)						\
   ((newnode)->link.back= (refnode),			\
    (newnode)->link.next= (refnode)->link.next,		\
    ((refnode)->link.next				\
       ? ((refnode)->link.next->link.back= (newnode))	\
       : ((list).tail= (newnode))),			\
    (refnode)->link.next= (newnode)))))
     
#endif /*DLIST_H*/
