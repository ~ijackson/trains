/**/

#include <assert.h>
#include <limits.h>
#include <string.h>

#include "common.h"
#include "nmra.h"

void nmra_encodeforpic(const Nmra *packet, PicInsn *pi) {
  const Byte *bp;
  int length;
  Byte *encp;
  unsigned working, newbits;
  int working_qty;

  length= packet->l;
  bp= packet->d;
  
  assert(length > 0);
  assert(length <= NMRA_PACKET_MAX);
  encp= pi->d;
  working= 0xfffc;   /* 16-bit temp register.  Top working_qty bits    */
  working_qty= 15;   /*  are some data bits to encode, rest are clear. */
  /* we start with the 14-bit preamble and the packet start bit */
  for (;;) {
    assert(working_qty >= 0);
    if (working_qty < 7) {
      if (length > 0) {
	/* plonk new data bits just to right of old data bits */
	length--;
	newbits= *bp++;
	newbits <<= 1;
	newbits |= !length; /* 9 bits, bottom one is `end of packet' */
	working |= (newbits << (7-working_qty));
	working_qty += 9;
      } else if (!working_qty) {
	/* all done */
	break;
      } else {
	/* pad with exactly enough 1 bits to make up the encoded byte */
	working |= 0xffU << (8-working_qty);
	working_qty= 7;
      }
    }
    assert(encp < pi->d + COMMAND_ENCODED_MAX);
    *encp++= ((working >> 9) & 0x7f) | 0x80;
      /* top 7 bits, right-justified, plus `more command' bit */
    working <<= 7;
    working_qty -= 7;
  }
  assert(encp > pi->d);
  encp[-1] &= ~0x80; /* clear `more command' bit */
  pi->l= encp - pi->d;
}

void nmra_addchecksum(Nmra *packet) {
  /* calculates checksum, S9.2 B l.63 */
  int left;
  Byte *bp;
  unsigned running;

  assert(packet->l >=0 && packet->l < NMRA_PACKET_MAX);

  for (left=packet->l, running=0, bp=packet->d;
       left > 0;
       bp++, left--)
    running ^= *bp;

  *bp= running;
  packet->l++;
}

static long argnumber(NmraParseEncodeCaller *pec, int argi,
		      unsigned long *au, int argc,
		      long min, long max) {
  long l;
  unsigned long abit;

  if (argi >= argc) nmra_problem(pec,"missing numeric arg value");

  l= nmra_argnumber(pec, argi);
  
  abit= 1ul << argi;
  assert(!(*au & abit));
  *au |= abit;

  if (l<min || l>LONG_MAX || l>max)
    nmra_problem(pec,"numeric arg value out of range");
  return l;
}

void nmra_parse_encode(Nmra *out, const char *arg, int argl,
		       int argc, NmraParseEncodeCaller *pec) {
  unsigned long au= 0;

  #define Aint(x,i)     , argnumber(pec,i,&au,argc, INT_MIN,INT_MAX)
  #define Abitmap(x,i)  , argnumber(pec,i,&au,argc, 0,~0u>>1)
  #define Abyte(x,i)    , argnumber(pec,i,&au,argc, 0,0xff)
  #define Anone
  #define NMRA(n,al,body)				\
  if (argl == sizeof(#n)-1 && !memcmp(arg,#n,argl)) {	\
    enco_nmra_##n(out al);				\
  } else
#include "nmra-packets.h"
  {
    nmra_problem(pec,"unknown instruction");
  }
  if (au != (1ul << argc)-1)
    nmra_problem(pec,"too many args for packet type");
}
