/*
 */

#include <assert.h>

#include "realtime.h"

/*---------- constructors/initialisers ----------*/

void trackloc_set_maxinto(TrackLocation *tloc, Segment *seg, int backwards) {
  tloc->seg= seg;
  tloc->backwards= backwards;
  tloc->remain= 0;
}

int trackloc_set_exactinto(TrackLocation *t, TrackAdvanceContext *c,
			   Segment *seg, int backwards, int into) {
  const SegPosCombInfo *pci;
  int r;

  t->seg= seg;
  t->backwards= backwards;
  t->remain= 0;

  r= trackloc_getlink(t,c,&pci,0,-1);
  if (r) return r;
  assert(pci);
  
  t->remain= pci->dist - into;
  return 0;
}

/*---------- enquirers ----------*/

int trackloc_getlink(TrackLocation *t, TrackAdvanceContext *c,
		     const SegPosCombInfo **pci_r,
		     const SegmentLinkInfo **link_r,
		     MovPosComb mpc) {
  const SegPosCombInfo *pci;
  int r;

  if (mpc<0)
    mpc= t->seg->movposcomb;

  if (c && c->getmovpos) {
    r= c->getmovpos(t,c,&mpc);
    if (r) return r;
  }
  if (mpc<0) {
    if (pci_r) *pci_r=0;
    if (link_r) *link_r=0;
    return 0;
  }
  assert(mpc >= 0);
  assert(mpc < t->seg->i->n_poscombs);
//fprintf(stderr,"trackloc_getlink returning %s %d %d\n",t->seg->i->pname,mpc,
//	t->backwards);
  pci= &t->seg->i->poscombs[mpc];
//fprintf(stderr,"trackloc_getlink returning  %s\n",pci->pname);
  if (pci_r) *pci_r= pci;
  if (link_r) *link_r= &pci->link[t->backwards];
  return 0;
}

/*---------- mutator ----------*/

static int nextseg(TrackLocation *t, TrackAdvanceContext *c,
		   const SegPosCombInfo **pci_r,
		   const SegmentLinkInfo **link_r,
		   const TrackLocation *leaving) {
  MovPosComb mpc_nego;
  int r;

  mpc_nego= t->seg->movposcomb;

  if (c->nextseg) {
    r= c->nextseg(t,c, &mpc_nego, leaving);
    if (r) return r;
  }

  r= trackloc_getlink(t,c, pci_r,link_r, mpc_nego);
  return r;
}

int trackloc_advance(TrackLocation *t, TrackAdvanceContext *c) {
  Segment *next;
  SegmentNum nextnum;
  const SegPosCombInfo *pci;
  const SegmentLinkInfo *link;
  TrackLocation leaving;
  int r;

  r= nextseg(t,c, &pci,&link, 0);
  if (r) return r;

//fprintf(stderr,"advance start getlink %s%s/%s -> %s\n",
//	t->backwards?"-":"", t->seg->i->pname,
//	pci ? pci->pname : "?",
//	link && SOMEP(link->next) ? info_segments[link->next].pname : "?");

  for (;;) {
    if (!c->distance) return 0;

    if (t->remain) {
      int use= t->remain < c->distance ? t->remain : c->distance;
      t->remain -= use;
      c->distance -= use;
      continue;
    }

    nextnum= link->next;

    if (!SOMEP(nextnum)) {
      if (c->trackend) return c->trackend(t,c);
      return 0;
    }
    next= &segments[nextnum];

    leaving= *t;

    t->seg= next;
    t->backwards ^= link->next_backwards;
    t->remain= -1;

    r= nextseg(t,c, &pci,&link, &leaving);
    if (r) { *t= leaving; return r; }

//fprintf(stderr,"advance ran getlink %s/%s -> %s\n",leaving->i->pname,
//	pci ? pci->pname : "?",
//	link && SOMEP(link->next) ? info_segments[link->next].pname : "?");

    t->remain= pci->dist;
  }
}

Segment *segment_interferer(Segment *base) {
  SegmentNum intern= base->i->interferes;
  if (!SOMEP(intern)) return 0;
  return &segments[intern];
}

static ErrorCode interfering_movposcomb(TrackAdvanceContext *c, Segment *seg,
					int *does_r) {
  MovPosComb mpc;
  int r;

  mpc= seg->movposcomb;
  if (c && c->getmovpos) {
    TrackLocation t;
    t.seg= seg;
    t.backwards= 0;
    t.remain= 0;

    r= c->getmovpos(&t,c,&mpc);
    if (r) return r;
  }

  *does_r= !SOMEP(mpc) || seg->i->interferes_movposcomb_map & (1u << mpc);
  return 0;
}

ErrorCode segment_interferer_does(TrackAdvanceContext *c, Segment *base,
				  Segment *inter, int *does_r) {
  ErrorCode ec;
  int does;

  if (!inter) goto doesnt;

  ec= interfering_movposcomb(c,base, &does);  if (ec) return ec;
  if (!does) goto doesnt;

  assert(base->i == &info_segments[inter->i->interferes]);
  ec= interfering_movposcomb(c,inter, &does);  if (ec) return ec;
  if (!does) goto doesnt;

  *does_r= 1;
  return 0;

 doesnt:
  *does_r= 0;
  return 0;
}

Segment *segment_interferes_simple(TrackAdvanceContext *c, Segment *base) {
  Segment *inter= segment_interferer(base);
  if (!inter) return 0;

  int does;
  ErrorCode ec= segment_interferer_does(c,base,inter,&does);
  assert(!ec);

  return does ? inter : 0;
}

int trackloc_reverse_exact(TrackLocation *t, TrackAdvanceContext *c) {
  const SegPosCombInfo *pci;
  int r;
  
  r= trackloc_getlink(t,c,&pci,0,-1);
  if (r) return r;
  assert(pci);
  t->remain= pci->dist - t->remain;
  t->backwards ^= 1;
  return 0;
}
