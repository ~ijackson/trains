#include "hidrawconv.h"

static void pr(const uint8_t *msg, int msglen, const uint8_t *last) {
  dumphex(stdout, msg, msglen);
  putchar('\n');
}

const char *progname= "hidrawconv-hex";

const char *const descriptor= "?";

ProcessReport *const report_processors[MAXREPORTS]= {
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,

  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,

  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,

  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
  pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr, pr,pr,pr,pr,
};
