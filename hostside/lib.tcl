
proc manyset {list args} {
    foreach val $list var $args { upvar 1 $var my; set my $val }
}

proc start_commandloop {} {
    commandloop -async -prompt1 { return "% " } -prompt2 { return "> " }
}

proc fconfig-trainproto {file} {
    fconfigure $file -translation {auto lf} -buffering line -blocking 0
}

proc badusage {m} {
    global argv0
    puts stderr "$argv0: bad usage: $m"
    exit 8
}

proc setting {varname defvalue regexp} {
    upvar #0 $varname var
    upvar #0 settings($varname) re
    set var $defvalue
    set re $regexp
}

proc readable {whatfor conn args} {
    while {[gets $conn l]>=0} { eval [list $whatfor-inputline $conn $l] $args }
    if {[eof $conn]} { eval [list $whatfor-eof $conn] $args }
}

proc parse-argv {formalargs} {
    # formalargs: list; if list is [list *] then any is allowed
    # sets argv to list of non-option args
    # checks settings($var)
    global argv settings
    for {set i 0} {$i < [llength $argv]} {incr i} {
	set a [lindex $argv $i]
	if {[regexp {^--(\w+)\=(.*)$} $a dummy var val]} {
	    if {[info exist settings]} {
		upvar #0 settings($var) re
		if {![info exists re]} {
		    badusage "unknown variable setting --$var=..."
		}
		if {![regexp -- "^$re\$" $val]} {
		    badusage "bad value for setting $var"
		}
	    }
	    upvar #0 $var varset
	    set varset $val
	} elseif {[regexp {^--$} $a]} {
	    incr i
	    break
	} elseif {[regexp {^-} $a]} {
	    badusage "unknown command line option $a"
	} else {
	    break
	}
    }
    set argv [lrange $argv $i end]
    if {[llength $formalargs]==1 && [string compare * [lindex $formalargs 0]] \
	    && [llength $formalargs] != [llength $argv]} {
	if {![llength $formalargs]} {
	    badusage "no non-option arguments expected"
	} else {
	    badusage "needed non-option arguments: [concat $formalargs]"
	}
    }
}

proc get-unique {prefix} {
    upvar #0 unique_ix($prefix) ix
    if {![info exists ix]} { set ix 0 }
    return "$prefix[incr ix]"
}

proc addexpr {varname expr} {
    set add [uplevel 1 [list expr $expr]]
    upvar 1 $varname var
    if {![info exists var]} { set var 0 }
    set var [expr {$var + $add}]
}

proc setexpr {varname expr} {
    upvar 1 $varname var
    set var [uplevel 1 [list expr $expr]]
}

proc defset {varname val} {
    upvar 1 $varname var
    if {[info exists var]} return
    set var $val
}

setting port 2883 {\d+}

