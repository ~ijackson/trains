/*
 * ASCII-format record and config file reader/writer
 *
 * File format:
 *
 *  max-trains num
 *  train <trainpn> is <addr> <head>+<detectable>+<tail>
 *  train <trainpn> home [-]<segpn> [-]<segpn> [-]<segpn>
 *  train <trainpn> step <step>=<speed>
 *  train <trainpn> stops <step> at <distance> after <milliseconds>
 *  seg <segpn> has [-]<ownerpn> [inverted]
 *  seg <segpn> at <movposcomb>
 *  adjunct <trainoradjpn> <adjpn> is <addr> <nmranum>
 *  adjunct <trainoradjpn> <adjpn> is <addr> step [-]<nmranum>
 *  adjunct - is <addr> <nmranum>             } prevents enabling adjunct;
 *  adjunct - is <addr> step [-]<nmranum>     }  xmits turning it off
 *
 * speed is floating point in m/s
 */

#include <sys/mman.h>

#include "record-i.h"
#include "record-l.h"

int n_adjtargs, n_adjaddrs;
AdjunctsTarget *adjtargs;
AdjunctsAddr **adjaddrs;

/*---------- input and error handling ----------*/

static const char *filename;

void record_yyerror(const char *m) {
  die("config: %s:%d: %s", filename, record_yylineno, m);
}

/*---------- pname lookup (also handles train counting) ----------*/

static char **train_pnames;

Train *record_pname2train(const char *pname) {
  int tran;
  char **trap;
  
  for (tran=0, trap=train_pnames; tran<n_trains; tran++,trap++) {
    if (!strcmp(*trap, pname)) goto found;
  }
  if (trains) record_yyerror("number of trains changed between passes!");
  tran= n_trains++;
  train_pnames= mrealloc(train_pnames, sizeof(*train_pnames)*n_trains);
  train_pnames[tran]= mstrdup(pname);

found:
  if (!trains) return 0;
  return trains + tran;
}

Segment *record_pname2seg(const char *pname) {
  SEG_IV;

  if (!segments) return 0;

  FOR_SEG
    if (!strcmp(segi->pname, pname))
      return seg;
  
  return 0; /* silently discard data for segments no longer in the layout */
}

#define SLIST_FIND_OR_INSERT(head,search,node,found,fillin) do{	\
    for ((search)= &(head);					\
	 ((node)=*(search)) && !(found);			\
	 (search)= &(node)->next);				\
    if (!(node)) {						\
      *(search)= (node)= mmalloc(sizeof(*(node)));		\
      fillin;							\
    }								\
  }while(0)

#define ARY_FIND_OR_APPEND(things,n_things,i,entry,found,fillin) do{	\
    for ((i)=0, (entry)=(things);					\
	 (i)<(n_things) && !(found);					\
	 (i)++, (entry)++);						\
    if ((i) >= (n_things)) {						\
      (n_things)++;							\
      (things)= mrealloc((things), sizeof(*things) * (n_things));	\
      entry= &(things)[(i)];						\
      fillin;								\
    }									\
  }while(0)

AdjunctsAddr *record_adjaddr(int num) {
  AdjunctsAddr **search;
  int ix;
  
  if (num<1 || num>0x3ff) record_yyerror("adjunct address out of range");

  ARY_FIND_OR_APPEND
    (adjaddrs,n_adjaddrs, ix,search, (*search)->addr == num, ({
      int i;
      AdjunctsAddr *item= mmalloc(sizeof(*item));
      item->next= 0;
      item->addr= num;
      item->speedstep= 0;
      item->current= item->permit= item->all= 0;
      for (i=0; i<4; i++) item->rn[i].pi.l= 0;
      *search= item;
    }));
  
  return *search;
}

AdjunctsAdjunct *record_pname2adjunct(const char *targpn, const char *adjpn) {
  int i;
  AdjunctsTarget *targ;
  AdjunctsAdjunct *adj;

  ARY_FIND_OR_APPEND
    (adjtargs,n_adjtargs,i,targ, !strcmp(targ->pname, targpn), ({
      targ->pname= mstrdup(targpn);
      targ->n_adjs= 0;
      targ->adjs= 0;
    }));

  ARY_FIND_OR_APPEND
    (targ->adjs,targ->n_adjs,i,adj, !strcmp(adj->pname, adjpn), ({
      adj->pname= mstrdup(adjpn);
      adj->a= 0;
      adj->bits= 0;
    }));

  return adj;
}

/*---------- zone allocator for strings ----------*/

typedef struct TempZoneBlock TempZoneBlock;
struct TempZoneBlock {
  TempZoneBlock *next; /* for deallocation */
  char buf[1];
};

static TempZoneBlock *tempzones_head;
static char *tempzone_ptr;
static int tempzone_remain, tempzone_head_len;

char *record_tempzone_strdup(const char *s) {
  int l;
  char *r;
  
  l= strlen(s) + 1;
  if (l > tempzone_remain) {
    TempZoneBlock *new;
    assert(l < INT_MAX/20);
    tempzone_head_len= tempzone_remain= l*10 + 65536;
    new= mmalloc(sizeof(new) + tempzone_remain);
    tempzone_ptr= (char*)new + sizeof(new);
    new->next= tempzones_head;
    tempzones_head= new;
  }
  r= tempzone_ptr;
  tempzone_remain -= l;
  tempzone_ptr += l;
  memcpy(r,s,l);
  return r;
}

void record_tempzone_clear(void) {
  TempZoneBlock *clear, *clear_next;
  if (!tempzones_head) return;

  clear= tempzones_head->next;
  while (clear) { clear_next= clear->next; free(clear); clear= clear_next; }
  tempzones_head->next= 0;
  tempzone_remain= tempzone_head_len;
  tempzone_ptr= tempzones_head->buf;
}

static void record_tempzone_freeall(void) {
  record_tempzone_clear();
  free(tempzones_head);
  tempzones_head= 0;
  tempzone_ptr= 0;
  tempzone_remain= tempzone_head_len= 0;
}

/*---------- semantic actions ----------*/
/*
 * These are all called only during the second pass, when trains is non-0.
 * Also, all Train* and Segment* arguments are known to be non-0.
 */

void record_train_is(Train *tra, int addr, int head, int det, int tail) {
  tra->addr= addr;
  tra->head= head;
  tra->detectable= det;
  tra->tail= tail;
}

void record_train_home(Train *tra, int backw, Segment *seg) {
  if (!tra) return;
  seg->home= tra;
  seg->ho_backwards= backw;
}
  
void record_train_step_speed(Train *tra, int step, double speed) {
  if (step<1 || step>SPEEDSTEPS)
    record_yyerror("speed step out of range 0..126");
  tra->speedcurve[step]= speed;
}
  
void record_seg_has(Segment *seg, int backw, Train *tra, int inverted) {
  seg->owner= tra;
  seg->tr_backwards= backw;
  seg->seg_inverted= inverted;
}

void record_seg_at(Segment *seg, const char *movposcombpname) {
  const SegPosCombInfo *spci;
  int poscomb;
  
  for (poscomb=0, spci=seg->i->poscombs;
       poscomb < seg->i->n_poscombs;
       poscomb++, spci++)
    if (!strcmp(spci->pname, movposcombpname))
      goto found;
  return;
  
found:
  seg->movposcomb= poscomb;
}

/*---------- adjuncts ----------*/

static void record_adjunct_bits(AdjunctsAdjunct *adj, AdjunctsAddr *addr,
				unsigned bits) {
  if (!adj) /* `-' adjunct */
    return;
  if (adj->a && adj->a != addr)
    record_yyerror("adjunct includes multiple decoder addresses");
  adj->a= addr;
  adj->bits |= bits;
  addr->permit |= bits;
  addr->all |= bits;
}

void record_adjunct_nmrafunc(AdjunctsAdjunct *adj, AdjunctsAddr *addr,
			     int num) {
  int bitval;

  if (num<0 || num>12) record_yyerror("NMRA function out of range 0..12");
  bitval= 1<<num;

  if (addr->all & bitval)
    record_yyerror("adjunct bit respecified");
  
  record_adjunct_bits(adj,addr,bitval);
}

void record_adjunct_motor(AdjunctsAdjunct *adj, AdjunctsAddr *addr,
			  int speed) {
  if (speed<-126 || speed>126) record_yyerror("motor speed out of range");
  if (!speed) record_yyerror("zero motor speed?!");
  if (addr->speedstep) record_yyerror("adjunct specifies multiple speeds");

  record_adjunct_bits(adj,addr,ADJS_SPEEDSTEP_BIT);

  addr->speedstep= speed;
  if (addr->speedstep<0) {
    addr->all |= ADJS_SPEEDSTEP_REVERSE;
    addr->speedstep= -addr->speedstep;
  }
}

static int pname1st_compar(const void *av, const void *bv) {
  const char *const *a= av;
  const char *const *b= bv;
  return strcmp(*a,*b);
}

static int adjaddrp_compar(const void *av, const void *bv) {
  const AdjunctsAddr *const *a= av;
  const AdjunctsAddr *const *b= bv;
  return (*a)->addr - (*b)->addr;
}

static void adjuncts_postprocess(void) {
  int i;
  AdjunctsTarget *targ;
  
  qsort(adjtargs, n_adjtargs, sizeof(*adjtargs), pname1st_compar);
  qsort(adjaddrs, n_adjaddrs, sizeof(*adjaddrs), adjaddrp_compar);

  for (i=0, targ=adjtargs;
       i<n_adjtargs;
       i++, targ++)
    qsort(targ->adjs, targ->n_adjs, sizeof(*targ->adjs), pname1st_compar);
}

/*---------- speed curves ----------*/

static SpeedRange *rangebuf;
static int rangebufsz, rangebufused;

void record_train_stopregime_count(void) {
  rangebufsz++;
}

void record_train_stopregime(Train *tra, int step, int xs, int ts) {
  Train *other;
  SpeedRange *new;
  int i;

  if (step<1 || step>126) record_yyerror("stop regime step out of range");

  if (rangebufused >= rangebufsz)
    record_yyerror("more speed points on 2nd pass!");

  if (!tra->speedregimes) {
    tra->speedregimes= rangebuf + rangebufused;
  }

  /* Make room for an extra speed step point for this train,
   * by moving all of the other trains up.  First move the data: */
  memmove(tra->speedregimes + tra->n_speedregimes + 1,
	  tra->speedregimes + tra->n_speedregimes,
	  (char*)(rangebuf + rangebufused)
	  - (char*)(tra->speedregimes + tra->n_speedregimes));
  /* Then adjust everyone's pointers: */
  for (i=0, other=trains;
       i<n_trains;
       i++, other++)
    if (other != tra && other->speedregimes &&
	other->speedregimes >= tra->speedregimes)
      other->speedregimes++;
  /* ... that whole thing is O(n^2) if the regimes aren't sorted by
   * train; we hope they are, in which case we are always adding to
   * the last train in the list and there is then no data to copy,
   * which makes it O(n*t) where n is the number of regimes and t is
   * the number of trains.  If we cared we could optimise away the
   * loop in the common case where this train is right at the end but
   * a special case seems like asking for a bug when we do have out of
   * order speed points. */

  new= &tra->speedregimes[tra->n_speedregimes++];
  rangebufused++;

  new->speed= step;
  new->xs= xs;
  new->ts= ts;
}

static int speedregime_compare(const void *av, const void *bv) {
  const SpeedRange *a= av, *b= bv;
  if (a->speed == b->speed)
    record_yyerror("multiple identical speed regimes");
  return a->speed > b->speed ? 1 : -1;
}

static void speeds_postprocess(void) {
  TRA_IV;
  int step, i;

  FOR_TRA {
    if (!tra->speedregimes || tra->speedcurve[1]<0) {
      tra->addr= -1;
      continue;
    }
    if (tra->n_speedregimes < 1)
      die("config: speed curve too short for %s", tra->pname);

    qsort(tra->speedregimes, tra->n_speedregimes,
	  sizeof(*tra->speedregimes), speedregime_compare);
    if (tra->speedregimes[tra->n_speedregimes-1].speed < 126)
      die("config: speed curve missing top point for %s", tra->pname);

    for (step=1; step<=SPEEDSTEPS; step++)
      if (tra->speedcurve[step] < 0)
	die("config: speed curve for %s missing step %d", tra->pname, step);
    tra->speedcurve[0]= 0;

    for (i=0; i<tra->n_speedregimes; i++) {
      step= tra->speedregimes[i].speed;
      tra->speedregimes[i].speed= tra->speedcurve[step];
    }
  }
}

/*---------- persistent data file layout ----------*/

static void *alloc_some(void *mapbase, int *offset, size_t sz, int count) {
  void *r;
  size_t totalsz;
  
  while (*offset % sz) {
    if (mapbase) ((Byte*)mapbase)[*offset]= 0xaa;
    (*offset)++;
  }
  totalsz= sz*count;
  if (mapbase) {
    r= (Byte*)mapbase + *offset;
    memset(r,0,totalsz);
  } else {
    r= 0;
  }
  *offset += totalsz;
  return r;
}

static void alloc(void) {
  TRA_IV;
  SEG_IV;
  void *mapbase=0;
  char **trap;
  int phase, offset, datalen=0, step;

#define ALLOC(array,count) \
  ((array)= alloc_some(mapbase,&offset,sizeof(*(array)),(count)))

  for (phase=0; ; phase++) {
    /* phase 0: count up how much space will be needed
     * phase 1: fill in most of the details, leaving header blank
     * phase 2: fill in the header, and then exit before doing rest
     */
    offset= 0;

    if (phase==1)
      mapbase= record_allocate(datalen);

#define PHI_SAVE(x) {				\
      typeof(x) *p;				\
      ALLOC(p,1);				\
      if (phase==2)				\
        memcpy(p, &(x), sizeof(x));		\
    }

    DO_PERSIST_HEADER_ITEMS(PHI_SAVE, PHI_SAVE, PHI_SAVE)
    
    if (phase==2)
      break;

    ALLOC(trains, n_trains);
    FOR_TRAIN(tra, trap=train_pnames, trap++) {
      char *pname;
      ALLOC(pname, strlen(*trap)+1);
      if (phase) {
	strcpy(pname, *trap);
	free(*trap);
	tra->pname= *trap= pname;
	tra->addr= -1;
	tra->foredetect= 0;
	tra->uncertainty= tra->maxinto= 0;
	tra->backwards= 0;
	tra->autopoint= 1;
	for (step=0; step<=SPEEDSTEPS; step++)
	  tra->speedcurve[step]= -1;
      }	
    }

    ALLOC(segments, info_nsegments);
    if (phase)
      FOR_SEG {
	seg->owner= 0;
	seg->seg_inverted= 0;
	seg->res_movposset= 0;
	seg->movposcomb= -1;
	seg->moving= 0;
	seg->motion= seg->motion_newplan= 0;
	seg->i= segi;
	seg->autopoint= 1;
      }

    if (phase==0)
      datalen= offset;
  }

  rangebuf= mmalloc(sizeof(*rangebuf) * rangebufsz);
}
  
/*---------- entrypoint from main, and its subroutines ----------*/

static void parse_file(const char *why) {
  FILE *f;
  int r;

  f= fopen(filename,"r");
  if (!f) diee("config: cannot open %s: %s", why, filename);
  record_yyrestart(f);
  record_yylineno= 1;
  r= record_yyparse();
  assert(!r); /* we're supposed to call yyerror which dies */
  fclose(f);
}

static void parse_pass(const char **argv) {
  while ((filename= *argv++))
    parse_file("commandline-specified record file");

  filename= persist_record_converted;
  if (filename)
    parse_file("converted persistent data file");
}

void records_parse(const char **argv) {
  parse_pass(argv); /* trains==0: counts trains and speed ranges. */
  alloc();
  parse_pass(argv); /* trains!=0: populates data area */
  record_tempzone_clear();
  speeds_postprocess();
  adjuncts_postprocess();
  record_tempzone_freeall();
  record_yylex_destroy();
}
