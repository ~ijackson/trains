/**/

#include <setjmp.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

#include "realtime.h"
#include "auproto-pic.h"

#define NMRA_MAX_NARGS 10

typedef struct ManualRetransmitNode ManualRetransmitNode;
struct ManualRetransmitNode {
  struct { ManualRetransmitNode *back, *next; } others;
  char *name;
  int lname;
  RetransmitUrgentNode rn;
};

#define bogus_volatile /*empty*/
#ifdef __GNUC__
#if __GNUC__ == 2
#undef bogus_volatile
#define bogus_volatile volatile
#endif
#endif


static void cmd_ppc(Train *tra, Segment *seg, void *pu, const char *message) {
  const CmdInfo *ci= pu;
  ouprintf("ack %s SignallingPredictedProblem %s %s : %s\n",
	   ci->name, tra->pname, seg ? seg->i->pname : "-", message);
}

#define MUSTECRPREDICT(requester) do{					\
    int mustecr__ec= (requester);					\
    if (mustecr__ec==EC_SignallingPredictedProblem) return EC_BadCmd;	\
    if (mustecr__ec) return mustecr__ec;				\
  }while(0)
#define CMDPPC cmd_ppc,(void*)ci

struct NmraParseEncodeCaller {
  ParseState *ps;
  unsigned long arg[NMRA_MAX_NARGS];
  jmp_buf jb;
};

typedef struct { const char *word; int backwards; int change; } DirectionInfo;
static const DirectionInfo directioninfos[]= {
  { "forwards",   0, 0 }, /* first two must be `forwards' and `backwards' */
  { "backwards",  1, 0 }, /*  for the benefit of cmd_speed                */
  { "change",    -1, 1 },
  { 0 }
};  

unsigned long nmra_argnumber(NmraParseEncodeCaller *pec, int argi) {
  return pec->arg[argi];
}

void nmra_problem(NmraParseEncodeCaller *pec, const char *problem) {
  badcmd(pec->ps,problem);
  longjmp(pec->jb, 1);
}

static ErrorCode cmd_nmra_command(ParseState *ps, PicInsn *pi) {
  int hex;
  const char *cmdarg;
  int lcmdarg;
  bogus_volatile int argc, checksum;
  NmraParseEncodeCaller pec;
  Nmra nmra;
  char *ep;

  assert(ps->remain);
  switch (ps->remain[0]) {
  case '_':  ps->remain++; return ps_needhextoend(ps, pi->d, &pi->l);
  case '=':  hex=1;  checksum=1;  break;
  case ':':  hex=1;  checksum=0;  break;
  default:   hex=0;  checksum=1;  break;
  }

  if (hex) {
    ps->remain++;
    nmra.l= NMRA_PACKET_MAX - checksum;
    MUSTECR( ps_needhextoend(ps, nmra.d, &nmra.l) );
  } else {
    MUSTECR( ps_needword(ps) );
    cmdarg= ps->thisword;
    lcmdarg= ps->lthisword;
    pec.ps= ps;
    argc= 0;

    while (ps_word(ps)>=0) {
      if (argc >= NMRA_MAX_NARGS)
	return badcmd(ps,"far too many nmra args");

      errno=0; pec.arg[argc++]= strtoul(ps->thisword, &ep, 0);
      if (errno || ep != ps->thisword + ps->lthisword)
        return badcmd(ps,"bad numeric argument for nmra");
    }
    if (setjmp(pec.jb))
      return EC_BadCmd;

    nmra_parse_encode(&nmra, cmdarg,lcmdarg, argc, &pec);
  }
  if (checksum)
    nmra_addchecksum(&nmra);

  nmra_encodeforpic(&nmra, pi);
  return 0;
}
  
static int cmd_nmra(ParseState *ps, const CmdInfo *ci) {
  static struct { ManualRetransmitNode *head, *tail; } mrns;
  
  PicInsn *pi, pi_buf;
  ManualRetransmitNode *mrn=0;
  void (*retrans)(RetransmitUrgentNode *urg, Nmra *n)= 0;
  ErrorCode ec;
  
  if (ps->remain) {
    if (ps->remain[0]=='*') retrans= retransmit_urgent_queue;
    else if (ps->remain[0]=='%') retrans= retransmit_urgent_queue_relaxed;
  }
  if (retrans) {
    const char *mrname;
    int lmrname;

    ps_word(ps);
    mrname= ps->thisword+1;
    lmrname= ps->lthisword-1;

    for (mrn= mrns.head;
	 mrn &&
	 !(mrn->lname == lmrname &&
	   !memcmp(mrn->name, mrname, lmrname));
	 mrn= mrn->others.next);
    if (mrn) {
      retransmit_urgent_cancel(&mrn->rn);
    } else {
      mrn= mmalloc(sizeof(*mrn));
      mrn->name= mmalloc(lmrname);
      memcpy(mrn->name, mrname, lmrname);
      mrn->lname= lmrname;
      DLIST2_APPEND(mrns,mrn,others);
    }
  }

  if (!ps->remain) {
    if (!retrans) {
      return badcmd(ps,"nmra must have slot to cancel or data to send");
    }
    DLIST2_REMOVE(mrns,mrn,others);
    free(mrn->name);
    free(mrn);
    return 0;
  }

  pi= retrans ? &mrn->rn.pi : &pi_buf;

  ec= cmd_nmra_command(ps, pi);
  if (ec) {
    if (retrans) { free(mrn->name); free(mrn); }
    return ec;
  }

  if (retrans)
    retrans(&mrn->rn, 0);
  else
    serial_transmit(pi);

  return 0;
}

static int cmd_noop(ParseState *ps, const CmdInfo *ci) {
  return 0;
}

static int cmd_pic(ParseState *ps, const CmdInfo *ci) {
  const PicInsnInfo *pii;
  PicInsn pi;
  long obj, v;

  if (ps->remain && ps->remain[0]=='=') {
    ps->remain++;
    pi.l= sizeof(pi.d);
    MUSTECR( ps_needhextoend(ps, pi.d, &pi.l) );
  } else {
    pii= some_needword_lookup(ps,pic_command_infos,"pic command");
    if (!pii) return EC_BadCmd;
  
    if (pii->argsbits) {
      MUSTECR( ps_neednumber(ps, &obj, 0,
			     (1L << (pii->argsbits - pii->vbits)) - 1,
			     "pic object number") );
    } else {
      obj= 0;
    }
    if (pii->vbits) {
      MUSTECR( ps_neednumber(ps, &v, 0, (1L << pii->vbits) - 1,
			     "pic command value") );
    } else {
      v= 0;
    }
    MUSTECR( ps_neednoargs(ps) );
    enco_pic_pii(&pi, pii, obj, v);
  }
  serial_transmit(&pi);
  return 0;
}

static int ps_needsegment(ParseState *ps, Segment **seg_r,
			  const SegmentInfo **segi_r) {
  const SegmentInfo *segi;
  segi= some_needword_lookup_counted(ps,info_segments,info_nsegments,
				     "segment");
  if (!segi) return EC_BadCmd;
  if (segi_r) *segi_r= segi;
  if (seg_r) *seg_r= segments + (segi - info_segments);
  return 0;
}

static int ps_needsegmentperhaps(ParseState *ps, Segment **seg_r,
				 const SegmentInfo **segi_r) {
  MUSTECR( ps_needword(ps) );
  if (!thiswordstrcmp(ps,"-")) { *seg_r=0; *segi_r=0; return 0; }
  ps_pushbackword(ps);
  return ps_needsegment(ps,seg_r,segi_r);
}

static int ps_needtrain(ParseState *ps, Train **tra_r) {
  const Train *tra= some_needword_lookup_counted(ps,trains,n_trains,"train");
  if (!tra) return EC_BadCmd;
  *tra_r= (Train*)tra;
  return 0;
}

static int cmd_route_movfeat(ParseState *ps, const CmdInfo *ci,
			     Segment *move, const SegmentInfo *movei,
			     int poscomb) {
  long ms;
  
  ms= -1;
  if (ps->remain && CIXF_FORCE)
    MUSTECR( ps_neednumber(ps,&ms,0,100000,"milliseconds") );

  MUSTECR( ps_neednoargs(ps) );

  if (ci->xarg & CIXF_FORCE) {
    if (!(sta_state < Sta_Resolving || sta_state == Sta_Manual))
      return badcmd(ps,"movpos queueing arrangements not ready");
    
    MovPosChange *reservation= 0;
    if (!move->moving && move->motion) {
      reservation= move->motion;
      move->motion= 0;
    }
    MUSTECR( movpos_change(move,poscomb,ms,reservation) );
  } else {
    MUSTECRPREDICT( safety_movposchange(move, poscomb, ci->xarg & CIXF_U,
					CMDPPC) );
  }

  return 0;
}

static int cmd_movfeat(ParseState *ps, const CmdInfo *ci) {
  Segment *move;
  const SegmentInfo *movei;
  const MovFeatInfo *mfi;
  MovPosComb target;
  long pos_l;

  MUSTECR( ps_needsegment(ps,&move,&movei) );
  mfi= some_needword_lookup_counted(ps, movei->movfeats, movei->n_movfeats,
				    "moveable feature name");
  if (!mfi) return EC_BadCmd;

  MUSTECR( ps_neednumber(ps,&pos_l,0,mfi->posns-1,"position number") );

  if (move->motion)
    target= movpos_change_intent(move->motion);
  else
    target= move->movposcomb;

  if (!SOMEP(target)) {
    if (movei->n_movfeats > 1)
      ouprintf("info movfeat-collapsing-unknown %s :"
	       " will set previously-unknown, but not set,"
	       " feature(s) to position 0\n",
	       movei->pname);
    target= 0;
  }
  target= movposcomb_feature_update(mfi,target,pos_l);

  return cmd_route_movfeat(ps,ci, move,movei,target);
}

static int cmd_route(ParseState *ps, const CmdInfo *ci) {
  Segment *move;
  const SegmentInfo *movei;
  MovPosComb poscomb;
  long poscomb_l;
  
  MUSTECR( ps_needsegmentperhaps(ps,&move,&movei) );
  MUSTECR( ps_needword(ps) );
  if (CTYPE(isdigit,*ps->thisword)) {
    if (!move) return badcmd(ps,"invalid movement specification");
    ps_pushbackword(ps);
    MUSTECR( ps_neednumber(ps,&poscomb_l,0,movei->n_poscombs,
			   "position number") );
    poscomb= poscomb_l;
  } else {
    Segment *back, *fwd;
    back= move;
    MUSTECR( ps_needsegment(ps,&move,&movei) );
    MUSTECR( ps_needsegmentperhaps(ps,&fwd,0) );
    MUSTECR( movpos_findcomb_bysegs(back,move,fwd,move->movposcomb,&poscomb) );
  }

  return cmd_route_movfeat(ps,ci, move,movei,poscomb);
}

static int cmd_speed(ParseState *ps, const CmdInfo *ci) {
  long speed;
  Train *tra;
  const DirectionInfo *di= 0;
  
  MUSTECR( ps_needtrain(ps,&tra) );
  MUSTECR( ps_neednumber(ps,&speed,0,126,"speed step") );
  if (ps->remain) {
    di= some_needword_lookup_counted(ps,directioninfos,2,"direction");
    if (!di) return EC_BadCmd;
  }
  MUSTECR( ps_neednoargs(ps) );

  if (di && di->backwards != tra->backwards) {
    ouprintf("ack %s CommandPreconditionsViolated direction\n", ci->name);
    return EC_BadCmd;
  }

  if (ci->xarg & CIXF_FORCE) {
    actual_speed(tra,speed);
  } else {
    MUSTECRPREDICT( speedmanager_speedchange_request(tra,speed,CMDPPC) );
  }

  return 0;
}

static int cmd_invert(ParseState *ps, const CmdInfo *ci) {
  Segment *seg;

  actual_inversions_start();
  while (ps->remain) {
    MUSTECR( ps_needsegment(ps,&seg,0) );
    seg->seg_inverted ^= 1;
    actual_inversions_segment(seg);
  }
  actual_inversions_done();

  return 0;
}

static int cmd_direction(ParseState *ps, const CmdInfo *ci) {
  Train *tra;
  int backwards;
  const DirectionInfo *di;

  MUSTECR( ps_needtrain(ps,&tra) );

  di= some_needword_lookup(ps,directioninfos,"direction");
  if (!di) return EC_BadCmd;

  backwards= di->change ? !tra->backwards : di->backwards;
    
  MUSTECR( ps_neednoargs(ps) );
  
  MUSTECRPREDICT( safety_setdirection(tra,backwards,CMDPPC) );
  return 0;
}

/*---------- general machinery and the command table ----------*/ 
 
void command_doline(ParseState *ps, CommandInput *cmdi_arg) {
  int r;
  const char *cmdline;

  simlog("command-in %s\n",ps->remain);
  simlog_flush();
  current_cmd= 0;

  debug_count_event("command");

  cmdline= ps->remain;
  if (!cmdline[0]) return;
  r= ps_word(ps);  assert(!r);
  current_cmd= some_lookup(ps,toplevel_cmds);
  if (!current_cmd) {
    ouprintf("nak UnknownCommand\n");
    return;
  }
  ouprintf("executing %s\n",cmdline);
  if (sta_state < Sta_Run && !(current_cmd->xarg & CIXF_ANYSTA)) {
    ouprintf("ack %s InvalidState : layout not ready\n",current_cmd->name);
    return;
  }
  r= current_cmd->fn(ps,current_cmd);
  switch (r) {
  case 0:  ouprintf("ack %s ok\n",current_cmd->name);                  break;
  case EC_BadCmd:                                                      break;
  default: ouprintf("ack %s %s\n",current_cmd->name,errorcodelist[r]); break;
  }
}

const CmdInfo toplevel_cmds[]= {
  { "!pic",       cmd_pic,        CIXF_ANYSTA|CIXF_FORCE   },
  { "!nmra",      cmd_nmra,       CIXF_ANYSTA              },
  { "noop",       cmd_noop,       CIXF_ANYSTA              },
  { "route",      cmd_route                                },
  { "route+",     cmd_route,      1                        },
  { "route++",    cmd_route,      2                        },
  { "movfeat",    cmd_movfeat                              },
  { "movfeat+",   cmd_movfeat,    1                        },
  { "movfeat++",  cmd_movfeat,    2                        },
  { "!route",     cmd_route,      CIXF_ANYSTA|CIXF_FORCE   },
  { "!movfeat",   cmd_movfeat,    CIXF_ANYSTA|CIXF_FORCE   },
//{ "autopoint",  cmd_autopoint                            },
  { "!invert",    cmd_invert,     CIXF_ANYSTA|CIXF_FORCE   },
  { "speed",      cmd_speed                                },
  { "!speed",     cmd_speed,      CIXF_ANYSTA|CIXF_FORCE   },
  { "direction",  cmd_direction                            },
  { 0 }
};
