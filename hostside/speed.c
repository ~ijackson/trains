/*
 * realtime
 * speed manager
 */

#include "realtime.h"
#include "nmra.h"

void actual_speed(Train *tra, int step) {
  Nmra n;
  enco_nmra_speed126(&n, tra->addr, step, tra->backwards);
  ouprintf("train %s speed commanding %d\n", tra->pname, step);
  retransmit_urgent_requeue(&tra->speed.rn, &n);
}

static void xmit(Train *tra) {
  actual_speed(tra,tra->speed.step);
}

static void decel_done(TimeoutEvent *toev) {
  Train *tra= (void*)((char*)toev - offsetof(Train, speed.decel));
  struct timeval tnow;
  ErrorCode ec;
  
  tra->speed.speed= tra->speedcurve[tra->speed.step];

  DPRINTF(speed,core, "decel_done %s step %d speed %f\n",
	  tra->pname, tra->speed.step, tra->speed.speed);
  mgettimeofday(&tnow);

  ec= predict(tra,tnow, PREDF_OLDPLAN, 0,0, 0, 0,(char*)"deceleration done");
  assert(!ec);
}

static const SpeedRange *stop_info(Train *tra, double speed) {
  int i;
  for (i=0; i<tra->n_speedregimes; i++)
    if (speed <= tra->speedregimes[i].speed + UNMARGIN_ROUNDING)
      return &tra->speedregimes[i];
  abort();
}

static double current_speed(Train *tra, const struct timeval tnow) {
  double v1, v2;
  double v1sq, v2sq, vtsq;
  double left_to_go, ts_v2, cs;
  
  DPRINTF(speed,query, "current? %s decel.running=%d speed=%f"
	  " step=%d\n", tra->pname, 
	  tra->speed.decel.running, tra->speed.speed, tra->speed.step);

  if (!tra->speed.decel.running) return tra->speed.speed;

  left_to_go= (tra->speed.decel.abs.tv_sec  - tnow.tv_sec) * 1000.0 +
              (tra->speed.decel.abs.tv_usec - tnow.tv_usec) * 0.001;

  if (left_to_go <= 0) {
    DPRINTF(speed,query, "current?  decel-done\n");
    /* we don't cancel the timeout because we need the callback to lay out
     * the train again at the lower speed */
    return tra->speed.speed;
  }

  v2= tra->speed.speed;
  v1= tra->speedcurve[tra->speed.step];
  assert(v2 >= v1);

  ts_v2= stop_info(tra,v2)->ts;
  v1sq= v1*v1;
  v2sq= v2*v2;
  vtsq= v1sq + (v2sq-v1sq) * left_to_go / ts_v2;
  cs= sqrt(vtsq);
  DPRINTF(speed,query, "current?  decel-inprogress v2=%f v1=%f"
	  " ts_v2=%f ltg=%f  returning %f\n",
	  v2,v1, ts_v2, left_to_go, cs);
  return cs;
}

static double stoppingdistance(Train *tra, struct timeval tnow,
			       const SpeedRange *regime, double v) {
  double xs;

  if (v==0) return 0;

  xs= tra->speedregimes[tra->n_speedregimes-1].xs;

  double v_ts_vcrit= v * regime->ts;  if (xs > v_ts_vcrit) xs= v_ts_vcrit;
  double xs_vcrit= regime->xs;        if (xs > xs_vcrit)   xs= xs_vcrit;

  return xs + v * MARGIN_STOPTIME;
}

void speedmanager_getinfo(Train *tra, struct timeval tnow, SpeedInfo *ir) {
  double vnow= current_speed(tra,tnow);

  ir->max_speed_estimate= vnow * MARGIN_SPEED;
  ir->stopping_distance= stoppingdistance(tra,tnow, stop_info(tra,vnow), vnow);
  ir->stopping= !tra->speed.step;

  DPRINTF(speed,query, "getinfo? %s  max=%f stopdist=%d %c\n", tra->pname,
	  ir->max_speed_estimate, ir->stopping_distance,
	  "-s"[ir->stopping]);
}

int speedmanager_stopped(Train *tra) {
  return tra->speed.speed == 0;
}  

static ErrorCode request_core(Train *tra, int step, struct timeval tnow,
			      unsigned predictflags,
			      PredictionProblemCallback *ppc, void *ppcu) {
  /* We ignore the value of PREDF_OLDPLAN in predictflags, as we can tell
   * whether the old plan is good by the change in speed */
  ErrorCode ec, ec2;
  double vnow, vtarg, vmax;
  SpeedInfo try;
  const SpeedRange *regime;
  Distance oldmaxinto;

  DPRINTF(speed,core, "request %s%s step %d\n",
	  tra->backwards?"-":"",tra->pname,
	  step);

  if (step == tra->speed.step)
    return 0;

  vnow= current_speed(tra,tnow);
  vtarg= tra->speedcurve[step];
  oldmaxinto= tra->maxinto;
  
  DPRINTF(speed,core, " request  vnow=%f vtarg=%f\n", vnow,vtarg);

  vmax= MAX(vtarg,vnow);
  try.max_speed_estimate= vmax * MARGIN_SPEED;
  regime= stop_info(tra,vmax);
  try.stopping_distance= stoppingdistance(tra,tnow, regime, vmax);
  try.stopping= !step;

  if (!try.stopping) {
    Segment *fdet= tra->foredetect;
    tra->maxinto= fdet->i->poscombs[fdet->movposcomb].dist;
    assert(tra->maxinto >= oldmaxinto);
    tra->uncertainty += tra->maxinto - oldmaxinto;
  }

  predictflags &= ~PREDF_OLDPLAN;

  ec= predict(tra,tnow,
	      predictflags | (step <= tra->speed.step ? PREDF_OLDPLAN : 0),
	      0,0, &try, ppc,ppcu);
  if (ec) {
    tra->uncertainty -= tra->maxinto - oldmaxinto;
    tra->maxinto= oldmaxinto;
    ec2= predict(tra,tnow, predictflags | PREDF_OLDPLAN, 0,0, 0,
		 0,(char*)"abandoned acceleration");
    assert(!ec2);
    return ec;
  }

  toev_stop(&tra->speed.decel);
  tra->speed.step= step;
  tra->speed.speed= vmax;

  if (vtarg <= vnow) {
    tra->speed.decel.duration= regime->ts;
    toev_start(&tra->speed.decel);
  }
  report_train_position(tra);
  xmit(tra);
  return 0;
}

ErrorCode speedmanager_speedchange_request(Train *tra, int step,
                           PredictionProblemCallback *ppc, void *ppcu) {
  struct timeval tnow;
  assert(ppc || ppcu);

  MUSTECR( safety_checktrain(tra,ppc,ppcu) );
  mgettimeofday(&tnow);
  return request_core(tra,step, tnow,0, ppc,ppcu);
}

void speedmanager_safety_stop(Train *tra, struct timeval tnow,
			      unsigned predictflags) {
  ErrorCode ec;
  ec= request_core(tra,0,tnow,predictflags, 0,(char*)"emergency stop");
  assert(!ec);
}

void speedmanager_reset_train(Train *tra) {
  tra->speed.step= 0;
  toev_init(&tra->speed.decel);
  tra->speed.decel.callback= decel_done;
  tra->speed.decel.pclass= "decel";
  tra->speed.decel.pinst= tra->pname;
  tra->speed.speed= 0;
  if (tra->addr < 0)
    return;

  actual_speed(tra, 0);
}

void speedmanager_init(void) {
  Nmra n;
  TRA_IV;
  FOR_TRA {
    enco_nmra_idle(&n);
    retransmit_urgent_queue_relaxed(&tra->speed.rn, &n);
  }
}
