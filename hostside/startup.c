/*
 * realtime
 * startup state machine
 */

#include "realtime.h"

const char *const stastatelist[]= DEFINE_STASTATE_DATA;
StartupState sta_state;

static TimeoutEvent sta_toev= { .pclass="startup", .pinst="sta" };

static PicInsn piob;
static void sta_goto(StartupState new_state);

/*---------- ping ----------*/

static int pong_seq;
static TimeoutEvent ping_toev= { .pinst="ping" };

static void timedout_ping(TimeoutEvent *toev) {
  assert(sta_state >= Sta_Ping);
  sta_goto(Sta_Off);
}

static void timefor_ping(TimeoutEvent *toev) {
  enco_pic_ping(&piob, pong_seq ^ PING_PONG_PATTERN);
  serial_transmit(&piob);
  ping_toev.callback= timedout_ping;
  ping_toev.pclass= "startup";
  toev_start(&ping_toev);
}

static void initial_ping(void) {
  struct timeval now;
  
  mgettimeofday(&now);
  pong_seq= (now.tv_sec & 0x1fU) << 5; /* bottom 5bi of secs: period 32s */
  pong_seq |= (now.tv_usec >> 15);      /* top 5bi of 20bi us: res.~2^15us */
  ping_toev.duration=  300;
  timefor_ping(0);
}

/*---------- watchdog ----------*/

static TimeoutEvent watchdog_toev= {
  .pclass=toev_fast_pclass,
  .pinst="watchdog",
  .duration= UNMARGIN_WATCHDOG - MARGIN_WATCHDOG,
};
static PicInsn watchdog_piob;

static void watchdog_transmit(TimeoutEvent *toev) {
  check_rusage_check(0);
  toev_start(&watchdog_toev);
  serial_transmit(&watchdog_piob);
}

static void watchdog_start(void) {
  check_rusage_baseline();
  watchdog_toev.callback= watchdog_transmit;
  enco_pic_watchdog(&watchdog_piob, UNMARGIN_WATCHDOG_16);
  watchdog_transmit(0);
}

static void watchdog_stop(void) {
  if (!watchdog_toev.running) return;
  toev_stop(&watchdog_toev);
  enco_pic_watchdog(&watchdog_piob, 0);
  serial_transmit(&watchdog_piob);
  check_rusage_check(1);
}

/*---------- main startup algorithm ----------*/

static void timedout_onward(TimeoutEvent *toev) {
  assert(sta_state != Sta_Run);
  if (sta_state == Sta_Settling) {
    enco_pic_off(&piob);
    serial_transmit(&piob);
  }
  sta_goto(sta_state == Sta_Flush ? Sta_Ping : sta_state + 1);
}

static void sta_startup_manual(void) {
  waggle_startup_manual();
  retransmit_start();
  ouprintf("stastate %s\n", stastatelist[sta_state]);
}

void sta_startup(void) {
  if (sta_state == Sta_Manual) sta_startup_manual();
  else sta_goto(Sta_Flush);
}

void sta_finalising_done(void) { sta_goto(Sta_Run); }

static void sta_goto(StartupState new_state) {
  toev_stop(&sta_toev);
  sta_toev.callback= timedout_onward;
  sta_toev.duration= -1;

  if (new_state < Sta_Ping) {
    toev_stop(&ping_toev);
  } else if (new_state == Sta_Ping) {
    initial_ping();
  } else {
    assert(sta_state >= Sta_Ping);
  }

  switch (new_state) {
  case Sta_Flush:      sta_toev.duration=   300;   break;
  case Sta_Off:                                    break;
  case Sta_Manual:                                 abort();
  case Sta_Ping:                                   break;
  case Sta_Settling:   sta_toev.duration=   750;   break;
  case Sta_Resolving:  sta_toev.duration=   500;   break;
  case Sta_Finalising:                             break;
  case Sta_Run:                                    break;
  case Sta_Crashed: abort();
  }

  if (new_state < Sta_Run)
    choreographers_all_abandon();
  if (new_state < Sta_Finalising) {
    safety_abandon_run();
    motions_all_abandon();
  }

  piob.l= 0;
  switch (new_state) {
  case Sta_Flush:                                                  break;
  case Sta_Off:   if (sta_state > Sta_Ping) enco_pic_off(&piob);   break;
  case Sta_Manual:                                                 abort();
  case Sta_Ping:                                                   break;
  case Sta_Settling:   waggle_settle();     enco_pic_off(&piob);   break;
  case Sta_Resolving:
    resolve_begin();
    points_turning_on();
    adjuncts_start_xmit();
    enco_pic_on(&piob);
    break;
  case Sta_Finalising:
    if (resolve_complete() <0)
      /* in this case, we get stuck - user has to power cycle the layout */
      return;
    /* resolve_motioncheck will move us to Run eventually, we hope */
    break;
  case Sta_Run:
    if (!simulate)
      persist_install();
    movpos_reportall();
    retransmit_start();
    break;
  case Sta_Crashed: abort();
  }
  if (piob.l) serial_transmit(&piob);

  if (new_state >= Sta_Run && !disable_watchdog) watchdog_start();
  else watchdog_stop();
     
  toev_start(&sta_toev);
  sta_state= new_state;

  /* notify various people: */
  ouprintf("stastate %s\n", stastatelist[sta_state]);
  /* ... add others here. */
  if (sta_state == Sta_Finalising) resolve_motioncheck();
}   

void serial_moredata(PicInsn *buf) {
  const PicInsnInfo *pii;
  int obj, v, suppress;
  Byte *ep;

  /* Called when more data is received from PICs.
   * On entry, buf->l is amount of data available.
   * Does one of the following:
   *  - determines that there is no complete message; sets buf->l = 0
   *  - finds and handles one PicInsn message, sets buf->l = message length
   *  - handles some series of bytes structured some other way,
   *       sets buf->l to the numer of bytes handled.
   */
  assert(buf->l > 0);
  
  if (sta_state == Sta_Flush) {
    ouhex("picioh in junk", buf->d, buf->l);
    toev_start(&sta_toev);
    return; /* junk absolutely everything */
  }
  if (PICMSG_AAARGH_P(buf->d[0])) {
    ouhex("picioh in aaargh", buf->d, buf->l);
    die("PIC sent us AAARGH!");
  }
  if (PICMSG_HELLO_P(buf->d[0])) {
    ouhex("picioh in hello", buf->d, 1);
    if (sta_state != Sta_Manual)
      sta_goto(Sta_Flush);
    buf->l= 1;
    return;
  }
  if (sta_state == Sta_Off) {
    ouhex("picioh in off", buf->d, 1);
    buf->l= 1;
    return;
  }

  assert(sta_state >= Sta_Manual);
  /* So, we are expecting properly formed messages. */
  
  for (ep= buf->d; ep < buf->d + buf->l; ep++)
    if (!(*ep & 0x80u))
      goto found_end;

  if (buf->l == sizeof(buf->d)) {
    ouhex("picioh in toolong", buf->d, buf->l);
    die("PIC sent packet too long");
  }
  buf->l= 0; /* message not yet finished, so consume nothing */
  return;

 found_end:
  /* Aha! */
  buf->l= ep - buf->d + 1;
  picinsn_decode(buf, pic_reply_infos, &pii, &obj, &v);
  suppress= pii && pii->noiselevel > picio_send_noise;

  if (!suppress && picio_send_noise >= 2)
    ouhex_nosim("picioh in msg", buf->d, buf->l);

  if (simlog_full || sta_state < Sta_Settling ||
      !((pii->opcode==PICMSG_NMRADONE && obj==1) ||
	(pii->opcode==PICMSG_PONG && obj==pong_seq)))
    simlog_serial(buf->d, buf->l);
  
  if (!pii) { ouprintf("picio in unknown\n"); return; }
  if (!suppress)
    oupicio("in",pii,obj,v,ouprintf_only);
  pii->input_fn(pii,buf,obj);
}

void on_pic_nul(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  /* layout turned off, probably */
  if (sta_state == Sta_Manual) return;
  enco_pic_off(&piob);
  serial_transmit(&piob);
  sta_goto(Sta_Flush);
  return;
}

void on_pic_pong(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  if (sta_state == Sta_Manual)
    return;

  if (objnum != pong_seq) {
    if (objnum == (pong_seq^PING_PONG_PATTERN))
      die("PIC connection is looped back (ping %#05x bounced)", objnum);
    die("PIC sent wrong ping response (%#05x, wanted %#05x)", objnum, pong_seq);
  }

  ping_toev.duration= 1000;
  ping_toev.callback= timefor_ping;
  ping_toev.pclass= toev_fast_pclass;
  toev_start(&ping_toev);

  if (sta_state == Sta_Ping)
    sta_goto(Sta_Settling);
}

void on_pic_fault(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  if (sta_state <= Sta_Ping) return;
  die("FAULT");
}

void on_pic_wtimeout(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  if (sta_state <= Sta_Settling) return;
  check_rusage_check(1);
  die("microcontrollers' watchdog timer triggered\n");
}

void on_pic_hello(const PicInsnInfo *pii, const PicInsn *pi, int objnum)
  { abort(); }
void on_pic_aaargh(const PicInsnInfo *pii, const PicInsn *pi, int objnum)
  { abort(); }
void on_pic_spurious(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  ouprintf("warning spurious %d : spurious short circuit (fault)"
	   " detection interrupts\n", objnum);
}
void on_pic_pointed(const PicInsnInfo *pii, const PicInsn *pi, int objnum) { }
void on_pic_retriable(const PicInsnInfo *pii, const PicInsn *pi, int objnum){}

static int coalescing_detects;

static void detect_report_now(Segment *seg) {
  ouprintf_only("detect %s %d\n", seg->i->pname, seg->detect_actual);
  seg->detect_reported= seg->detect_actual;
}

static SegmentNum on_pic_detect_prep(int detyn, int objnum) {
  SegmentNum segn;

  if (objnum >= info_segmentmaplen ||
      (segn= info_segmentmap[objnum]) < 0)
    die("PIC sent detect%d @#%#x not in map",detyn,objnum);

  Segment *seg= &segments[segn];
  seg->detect_actual= detyn;

  if (!(picio_send_noise <= 1 &&
	seg->owner &&
	seg->det_ignore)) {
    if (cmdi.out.total >= 1024) coalescing_detects= 1;

    if (coalescing_detects &&
	seg->det_ignore &&
	seg->detect_reported) {
      assert(seg->detect_flaps < INT_MAX);
      seg->detect_flaps++;
    } else {
      detect_report_now(seg);
    }
  }

  return segn;
}

void cmdi_output_bufferempty(OutBufferChain *obc) {
  SEG_IV;
  
  if (!coalescing_detects) return;
  coalescing_detects= 0;

  FOR_SEG {
    if (seg->detect_flaps) {
      ouprintf_only("detect-flaps %s %d\n", segi->pname, seg->detect_flaps);
      seg->detect_flaps= 0;
    }
    if (seg->detect_actual != seg->detect_reported) {
      detect_report_now(seg);
    }
  }
}

void on_pic_detect1(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  SegmentNum segn;
  Segment *seg;
  const char *pname;
  
  segn= on_pic_detect_prep(1,objnum);
  seg= &segments[segn];
  pname= info_segments[segn].pname;
  
  switch (sta_state) {
  case Sta_Flush:
  case Sta_Off:
  case Sta_Manual:
  case Sta_Ping:
  case Sta_Settling:
    break;
  case Sta_Resolving:
    seg->res_detect= 1;
    break;
  case Sta_Finalising:
    if (!seg->res_detect) {
      ouprintf("resolution new-detection-in-finalising @%s\n", pname);
      sta_goto(Sta_Settling);
    }
    break;
  case Sta_Run:
    safety_notify_detection(seg);
    break;
  case Sta_Crashed: abort();
  }
}

void on_pic_detect0(const PicInsnInfo *pii, const PicInsn *pi, int objnum) {
  on_pic_detect_prep(0,objnum);
}

void choreographers_all_abandon(void) { }
