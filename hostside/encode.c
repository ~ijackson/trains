/*
 * arranges for the definitions of
 *  enco_nmra_WHATEVER
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "nmra.h"

#define CONST(...)				\
  static const Byte m[]= { __VA_ARGS__ };	\
  memcpy(c,m,sizeof(m));			\
  c+=sizeof(m)

#define FUNCS(v)					\
  ADDR;							\
  nmra_errchk(cn, bitmap, !(bitmap & ~0x1fffu));	\
  *c++= (v)

#define nmra_errchk(cn, v, truth) \
  if (!(truth)) nmra_errchk_fail(cn, v, #v, #truth);

static void nmra_errchk_fail(const char *m, long v,
			     const char *vn, const char *truth) {
  fprintf(stderr,"nmra encode %s: %s %ld (0x%lx) fails condition %s\n",
	  m, vn, (unsigned long)v, (unsigned long)v, truth);
  exit(15);
}
  
#define ADDR     c= encode_addr(cn, addr, c);

static Byte *encode_addr(const char *cn, int addr, Byte *ap) {
  /* encodes decoder address */
  nmra_errchk(cn, addr, addr>0 && addr<=0x3ff);
  if (addr < 0x3f) {
    /* Short addresses: S9.2 B l.41 which is the same as RP9.2.1 C l.65
     * first sentence. */
    *ap++= addr;
  } else {
    /* Long addresses: RP9.2.1 C l.65-69. */
    *ap++= addr;
    *ap++= (addr >> 8) | 0xc0;
  }
  return ap;
}

#define Aint(v,i)    , int v
#define Abitmap(v,i) , unsigned v
#define Abyte(v,i)   , Byte v
#define Anone

#define NMRA(n,al,body)				\
void enco_nmra_##n(Nmra *p al) {		\
  static const char cn[]= #n;			\
  Byte *c= p->d;				\
  (void)cn;					\
  do { body } while(0);				\
  p->l= c - p->d;				\
  assert(p->l <= sizeof(p->d));			\
}

#include "nmra-packets.h"
