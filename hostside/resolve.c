/*
 * resolve detected trains into initial state
 */

#include "realtime.h"

#define NOOP (void)0

/*---------- main resolution processing ----------*/
 /*
  * Algorithm:
  *
  *  Notation and background:
  *
  *	S			set of segments
  *	D \subset S		set of segments where we have detection
  *
  *	T			set of trains
  *	H(t) \subset S		home range for train t \elem T
  *	E(t) \subset S		set of segments where train t last expected
  *
  *	H(t) \disjoint H(t')   for t != t'
  *	E(t) \disjoint E(t')   for t != t'
  *	but not necessarily E(t) \disjoint H(t')
  *
  *  We want to find a mapping R(t)
  *	R(t) \elem { N, E, H }		t \elem T,  N(t') = \null
  *  giving
  *	A(t) = (R(t))(t)		segments allocated to train t
  *      U = \union_{t} A(t)
  *  satisfing
  *      A(t) \disjoint A(t')		  for t != t'	`disjoincy'
  *      D \subset U					`coverage'
  *  and the solution is somehow optimal or at least not badly suboptimal.
  *
  * We compute R incrementally, maintaining disjoincy,,
  * while increasing U.  At each stage R is an optimal solution
  * to the problem posed by D' = U, and we maintain this invariant.
  *
  * We define an optimality ordering on R by counting occurrences of
  * H, E, and H in the range, in that order.  Ie,
  *      Count(R,x) = |{ t: R(t) = x }|
  *	Rank(R) = Count(R,H) * (|T|+1) + Count(R,E)
  * and compare Ranks numerically, lower being better.
  *
  * So we mean that for any incrementally computed R, there is no
  * R' satisfying the same subproblem D' = U for which Rank(R') < Rank(R).
  *
  * For the benefit of client programs, we record the key elements of
  * the proofs that we generate for non-N values of R.
  *
  *  1.	Initially we set \all_{t} R(t) = N.
  *
  *	Optimality: This is minimal for U = \null, as Rank(R) = 0.
  *
  *  2.	Then we search for lack of coverage, ie violations of D \subset U.
  *	If none are found we are done.
  *
  *  3.  Let  d \elem D  but  d !\elem U.
  *	We will now calculate new R which we will call R2
  *	giving new U which we will call U2.
  *
  *  3a. Hope that  \exists{t} st d \elem E(t)  with
  *      E(t) \disjoint U  and  R(t) = N.
  *	If so set R2(t) = E giving a solution to U2.
  *
  *      Optimality: if d \elem D then we need U2 to include d.
  *      That means d \elem E(t) or d \elem H(t').  No E(t')
  *	other than E(t) can help since they are disjoint, and
  *	we would certainly prefer adding E(t) to adding H(t').
  *	(Adding H(t') and removing some other H(t'') doesn't
  *	work either because the Hs are disjoint, so no
  *	H can stand in for any other.)
  *	
  *	So the rank increase by 1 is is minimal for U2.
  *
  *	Proof elements: R(t) = E is demonstrated by
  *	every d meeting these conditions.
  *
  *  3b.	Alternatively, hope that  d \elem H(t')
  *
  *	If so set  R2(t') = H
  *		   \all_{t+} R2(t+) = N where R(t+) = E.
  *
  *	Optimality: in the case we are now dealing with, we
  *	didn't succeed with the test for 3a, so either:
  *
  *      (i)  There is no t where d \elem E(t), so R(t') = H is
  *           essential because no solution without R(t') = H has d \elem U2.
  *
  *      (ii) There is t where d \elem E(t) but R(t) = H.
  *           We have therefore already proved that R(t) cannot be E.
  *
  *      (iii) There is t where d \elem E(t) but E(t) !\disjoint U
  *	     In this case, consider a clash d' between E(t) and U
  *	         d' \elem U,  d' \elem E(t)
  *
  *	     This clash at d' is preventing us covering d with E(t).
  *	     E's can't clash since they are disjoint so it must be
  *	     a clash with some H.  But we have no unavoidable H's
  *	     in our solution to U, so this solution to U2 has no
  *	     unavoidable H's either.
  *
  *	     Or to put it algebraically,
  *	      d' != d, because d !\elem U.
  *	      d' \elem A(t'') for some t'' since d' \elem U.
  *	      If R(t'') = E, d' \elem E(t'') but E's are disjoint.
  *            So R(t'') = H, which must have been unavoidable by our
  *	      inductive premise.  Thus for U2, R2(t'') = H is also
  *	      unavoidable.
  *
  *	Proof elements: R(t') = H is demonstrated by
  *	every d meeting these conditions
  *	together with for each such d
  *	    the alternative train t if applicable
  *	    (there can be only one)
  *	plus in case (iii)
  *	    every clash d' between E(t) and U
  *	    plus for each such d' the corresponding t''
  *          (we record all this indexed by t so we can reuse it
  *           if needed)
  *
  *	R2 consists only of Hs and Ns.  All of the Hs are necessary
  *	for U2 by the above, so R2 is minimal for U2.
  *
  *	The rank is increased: we increase Count(R,H) and set
  *	Count(R,E) to 0.
  *
  *  3c. If neither of these is true, we are stuck and cannot
  *	satisfy d.  This is an error.  We remove d from D
  *	and continue anyway to see if we can find any more.
  *
  *	Proof elements: Lack of solution is demonstrated
  *	separately by every such  d  together with
  *	for each d if d \elem E(t) for some t
  *	    that t
  *	    plus the clashes d'' as for 3b(iii).
  *
  *  Termination: at each iteration 3a or 3b, we strictly increase Rank.
  *  At each iteration 3c we reduce D.
  *  Rank cannot exceed |T|*(|T|+1) and D starts out finite. So
  *  eventually we must succeed or fail.
  *
  */

/* values for Train->resolution: */
#define RR_N 0u
#define RR_E 1u
#define RR_H 2u
#define RESOLUTION_CHARS "NEH"

/* During the main algorithm:
 *  We record R in tra->resolution,
 *  U in segi->mark0 and D in segi->res_detect
 */
#define iselem_u mark0

static int resmain_getmovpos(TrackLocation *t, TrackAdvanceContext *c,
			     MovPosComb *use_io) {
  Segment *d= t->seg;
  const char *pn= d->i->pname;

  if (d->home && d->home->resolution == RR_H) {
    *use_io= 0; /* a bit kludgey */
    DPRINTF(resolving,main, "getmovpos %s H\n", pn);
  } else if (d->owner) {
    *use_io= d->movposcomb;
    DPRINTF(resolving,main, "getmovpos %s E %d\n", pn, *use_io);
  } else if (!d->res_movposset) {
    *use_io= -1;
    DPRINTF(resolving,main, "getmovpos %s N -1\n", pn);
  } else if (d->motion) {
    *use_io= movpos_change_intent(d->motion);
    DPRINTF(resolving,main, "getmovpos %s M %u\n", pn, *use_io);
  }
  return 0;
}

static MovPosComb just_getmovpos(Segment *d, TrackAdvanceContext *tac) {
  MovPosComb target;
  TrackLocation tloc;
  int r;

  tloc.seg= d;
  r= resmain_getmovpos(&tloc,tac,&target);  assert(!r);
  return target;
}

static int resolve_complete_main(void) {
  int problems, phase, nextphase;
  TRAIN_ITERVARS(t);
  TRAIN_ITERVARS(t2);
  TRAIN_ITERVARS(tplus);
  SEGMENT_ITERVARS(d);
  SEGMENT_ITERVARS(d1);
  SEGMENT_ITERVARS(dplus);

  problems= 0;
  FOR_TRAIN(t,NOOP,NOOP) t->resolution= RR_N;

  for (phase=0; phase<3; phase=nextphase) {
    nextphase= phase+1;
    DPRINTF(resolving,alg, "iteration %c\n", "EHX"[phase]);

    DPRINTF(resolving,alg, " calculate-u\n");

    FOR_SEGMENT(d,NOOP,NOOP) { /* calculate U */
      unsigned updated= 0;

#define ADDTO_U_EH(homeowner,HH_HE,string)				\
      if (d->homeowner && d->homeowner->resolution == HH_HE) {		\
	DPRINTF(resolving,alg, "  covered %s " string " %s\n",	\
		di->pname, d->homeowner->pname);			\
	updated++;							\
      }
      ADDTO_U_EH(home,  RR_H, "at-home");
      if (d->movposcomb >= 0)
	ADDTO_U_EH(owner, RR_E, "as-expected");

      assert(updated<=1);
      d->iselem_u= updated;
    }

    DPRINTF(resolving,alg, " searching\n");
    FOR_SEGMENT(d, NOOP, NOOP) {
      if (!(d->res_detect && !d->iselem_u))
	continue;
      /* 3. we have a violation of D \subset U, namely d */

      DPRINTF(resolving,alg, "  violation %s\n", di->pname);

      if (d->owner) { /* 3a perhaps */
	t= d->owner;
	DPRINTF(resolving,alg, "   expected %s\n", t->pname);

	if (t->addr < 0) {
	  DPRINTF(resolving,alg, "    expected-is-not-addressable\n");
	  goto not_3a;
	}

	if (t->resolution == RR_H) {
	  DPRINTF(resolving,alg, "    expected-is-at-home\n");
	  goto not_3a;
	}

	if (d->movposcomb < 0) {
	  DPRINTF(resolving,alg, "    track-unknown-position\n");
	  goto not_3a;
	}

	if (t->resolution == RR_E)
	  /* we added it in this sweep and have found another d */
	  goto already_3a;
	assert(t->resolution == RR_N);

	/* check E(t) \disjoint U */
	int clashes= 0;
	FOR_SEGMENT(d1, NOOP, NOOP) {
	  if (d1->owner == t && d1->iselem_u) {
	    FOR_TRAIN(t2, NOOP, NOOP) {
	      if (t2->resolution == RR_H && d1->owner == t2) {
		DPRINTF(resolving,alg, "    clash %s %s\n",
			d1i->pname, t2->pname);
		clashes++;
	      }
	    }
	  }
	}
	if (clashes) {
	  DPRINTF(resolving,alg, "   expected-has-clashes\n");
	  goto not_3a;
	}

	/* Yay! 3a. */
	t->resolution= RR_E;
	nextphase= 0;
      already_3a:
	DPRINTF(resolving,alg, "   supposing %s as-expected\n", t->pname);
	continue;
      }
    not_3a:

      if (d->home) { /* 3b then */
	if (phase<1)
	  continue;

	Train *t1= d->home; /* t' st d \elem H(t') */
	DPRINTF(resolving,alg, "   home %s\n", t1->pname);
	
	if (t1->addr < 0) {
	  DPRINTF(resolving,alg, "    home-is-not-addressable\n");
	  goto not_3b;
	}

	DPRINTF(resolving,alg, "   reset-expecteds\n");
	FOR_TRAIN(tplus, NOOP,NOOP) {
	  if (tplus->resolution == RR_E) {
	    DPRINTF(resolving,alg, "   supposing %s absent\n", tplus->pname);
	    tplus->resolution= RR_N;
	    nextphase= 0;
	  }
	}
	/* Now must trim U to correspond to our setting of R(t+)=N
	 * so that any other implied Hs are spotted. */
	FOR_SEGMENT(dplus, NOOP,NOOP) {
	  Train *tplus= dplus->owner;
	  if (!(tplus && tplus->resolution == RR_E)) continue;
	  dplus->iselem_u= 0;
	}

	t1->resolution= RR_H;
	nextphase= 0;

	DPRINTF(resolving,alg, "   supposing %s at-home\n", t1->pname);
	continue;
      }
    not_3b:

      /* Oh dear, 3c. */
      if (phase<2)
	continue;

      ouprintf("resolution inexplicable %s\n", di->pname);
      d->res_detect= 0;
      problems++;
    }
  }

  FOR_TRAIN(t,NOOP,NOOP) {
    if (t->resolution == RR_H)
      t->backwards= 0;
  }
  FOR_SEGMENT(d,NOOP,NOOP) {
    if (d->home && d->home->resolution == RR_H)
      d->tr_backwards= d->ho_backwards;
  }
    
  TrackAdvanceContext tac;
  MovPosComb target;
  tac.getmovpos= resmain_getmovpos;

  FOR_SEGMENT(d,NOOP,NOOP) {
    if (problems) return problems;

    d->iselem_u= 0;

    target= just_getmovpos(d,&tac);

    if (d->i->invertible) {
      Segment *interferer= segment_interferes_simple(&tac,d);
      if (interferer) {
	actual_inversions_start();
	d->seg_inverted= interferer->seg_inverted
	    ^ d->i->interferes_polarity_opposed;
	actual_inversions_segment(d);
	actual_inversions_done();
      }
    }

    if (d->i->n_poscombs>1) {
      if (target >= 0) {
	if (!d->moving) {
	  movpos_unreserve(d->motion);
	  d->motion= 0;
	}
	if (!d->res_movposset)
	  d->movposcomb= -1;

	ErrorCode ec= movpos_reserve(d,-1,&d->motion,target,-1);
	if (ec) {
	  ouprintf("resolution movpos-change-failed %s/%s %s\n",
		   d->i->pname, d->i->poscombs[target].pname,
		   errorcodelist[ec]);
	  problems++;
	} else {
	  d->res_movposset= 1;
	}
      }
    } else {
      assert(!d->i->n_movfeats);
      d->movposcomb= 0;
    }
  }
  return problems;
}

#undef iselem_u

/*---------- heads and tails of trains, final placement ----------*/

#define resfin_ours mark1

typedef struct {
  Distance hardallow, min, max;
  int hardwhy;
  Segment *lastdetect, *hardend;
} FindEndConstraint;

typedef struct {
  TrackAdvanceContext tc; /* first! */
  TrackLocation t;
  Train *train;
  Segment *startpoint;
  Distance atlastdetect;
  Segment *lastdetect;
  FindEndConstraint constraints[2];
  int problems, rear;
} FindEndUserContext;

static int findends_getmovpos(TrackLocation *t, TrackAdvanceContext *c,
			      MovPosComb *use_io) {
  const char *pn= t->seg->i->pname;
  if (t->seg->motion) *use_io= movpos_change_intent(t->seg->motion);
  if (*use_io<0) {
    DPRINTF(resolving,ends, "   getmovpos %s fails\n", pn);
    return 'm';
  }
  DPRINTF(resolving,ends, "   getmovpos %s -> %d\n", pn, *use_io);
  return 0;
}

static int constraint_nextseg(TrackLocation *t, struct TrackAdvanceContext *c,
			      MovPosComb *mpc, const TrackLocation *before) {
  int r;
  FindEndUserContext *u= (void*)c;
  
  DPRINTF(resolving,ends, "  constraint_nextseg %c"
	  " %s%s remain=%d dist=INF-%d det=%d ours=%d owner=%s home=%s\n",
	  "frx"[u->rear],
	  t->backwards?"-":"", t->seg->i->pname, t->remain,
	  TL_DIST_INF - c->distance,
	  t->seg->res_detect,
	  t->seg->resfin_ours,
	  t->seg->owner ? t->seg->owner->pname : "-",
	  t->seg->home ?  t->seg->home->pname  : "-");

  if (!t->seg->resfin_ours) return 'o';
  r= findends_getmovpos(t,0,mpc);  if (r) return r;

  const SegPosCombInfo *pci= &t->seg->i->poscombs[*mpc];

  if (before) {
    if (t->seg == u->startpoint) return 'l';
    const SegmentLinkInfo *rlink= &pci->link[!t->backwards];
    if (before->seg != &segments[rlink->next]) {
      DPRINTF(resolving,ends, "   j mismatch %s %s\n",
	      before->seg->i->pname, info_segments[rlink->next].pname);
      return 'j';
    }
  }
  if (t->seg->res_detect) {
    u->atlastdetect= c->distance;
    if (!before)
      /* wind back to start of this segment which is necessary if this
       * we are looking rearwards */
      u->atlastdetect += (pci->dist - t->remain);
    u->lastdetect= t->seg;
  }
  return 0;
}

static int constraint_trackend(TrackLocation *t,
			       struct TrackAdvanceContext *c) {
  return 'e';
}

static void end_startpoint(FindEndUserContext *u) {
  int r;

  u->tc.distance= TL_DIST_INF;
  u->tc.nextseg= constraint_nextseg;
  u->tc.getmovpos= findends_getmovpos;
  u->tc.trackend= constraint_trackend;
  u->tc.u= 0;

  r= trackloc_set_exactinto(&u->t, &u->tc,
			    u->startpoint, u->startpoint->tr_backwards,
			    0);
  assert(!r);
}

static void end_constraints(FindEndUserContext *u) {
  FindEndConstraint lim;
  Train *tra= u->train;
  int r;

  end_startpoint(u);
  u->tc.distance= TL_DIST_INF;

  u->lastdetect= 0;
  u->atlastdetect= -1;

  if (u->rear) {
    r= trackloc_reverse_exact(&u->t,&u->tc);
    assert(!r);
  }

  FindEndConstraint *cons= &u->constraints[u->rear];

  cons->hardwhy= trackloc_advance(&u->t,&u->tc);
  
  assert(cons->hardwhy);
  assert(u->lastdetect);
  assert(u->atlastdetect >= 0);

  Distance nose= MARGIN_NOSE +
    ((tra->backwards ^ u->rear) ? tra->head : tra->tail);

  lim.min= TL_DIST_INF - u->atlastdetect;
  lim.max= (TL_DIST_INF - u->tc.distance) - nose;

  if (!u->rear) {
    cons->min= lim.min;
    cons->max= lim.max;
  } else {
    cons->max= -lim.min + ceil(tra->detectable * MARGIN_TRAINLENGTH);
    cons->min= -lim.max + ceil(tra->detectable * MARGIN_TRAINLENGTH);
  }
  cons->lastdetect= u->lastdetect;
  cons->hardend= u->t.seg;

  if (cons->min > cons->max) {
    ouprintf("resolution implausible %s %s overhang %d %d\n",
	     cons->lastdetect->i->pname, tra->pname,
	     cons->min - cons->max, nose);
    u->problems++;
  }

  DPRINTF(resolving,ends, " lims %c %s,%s,%c dist=INF-%d"
	  " lim=%d..%d out=%d..%d\n",
	  "fr"[u->rear],
	  cons->lastdetect->i->pname, u->t.seg->i->pname, cons->hardwhy,
	  TL_DIST_INF - u->tc.distance,
	  lim.min, lim.max, cons->min, cons->max);
}

static int resolve_complete_ends_train(Train *tra) {
  SEG_IV;
  FindEndUserContext u;
  const SegPosCombInfo *pci;
  int r, DP;

  switch (tra->resolution) {
  case RR_H: break;
  case RR_E: break;
  case RR_N: return 0;
  default: abort();
  }

  DPRINTF1(resolving,ends, "%s %c",
	   tra->pname, RESOLUTION_CHARS[tra->resolution]);

  memset(&u,0,sizeof(u));
  u.startpoint= 0;
  
  FOR_SEGMENT(seg,NOOP,NOOP) {
    seg->resfin_ours= tra ==
      (tra->resolution==RR_H ? seg->home : seg->owner);
    if (!seg->resfin_ours) continue;
    DPRINTF2(" %s%s", seg->tr_backwards?"-":"", seg->i->pname);
    if (seg->res_detect) {
      DPRINTF2("*");
      u.startpoint= seg;
    }
  }
  assert(u.startpoint);
  DPRINTF2("\n");

  /* There are four pieces of information we have:
   * The rearmost detection, the rearmost end of ownership
   * the foremost detection, the foremost end of ownership
   *
   * From each we compute a range of locations.  These are
   * in the form of a minimum and maximum distance of the
   * foredetect ahead of 0 into the startpoint.
   */

  u.train= tra;
  for (u.rear=0; u.rear<2; u.rear++)
    end_constraints(&u);

  Distance wrongness;

  if ((wrongness= u.constraints[0].min - u.constraints[1].max) > 0) {
    for (u.rear=1; u.rear>0; u.rear--)
      ouprintf("resolution implausible %s %s over-detect %d\n",
	       u.constraints[u.rear].lastdetect->i->pname,
	       tra->pname, wrongness);
    u.problems++;
  }
  if ((wrongness= u.constraints[1].min - u.constraints[0].max) > 0) {
    for (u.rear=1; u.rear>0; u.rear--)
      ouprintf("resolution implausible %s %s cramped %d\n",
	       u.constraints[u.rear].hardend->i->pname,
	       tra->pname, wrongness);
    u.problems++;
  }

  int min= MAX(u.constraints[0].min, u.constraints[1].min);
  int max= MIN(u.constraints[0].max, u.constraints[1].max);
  DPRINTF(resolving,ends, " lims a %d..%d problems=%d\n",
	  min, max, u.problems);

  if (u.problems)
    return u.problems;
  
  assert(min <= max);
  assert(max > 0); /* startpoint is detected so foredet is there or later */

  /* Now we just need to turn this into the canonical format. */

  end_startpoint(&u);

  u.tc.distance= max;
  r= trackloc_advance(&u.t, &u.tc);  assert(!r);

  tra->foredetect= u.t.seg;
  tra->foredetect->tr_backwards= u.t.backwards;

  r= trackloc_getlink(&u.t, &u.tc, &pci,0,0);  assert(!r);
  tra->maxinto= pci->dist - u.t.remain;
  tra->uncertainty= max - min;

  report_train_position(tra);
  /* We don't lay the train out now since the moveable features
   * are not yet in place.  We can defer it, though, as it is guaranteed
   * to work, now. */

  return 0;
}

static int resolve_complete_ends(void) {
  TRA_IV;
  int problems;

  problems= 0;
  FOR_TRAIN(tra,NOOP,NOOP)
    problems += resolve_complete_ends_train(tra);
  return problems;
}

#undef resfin_ours
#undef resfin_done

/*---------- main resolutionentrypoints ----------*/

void resolve_begin(void) {
  SEG_IV;
  actual_inversions_start();
  FOR_SEG {
    seg->res_detect= 0;
    if (segi->invertible)
      actual_inversions_segment(seg);
    else
      seg->seg_inverted= 0;
    if (segi->n_poscombs==1)
      seg->movposcomb= 0;
  }
  actual_inversions_done();
}

int resolve_complete(void) {
  int problems;
  ErrorCode ec;
  TRA_IV;
  SEG_IV;

  problems= resolve_complete_main();
  if (!problems)
    problems += resolve_complete_ends();

  if (problems) {
    ouprintf("resolution problems %d\n",problems);
    return -1;
  }

  FOR_SEG {
    if (seg->owner && seg->owner->resolution == RR_N)
      seg->owner= 0;

    MovPosChange *reservation= seg->motion;
    if (!reservation) continue;
    seg->motion= 0;
    MovPosComb target= movpos_change_intent(reservation);
    ec= movpos_change(seg, target, -1, reservation);  assert(!ec);
  }

  FOR_TRA
    speedmanager_reset_train(tra);

  return 0;
}

void resolve_motioncheck(void) {
  ErrorCode ec;
  SEG_IV;
  TRA_IV;
  struct timeval tnow;

  assert(sta_state == Sta_Finalising);
  
  FOR_SEG
    if (seg->moving) return;

  FOR_TRA {
    if (tra->resolution == RR_N) continue;
    
    mgettimeofday(&tnow);
    tra->plan_lookahead_nsegs= INT_MAX;
    ec= predict(tra,tnow, PREDF_OLDPLAN, 0,0, 0,
		0,(char*)"resolution confirmation");
    assert(!ec);
  }

  FOR_SEG {
    if (seg->i->n_poscombs==1) continue;
    if (seg->res_movposset) continue;
    assert(!seg->motion);
    seg->movposcomb= -1;
  }

  sta_finalising_done();
}
