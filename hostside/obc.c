/*
 * daemons
 * output buffer chains
 */

#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>

#include "daemons.h"
#include "../layout/dlist.h"

struct OutBuffer {
  OutBuffer *back, *next;
  char *m;
  int l;
};

int obc_tryflush(OutBufferChain *ch) {
  OutBuffer *ob;
  int r;
  
  for (;;) {
    ob= ch->obs.head;
    if (!ob)
      return 0;
    if (ch->done_of_head == ob->l) {
      LIST_UNLINK(ch->obs, ob);
      free(ob->m);
      free(ob);
      ch->done_of_head= 0;
      continue;
    }
    r= write(ch->fd, ob->m + ch->done_of_head, ob->l - ch->done_of_head);
    if (r==-1) {
      if (errno==EINTR) continue;
      if (errno==EWOULDBLOCK) return errno;
      ch->error(ch,"write",strerror(errno));
      return errno;
    }
    assert(r>=0);
    ch->done_of_head += r;
    ch->total -= r;
    assert(ch->done_of_head <= ob->l);
  }
}

static void *writeable(oop_source *evts, int fd,
		       oop_event evt, void *ch_v) {
  OutBufferChain *ch= ch_v;
  assert(fd == ch->fd);
  assert(evt == OOP_WRITE);
  obc_tryflush(ch);
  if (evts && !ch->obs.head) {
    events->cancel_fd(events, ch->fd, OOP_WRITE);
    ch->done_of_head= -1;
    if (ch->empty) ch->empty(ch);
  }
  return OOP_CONTINUE;
}

static void addlink(OutBufferChain *ch, OutBuffer *ob,
		    CopyCallBack *ccb, void *ccbu) {
  if (ccb) ccb(ob->m,ob->l,ccbu);

  if (ch->done_of_head < 0) {
    assert(!ch->obs.head);
    if (events) /* in simulation, events==0 */
      events->on_fd(events, ch->fd, OOP_WRITE, writeable, ch);
    ch->done_of_head= 0;
  }
    
  LIST_LINK_TAIL(ch->obs, ob);
  ch->total += ob->l;
  if (ob->l>0 && ob->m[ob->l-1]=='\n')
    obc_tryflush(ch);
  if (ch->total > ch->limit) {
    char what[128];
    snprintf(what,sizeof(what)-1,"`%.*s...'", ob->l,ob->m);
    what[sizeof(what)-1]= 0;
    obc_tryflush(ch);
    ch->error(ch,"buffer limit exceeded",what);
  }
}

void obc_init_core(OutBufferChain *ch) {
  ch->done_of_head= -1;
  ch->total= 0;
  if (!ch->limit) ch->limit= 128*1024;
  LIST_INIT(ch->obs);
}

void obc_init(OutBufferChain *ch) {
  int r;
  obc_init_core(ch);
  r= oop_fd_nonblock(ch->fd, 1);
  if (r) diee("nonblock(OutBufferChain->fd,1)");
}

void ovprintf_ccb(OutBufferChain *ch, CopyCallBack *ccb, void *ccbu,
		   const char *fmt, va_list al) {
  OutBuffer *ob;

  ob= mmalloc(sizeof(*ob));
  ob->l= vasprintf(&ob->m, fmt, al);
  if (ob->l < 0) diem();
  if (!ob->l) { free(ob->m); free(ob); return; }
  addlink(ch,ob,ccb,ccbu);
}

void ovprintf(OutBufferChain *ch, const char *fmt, va_list al) {
  ovprintf_ccb(ch,0,0,fmt,al);
}

void oprintf(OutBufferChain *ch, const char *msg, ...) {
  va_list al;
  va_start(al,msg);
  ovprintf(ch,msg,al);
  va_end(al);
}

void voerror(OutBufferChain *ch, const char *fmt, va_list al) {
  oprintf(ch,"error ");
  ovprintf(ch,fmt,al);
  owrite(ch,"\n",1);
}

void owrite(OutBufferChain *ch, const char *data, int l) {
  OutBuffer *ob;
  ob= mmalloc(sizeof(*ob));
  ob->l= l;
  ob->m= mmalloc(l);
  memcpy(ob->m, data, l);
  addlink(ch,ob,0,0);
}
