/*
 * usage:
 *   .../hidrawconv-<controller> -d
 *   .../hidrawconv-<controller> -e </dev/hidrawN
 * where -a means all, and the other letters are:
 *   -d   print expected descriptor (as for hidraw-ioctl -d)
 *   -e   pretend to be evdev-manip
 *   -E   pretend to be evdev-manip, print to stderr about the device at start
 * exit status:
 *   0      all ok
 *   other  some other problem
 *
 * joystick values are always doubles from -1 to 0 to +1
 */

#include "hidrawconv.h"

void die_vprintf_hook(const char *fmt, va_list al) { }
void die_hook(void) { }

static LastReports lasts;


/*---------- helpful macros for reporter functions ----------*/

/*
 * These expect/define:
 *
 *   const uint8_t msg[], last[];
 *   int len;
 *   const EntType *ent;
 *   uint8_t mb, lb;  // bits from the message, masked but unshifted
 *
 *   typedef struct {
 *     const char *str;
 *     uint8_t mask;
 *     int pos;
 *   } EntType;
 */

#define FOR_REPORTENTRIES(ent)			\
  /* Like for(;;) */				\
  for (; (ent)->str; (ent)++)			\
    if ((ent)->pos >= len) continue;		\
    else

#define MSG_BITS(ent)  (msg[(ent)->pos] & (ent)->mask)
#define LAST_BITS(ent) (last[(ent)->pos] & (ent)->mask)
  /* uint8_t MSG_BITS(EntType *ent);
   * uint8_t LAST_BITS(EntType *ent);
   */

#define DEFINE_STANDARD_REPORTER(EntType, ent, BODY_BLOCK)		\
  void report##ent##s(const uint8_t msg[], const uint8_t last[],	\
		      int len, const EntType *ent) {			\
    FOR_REPORTENTRIES(ent) {						\
      uint8_t mb= MSG_BITS(ent);					\
      uint8_t lb= LAST_BITS(ent);					\
      if (mb==lb) continue;						\
      BODY_BLOCK							\
    }									\
  }


/*---------- reporter functions ----------*/

DEFINE_STANDARD_REPORTER(KeyBit, bit, {
    printf("%s %d\n", bit->str, !!mb);
  })

DEFINE_STANDARD_REPORTER(ValLoc, val, {
    mb >>= val->rshift;
    mb -= val->zero;
    double v= (int8_t)mb;
    v /= (v >= 0 ? 127 : 128);
    printf("%s %.5f\n", val->str, val->sign * v);
  })

DEFINE_STANDARD_REPORTER(HatLoc, hat, {
    /* nyi */
  })


/*---------- core functions ----------*/

void dispatch(LastReports *lasts, const char *message_prefix,
	      ProcessReport *const report_processors[MAXREPORTS],
	      const uint8_t *msg, int l) {
  if (!l) {
    fprintf(stderr,"%s:%s report too short\n", progname, message_prefix);
    return;
  }

  ProcessReport *pr= report_processors[msg[0]];
  Last *last= &lasts->lasts[msg[0]];
  if (!pr) {
    if (!last->len)
      fprintf(stderr,"%s:%s unexpected report 0x%02x\n",
	      progname, message_prefix, msg[0]);
    last->len= l;
    return;
  }
  if (last->len < l) {
    last->msg= mrealloc(last->msg, l);
    memset(last->msg + last->len, 0, l - last->len);
    last->len= l;
  }
  pr(msg, l, last->msg);
  memcpy(last->msg, msg, l);
}  

/*---------- main program ----------*/

static void events(int verbose) {
  uint8_t msg[MAXREPORTLEN+1];
  char phys[PATH_MAX], name[PATH_MAX];
  int rphys, errnophys=0, rname, errnoname=0;
  int reportnumbug;

  rphys= ioctl(0, HIDIOCGRAWPHYS(PATH_MAX), phys);  errnophys=errno;
  rname= ioctl(0, HIDIOCGRAWNAME(PATH_MAX), name);  errnoname=errno;
  if (rphys>=0 && rname>=0) {
    reportnumbug= 0;
    if (verbose)
      fprintf(stderr,"%s: %.*s %.*s\n",progname,rphys,phys,rname,name);
  } else if (rphys<0 && errnophys==EINVAL &&
	     rname<0 && errnoname==EINVAL) {
    fprintf(stderr,"%s: warning, HIDIOCGRAWPHYS/NAME gave EINVAL,"
	    " assuming kernel eats report number, assuming reports are 00\n",
	    progname);
    reportnumbug= 1;
  } else {
    die("HIDIOCGRAWPHYS %s / HIDIOCGRAWNAME %s",
	rphys<0 ? strerror(errnophys) : "ok",
	rname<0 ? strerror(errnoname) : "ok");
  }

  if (reportnumbug) msg[0]=0;

  for (;;) {
    int l= read(0, msg+reportnumbug, sizeof(msg)-reportnumbug);
    if (!l) break;
    if (l<0) { perror("hidrawconv: read"); exit(-1); }
    l += reportnumbug;
    dispatch(&lasts,"",report_processors, msg,l);
    if (ferror(stdout) || fflush(stdout))
      diee("failed flushing event to stdout");
  }
}

int main(int argc, char **argv) {
  const char *how;

  if (!*argv || !(how=*++argv) || *how++!='-' || !*how || how[1] || *++argv)
    badusage("need exactly one argument, -d, -e or -E");

  switch (how[0]) {
  case 'd':
    puts(descriptor);
    break;

  case 'E':
    events(1);
    break;

  case 'e':
    events(0);
    break;

  default:
    badusage("unknown option/mode");
  }

  if (ferror(stdout) || fflush(stdout))
    diee("write/flush stdout");

  return 0;
}
