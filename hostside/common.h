/*
 * declarations common to
 *  - simple test program
 *  - realtime daemon
 *  - multiplexer daemon
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stddef.h>
#include <ctype.h>
#include <math.h>

typedef struct ParseState ParseState;
typedef struct CmdInfo CmdInfo;
typedef struct Client Client;

extern const char *progname;

#define TRAINMX_PORT 2883

/*---------- types ----------*/

typedef unsigned char Byte;

#define COMMAND_ENCODED_MAX 16
#define NMRA_PACKET_MAX ((COMMAND_ENCODED_MAX*7 - 14) / 8)

typedef struct Nmra {
  Byte d[NMRA_PACKET_MAX];
  int l;
} Nmra;

typedef struct PicInsn {
  Byte d[COMMAND_ENCODED_MAX];
  int l;
} PicInsn;

/*---------- from parseutils.c ----------*/

struct ParseState {
  const char *remain;
  const char *thisword;
  int lthisword;
};

int vbadcmd(ParseState *ps, const char *fmt, va_list al)
  __attribute__((format(printf,2,0)));
int badcmd(ParseState *ps, const char *fmt, ...)
  __attribute__((format(printf,2,3)));

int lstrstrcmp(const char *a, int la, const char *b);
int thiswordstrcmp(ParseState *ps, const char *b);

int ps_word(ParseState *ps); /* returns 0 or -1, does not log */
void ps_pushbackword(ParseState *ps);

/* All of these return 0 or the return value from vbadcmd: */
int ps_needword(ParseState *ps);
int ps_needhextoend(ParseState *ps, Byte *dbuf, int *len_io);
int ps_neednumber(ParseState *ps, long *r, long min,
			long max, const char *wh);
int ps_neednoargs(ParseState *ps);

#define MUSTECR(something) do{		\
    int mustecr__ec= (something);	\
    if (mustecr__ec) return mustecr__ec;	\
  }while(0)
  

/*---------- macro for table lookups, with help from parseutils.c ----------*/

#define some_lookup_counted(ps, infos, ninfos)			\
  ((const typeof(infos[0])*)					\
   any_lookup((ps),(infos),(ninfos),sizeof((infos)[0])))

#define some_lookup(ps, infos)			\
  (some_lookup_counted((ps),(infos),INT_MAX))

#define some_needword_lookup_counted(ps, infos, ninfos, what)		  \
  ((const typeof(infos[0])*)						  \
   any_needword_lookup((ps),(infos),(ninfos),sizeof((infos)[0]),(what)))

#define some_needword_lookup(ps, infos, what)			\
  (some_needword_lookup_counted((ps),(infos),INT_MAX,(what)))

const void *any_lookup(ParseState *ps,
		       const void *infos, int ninfsmax, size_t infosz);
const void *any_needword_lookup(ParseState *ps, const void *infos,
				int ninfsmax, size_t sz, const char *what);

/*---------- from client.c ----------*/

struct CmdInfo {
  const char *name;
  int (*fn)(ParseState *ps, const CmdInfo *ci);
    /* return values:
     *   0: ok
     *   return value from badcmd
     *   some other error to report (if mainloop is expecting it)
     */
  int xarg;
};

void stdin_client(void);

extern const CmdInfo toplevel_cmds[]; /* defined in commands.c*/

/*---------- from utils.c ----------*/

void vdie(const char *fmt, int ev, va_list al)
     __attribute__((noreturn,format(printf,1,0)));
void die(const char *fmt, ...) __attribute__((noreturn,format(printf,1,2)));
void diee(const char *fmt, ...) __attribute__((noreturn,format(printf,1,2)));
void diem(void) __attribute__((noreturn));

void die_hook(void);
void die_vprintf_hook(const char *fmt, va_list al)
     __attribute__((format(printf,1,0)));

void *mmalloc(size_t sz);
void *mrealloc(void *old, size_t sz);
char *mstrdupl(const char *s, int l);
char *mstrdup(const char *s);
void real_mgettimeofday(struct timeval *tv);

void mrename(const char *old, const char *new);
void mwaitpid(pid_t child, const char *what);
void mflushstdout(void);

void badusage(const char *why) __attribute__((noreturn));
void dumphex(FILE *f, const void *pu, int l); /* caller must check ferror */

#define massert(x) ((x) ? (void)0 : diem())

/*---------- from serialio.c ----------*/

void serial_open(const char *device);
void serial_transmit_now(const Byte *command, int length);

extern int serial_fd, serial_fudge_delay;

/*---------- nmra parsing, nmra.c et al ----------*/

typedef struct NmraParseEncodeCaller NmraParseEncodeCaller;
void nmra_parse_encode(Nmra *out, const char *arg, int argl,
		       int argc, NmraParseEncodeCaller *pec);
  /* will call back to these, supplied by caller: */
  unsigned long nmra_argnumber(NmraParseEncodeCaller *pec, int argi);
  void nmra_problem(NmraParseEncodeCaller *pec, const char *problem);

void nmra_addchecksum(Nmra *packet);
void nmra_encodeforpic(const Nmra *packet, PicInsn *pi_out);


#define ARRAY_SIZE(a) (sizeof((a))/sizeof(*(a)))
#define ARRAY_END(a) ((a) + ARRAY_SIZE((a)))
#define CTYPE(isfoobar,ch) (isfoobar((unsigned char)(ch)))
#define COMMA ,

#define MAX(a_,b_) ({ typeof(a_) a=(a_); typeof(b_) b=(b_); a >= b ? a : b; })
#define MIN(a_,b_) ({ typeof(a_) a=(a_); typeof(b_) b=(b_); a <= b ? a : b; })
#define ENSURE(var,rel,value_) do{		\
    typeof(value_) value= (value_);		\
    if ((var) rel value) ; else (var)= value;	\
  }while(0)

#define STR2(x) #x
#define STR(x) STR2(x)

#endif /*COMMON_H*/
