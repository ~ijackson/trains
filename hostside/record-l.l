/* -*- fundamental -*- */

%{
#include "record-i.h"
%}

%option warn
%option batch
%option noyywrap
%option yylineno
%option prefix="record_yy"
%option header-file="record-l.h"

%{
#define STRT  record_yylval.name= record_tempzone_strdup(yytext);  return
%}

%%

train		{ STRT TRAIN; }
seg		{ STRT SEG; }
adjunct		{ STRT ADJUNCT; }
is		{ STRT IS; }
at		{ STRT AT; }
has		{ STRT HAS; }
inverted	{ STRT INVERTED; }
step		{ STRT STEP; }
stops		{ STRT STOPS; }
home		{ STRT HOME; }
end		{ STRT END; }
 /* new keywords must be added to %token<name> and ident: in record-y.y */

[A-Za-z][-A-Za-z0-9]+	{ STRT IDENT; }

[0-9]{0,8}	{ record_yylval.num= strtoul(yytext,0,10); return NUM; }
[0-9]{9}	{ record_yyerror("number too long"); }
[0-9][0-9]*\.[0-9]* { record_yylval.dbl= strtod(yytext,0); return DBL; }

[-+:=~/]	{ record_yylval.name= 0; return yytext[0]; }

\#.*\n|\n	{ record_yylval.name= 0; return NL; }

[ \t]		{ }

.		{ record_yyerror("lexically invalid input"); }
