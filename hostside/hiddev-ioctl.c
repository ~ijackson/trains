/**/

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/types.h>
#include <linux/hiddev.h>

#include "common.h"

static void report_scan(const char *which, int type) {
  struct hiddev_report_info rinfo;
  int r, i, j;
  int fd=0;

  rinfo.report_type = type;
  rinfo.report_id = HID_REPORT_ID_FIRST;

  for (;;) {
    r = ioctl(fd, HIDIOCGREPORTINFO, &rinfo);
    if (r && errno==EINVAL) break;
    if (r) diee("HIDIOCGREPORTINFO");

    printf("%s type=%08"PRIx32" id=%08"PRIx32" num_fields=%08"PRIx32"\n",
	   which, rinfo.report_type, rinfo.report_id, rinfo.num_fields);
    for (i=0; i < rinfo.num_fields; i++) {
      struct hiddev_field_info finfo;
      finfo.report_type= rinfo.report_type;
      finfo.report_id= rinfo.report_id;
      finfo.field_index= i;
      r= ioctl(fd, HIDIOCGFIELDINFO, &finfo);
      if (r) diee("HIDIOCGFIELDINFO");
      printf("%s type=%08"PRIx32" id=%08"PRIx32" fi=%08"PRIx32
	     " maxusage=%08"PRIx32" flags=%08"PRIx32
	     " phys=%08"PRIx32"/log=%08"PRIx32"/app=%08"PRIx32
	     " log=%08"PRIx32"..%08"PRIx32
	     " phys=%08"PRIx32"..%08"PRIx32
	     "\n",
	     which, finfo.report_type, finfo.report_id, finfo.field_index,
	     finfo.maxusage, finfo.flags,
	     finfo.physical, finfo.logical, finfo.application,
	     finfo.logical_minimum, finfo.logical_maximum,
	     finfo.physical_minimum, finfo.physical_maximum
	     );
      for (j=0; j < finfo.maxusage; j++) {
	struct hiddev_usage_ref uref;
	uref.report_type= finfo.report_type;
	uref.report_id= finfo.report_id;
	uref.field_index= i;
	uref.usage_index= j;
	r= ioctl(fd, HIDIOCGUCODE, &uref);
//dump(&uref,sizeof(uref));
	r= ioctl(fd, HIDIOCGUSAGE, &uref);
//dump(&uref,sizeof(uref));
	printf("%s type=%08"PRIx32" id=%08"PRIx32" fi=%08"PRIx32
	       " ui=%08"PRIx32" uc=%08"PRIx32" val=%08"PRIx32"\n",
	       which, uref.report_type, uref.report_id, uref.field_index,
	       uref.usage_index, uref.usage_code, uref.value);
      }
    }
    rinfo.report_id |= HID_REPORT_ID_NEXT;
  }
}

void die_vprintf_hook(const char *fmt, va_list al) { }
void die_hook(void) { }
const char *progname= "hiddev-ioctl";

int main(int argc, char **argv) {
  int r;
  if (!argv[0] || !argv[1]) badusage("too few args");
  const char *act= *++argv;
  if (!strcmp(act, "scan")) {
    report_scan("input", HID_REPORT_TYPE_INPUT);
    report_scan("output", HID_REPORT_TYPE_OUTPUT);
  } else if (!strcmp(act, "send")) {
    /* type id field val */
    if (!argv[1] || !argv[2] || !argv[3] || !argv[4]
	 || !argv[5] || !argv[6] || argv[7])
      badusage("wrong # args");
    struct hiddev_usage_ref uref;
    struct hiddev_report_info rinfo;
    uref.report_type= strtoul(argv[1],0,0);
    uref.report_id=   strtoul(argv[2],0,0);
    uref.field_index= strtoul(argv[3],0,0);
    uref.usage_index= strtoul(argv[4],0,0);
    uref.usage_code=  strtoul(argv[5],0,0);
    uref.value=        strtol(argv[6],0,0);
    rinfo.report_type= uref.report_type;
    rinfo.report_id=   uref.report_id;
    rinfo.num_fields=  0;
//    r= ioctl(0, HIDIOCGREPORT, &rinfo);    if (r) diee("HIDIOCGREPORT");
//    r= ioctl(0, HIDIOCGUSAGE, &uref);    if (r) diee("HIDIOCGUSAGE");
//dump(&uref,sizeof(uref));
//    printf("%08"PRIx32"...",uref.value);
    r= ioctl(0, HIDIOCSUSAGE, &uref);    if (r) diee("HIDIOCSUSAGE");
    r= ioctl(0, HIDIOCSREPORT, &rinfo);  if (r) diee("HIDIOCSREPORT");
  } else {
    badusage("unknown action");
  }
  return 0;
}
