/* -*- fundamental -*- */

%{
#include "record-i.h"
#include "record-l.h"

static Train *cur_train;
%}

%union {
  const char *name;
  Train *train;
  Segment *seg;
  AdjunctsAdjunct *adjunct;
  AdjunctsAddr *adjaddr;
  int num;
  double dbl;
}

%token <name>	TRAIN ADJUNCT SEG  IS AT HAS INVERTED STEP STOPS HOME  END
%token <name>	IDENT
%token <name>	NL
%token <num>	NUM
%token <dbl>	DBL

%type <name>		ident
%type <train>		train
%type <seg>		seg
%type <num>		backwards inverted
%type <dbl>		dbl
%type <adjunct>		adjunct
%type <adjaddr>		adjaddr

%defines
%error-verbose
%name-prefix="record_yy"

%%

file:		end
	|	line NL { record_tempzone_clear(); } file

line:		/* empty */
	|	TRAIN train IS NUM NUM '+' NUM '+' NUM
	{	  if ($2) record_train_is($2,$4,$5,$7,$9);
	}
	|	TRAIN train HOME { cur_train=$2; } segments
	{
	}
	|	TRAIN train STEP NUM '=' dbl
	{	  if ($2) record_train_step_speed($2,$4,$6);
	}
	|	TRAIN train STOPS NUM ':' NUM NUM
	{	  if (!trains) record_train_stopregime_count();
		  else if ($2) record_train_stopregime($2,$4,$6,$7);
	}
	|	SEG seg HAS backwards train inverted
	{	  if ($2 && $5) record_seg_has($2,$4,$5,$6);
	}
	|	SEG seg AT ident
	{	  if ($2) record_seg_at($2,$4);
	}
	|	ADJUNCT adjunct IS adjaddr NUM
	{	  if (trains) record_adjunct_nmrafunc($2,$4,$5);
	}
	|	ADJUNCT adjunct IS adjaddr STEP NUM
	{	  if (trains) record_adjunct_motor($2,$4,$6);
	}

backwards:	/* empty */ { $$= 0; }
	|	'-' { $$= 1; }

inverted:	/* empty */ { $$= 0; }
	|	INVERTED { $$= 1; }

segments:	{ cur_train= (void*)-1; }
	|	backwards seg { record_train_home(cur_train,$1,$2); } segments

ident:		TRAIN | SEG
	|	IS | AT | HAS | INVERTED | STEP | STOPS | HOME
	|	END
	|	IDENT

dbl:		DBL
	|	NUM { $$= $1 }

seg:		ident { $$= record_pname2seg($1); }
train:		ident { $$= record_pname2train($1); }
adjunct:	ident ident { $$= record_pname2adjunct($1,$2); }
	|	'-' { $$= 0; }
adjaddr:	NUM { $$= record_adjaddr($1); }

end:		END NL

%%
