/*
 * common
 * general utility functions
 */

#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "common.h"

static void vdie_vprintf(const char *fmt, va_list al) {
  va_list al_copy;
  va_copy(al_copy,al);
  vfprintf(stderr,fmt,al_copy);
  die_vprintf_hook(fmt,al);
}

static void vdie_printf(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  vdie_vprintf(fmt,al);
  va_end(al);
}

void vdie(const char *fmt, int ev, va_list al) {
  vdie_printf("%s: fatal: ", progname);
  vdie_vprintf(fmt,al);
  if (ev) vdie_printf(": %s",strerror(ev));
  vdie_printf("\n");
  die_hook();
  exit(12);
}

void badusage(const char *why) {
  fprintf(stderr,"%s: bad usage: %s\n",progname,why); exit(8);
}

void die(const char *fmt, ...)
  { va_list al; va_start(al,fmt); vdie(fmt,0,al); }
void diee(const char *fmt, ...)
  { va_list al; va_start(al,fmt); vdie(fmt,errno,al); }
void diem(void)
  { diee("malloc failed"); }

void *mmalloc(size_t sz) {
  void *p;
  if (!sz) return 0;
  p= malloc(sz);
  if (!p) diem();
  return p;
}
		  
void *mrealloc(void *o, size_t sz) {
  void *r;
  if (!sz) { free(o); return 0; }
  r= realloc(o,sz);
  if (!r) diem();
  return r;
}
		  
char *mstrdupl(const char *s, int l) {
  char *p;
  p= mmalloc(l+1);
  memcpy(p,s,l);
  p[l]= 0;
  return p;
}
		  
char *mstrdup(const char *s) { return mstrdupl(s,strlen(s)); }

void mrename(const char *old, const char *new) {
  if (rename(old,new))
    diee("failed to rename `%s' to `%s'", old,new);
}

void mwaitpid(pid_t child, const char *what) {
  pid_t rpid;
  int status;
  
  rpid= waitpid(child,&status,0);  if (rpid!=child) diee("waitpid");
  if (WIFEXITED(status)) {
    int st= WEXITSTATUS(status);
    if (st) die("%s exited with nonzero status %d",what,st);
  } else if (WIFSIGNALED(status)) {
    die("%s died due to %s%s", what, strsignal(WTERMSIG(status)),
	WCOREDUMP(status) ? " (core dumped)" : "");
  } else {
    die("%s failed with unexpected wait status 0x%x", what, status);
  }
}

void mflushstdout(void) {
  if (ferror(stdout) || fflush(stdout)) diee("write to stdout");
}

void dumphex(FILE *f, const void *pu, int l) {
  const uint8_t *p= pu;
  while (l>0) {
    fprintf(f,"%02x",*p++);
    l--;
  }
}
