
recurse = +set -e; for f in $($(1)_DIRS); do $(MAKE) -C $$f $(2); done

PIC_DIRS=	iwjpictest cebpic detpic
HOST_DIRS=	pcb layout hostside

all:
	$(call recurse,HOST,recurse)
	$(call recurse,PIC,)

client:
	$(call recurse,HOST,client)

host: host_recurse

host_%:
	$(call recurse,HOST,$*)

pic:
	$(call recurse,HOST,for-pic)
	$(call recurse,PIC,)

pic_%:
	+$(MAKE) -C layout for-pic
	$(call recurse,PIC,$*)

pic_clean:
	$(call recurse,PIC,clean)

clean:		host_clean pic_clean
		rm -f *~ .*~

tidy:		host_tidy pic_tidy

realclean:
	$(MAKE) tidy
	$(MAKE) clean
