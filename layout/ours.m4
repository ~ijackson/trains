# -*- fundamental -*-

include(`parts.i4')

define(`pt_main_r',`pt_peco_mr')

defobj dualjn_peco_r
  abs ic 200 200 0
  part pt_i pt_peco_lr c ic
  part x cross_peco_l br pt_i_a
  part pt_o pt_peco_lr b x_bl
  rel pt_o_c oc
  rel x_tl ia
  extend pt_o_a oa upto ia
  rel x_tr ob
  rel pt_i_b ib
enddef

defobj demos
  abs right 200 200 0
  part main_up   ^pt_main_r  c right b right_main_up
  part main_down  pt_main_r  c right b right_main_down
  part short_down pt_peco_sr c right b right_short_down
  abs up 200 200 90
  part main_left  ^pt_main_r c up b up_main_left
  part main_right  pt_main_r c up b up_main_right
  part dualjn_tr ^dualjn_peco_r ib -right
enddef

# positive segment direction is anticlockwise, since T rail is
# on the inside (see informat.txt)

#---------- main line figure-of-8

layer 0

abs midx_x_m 930 700 90

segment midx_
segmap  midx_F/ -X2/R1  midx_B/ X2/R0
part midx_x cross_peco_s m

segment -X2/R1 165 -X3
extend midx_x_tr -top_0rm_b parallel demos!right_main_up 315

segment X7
part top_0rm ^pt_main_r b

segment X2/R0 155 X4
extend midx_x_tl -top_0lm_b parallel demos!-right_main_down -315

segment -X8
part top_0lm pt_main_r b
rel top_0lm_c top_2lk 0 -37
rel top_2lk top_3lk 0 -37

segment /

layer 8
#rel top_3lk top_45x_br 67 -39 -2.25

#rel top_3lk top_45x_br 52 -47 0

#extend -top_45x_br -top_4le1 len 45 -600

layer s
rel top_3lk top_5_ref 0 -74
layer =

layer 0

segment -A2/P0
extend -midx_x_bl -midx_j_a len 10 315

segment A2
part midx_j pt_peco_sl a

segment -A2 155 -A4
extend -midx_j_c -bot_0lm_b parallel demos!right_main_up 315

segment A6
part bot_0lm ^pt_main_r b

segment -X12 280 -X10
extend -bot_0lm_c -leb_0k uptoang 90 315
segment /

rel -leb_0k -leb_2k 0 -37
rel -leb_2k -leb_3k 0 -37

rel -bot_0lm_c bot_2lk 0 -37
rel bot_2lk bot_3lk 74 -37

layer s
extend -midx_x_br -demo_midx_x_br_x ang 7 316
extend -demo_midx_x_br_x -demo_bot_0rm_b parallel demos!right_main_down -316
part demo_bot_0rm pt_main_r b

layer s
extend bot_0lm_a -bot_0rm_c upto demo_bot_0rm_c

layer =

segment -A5
part bot_0rm pt_main_r c

segment -X7
extend -top_0rm_c top_0rme upto bot_0rm_c

segment -A3 180 -A1
join bot_0rm_b midx_x_br 315

segment -X7
extend top_0rme top_0rme1 ang 9 315
extend top_0rme1 rim_0t uptoang -90 315
segment /

rel rim_0t rim_2t 0 -37
rel rim_2t rim_3t 44 -37

segment -X6 250 -X5
join top_0lm_a -top_0rm_a 450

segment /

#---------- box

layer l*

extend bot_3lk sq_bl_o upto leb_3k
rel sq_bl_o sq_bl_i 0 0 -90

extend sq_bl_i sq_tl_o upto top_5_ref
rel sq_tl_o sq_tl_i 0 0 -90

extend sq_tl_i sq_tr_o upto rim_3t
rel sq_tr_o sq_tr_i 0 0 -90

extend sq_tr_i sq_br_o upto sq_bl_i
rel sq_br_o sq_br_i 0 0 -90

extend sq_br_i sq_bl_o2 upto sq_bl_o

#---------- scale

layer g*

rel -sq_bl_o scale_0a 10 40

define(`morescale',`
extend scale_$1a scale_$1b len $3
rel scale_$1b scale_$2a 0 $4
')

morescale(0,1,20,-5)
morescale(1,2,50,5)
morescale(2,3,100,5)
morescale(3,4,200,-5)
morescale(4,5,500,5)

#---------- bottom junction between main loop, main-8

layer p4

rel sq_tr_o ctrlbox_bl -64 149
extend ctrlbox_bl ctrlbox_br len 50
rel ctrlbox_bl ctrlbox_blv 0 0 90
extend ctrlbox_blv ctrlbox_tl len 100

layer 0

#rel sq_br_i bot_3rj_c 315
rel sq_br_i bot_3rj_c 345

rel bot_0rm_c bot_2rk 0 -37
segment -B10
extend bot_2rk bot_2rx_br len 130

segment -C9
part bot_3rj pt_main_r c

segment -B9
part bot_2rx slip2_pecof br

segment -B9
join bot_3rj_b bot_2rx_bl 450

segment -B9
extend bot_2rx_tr bot_0mx_bl len 19 1400

segmap  bot_0mx_B -A5  bot_0mx_F -A6
segment bot_0mx_
part bot_0mx cross_pecof_s bl
segment A5
extend -bot_0mx_br -bot_0rj_b len 8

segment -A5
segmap A5/P J
part bot_0rj ^pt_main_r b
join bot_0rm_a bot_0rj_c 450
segmap A5/P P

rel bot_0mx_tr -bot_0lj_b

segment A6
segmap A6/P J
part bot_0lj pt_main_r b
segmap A6/P P

segment -A5 53 -A6
join bot_0rj_a -bot_0lj_a 450

segment -A6
join -bot_0lj_c -bot_0lm_a 450

segment -B7
extend bot_0mx_tl -bot_02lj_b uptoang 194 -315
segment /

segment B7
segmap B7/P J
part bot_02lj ^pt_main_r b
segmap B7/P P
segment -B7
part bot_23lj ^pt_main_r c -bot_02lj_c

segment -B8
join bot_2rx_tl -bot_02lj_a 450

segment C5
part bot_3lj ^pt_main_r c -bot_3lk

segment C6 211 C7 181 C8
join bot_3lj_a -bot_3rj_a 450

segment -C5/P1
join bot_23lj_b -bot_3lj_b 315 twoarcs

segment -C5 120 -C4 245 -C3
join bot_3lk -leb_3k 315 arcline

segment -B6 234 -B5 222 -B4
join bot_23lj_a bot_2lk 450
join bot_2lk -leb_2k 352

segment -X7 20 -X9 220 -X11
join rim_0t bot_0rm_c 315 arcline
segment /

layer 2
segment B10 145 B11
join -bot_2rk -rim_2t 352 arcline
segment /

layer 2
segment -C0 90 -C10 368 -C9
join rim_3t bot_3rj_c 315 arcline
segment /

#---------- station branch curve

layer 5
rel sq_tl_i -top_5le 352 0 180

layer 8
extend -top_5le leb_5e ang 90 -352
rel leb_5e leb_4e 0 -37
extend -leb_4e top_4le0 ang 70 315
#join -leb_4e top_4le0 315
#352
#extend -top_4le3 -top_4le2 len 30 -750
#extend -top_4le2 -top_4le1 len 30 -450
#join -top_45x_br -top_4le0 550 twoarcs

layer 2
segment -C3 556 -C2
join -leb_3k top_3lk 389 arcsline

segment -B4 215 -B3
join -leb_2k top_2lk 352 arcsline

layer 0
segment -X10 160 -X8
join -leb_0k top_0lm_c 315 arcsline
segment /

layer 8
#extend top_5le top_5re0 len 30.0 315
extend top_5le top_5re0 len 35.0 450
extend top_5re0 top_45x_bl len 65 -1000

part top_45x scis_pecof bl
#part top_45x scis_pecof br
#join -top_45x_br -nr_top_45x_br 315
#join top_5re0 top_45x_bl 1200
#join top_5le top_45x_bl 500

extend -top_45x_br -top_4le3 len 90 -1000
extend -top_4le3   -top_4le2 len 25 -800
#join -top_4le1 -top_4le0 315 twoarcs
extend -top_4le2   -top_4le1 len 10 -350
join -top_4le1 -top_4le0 315

#---------- top main-line/station junction

layer 6
segment B11 224 B12
#extend -rim_2t top_2j_c len 420 -352
extend -rim_2t top_2j_c len 445 -352
#extend -rim_2t top_2j_c upto top_0rme1 -352

layer 8

segment top_2j
segmap top_2j/P0 B0/P1 top_2j/P1 B0/P0
part top_2j pt_peco_cl c

#rel top_2j_r top_2j_a
#rel top_2j_l top_2j_b
#part top_2j pt_pecofguess_ll c

layer 4
segment -B3 8 -B2 493 -B1
extend top_2lk top_2j21 len 300

layer 8
join top_2j21 -top_2j_b 600

segment /

layer 8

#rel top_45x_tr top_4j20_side_1 0 1 -90
#rel top_45x_tr top_4j20_side_2 0 2 -90

extend top_45x_tr top_4j22 len 180 1000
#extend top_4j22 top_4j21 len 80 -1000
join top_4j22 -top_2j_a 1000 twoarcs

#extend top_4j20 top_4j21 len 30 800
#extend top_4j21 top_4j22 len 30 450

#extend top_45x_tr top_4j20 len 20 600
#extend top_4j20 top_4j21 len 40 1500
#extend top_4j21 top_4j22 len 40 700



#extend top_45x_tr top_4j21 len 95 900

#extend top_2j_a -top_4j22 len 0.010 -352
#join top_4j21 top_4j22 352

#extend  -top_4j22 len 0.010 -352

#twoarcs

rel -top_0rm_a top_5j3b 0 -148
#extend top_45x_tl top_5j3a len 95 -1200
#join top_45x_tl top_5j3b 315 twoarcs
join top_45x_tl top_5j3b 1000 twoarcs

layer 0

segment -C2
extend top_3lk top_3u1 len 270

rel -top_0rm_a top_3u2 35 -111
join top_3u1 top_3u2 800 twoarcs !arcsline
#cross

layer 4
segment C0
extend -rim_3t -rim_3tt len 27.0
#extend -rim_3tt1 -rim_3tt ang 0.0 -315

segment top_3j
segmap top_3j/P0 C0/P1 top_3j/P1 C0/P0
part top_3j pt_peco_cl c -rim_3tt
segment /

layer 2
segment C1
extend top_3j_b -top_3u3 len 40 -315
segment -C2 239 -C1
join top_3u2 top_3u3 315 arcline
segment /

layer 6
extend top_3j_a top_5j3c2 len 20 -550
extend top_5j3c2 top_5j3c1 ang 32 -315
extend top_5j3c1 top_5j3c len 120 -650

layer 8
join top_5j3b -top_5j3c 315 arcline

#---------- station

# left bridge, `leb'

layer 10

part leb_4y pt_peco_sl c leb_4e
extend leb_4y_a sta_4nears9 len 35 -600
extend sta_4nears9 sta_4nears2 uptoang 0 -315
extend sta_4nears2 sta_4neare uptoang 5 -450
extend sta_4neare sta_4far len 1148

rel sta_4neare sta_5nears 130 30
extend sta_5nears sta_5far upto sta_4far

extend -sta_5nears -sta_5neare1 len 50 -1200
extend -sta_5neare1 -sta_5neare2 len 50 -600

extend leb_5e leb_5eb_c upto leb_4y_a
part leb_5eb pt_peco_ml c
extend leb_5eb_b sta_5neare3 ang 50 -315
join sta_5neare3 sta_5neare2 352

layer 12
extend leb_5eb_a super_h00 uptoang 5 -325
layer 16
extend super_h00 super_h01 len 300
extend super_h01 super_h0 parallel sta_5nears -600
extend super_h0 super_h1 len 400
#extend super_h10 super_h1 uptoang -1.5 800

layer s
rel -sq_br_i place_super_h2 -350 0

layer 20
extend super_h1 super_h2 upto place_super_h2

layer 24
rel -sq_tr_i super_h3 -315 0
join super_h2 super_h3 315 arcline

layer 28
rel -sq_tr_o super_h4 315 0
join super_h3 super_h4 315 arcline

layer 30
extend super_h4 -super_jtrr_a ang 5 -315
part super_jtrr pt_peco_mr a
part super_jtrl pt_peco_ml c -super_jtrr_c
extend super_jtrl_b super_mid uptoang 275 -315
extend super_mid -super_jbl_b parallel super_jtrl_b 315
part super_jbl pt_peco_ml b
join super_jbl_a -super_jtrr_b 315 arcline

layer p30
rel super_jbl_a super_sta_l_platnear 100 -14.5
extend super_sta_l_platnear super_sta_r_platnear len 500
rel super_sta_l_platnear super_sta_l_platfar 0 -17
extend super_sta_l_platfar super_sta_r_platfar upto super_sta_r_platnear

join -super_sta_l_platnear super_sta_l_platfar 1.0
join super_sta_r_platnear -super_sta_r_platfar 1.0

layer s
extend super_jtrl_a -super_jtl_c upto super_jbl_c

layer 30
part super_jtl pt_peco_sr c
join super_jtrl_a -super_jtl_a 315
join -super_jtl_c super_jbl_c 315 arcline

### layer 20
### 
### # high-level lines and platforms
### 
### rel sta_4neare sta_6far 500 76
### part sta_67r pt_peco_sl a -sta_6far
### 
### rel sq_br_i high_6br0 230 0 -180
### extend high_6br0 high_6br1 ang 90 -230
### ##join -sta_67r_c high_6br0 230
### 
### extend -sta_6far -sta_6nears len 630
### part sta_67l pt_peco_sl b sta_6nears
### join sta_67l_a -sta_67r_b 230
### #extend -sta_67l_c high_6bl5 uptoang -30 230
 
### layer p20
### 
### rel sta_6nears sta_6nears_platnear 0 -14.5
### rel sta_6nears_platnear sta_6nears_platfar 0 -17
### extend sta_6nears_platnear sta_6far_platnear upto sta_6far
### extend sta_6nears_platfar  sta_6far_platfar  upto sta_6far
### join sta_6nears_platnear -sta_6nears_platfar 1 
### join sta_6far_platnear -sta_6far_platfar 1 

# medium-level platforms

layer p10

rel sta_4far sta_4far_platnear 0 -14.5
rel sta_4far_platnear sta_4far_platfar 0 -45
extend -sta_4far_platnear -sta_4neare_plat upto sta_4neare
extend -sta_4far_platfar -sta_4nears_platfar upto sta_5nears
join sta_4neare_plat sta_4nears_platfar 1
join sta_4far_platnear -sta_4far_platfar 1 

rel sta_5far sta_5far_platnear 0 14.5
rel sta_5far_platnear sta_5far_platfar 0 30
extend -sta_5far_platnear -sta_5neare_plat upto sta_5nears
extend -sta_5far_platfar -sta_5neare_platfar upto sta_4neare
join sta_5neare_platfar sta_5neare_plat 1
join sta_5far_platnear -sta_5far_platfar 1

layer 10

# turntable

rel sta_4far_platnear sta_tt_mid -80 -148
part sta_tt tt_atlas mid
part sta_ttmotor tt_atlas_motor cout sta_tt_out90

# station line 3

rel sta_4neare sta_3neare 15 -76 -8
part sta_3sc pt_peco_sr c -sta_3neare
join leb_4y_b -sta_3sc_a 230

extend sta_3neare sta_3y0_c uptoang 5 -230
part sta_3y0 pt_peco_sl c

# junctions

layer 10
extend sta_3y0_b sta_0y1_c ang 1 -230
part sta_0y1 pt_peco_sl c

layer 10
rel sta_4neare sta_3nears 280 -74
part sta_3y1 pt_peco_sl c sta_3y0_a
join sta_3y1_a sta_3nears 450

extend sta_3nears sta_3far len 500
extend sta_3far sta_3far1 len 30 -650
join sta_3far1 -sta_tt_out210 230

# station line 2

part sta_2y0 pt_peco_sr c sta_3y1_b
extend sta_2y0_b sta_2nears parallel sta_3nears 230
extend sta_2nears sta_2far upto sta_3far1
join sta_2far -sta_tt_out195 230

# station line 1

rel sta_2far sta_1far 0 -37
join sta_1far sta_tt_in 230
join sta_2y0_a sta_1far 230 arcline

#---------- sort-of helix (regions sc and h)

layer 8
extend sta_3sc_b sc_31 ang 90 260

layer 6
extend sc_31 sc_4 ang 110 230

layer 4
extend sc_4 sc_5 ang 140 230

layer 2
extend sc_5 sc_6 ang 150 230

layer 0
extend sc_6 sc_7 ang 30 285

extend midx_j_b -sc_9 len 0.4 -230
join sc_7 sc_9 230 arcline

layer 12
rel -sc_4 high_6s0 0 37
extend -high_6s0 -high_6s01 ang 45 267
join sta_0y1_b high_6s01 230 arcline

layer 14
extend high_6s0 high_6s11 ang 90 -230
extend high_6s11 high_6s2 len 37

layer 16
extend high_6s2 high_6s3 ang 90 -230

layer 18
extend high_6s3 high_6s4 parallel sta_0y1_b -230

layer 22
extend high_6s4 high_6s5 ang 180 -230

layer 26
extend high_6s5 high_6s6 ang 150 -230

layer 28
join high_6s6 -super_jtl_b 230

### #---------- high-level goods
### 
### layer 20
### 
### rel sta_1far -high_sta6far -10 0
### extend high_sta6far high_sta6near upto sta_2y0_a
### 
### extend highr_x_tl highr_6sc_c ang 20 -230
### part highr_6sc pt_peco_sl c
### extend highr_6sc_a highr_6out ang 15 230
### ##join highr_6out high_sta6far 230
### 
### layer s
### extend highr_x_bl -demo_highr_6jb_c uptoang 90 -230
### extend sq_bl_i place_highr_6jb_c upto demo_highr_6jb_c
### 
### layer 20
### rel place_highr_6jb_c highr_6jb_c -10 0
### part highr_6jb pt_peco_sr c
### join highr_6jb_b -highr_x_bl 230
### join highr_6jt_a -highr_6jb_a 230 arcline
### ##join high_6br1 highr_6jb_c 230
### 
### extend -highr_x_tr highr_7kink uptoang -45 230
### extend highr_7kink -highr_7sc_a ang 30 -230
### rel high_sta6near high_sta7near 0 37
### 
### part highr_7sc pt_peco_sl a
### ##join -highr_7sc_c high_sta7near 230
### 
### ##join highr_7sc_b -highr_6sc_b 230
### 
### #part high_67j pt_peco_sr c high_6bl5
### ##join high_67j_b -high_sta6near 267
### ##join high_67j_a -high_sta7near 230
### 
#---------- shunting yard

layer 10

#defobj yard
# define(`extendyard',`
#  part j$1 pt_peco_sr c $2
#  part k$1 pt_peco_sr c j$1_b
#  extend k$1_a near$1a ang 0.001 -230
##  extend k$1_a near$1b len 90
#  extend k$1_b near$1b2 ang 22.501 -230
#  extend near$1b2 far$1b $4
#  extend near$1a far$1a $3
# ')
# abs e 200 200 0
# extendyard(`0',`e',   `len 600',`len 500')
# extendyard(`1',`j0_a',`len 500',`len 400')
# extend j1_a e2a ang 2 -230
# extend e2a near2a parallel near0a 230
# extend near2a far2a len 370
#enddef

#extend sta_0y1_a yard_0e ang 31 -230
#part yard yard e yard_0e
extend sta_0y1_a yard_0e ang 0.01 -230

part yard_p   pt_peco_sl c yard_0e
part yard_pr  pt_peco_sr c yard_p_a
part yard_pl  pt_peco_sr c yard_p_b
part yard_pll pt_peco_sr c yard_pl_a

extend yard_pr_b yard_err_s len 180
extend yard_pr_a yard_erl_s len 120
extend yard_pl_b yard_elr_s len 180
extend yard_pll_b yard_ellr_s len 180
extend yard_pll_a yard_elll_s len 120

extend yard_err_s yard_err_t len 275 4500

extend yard_erl_s yard_erl_t len 180 2400
extend yard_erl_t yard_erl_u len 150 800
extend yard_erl_u yard_erl_v len 200 300

extend yard_elr_s yard_elr_t upto yard_erl_t 2400
extend yard_elr_t yard_elr_u upto yard_erl_u 800
extend yard_elr_u yard_elr_v len 240 315

extend yard_ellr_s yard_ellr_t upto yard_erl_t 2400
extend yard_ellr_t yard_ellr_u upto yard_erl_u 800
extend yard_ellr_u yard_ellr_v len 260 330

extend yard_elll_s yard_elll_t parallel yard_ellr_s 230
extend yard_elll_t yard_elll_u upto yard_ellr_u 600
extend yard_elll_u yard_elll_v len 200 365

#---------- train houses

extend sta_tt_out135 sta_9house len 200  -230
extend sta_tt_out150 sta_8house len 230  -400
extend sta_tt_out165 sta_7house len 115

extend sta_8house sta_8house_b len 120
rel sta_8house_b sta_7house_b 0 -37
join sta_7house sta_7house_b 300

ident
eof
