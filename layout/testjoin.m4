#!/usr/bin/m4 -Dchoice=

dnl run with -Dchoice='...' -Dminradius=NNN

layer l

define(`simple',`
rel b$2 b$2x$1 0 0 $1
join a$2 -b$2x$1 minradius choice
')

define(`complex',`
abs org$1 $2 $3 0
rel org$1 a$1 0 0 $4
rel org$1 b$1 200 0 0
 simple(0,$1)
 simple(30,$1)
 simple(60,$1)
 simple(90,$1)
 simple(135,$1)
 simple(230,$1)
 simple(300,$1)
 simple(345,$1)
')

 complex(0,     200,  400,    0)
 complex(30,    200, 1200,   30)
 complex(60,    800,  400,   60)
 complex(130,   800, 1200,  130)
 complex(m130, 1500,  400, -130)
 complex(m165, 1500, 1200, -165)

#abs a 400 400 90
#abs b 300 700 30
#join a b minradius choice
