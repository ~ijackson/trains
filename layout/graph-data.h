/*
 */

#ifndef GRAPH_DATA_H
#define GRAPH_DATA_H


typedef struct Edge Edge;
typedef struct Segment Segment;
typedef struct MovFeat MovFeat;
typedef struct EdgeEnd EdgeEnd;
typedef struct Node Node;
typedef struct NodeList NodeList;
typedef struct NodeSide NodeSide;

struct Segment {
  const char *segname; /* 0 if unknown (usually elided by extractgraph) */
  int n_movfeats;
  MovFeat *movfeats; /* [0] is fixed */
  MovFeat *starfeature; /* set by movfeatsplitedges */
  int u;
};

struct MovFeat {
  Segment *segment;
  const char *movfeat; /* 0 if fixed */
  int n_positions;
};

struct EdgeEnd {
  EdgeEnd *back, *next; /* other ends at same side of same node */
  Edge *edge; /* edge->ends[end].edge==edge */
  int end; /* edge->ends[end].edge==end */
  NodeSide *node;
};

struct Edge {
  const char *pname;
  double distance;
  MovFeat *subseg;
  int movpos; /* 0 if fixed */
  EdgeEnd ends[2];
};

struct NodeSide {
  Node *node; /* node->edges[side].node==node */
  int side; /* node->edges[side].side==side */
  EdgeEnd *head, *tail;
};

struct Node {
  const char *pname;
  Node *back, *next;
  double x,y,a;
  int layermin,layermax;
  NodeSide sides[2];
};

struct NodeList {
  Node *head, *tail;
};

extern NodeList all_nodes;
extern int next_nodenum, next_edgenum;

extern Segment *all_segments[];


#define FOR_ALL_NODES(node) for (node=all_nodes.head; node; node=node->next)
#define FOR_BOTH(sideend) for (sideend=0; sideend<2; sideend++)

#define FOR_NODE_EDGEENDS(side,edgeend, node)	\
  FOR_BOTH(side)				\
    FOR_SIDE_EDGEENDS(edgeend, (node),side)

#define FOR_SIDE_EDGEENDS(edgeend, node,side) \
  for (edgeend=(node)->sides[(side)].head; edgeend; edgeend=edgeend->next)

#define FOR_SEGMENT_MOVFEATS(i,f, segment) \
  for (i=0, f=segment->movfeats; i < segment->n_movfeats; i++, f++)

/* Iteration over edges: 
 *   FOR_ALL_NODES(node) {
 *     FOR_EDGES(edge, node,side,edgeend)
 *       <statement>|<block>
 *   }
 * arranges execute statement/block once for each edge, setting edge
 * appropriately.  edgeend must be of type EdgeEnd*, and side of type
 * int; these are used as working variables by FOR_EDGES.
 */
#define FOR_EDGES(edge, node,side,edgeend)		\
    FOR_NODE_EDGEENDS(side,edgeend, node)			\
      if ((edge= edgeend->edge), edgeend->end) {	\
        /* do each edge once, from end 0, only */	\
      } else

#define FOR_ALL_SEGMENTS(segmentp,segment)	\
  for (segmentp=all_segments;			\
       (segment= *segmentp);			\
       segmentp++)

#endif /*GRAPH_DATA_H*/
