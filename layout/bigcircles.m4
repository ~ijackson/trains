define(pt_circ, pt_peco_mr)
define(radius_circ, 315)

include(parts.i4)
include(circles.i4)

abs tcp0_p_c 1600 800 7
part tcp0 tricirpair p_c

circle(700, 800)

abs tcp1_p_c 500 800 173
part tcp1 ^tricirpair p_c
