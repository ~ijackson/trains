/*
 * usage:
 *   subseg2display encoding.ppm [status-info ...]
 *
 * status-info args are:
 *   <idchar> <alpha> <red> <green> <blue>
 *   .. <alpha> <red> <green> <blue>
 *   0. <alpha> <red> <green> <blue>
 *   <idchar><subseg>
 *
 * `..' group is for unspecified segments.
 * `0.' group is for background.
 *
 * <subseg> is
 *   <segnum>               for fixed track
 *   <segnum>.<movfeatpos>  for part of a moveable feature
 * where numbers are in strtoul base=0 format, movfeatpos is the
 * moveable feature and position as in segcmapassign and
 *
 * Resulting pixel is
 *                                                   [ red   ]
 *    (|alpha| * DIRECTION_COLOUR) + ((1-|alpha|) *  [ green ] )
 *                                                   [ blue  ]
 *
 * where DIRECTION_COLOUR corresponds to the reverse of the stated
 * direction iff alpha<0.
 *
 * All of alpha, red, green, blue are out of 1000.
 */


#include <publib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <math.h>

#include <ppm.h>

#include "segcmap.h"

/*---------- lookup data structure ----------*/

struct result {
  double alpha, rgb[3];
};

typedef unsigned char id;

struct movperseg {
  id *lookup;
  int lookup_a;
};

#define NCHARS 256

static struct result idinfo[NCHARS];
static struct result background;

static id *fixed;
static int fixed_a;

static struct movperseg *moveable;
static int moveable_a;

/*---------- arg parsing helper routines ----------*/

static char **argv;

static void badusage(void) {
  fprintf(stderr,"bad usage\n");
  exit(12);
}

static const char *nextarg(void) {
  if (!*argv) badusage();
  return *argv++;
}

static double channelarg(void) {
  const char *arg= nextarg();
  char *ep;
  double v;

  errno= 0;
  v= strtod(arg,&ep);
  if (*ep) badusage();
  if (errno) badusage();
  if (!finite(v)) badusage();

  return v / 1000.0;
}

/*---------- specification args ----------*/

static void resultargs(struct result *r) {
  int i;

  r->alpha= channelarg();
  for (i=0; i<3; i++)
    r->rgb[i]= channelarg();
}
  
static void idgroupargs(id ix) {
  resultargs(&idinfo[ix]);
}

static void ensure_somethings(void **ary, int *allocd, int want, size_t item,
			      void zero(void *data, int count)) {
  int new_allocd;
  
  if (*allocd > want) return;
  new_allocd= (want+1)*2;
  *ary= xrealloc(*ary, new_allocd * item);
  zero((char*)*ary + item * *allocd, new_allocd - *allocd);
  *allocd= new_allocd;
}

static void zero_ids(void *data, int count) {
  memset(data,0, count*sizeof(id));
}

static void zero_mps(void *data_v, int count) {
  struct movperseg *data= data_v;
  for (count /= sizeof(*data);
       count > 0;
       count--, data++) {
    data->lookup= 0;
    data->lookup_a= 0;
  }
}

#define ENSURE_THINGS(sfx,type)						\
  static void ensure_##sfx(type **ary_io, int *allocd, int want) {	\
    void *ary= *ary_io;							\
    ensure_somethings(&ary,allocd,want,sizeof(**ary_io),zero_##sfx);	\
    *ary_io= ary;							\
  }									\
  struct eat_semicolon

ENSURE_THINGS(ids,id);
ENSURE_THINGS(mps,struct movperseg);

static void subsegarg(id ix, const char *arg) {
  struct result *r;
  struct movperseg *mps;
  unsigned long segnum, movfeatpos;
  char *ep;
  
  r= &idinfo[ix];

  errno= 0;
  segnum= strtoul(arg,&ep,0);
  if (errno) badusage();
  if (segnum > SEGNUM_MAX) badusage();

  if (!*ep) {
    ensure_ids(&fixed,&fixed_a,segnum);
    fixed[segnum]= ix;
    return;
  }

  if (*ep != '.') badusage();

  errno= 0;
  movfeatpos= strtoul(ep+1,&ep,0);
  if (*ep || errno) badusage();
  if (movfeatpos > MOVFEATPOS_MAX) badusage();

  ensure_mps(&moveable,&moveable_a,segnum);
  mps= &moveable[segnum];
  ensure_ids(&mps->lookup,&mps->lookup_a, movfeatpos ^ MOVFEATPOS_MAX);
  mps->lookup[movfeatpos ^ MOVFEATPOS_MAX]= ix;
}  

/*---------- actual image processing ----------*/

static int cols, rows, informat, row, col;
static pixval maxval;
static FILE *encodingfile;

static void badformat(const char *m) {
  fprintf(stderr,"bad format (row=%d,col=%d): %s\n", row,col, m);
  exit(12);
}

static void angle_to_colour(double result[3],
			    int angle_i /* [0..ANGLE_MAX> */) {
  int s;
  double angle, f, u, d, U, D;

  angle= angle_i / (double)NANGLES * 6.0;
  s= floor(angle);
  f= angle - s;
  
  u= f * 0.5;
  U= u + 0.5;
  d= 0.5 - u;
  D= d + 0.5; 

  switch (s) {
#define R(r,g,b) if(1){ result[0]=r; result[1]=g*0.9; result[2]=b; break; }else
  case 0: R( D, U, 0 );
  case 1: R( d, 1, u );
  case 2: R( 0, D, U );
  case 3: R( u, d, 1 );
  case 4: R( U, 0, D );
  case 5: R( 1, u, d );
#undef R
  }
}

static double anglemap[NANGLES][3];

static void angle_to_colour_init(void) {
  int i;
  
  for (i=0; i<NANGLES; i++)
    angle_to_colour(anglemap[i],i);
}

static void process(void) {
  int i;
  unsigned char rgbob[3];
  
  ppm_readppminit(encodingfile, &cols, &rows, &maxval, &informat);
  if (maxval != 255) badformat("wrong maxval");
  if (informat != RPPM_FORMAT) badformat("wrong format");
  ppm_writeppminit(stdout, cols, rows, 255, 0);

  for (i=0; i<3; i++)
    rgbob[i]= background.rgb[i] * 255.0;

  angle_to_colour_init();

  for (row=0; row<rows; row++)
    for (col=0; col<cols; col++) {
      unsigned char rgbi[3], rgbo[3];
      unsigned long datum;

      if (fread(rgbi,1,3,encodingfile)!=3) {
	if (ferror(encodingfile)) { perror("reading"); exit(12); }
	else badformat("truncated file");
      }

      datum= ARY2DATUM(rgbi);
      
      if (datum == BACKGROUND) {

	if (fwrite(rgbob,1,3,stdout)!=3) { perror("filling"); exit(12); }
	
      } else {
	int segnum, movfeatpos, movfeatposix, ix, angle;
	double *rgbdirn, alpha;
	struct result *r;

	if (datum & RESERVED_MASK) badformat("reserved bits set");
	angle= DATUM2(ANGLE,datum);

	segnum= DATUM2(SEGNUM,datum);
	movfeatpos= DATUM2(MOVFEATPOS,datum);
	movfeatposix= movfeatpos ^ MOVFEATPOS_MAX;

	ix= !movfeatpos ?
	  (segnum < fixed_a ? fixed[segnum] : 0) :
	  (segnum < moveable_a && movfeatposix < moveable[segnum].lookup_a
	   ? moveable[segnum].lookup[movfeatposix] : 0);

	r= &idinfo[ix];

	rgbdirn= anglemap[ angle ^ (r->alpha < 0 ? ANGLE_TOPBIT : 0) ];
	alpha= fabs(r->alpha);

	for (i=0; i<3; i++) {
	  double v;
	  v= alpha * rgbdirn[i] + (1.0 - alpha) * r->rgb[i];
	  rgbo[i]= v * 255.0;
	}

	if (fwrite(rgbo,1,3,stdout)!=3) { perror("writing"); exit(12); }
      }
    }
}

/*---------- main program ----------*/

int main(int argc_spec, char **argv_spec) {
  const char *arg, *encodingfilename;

  ppm_init(&argc_spec,argv_spec);
  
  argv= argv_spec;
  nextarg();
  encodingfilename= nextarg();
  while (*argv) {
    arg= nextarg();
    if (!*arg) badusage();
    if (!arg[1]) idgroupargs(arg[0]);
    else if (!strcmp(arg,"..")) idgroupargs(0);
    else if (!strcmp(arg,"0.")) resultargs(&background);
    else subsegarg(arg[0],arg+1);
  }

  encodingfile= fopen(encodingfilename, "rb");
  if (!encodingfile) { perror(encodingfilename); exit(8); }
  process();
  return 0;
}
