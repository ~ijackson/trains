/*
 *
 * Encodings in segment encodings are as follows:
 *  firstly, assemble the R, G, B bytes like this:
 *       blue << 16  |  green << 8  |  red
 *
 * Now interpret the resulting 24-bit datum as a bit sequence with the
 * following parts (LSbit first):
 *     bits
 *       6*     moveable feature and position                  } blue
 *      10*     segment number                                 } blue/green
 *       1*     reserved - always 0                            } red
 *       1      `edge'(1) or `core'(0) - currently always 0    } red
 *       6*     angle                                          } red
 *
 * Each moveable feature has an unambiguous prefix the remainder of the
 * moveable feature bits are the positions of that feature (LSbit is
 * position 0, etc.).  The feature all-bits-0 (0b000000) is for fixed track
 * (so every moveable feature prefix must contain at least one 1).
 *
 * The unknown/unidentified segment is all-bits-0, with feature
 * all-bits-0 for fixed parts and feature 0b1.... for moveable parts.
 * Background is white (all bits set).
 *
 * Items marked * can have the number of bits allocated to them
 * adjusted - see below.
 */

#ifndef SEGMAP_H
#define SEGMAP_H

/*---------- bit widths of various datum fields ----------*/

/* Be careful editing this bit: you may change the number of bits
 * within reason, but preserve the format of the file and the ordering
 * of info in the datum.  Both segcmapassign and layout have knowledge
 * of the datum format that doesn't come from here, although there
 * are arrangements to read the widths of the fields from here. */

#define ANGLE_BITS        6
#define MOVFEATPOS_BITS   6
#define SEGNUM_BITS      10
/* do not make these add up to more than 23 */

/*---------- datum assembly/disassembly macros ----------*/

/* It wouldn't be adviseable to change things beyond this point
 * without considering editing segcmapassign, layout and various
 * comments and associated machinery. */

#define BACKGROUND    0x00ffffffUL

#define MOVFEATPOS_BASEBIT (24-MOVFEATPOS_BITS)
#define SEGNUM_BASEBIT (MOVFEATPOS_BASEBIT-SEGNUM_BITS)
#define ANGLE_BASEBIT 0
#define RESERVED_BITS (SEGNUM_BASEBIT-ANGLE_BITS-1)

#define MKMASK(v) (((1UL)<<(v))-1)
#define DATUM2(q,l) (((l) >> q##_BASEBIT) & MKMASK(q##_BITS))
#define RESERVED_MASK (MKMASK(RESERVED_BITS+1) << ANGLE_BITS)

/*---------- useful utility macros ----------*/
		       
#define RGB2DATUM(r,g,b) (((b)<<16) | ((g)<<8) | (r))
#define ARY2DATUM(a) RGB2DATUM((a)[0],(a)[1],(a)[2])
#define DATUM2RGB(r,g,b,l) ((r) = (l)&0xff,		\
			    (g) = ((l)>>8)&0xff,	\
			    (b) = ((l)>>16)&0xff)
#define DATUM2ARY(a,l) DATUM2RGB((a)[0],(a)[1],(a)[2],(l))

/*---------- consequences of the datum format ----------*/

#define ANGLE_TOPBIT (1<<(ANGLE_BITS-1))
#define NANGLES (1<<ANGLE_BITS)
#define ANGLE_MAX (NANGLES-1)

#define SEGNUM_MAX ((1<<SEGNUM_BITS)-1)
#define MOVFEATPOS_MAX ((1<<MOVFEATPOS_BITS)-1)


#endif /*SEGMAP_H*/
