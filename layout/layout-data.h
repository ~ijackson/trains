/*
 */
/* The output from the layout processing system to the control
 * software, describing the layout and the trains, is a data structure
 * as described in this header file, in <name-of-layout>.layout-data.o
 * (where <name-of-layout> is currently `ours').
 *
 * Alternatively, <name-of-layout>.redacted.forsafety contains data in
 * the form described at the top of redactgraph.c.
 */

#ifndef LAYOUT_DATA_H
#define LAYOUT_DATA_H

/*========== basic types etc. ==========*/

typedef int TrainNum;
typedef int SegmentNum;
typedef int MovPosComb;
typedef int BoardObject;
typedef int Small;
typedef short SegmentNumInMap;

typedef double Speed;
typedef int Distance;

typedef enum {
  mfk_none,
  mfk_point,
  mfk_relay,
  /* must also add new entries to movpos.c:methods[] and topology-dump.c */
  /* only entries up to here are used by movpos.c, since only
   *  the n_fixedmovfeats entries use mfk_ifxed. */
  mfk_fixed
} MovFeatKind;

/*========== data from config files and layout cad/cam ==========*/

typedef struct {
  unsigned next_backwards:1;
  SegmentNum next;
} SegmentLinkInfo;

typedef struct {
  const char *pname;
  MovFeatKind kind;
  Small posns;             /* for mfk_fixed: is the actual position */
  MovPosComb weight;       /* for mfk_fixed: is 0 */
  const BoardObject *boob; /* for mfk_fixed: is 0 */
} MovFeatInfo;

typedef struct {
  const char *pname;
  Distance dist;
  SegmentLinkInfo link[2]; /* 0: forwards; 1: backwards */
} SegPosCombInfo;

typedef struct {
  const char *pname;
  unsigned invertible:1, interferes_polarity_opposed:1;
  Small n_movfeats, n_fixedmovfeats;
  const MovFeatInfo *movfeats; /* first n_movfeats then n_fixedmovfeats */
  MovPosComb n_poscombs;
  const SegPosCombInfo *poscombs;
  BoardObject sense, invert;
  SegmentNum interferes;
  unsigned char interferes_movposcomb_map;
} SegmentInfo;

/* This data array has no sentinel member.  Use the info_nsegments
 * constant.  Alternatively, it is legal to sed out everything from
 * <name-of-layout>.layout-data.c from the first #include onwards, and
 * this will give a definition of NUM_SEGMENTS.
 */

extern const SegmentNum info_nsegments;
extern const SegmentInfo info_segments[];
extern const SegmentNumInMap info_segmentmap[];
extern const int info_segmentmaplen;

extern const BoardObject info_maxreverse; /* max. reverse + 1 */

#define NOTA(thing) (-1)
#define SOMEP(x) ((x) >= 0)

#endif /*LAYOUT_DATA_H*/
