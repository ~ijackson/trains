/*
 * usage:
 *   compose-segenco \
 *     thing.pf.segenco.ppm \
 *     thing.p0.segenco.ppm \
 *     thing.p1.segenco.ppm \
 *     thing.p2.segenco.ppm \
 *     ...
 *     thing.p5.segenco.ppm
 *
 * writes resulting composite encoding
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ppm.h>

#include "segcmap.h"

#define FILES (MOVFEATPOS_BITS+1)

struct file {
  const char *fn;
  FILE *f;
};

typedef unsigned char val;

static struct file f[FILES];
static int nf, fi, rows=-1, cols;
static int row, col;

static void badformat(const char *m) {
  fprintf(stderr,"bad format: %s: row=%d,col=%d:\n %s\n",
	  f[fi].fn, row,col, m);
  exit(12);
}

int main(int argc, char **argv) {
  const char *arg;
  int trows, tcols, informat;
  pixval maxval;

  ppm_init(&argc,argv);
  if (!*argv || !*++argv || (*argv)[0]=='-') {
    fprintf(stderr,"bad usage\n"); exit(12);
  }

  for (fi=0, nf=0;
       (arg= *argv++);
       fi++, nf++) {
    if (nf >= FILES) { fprintf(stderr,"too many files\n"); exit(12); }

    f[fi].fn= arg;
    f[fi].f= fopen(arg, "rb");
    if (!f[fi].f) { perror(arg); exit(8); }

    ppm_readppminit(f[fi].f, &tcols, &trows, &maxval, &informat);
    if (maxval != 255) badformat("wrong maxval");
    if (informat != RPPM_FORMAT) badformat("wrong format");

    if (rows==-1) {
      rows= trows;
      cols= tcols;
    } else {
      if (rows != trows || cols != tcols) badformat("wrong size");
    }
  }
    
  ppm_writeppminit(stdout, cols, rows, 255, 0);

  for (row=0; row<rows; row++)
    for (col=0; col<cols; col++) {
      int gotsegnum=-1, gotmovfeatpos=-1;
      val ov[3];

      DATUM2ARY(ov, BACKGROUND);
      
      for (fi=0; fi<nf; fi++) {
	val iv[3];
	unsigned long datum;
	int segnum, movfeatpos;
	
	if (fread(iv,1,3,f[fi].f)!=3) {
	  if (ferror(f[fi].f)) { perror(f[fi].fn); exit(16); }
	  else badformat("truncated file");
	}

	datum= ARY2DATUM(iv);

	if (datum == BACKGROUND)
	  continue;

	segnum= DATUM2(SEGNUM,datum);
	movfeatpos= DATUM2(MOVFEATPOS,datum);

	if (!!fi != !!movfeatpos)
	  badformat("moveable featureness disagrees with file index");

	if (gotsegnum==-1) {
	  memcpy(ov,iv,sizeof(ov));
	  gotsegnum= segnum;
	  gotmovfeatpos= movfeatpos;
	} else if (segnum!=gotsegnum || movfeatpos!=gotmovfeatpos) {
	  continue;
	}
	if (fi) {
	  datum= ARY2DATUM(ov);
	  datum |= 1UL << (fi-1 + MOVFEATPOS_BASEBIT);
	  DATUM2ARY(ov,datum);
	}
      }

      if (fwrite(ov,1,3,stdout)!=3) { perror("writing"); exit(16); }

    }
  
  return 0;
}
