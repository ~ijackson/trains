/**/

#ifndef PLAN_DATA_FORMAT_H
#define PLAN_DATA_FORMAT_H

typedef const char *const *PlanPixmapDataOnlyRef;

typedef struct PlanPixmapDataRef {
  int x, y;
  PlanPixmapDataOnlyRef d;
} PlanPixmapDataRef;

typedef struct {
  PlanPixmapDataRef pedge;
  PlanPixmapDataRef on[2/*i*/][2/*det*/];
} PlanPixmapOnData;

typedef struct {
  const char *movfeatname;
  PlanPixmapDataRef mask, unknown[2/*i*/][2/*det*/];
  int n_posns;
  const PlanPixmapOnData *posns;
} PlanSegmovfeatData;

typedef struct {
  const char *segname;
  int n_movfeats;
  const PlanSegmovfeatData *movfeats;
} PlanSegmentData;

typedef struct {
  int xsz, ysz;
  PlanPixmapDataOnlyRef background;
  int n_segments;
  const PlanSegmentData *segments;
} PlanData;

extern const PlanData ui_plan_data;

#endif /*PLAN_DATA_FORMAT_H*/
