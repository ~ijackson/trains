include(`parts.i4')

layer 0
abs zero 200 200 90

rel zero cr_c 50 150 -90
part cr pt_peco_cr c

rel zero cl_c 100 150 -90
part cl pt_peco_cl c

rel zero cy_c 150 250 90
part cy pt_peco_y c
