define(pt_circ, pt_peco_sr)
define(radius_circ, 230)

include(parts.i4)
include(circles.i4)

abs pt_i_c 400 800 130
part pt_i pt_peco_cl c
extend pt_i_a x_bl ang 1 -315
# len 0.0001
# 30
part x cross_peco_l bl
#part x cross_mythical bl
extend -x_br -pt_o_b ang 0.0030 315
part pt_o pt_peco_ml b

extend x_tl o_end uptoang 180 -315

#extend pt_i_b i_end uptoang 180 -315
rel o_end i_end 0 -37
#extend pt_i_b i_mid upto x_tl
#extend -i_end -i_mid_ ang 60 315
#join pt_i_b i_end 315

extend x_tr si_end uptoang 180 -315
extend -pt_i_c -i_begin uptoang 180 315
extend -pt_o_c -o_begin uptoang 180 315
#extend pt_o_a so_end uptoang 180 -315
rel si_end so_end 0 37

abs tcp0_p_c 1500 1100 90
part tcp0 tricirpair p_c

abs tcp1_p_c 1500 400 90
part tcp1 ^tricirpair p_c

circle(600,800)
