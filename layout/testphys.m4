# -* fundamental -*-

include(`parts.i4')

layer 4

abs begin 200 300 0
extend begin pt_c len 200
part pt pt_peco_ml c

layer 0
extend pt_a xb_a len 660

layer 8
rel xb_a xe_b 250
rel xb_a xb_b 0 -37

extend pt_b ptx_b len 50
join ptx_b xb_b 315 arcline
join xb_b xe_b 315
extend xe_b e_b len 200

layer 0
rel xe_b xe_a 0 -37
join xb_a xe_a 315
extend xe_a e_a len 200
