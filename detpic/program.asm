;======================================================================
; MAIN PROGRAM AND GLUE

  include common.inc
  code

;======================================================================
; COMMON TO MASTER AND SLAVE

;----------------------------------------
vector_reset @
	clr_f	INTCON
	bs_f	RCON, IPEN	; interrupt priorities

	call	idlocs_init

	bt_f_if0 idloc1,idloc1_master
	call	slave_init

	bt_f_if1 idloc1,idloc1_master
	call	master_init

	bs_f	INTCON, GIEH
	bs_f	INTCON, GIEL

	bt_f_if1 idloc1,idloc1_master
	goto	backgroundloop_master
	bt_f_if1 idloc1,idloc1_boarddet
	goto	backgroundloop_detectors
	goto	backgroundloop_reversers

;----------
common_local_init
; called by {master,slave}_init immediately after they've init'd i2c.
	clr_f	PCLATU
	call	bitnum2bit_init

	call	points_local_init
	call	polarity_local_init
	call	detect_local_init
	call	wagglers_local_init
	return

;----------------------------------------
panic_kill_hook @
	call	power_panichook
	call	cdu_panichook
	return

;----------
intrl_handled_routine @
	mov_ff	isr_low_save_stkptr, STKPTR
	intrl_handled_core

;======================================================================
; MASTER

;----------------------------------------
master_init
; Master-specific initialisation.
	call	memory_erase
	clr_f	flags
	call	serial_init
	call    i2cm_init
	call	serialtxbuf_init
	call    polarity_master_init
	call    points_master_init
	call    wagglers_master_init
	call	common_local_init
	call	nmra_init
	call	serialtxfc_init
	call	mascan_init
	call	tick_init
	call	cdu_init
	call	power_polarising_init
	call	power_fault_init
	call	watchdog_init
	call	i2c_consider_restartread ; sends hello when all slaves ack'd
	return

;----------------------------------------
master_interrupt_low @
	enter_interrupt_low

	Df 	debug_intrdiag
	Dl 	'|'
	Df	INTCON
	Df	INTCON2
	Df	INTCON3
	Df	PIR1
	Df	PIR2
 Df FSR2L
	Dl 0x8d
	clr_f debug_intrdiag

	call	tick_intrl
 DI
	call	nmra_serialrx_intrl
 DI
	call	power_fault_intrl
 DI
	call	serialtxfc_intrl
 DI
	call	serialtx_intrl
 DI
	call	points_local_intrl
 DI
	call	i2cm_intrl
 DI
	Dl 0x8e
	Df	INTCON
	Df	INTCON2
	Df	INTCON3
	Df	PIR1
	Df	PIR2
	Dl 0x8f
	panic	morse_IL

;----------------------------------------------------------------------
serialrx_table_section	code	0x2000

;--------------------
command_tellmode
	mov_lw	0x09
	goto	serial_addbyte

;--------------------
serialrx_generalmsg @
;command_<something>  has same calling convention:
;
;  FSR0 ->	start of message		undefined
;  *FSR0	message				undefined
;
	mov_fw	INDF0
	bra_n	serialrx_if_multibyte
	bra_z	command_crashed

	xor_lw	0x11
	bra_z	command_power_on
	xor_lw	0x10 ^ 0x11
	bra_z	command_power_off
	xor_lw	0x0a ^ 0x10
	bra_z	command_tellmode
	bra	serialrx_bad

;-----
serialrx_if_multibyte
	rr_fw	INDF0		; W =	ii MM ww ww ww ww ii ii
	and_lw	0x3c		; W =	zz zz ww ww ww ww zz zz
	bt_f_if0 PCL, 0 ; always true, but reads PCL
	add_wff	PCL
	; <--- here is zero

       	goto	serialrx_bad		; 1 0000 xxx
	goto	command_ping		; 1 0001 xxx
	goto	command_polarity	; 1 0010 xxx
	goto	command_watchdog	; 1 0011 xxx
	goto	command_point		; 1 0100 xxx
	goto	command_waggle		; 1 0101 xxx
	goto	serialrx_bad		; 1 0110 xxx
       	goto   	serialrx_bad	   	; 1 0111 xxx
	goto	serialrx_bad		; 1 1000 xxx
	goto	serialrx_bad		; 1 1001 xxx
	goto	serialrx_bad		; 1 1010 xxx
	goto	serialrx_bad		; 1 1011 xxx
	goto	serialrx_bad		; 1 1100 xxx
	goto	serialrx_bad		; 1 1101 xxx
	goto	serialrx_bad		; 1 1110 xxx
	goto	serialrx_bad		; 1 1111 xxx

;-----
serialrx_bad
	mov_ff	INDF0, t
	panic	morse_HX

code2 code
;--------------------
command_ping
	mov_fw	POSTINC0
	mov_ff	INDF0, t
	call	serial_addbyte
	mov_lw	0x5a
	xor_wfw	t
	bra_n	command_ping_bad
	goto	serial_addbyte_another

command_ping_bad panic morse_HP

;======================================================================
; SLAVE

;----------------------------------------
slave_init
; Slave-specific initialisation.
	clr_f	flags

	mov_fw	picno
	call    i2cs_init

	bc_f	T0CON, TMR0ON

	call	common_local_init
	call	detect_slave_init
	return

;----------------------------------------
slave_interrupt_low @
	enter_interrupt_low
	call	points_local_intrl
	panic	morse_IL

;----------------------------------------
i2csu_write_begin
	return

near_local_do code
;----------------------------------------
i2csu_write_data
	call	led_green

	bt_w_if0 7
	bra	i2csu_write_if_special
	bt_w_if1 6
	bra	polarity_local_do
	bt_w_if0 5
	bra	point_local_do
	bra	waggle_local_do

i2csu_write_if_bad
	mov_wf	t
	panic	morse_DX

i2csu_write_if_special
	tst_w_ifnz
	bra	i2csu_write_if_bad
	goto	panic_crashread_commanded

code3 code
;======================================================================
; MASTER/SLAVE deviations

;----------
message_for_master @
; Either transmits the message to the master, or if we are the master,
; handles it as an incoming message from the notional `slave 0'.
;  W		message		unchanged
;  GIEH		set		set
	bt_f_if0 idloc1,idloc1_master
	goto	slave_add_short_message
	goto	loopback_read_byte

;======================================================================
  include final.inc
