;======================================================================
; TICK - REGULAR 10MS TIMER INTERRUPT
;
; after tick_init, and after interrupts enabled,
; we call <foo>_tick every 1ms and <foo>_tickdiv every <tickdiv>ms.

  include common.inc

  udata_acs
tickdiv_count	res	1

  code
;----------------------------------------
tick_init @
;  Timer 2		any			used for tick
	mov_lw	tick_master_t2scale ; disable (in case already running)
	mov_wf	T2CON
	mov_lw	tick_master_t2cycles
	mov_wf	PR2
	bc_f	PIR1, TMR2IF ; clear any previous interrupt
	bc_f	IPR1, TMR2IP ; low priority
	bs_f	PIE1, TMR2IE ; enable interrupts
	bs_f	T2CON, TMR2ON ; enable timer
	mov_lw	tickdiv
	mov_wf	tickdiv_count
	return

;----------------------------------------
tick_intrl @
	bt_f_if0 PIR1, TMR2IF
	return
	; we have an interrupt

	bc_f	PIR1, TMR2IF

	; add calls to <foo>_tick here:
	call	power_fault_tick
	call	power_polarising_tick
	; end of list of calls to <foo>_tick

	dec_f_ifz tickdiv_count
	bra	tickdiv_do

	intrl_handled_nostack

;----------------------------------------
tickdiv_do
 Dl '/'
	; add calls to <foo>_tickdiv here:
	call	power_fault_tickdiv
	call	watchdog_tickdiv
	call	cdu_tickdiv
	; end of list of calls to <foo>_tickdiv

	mov_lw	tickdiv
	mov_wf	tickdiv_count
	intrl_handled_nostack

;----------------------------------------------------------------------
  include final.inc
