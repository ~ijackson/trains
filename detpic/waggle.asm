;======================================================================
; WAGGLE - SET PIN HIGH OR LOW

  include common.inc

;----------------------------------------
; LOCAL WAGGLERS - Variables

maxwagglers equ 16

waggle_lpins equ 0x3a0 ; has to be 0x?a0
waggle_lpins_section udata waggle_lpins
  res maxwagglers*2
	; Each pair of bytes is:
	;	bitmask containing only the relevant bit
	;	bottom byte of LAT*

;----------------------------------------
; MASTER - Access bank variables etc.

  udata_acs				; idle		busy
waggle_slave		res	1	; 11111111	000 SSSss
waggle_message		res	1	; ????????	101 wwwwY

;======================================================================
; LOCAL WAGGLERS

near_local_do code
;----------
waggle_local_do @
; On master, called during serial receive, ie Low ISR
; On slave, called during i2c receive, ie High ISR
;		W	fire point msg		undefined
;
;	on entry:		; W =		101 WWWWY

	mov_wf	t		; t =		101 WWWWY

	intrlh_fsr0_save
	mov_lfsr waggle_lpins | 0x01, 0
				; FSR0 =   aaaa 000 00001
	ior_wff	FSR0L		; FSR0 =   aaaa	101 WWWW1
				; *FSR0 =	bbbb bbbb  (one-bit mask)
	mov_fw	POSTDEC0	; W =		bbbb bbbb
				; *FSR0 =	LAT*-addrl
	bra_z	waggle_nonexistent

	mov_ff	INDF0, FSR0L	; FSR0L =	LAT*-addr
	set_f	FSR0H		; FSR0 =   1111	LAT*-addr
				; *FSR0 =	<latch>

	bt_f_if0 t, 0		; test Y
	bra	waggle_ifclear
	; set:

	ior_wff	INDF0
;...
waggle_local_done
	intrlh_fsr0_restore
	return

;-----
waggle_ifclear
	com_w
	and_wff	INDF0
	bra	waggle_local_done

;----------
waggle_nonexistent
	panic	morse_AU

;----------------------------------------------------------------------
; LOCAL WAGGLERS -  INITIALISATION

;----------------------------------------
wagglers_local_init @
 outputs_local_init picno2wagglemap, maxwaggles, waggle_lpins, bkwaggleix2portnumbitnum
	return

;======================================================================
; MASTER

; No serious initialisation needed;
; we just pass on the commands as they come in.

wagglers_master_init @
	set_f	waggle_slave
	return

;----------------------------------------------------------------------
command_waggle @
				;	      1010 1sss OSS wwwwV
				;
				; 	ie		1010 1sss
				;			OSS wwwwV
	bt_f_if0 waggle_slave, 7
	bra	command_waggle_busy

	mov_fw	PREINC0			; F0->[1], W =	OSS wwwwV  N = 0
	bra_n	command_waggle_badmsg
	and_lw	0x1f			; W =		000 wwwwV
	xor_wff INDF0			; [1] =		0SS 00000
	ior_lw	0xa0			; W =		101 wwwwV
	mov_wf	waggle_message		; message =	101 wwwwV

	rr_fw	POSTDEC0		; F0->[0], W =	00SS 0000
	rr_w				; W =		000S S000
	xor_wfw	INDF0			; W =		101S Tsss
	xor_lw	0xa8			; W =		000S Ssss
	mov_wf	waggle_slave		; slave =	000S Ssss

	bra_z	waggle_local_do
	goto	i2c_needwrite

;----------
waggle_needwrite @
	bt_f_if1 waggle_slave, 7
	return
	pop
	mov_fw	waggle_slave
	goto	i2c_needwrite

;----------
waggle_getwritebyte @
	mov_fw	waggle_slave
	bra_n	waggle_getwritebyte_ifidle
	cmp_fw_ifne cwslave
waggle_getwritebyte_ifidle
	return
	;...
	mov_lw	0x04 ; WAGGLED
	call	serial_addbyte
	; actually it's not waggled until the slave acks it but
	;  that's a foregone conclusion

	mov_fw	waggle_message
	set_f	waggle_slave
	goto	i2c_getwritebyte_yes

command_waggle_badmsg panic morse_AX
command_waggle_busy panic morse_AB

;======================================================================
  include final.inc
