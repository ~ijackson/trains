;======================================================================
; NMRA DATA TO TRACK - also, initially deals with all incoming RS232
;
;======================================================================
; TESTING INSTRUCTIONS
;
; can be tested with liberator's mic input:
;   1k1, 110k resistor divider from track gives 1/100 track
;   voltage to mic input
; then
;   sox -r 96000 -u -b -t ossdsp /dev/dsp t.dat
;   perl -pe '$_= m/\-/ ? "_" : "~"' <t.dat | fold -290 | perl -pe 's/$/\n\n\n/' >u
; in an xterm showing font "tiny" and then resized to eg 295x54
;   less u
;
;======================================================================

clock equ mclock ; this part runs really only on master

  include common.inc

  udata_acs

nmractrl	res	1	; state relevant to NMRA control
transmitbit	equ	7	; bit 7: 0/1 bit currently being transmitted
nextaction	equ	6	; bit 6: change polarity on next interrupt y/n
  ; nmractrl is a bitmask, and only the bits above are used

fromserial	res	1	; from-serial buffer location (in buffer bank)
totrack		equ	FSR2L	; to-track buffer location (in buffer bank)
totrackbit	res	1	; bit location of pointer within byte
  ;
  ; buffers are each 16 bytes (this is hardwired)
  ; and there are four of them starting at 0x500, 0x510, 0x520, 0x530.
  ; plus a copy of the watchdog `stop everything' command at 0x540.
  ;
  ; fromserial and totrack are pointers into the buffer page
  ;
  ; fromserial points to the first empty byte, where we will put new
  ; data provided by the host; this means that (fromserial & 0xf0) is
  ; the first buffer which fails to contain a complete nmra message.
  ; fromserial is updated only by the (low priority) serial receive
  ; interrupt handler.  Furthermore, it always maintains a correct
  ; and valid value for use by the high-priority nmra readout ISR.
  ;
  ; totrack points to the first remaining full byte, which is currently
  ; being transmitted; if nothing is currently being transmitted
  ; because there is no nmra data at all then totrack points to the
  ; start of the same buffer as fromserial
  ;
  ; We maintain the following properties, notionally:
  ;
  ;  totrack <= fromserial < totrack
  ;
  ;            \            \
  ;             \            `- equal would mean overrun
  ;              \
  ;               `- equal if we have nothing but
  ;                   idle to trasmit
  ;

bufferpage	equ	5

buffer_section udata bufferpage << 8
buffer0 res 16
buffer1 res 16
buffer2 res 16
buffer3 res 16
bufferw res 16

  code

;****************************************************************************

macros

; macro to call subroutine to transmit over serial port for debugging
; takes 8-bit value, puts in W, invokes debug_serial_transmit

	ifndef	SLOW_VERSION
debug macro debugvalue
 ; uncomment this to get copious stuff in the debug buffer
 ; NB that strictly this does not work because debugbyte messes with
 ; FSR0[LH] and if we interrupt debugbyte we will trash FSR0
 ;	Dl	debugvalue
	endm
	endif

	ifdef	SLOW_VERSION
debug macro debugvalue
	mov_lw	debugvalue
	call	debug_serial_transmit
	endm

debug_serial_transmit
	mov_wfa	TXREG  		; move contents of W (i.e. debugvalue)
	endif
				;	to TXREG for transmission
waitfortsr
	bt_fa_if0 TXSTA,1
	bra	waitfortsr
	return

;****************************************************************************

serial_init @
; serial set-up
; initial config - TXSTA register p181
        bc_fa     TXSTA,6  	; p181, set 8-bit mode
  	bs_fa	TXSTA,5  	; transmit enable
	bc_fa	TXSTA,4  	; asynchronous mode
	bsc_txsta_brgh

; initial config - RCSTA register p182
        bs_fa	RCSTA,7  	; serial port enable (p182)
	bc_fa	RCSTA,6  	; 8-bit reception
	bs_fa	RCSTA,4  	; enable continuous receive

; set SPBRG to get correct baud rate according to table top right p186
; (Tosc = 20MHz, desired baud rate = 9600)
	movlw_movwf_spbrg

	mov_lw	~((1<<RCIP) | (1<<TXIP))
	and_wff	IPR1		; serial interrupts: low priority
	mov_lw	(1<<RCIE) | (1<<TXIE)
	ior_wff	PIE1		; serial interrupt: interrupt enable

	return

;----------------------------------------------------------------------------

nmra_init @
; timer 0 set-up
; timer0 initial config for NMRA timer

	ifdef	SLOW_VERSION
        bc_fa     T0CON,6         ; p107 Timer0 -> 16bit mode (testing)
	endif

	ifndef	SLOW_VERSION
        bs_fa     T0CON,6         ; p107 Timer0 -> 8bit mode (not-testing)
	endif

        bc_fa     T0CON,5         ; timer0 use internal clock

	ifndef	SLOW_VERSION
	mov_fw	T0CON
	and_lw	~((1<<PSA)|(1<<T0PS2)|(1<<T0PS1)|(1<<T0PS0))
	ior_lw	nmra_master_t0scale
	mov_wf	T0CON		; prescale value calculated from
				;  program.clocks (not-testing)
	endif

	ifdef	SLOW_VERSION
        bc_fa     T0CON,3         ; use prescaler
        bs_fa     T0CON,2         ; }
        bc_fa     T0CON,1         ; } prescale value 1:16
        bc_fa     T0CON,0         ; } (testing)
	endif

	debug 	'b'	; write 'b' to serial port
;----------------------------------------------------------------------------

; initialise buffers (for bank 5, for nmra from-serial/to-track buffers)

	mov_lw	bufferpage
	mov_wf	FSR2H

	clr_fa	nmractrl  	; for bits relevant to control of nmra stream
	clr_fa	fromserial  	; for loc'n of write-from-usart ptr in buffers
	clr_fa	totrack  	; for loc'n of send-to-track ptr in buffers
				; all in access bank
	clr_f	acknmra

	debug 	'c'	; write 'c' to serial port
;----------------------------------------------------------------------------


; initialise next action/transmit bit
	bs_fa	nmractrl,nextaction
	bs_fa	nmractrl,transmitbit

nmra_restartmessage @	; Entrypoint from power_polarising_tick, at end
			;  of settle time.  Goes back to beginning of
			;  current message (if any) and retransmits it.
			;  Also, enables booster PWM and Timer 0 interrupts.
	mov_lw	0xf0
	and_wff	totrack

; initialise totrackbit bitmask
	mov_lw	0x80
	mov_wfa	totrackbit  	; make bit mask be 1000 0000

; initialise booster direction
	bc_fa	TRISC,0  	; make pin 0 (booster direction) output
	bc_fa	PORTC,0  	; set low initially

; set booster pwm high
	bs_fa	PORTC,1  	; booster pwm high
	bc_fa	TRISC,1  	; make pin 1 (booster pwm) output

	debug 	'd'	; write 'd' to serial port
;----------------------------------------------------------------------------

; interrupt set-up

; interrupt set-up for timer0 interrupt p79/80
	bs_fa	INTCON2,2  	; timer0 overflow high priority

	bs_fa	INTCON,5  	; enable timer0 interrupts
	; ^ to disable NMRA output and get DC onto the track,
	;    comment out the line above


	debug 	'e'	; write 'e' to serial port

	return

;****************************************************************************

nmra_sendwatchdog_start @
	mov_lw	0x40	; if we were in a message it's going to get
	mov_wf	totrack	; garbled; oh well.
	return

nmra_sendwatchdog_stop @
	bt_f_if0 totrack, 6
	return ; not transmitting watchdog
	mov_fw	fromserial
	and_lw	0x30
	mov_wf	totrack
	return

;****************************************************************************

nmra_serialrx_intrl @
	bt_f_if0 PIR1,RCIF	; check whether serial recv interrupt bit set
	return

	; serial_receive:

	debug 	'h'	; write 'h' to serial port
	Df	fromserial
	Df	totrack

	bt_fa_if1 RCSTA,FERR  		; if FERR set (= framing error), then panic
	goto	panic_ferr
	bt_fa_if1 RCSTA,OERR  		; if OERR set (= overrun error), then panic
	goto	panic_oerr

	mov_ff	fromserial,FSR0L	; set low byte of INDF0 pointer
	mov_lw	bufferpage
	mov_wfa	FSR0H  			; set high byte of INDF0 pointer
	debug 	'1'
	mov_fw	RCREG			; get received byte
	mov_wf	INDF0			; copy to received register

; check whether bit 7 is clear.
; If so, move to next buffer (or process other kind of message)
	bra_nn	end_message		; (STATUS N still from mov_fw RCREG)
	debug 	'3'

; If not, move to next byte
	inc_fa	fromserial    		; advance fromserial pointer by 1 byte
	debug 	'4'

	mov_lw	0x0f
	and_wfw	fromserial
	bra_z	receive_message_too_long
serial_receive_done
	intrl_handled_nostack

receive_too_much_nmra		panic	morse_HN
receive_message_too_long	panic	morse_HW

; *** I *think* the interrupt bit is cleared by reading out of RCREG
; but this may be something to try in debugging if stuff doesn't work

end_message
; so what's the first byte then ?
	mov_lw	0xf0
	and_wff	FSR0L
	and_wff	fromserial		; fromserial := 00BB0000
	com_fw	INDF0			;  where BB is this buffer
	bra_nz	not_nmra_message

; so, first byte is FF (since complement of it is zero)
; so, move to next buffer
advance_write_buffer

; increment top 4 bits of fromserial (already cleared low 4 bits)
	debug 	'5'
	mov_fw	fromserial		; W =		00BB0000
	add_lw	0x10			; W =		0?CC0000
	bc_w	6			; W =		00CC0000
	mov_wf	t			; t =		00CC0000
					;  where BB is this buffer
					;   and CC is next buffer

	xor_wfw	totrack			; W =		0WDD????
	and_lw	0x70			;		.WDD....
	bra_z	receive_too_much_nmra	; where DD is (fromserial buffer)
					;  xor (totrack buffer)
					; and W is 1 iff xmitting watchog

	mov_fw	t			; W =		00CC0000
	mov_wf	fromserial

	bra	serial_receive_done

not_nmra_message
	; we've already set FSR0 and fromserial to point
	;  back to beginning of the same buffer
	call	serialrx_generalmsg
	bra	serial_receive_done

;****************************************************************************

near_interrupt_high code

master_interrupt_high_notnmra
	panic	morse_IH

master_interrupt_high @
	bt_f_if0 INTCON,TMR0IF	; check whether timer0 interrupt set
	bra	master_interrupt_high_notnmra
	; timer0 interrupt

	debug 	','	; write ',' to serial port
	Df	fromserial
	Df	totrack
	Df	nmractrl

	bc_fa	INTCON,TMR0IF  	; clear interrupt-set bit

	ifdef	SLOW_VERSION
	mov_lw	0x01	; (testing)
	endif

	ifndef	SLOW_VERSION
	mov_lw	nmra_master_t0init
	endif

	mov_wfa	TMR0L  		; set timer0 to appropriate value (so interrupt takes 58us)


	debug 	'k'	; write 'k' to serial port
; check next action - if 0, change to 1 and return
	bt_fa_if1 nmractrl,nextaction
	bra	toggle_output
	bs_fa	nmractrl,nextaction
	debug 	'1'	; write 'k' to serial port
	retfie_r


; if next action = 1, then toggle output

toggle_output
	debug 	'l'	; write 'l' to serial port
	btg_fa	LATC,0  	; toggle booster output pin
	bt_fa_if0 LATC,0
	bra	decide_next_bit
	; we're in mid-bit:
; if transition was 0->1 then we are mid-bit, so copy transmitbit to
; nextaction in preparation for 2nd half of bit and then return

	debug 	'm'	; write 'm' to serial port
	bc_fa	nmractrl,nextaction
	bt_fa_if1 nmractrl,transmitbit
	bs_fa	nmractrl,nextaction
	debug 	'2'	; write 'k' to serial port
	retfie_r


decide_next_bit

	debug 	'n'	; write 'n' to serial port
; check whether current to-track buffer = current from-serial buffer
; if yes, transmit 1s (set transmitbit=1, nextaction=1 and return)

	mov_fw	fromserial
	xor_wfwa totrack
	and_lw	0x70
	bra_nz	read_from_buffer
	bs_fa	nmractrl,transmitbit
	bs_fa	nmractrl,nextaction
	debug 	'3'
	retfie_r


read_from_buffer
	debug 	'o'

; if currently on bit 7, want to skip to bit 6
;*** wouldn't it be easier to start on bit 6 ? :-)  -iwj

	bt_fa_if1 totrackbit,7
	rr_fa	totrackbit    		; rotate mask right

; if not on bit 7 ,
; set na=cb=bit value, advance bit (i.e. rotate transmitbit right),
; check if bit7, if so, advance byte, return

	debug 	'p'
	mov_fw	INDF2
	and_wfwa totrackbit    		; select bit to be transmitted

	bra_z	zero_bit_to_track
	bra	one_bit_to_track

zero_bit_to_track
	debug 	'_'	; write 'q' to serial port
	debug 	'0'	; write 'q' to serial port
	debug 	'_'	; write 'q' to serial port
	bc_fa	nmractrl,transmitbit
	bc_fa	nmractrl,nextaction
	bra	advance_bit

one_bit_to_track
	debug 	'_'	; write 'q' to serial port
	debug 	'1'	; write 'r' to serial port
	debug 	'_'	; write 'q' to serial port
	bs_fa	nmractrl,transmitbit
	bs_fa	nmractrl,nextaction
	bra	advance_bit



advance_bit
; rotate transmitbit to next position

	debug 	's'	; write 's' to serial port
	rr_fa	totrackbit    		; rotate mask right
;*** surely rrnc (`rotate right not through carry' I assume)
;*** will leave a copy of the top bit in the N flag ?  Then you
;*** can use branch if negative.  -iwj
	bt_fa_if1 totrackbit,7
	rcall	advance_pointer
	debug 	'5'	; write 's' to serial port

	retfie_r



advance_pointer

	debug 	't'	; write 't' to serial port

; currently on bit 7 of the byte, after having read rest of byte to
; track; check whether it is 1 or 0

; if clear, move to next buffer
	bt_fa_if0 INDF2,7
	bra	advance_read_buffer

; if set, move to next byte of samebuffer (increment totrack)
; (will be on bit 7 at this point anyway so no need to change totrackbit)
	bt_fa_if1 INDF2,7
	inc_fa	totrack
	return


advance_read_buffer

; move pointer to next buffer
; clear low 4 bits of totrack and increment top 4 bits
; aaaabbbb -> bbbbaaaa -> bbbb(aaaa+1) -> 0000(aaaa+1) -> (aaaa+1)0000
	bt_f_if1 totrack, 6 ; were we doing watchdog broadcasts ?
	bra	advance_read_buffer_ifwatchdog

	mov_fw	totrack
	add_lw	0x10
	and_lw	0x30
	mov_wf	totrack
	debug 	'9'	; write '9' to serial port

	inc_f	acknmra
	bs_f	PIE1, TXIE	; ensure we tell host to send us more
	bt_f_if0 acknmra, 2
	return
	; oops:
	panic	morse_HM

advance_read_buffer_ifwatchdog
	; just loop round the buffer
	mov_lw	0x40
	mov_wf	totrack
	return

;****************************************************************************

panic_oerr	panic	morse_HO
panic_ferr	panic	morse_HF

;****************************************************************************

  include final.inc
