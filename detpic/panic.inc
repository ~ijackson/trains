;---------------------------------------------------------------------------
; macro is passed a message code morse_XY and panics displaying this message
; by flashing the perpic LED.  After disabling interrupts and resetting
; the stack this will call panic_kill_hook (which should shut down any
; scary peripherals).

panic macro message
        movlw   (message - morse_messages_start)/4
	goto	panic_routine
        endm

;--------------------
; Area filled in by morse-auto.asm (see morse-auto.asm-gen for details)
morse_messages_start    equ     0x4000
morse_messages_end      equ     0x4400
