# -*- perl -*-

package NmraAssist;

BEGIN {
    use Exporter ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw();
    %EXPORT_TAGS = ( );

    @EXPORT_OK   = qw();
}

package NmraAssist::HighPassFilter;

# We subtract an alpha-smoothed value
our $default_timeconstant= 300e-6; # seconds, e-life

sub new ($) {
    my ($class) = @_;
    my $sf= {
	SmoothedValue => 0,
	TimeConstant => $default_timeconstant,
	LastSampleTime => 0
    };
    bless $sf, $class;
    return $sf;
}

sub transform ($$$) {
    my ($sf, $t, $value) = @_;

    my $interval= $t - $sf->{LastSampleTime};
    $sf->{SmoothedValue} *= exp( -$interval / $sf->{TimeConstant} );
    $sf->{SmoothedValue} += $value * $interval / $sf->{TimeConstant};
    $sf->{LastSampleTime}= $t;
    $value -= $sf->{SmoothedValue};
    return $value;
}

1;
