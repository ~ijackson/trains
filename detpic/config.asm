; -*- fundamental -*-

	include		/usr/share/gputils/header/p18f458.inc

;	__config	_CONFIG1L, 0xff
	__config	_CONFIG1H, _OSCS_OFF_1H & _ECIO_OSC_1H
	__config	_CONFIG2L, _BOR_ON_2L & _PWRT_ON_2L & _BORV_45_2L
	__config	_CONFIG2H, _WDT_OFF_2H
;	__config	_CONFIG3L, 0xff
;	__config	_CONFIG3H, 0xff
	__config	_CONFIG4L, _DEBUG_OFF_4L & _LVP_OFF_4L & _STVR_ON_4L
;	__config	_CONFIG4H, 0xff
	__config	_CONFIG5L, 0xff
	__config	_CONFIG5H, 0xff
	__config	_CONFIG6L, 0xff
	__config	_CONFIG6H, 0xff
	__config	_CONFIG7L, 0xff
	__config	_CONFIG7H, 0xff

	end
