;======================================================================

	radix dec

picno2ptmap		equ	0x6100
bkptix2portnumbitnum	equ	0x6000

picno2wagglemap		equ	0x6240
bkwaggleix2portnumbitnum equ	0x6200

pic2detinfo		equ	0x6040
picno2revmasks		equ	0x6080

maxpics_ln2		equ	5
maxpics			equ	1 << maxpics_ln2

maxpoints_ln2		equ	5
maxpoints		equ	1 << maxpoints_ln2

maxwaggles_ln2		equ	4
maxwaggles		equ	1 << maxwaggles_ln2
