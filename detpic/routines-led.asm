;**********************************************************************
; routines-led
; subroutines for controlling the per-pic LED (pin 21, DS100)

	include		common.inc

	code

;----------------------------------------
led_red @
; makes the LED be red (or orange), no matter its previous state
; no RAM locations used or modified
; W is unchanged
	bcf		LATD, 2, 0		; output low
	bcf		TRISD, 2, 0		; output enabled
	return

;----------------------------------------
led_green @
; makes the LED be green (or blue), no matter its previous state
; no RAM locations used or modified
; W is unchanged
	bsf		LATD, 2, 0		; output high
	bcf		TRISD, 2, 0		; output enabled
	return

;----------------------------------------
led_black @
; makes the LED be black (off), no matter its previous state
; no RAM locations used or modified
; W is unchanged
	bsf		TRISD, 2, 0		; disable flasher output
	return

	include		final.inc
