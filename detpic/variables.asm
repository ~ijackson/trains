;======================================================================

  include pindata.inc

  udata_acs
picno		res	1	; from first idlocs byte

idloc1		res	1	; from 2nd idlocs byte; bit 7 is master:
idloc1_master		equ 7
idloc1_boarddet		equ 6

acknmra		res	1	; must tell host about NMRA buffer state
flags		res	1	; miscellaneous state flags
flags_polarising	equ 6

t		res	1	; general temporary
u		res	1	; general temporary (another)
v		res	1	; general temporary (another)
t_dolocal	res	1	; temporary for <foo>_local_do
outmsg_end	res	1	; first empty byte in outbuf
outmsg_begin	res	1	; first full byte in outbuf

isr_low_save_w		res	1 ; see {enter,return}_interrupt_low
isr_low_save_status	res	1 ;  in common.inc
isr_low_save_stkptr	res	1 ;

isr_lh_save_fsr0	res	2 ; for intrlh_fsr0_{save,restore}

xdebug			res	8

cwslave			res	1 ; slave we're currently actually writing to
				  ; undefined except in <something>_needwrite

debug_intrdiag		res	1 ; used only for debugging, interrupt
				  ;  source recording

;----------------------------------------------------------------------
; NON-ACCESS-BANK SECTIONS (TABLES)

outbuf_section udata 0x200
outbuf_szln2	equ	7
outbuf_size	equ	(1<<outbuf_szln2)
outbuf		res	outbuf_size

bitnum2bit_section udata 0x0f8
bitnum2bit	res	8	; bitnum2bit[x] = 1<<x

slavetable equ 0x400
slavetable_section udata slavetable
; each board actually present has an entry in this table
ste_szln2	equ	3
ste_size	equ	(1<<ste_szln2)
  res maxpics * ste_size

; each entry is a number of bytes, at these offsets:
ste_slave	equ 0 ; Gk slave number
ste_flags	equ 1 ; ** flags (stf_...), see below
ste_detbasel	equ 2 ; dk added to 11111bbb to make 0SSSSSSS; ie first-0xf8
ste_lastd0	equ 3 ; d  } [o0]*  ie every bit is either 0 (for an
ste_lastd2	equ 4 ; d  }        irrelevant bit) or o, meaning the
ste_lastd1	equ 6 ; d  }        previously seen detection data bit
ste_detmsgh	equ 7 ; dk 1 001 1 000 being 1 001 Y SSS
;             offset^   ^which module(s) use this data:
;			  G	global - for use by any code
;			  d	for master detection, see mascan.asm
;			  *	varies per bit
;			  .k	entry is constant, created during init'n
; flags in ste_flags:
stf_detect	equ 7 ; Gk  this is a detectors board
stf_sentinel	equ 6 ; Gk  sentinel slot at end of table
stf_responding	equ 5 ; G   board has ack'd i2c addressing at least once

  include final.inc

;======================================================================
