;======================================================================
; WATCHDOG TIMEOUTS

  include common.inc

  udata_acs
watchdog	res	1

  code
;----------------------------------------------------------------------
command_watchdog @
	mov_fw	POSTINC0	; W =		10011OOO
	and_lw	0x07		; W =		00000OOO
	bra_nz	command_watchdog_bad
	mov_fw	INDF0		; W =		OMMMMMMM
	bra_n	command_watchdog_bad
	mov_wf	watchdog	; watchdog =	OMMMMMMM

	goto	nmra_sendwatchdog_stop

command_watchdog_bad panic morse_WX

;----------------------------------------------------------------------
watchdog_tickdiv @
	tst_f_ifnz watchdog
	dec_f_ifnz watchdog
	return
	; oops, we've timed out

	call	power_watchdog_timeout
	mov_lw	0x0d
	goto	serial_addbyte

;----------------------------------------------------------------------
watchdog_init @
	clr_f	watchdog
	mov_lfsr bufferw, 1

wdog_byte macro wdog_byte_v
	mov_lw	wdog_byte_v
	mov_wf	POSTINC1
	endm

	; An extra idle byte:
	set_f	POSTINC1	; ff
	; Baseline Broadcast stop Forwards(I) non-Emergency S9.2 B l.98-
	; as computed by ./hostside-old -st bstop
	set_f	POSTINC1	; ff
	set_f	POSTINC1	; ff
	wdog_byte		0x80
	wdog_byte		0x87
	wdog_byte		0x81
	wdog_byte		0x61

	return

;======================================================================
  include final.inc
