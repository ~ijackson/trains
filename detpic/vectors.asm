;======================================================================
; VECTORS: special locations, where the PIC starts executing
; after reset and interrupts

	include common.inc

;----------
; reset vector
        org     0
        goto    vector_reset

;----------
; high priority interrupt
        org     000008h

	bt_f_if1 idloc1,idloc1_master
        goto    master_interrupt_high
	call	i2cs_interrupt
	retfie_r

;----------
; low priority interrupt
        org     000018h
	bt_f_if1 idloc1,idloc1_master
        goto    master_interrupt_low
	goto	slave_interrupt_low

;======================================================================

triplinker_size equ 0x0a00
; these seem to trick the linker's rather simple algorithm
; into tesselating the code into only the first part of each code
; page, which should speed up programming

triplinker1 org 0x0000 + triplinker_size - 2
	nop
triplinker2 org 0x2000 + triplinker_size - 2
	nop

	include final.inc
