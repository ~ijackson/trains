; macro for copying bits about

copybit_mi macro sourcereg, sourcebitno, destreg, destbitvals, xorinv
	movf	destreg, 0, 0			; read output latch
	andlw	~destbitvals			; mask out output bit
	btfsc	sourcereg, sourcebitno, 0	; skip if input clear
	iorlw	destbitvals			;    add output bit
	xorlw	xorinv				; invert?
	movwf	destreg, 0			; write output latch
	endm						; (6cy total)

copybiti macro sreg, sbitno, dreg, dbitvals
	copybit_mi	sreg, sbitno, dreg, dbitvals, dbitvals
	endm

copybit macro sreg, sbitno, dreg, dbitvals
	copybit_mi	sreg, sbitno, dreg, dbitvals, 0
	endm
