;======================================================================
;
; these routines all use serial_write_char which writes
; bytes to the serial port synchronously

	radix		dec
	include		../iwjpictest/insn-aliases.inc

	udata_acs
serial_hex_temp res 1

 code
;----------------------------------------------------------------------

;----------------------------------------
serial_write_hex
;	transmits W in hex through serial port, using serial_write_char
;			Before		After
; W			value		undefined
; serial_hex_temp	undefined	undefined
	mov_wf		serial_hex_temp
	rcall		serial_write_hex_1digit_for_both
	rcall		serial_write_hex_1digit_for_both
	return

;--------------------
serial_write_hex_1digit_for_both
;	transmits top nybble of serial_hex_temp in hex
;	through serial port, as above, and swaps nybbles
;		Before		After
; W			any		undefined
; serial_hex_temp	BBBBaaaa	aaaaBBBB	(BBBB was sent)
	swap_f	serial_hex_temp
	mov_fw	serial_hex_temp
;...
;--------------------
serial_write_hex_digit
;	transmits bottom nybble of W in hex
; W		????VVVV	undefined
	and_lw	0x0f
	sub_lw	10
	sub_lw	0
	bra_n	serial_write_hex_digit_ifnot_ge10
	add_lw	'a'-('0'+10)
serial_write_hex_digit_ifnot_ge10
	add_lw	'0'+10
	bra	serial_write_char

;======================================================================
 include ../iwjpictest/syncwrite.inc
 end
