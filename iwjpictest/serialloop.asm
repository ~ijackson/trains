; -*- fundamental -*-

; This program:
;   * copies serial port flow control and data straight through,
;     as a serial port loopback test
;   * copies a few bits about
;   * flashes PER-PIC-LED
;   * leaves all other pins set to their default states (usually Z).
; as shown in diagram SERIAL LOOP TEST
; 
	include		/usr/share/gputils/header/p18f458.inc
	radix		dec
	include		onecopybit.inc

COUNTINNER	equ		0x00
COUNTOUTER	equ		0x02

 if mclock>=20000
OUTEREXP		equ		7	; 2^7 * 1.4ms = 177ms
 else
 if mclock==1000
OUTEREXP		equ		3	; 2^3 * 28ms = 224ms
 endif
 endif

; we always leave each loop when its counter has reached 0, so on
; entry we assume it's already 0.  For the loops which want to
; count all the way (256 iterations) that's good.

	code

start
	bcf		TRISD, 2, 0	; enable per-pic led output
	bcf		TRISC, 6, 0	; enable TXD output (RC6)
	bcf		TRISC, 5, 0	; enable FCO output (RC5)
	bcf		TRISD, 4, 0	; enable output D

loop
	btg		LATD, 2, 0	; toggle per-pic led output

	; set a bit which says how fast the led
	; should flash and count down from 2^(that bit)
	bsf		COUNTOUTER, OUTEREXP, 0
delayouter_loop

delayinner_loop
	copybit		PORTB, 3, LATA,  0x08		; A data
	copybiti	PORTD, 7, TRISA, 0x08		; A enable
	copybiti	PORTA, 6, LATD,  0x10		; D
	copybit		PORTB, 4, LATC,  0x20		; serial FC
	copybit		PORTC, 7, LATC,  0x40		; serial data
						; 6 x copybit @6 = 24cy

	decfsz		COUNTINNER, 1, 0	; 1 cycle
	goto		delayinner_loop		; 2 cycles (skipped or not)
; exited delayinner_loop			; total: 27cy * 256 = 6912cy

						; each cycle 0.2us
						; so each inner loop is ~1.4ms

	decfsz		COUNTOUTER, 1, 0
	goto		delayouter_loop
; exited delayouter_loop

	goto		loop

	end
