;----------------------------------------------------------------------
; various definitions for clock variation

 radix dec

;----------------------------------------------------------------------
; for serial port at 9600
; according to table in datasheet top right p186

 if mclock==32000
serial_brgh equ (1<<BRGH)
serial_spbrg equ 207
 endif
 if mclock==20000
serial_brgh equ (1<<BRGH)
serial_spbrg equ 129
 endif
 if mclock==1000
serial_brgh equ (1<<BRGH)
serial_spbrg equ 6 ; 7% error :-(
 endif

bsc_txsta_brgh macro
 if serial_brgh
	bsf	TXSTA,2,0	; set high baud rate
 else
	bcf	TXSTA,2,0	; set low baud rate
 endif
 endm

movlw_movwf_spbrg macro
 if serial_spbrg > 0
 else
  error "unsupported clock speed (serial_spbrg)"
 endif
	movlw	serial_spbrg
	movwf	SPBRG,0
	endm

;----------------------------------------------------------------------
; for i2c at 50kbit/s

i2c_kbitpersec equ 50
i2c_sspadd equ (mclock/(i2c_kbitpersec*4)) - 1 ; 50kbit/s

;----------------------------------------------------------------------
; NMRA at 50us per division
; ... uh, this isn't going to work at 1MHz because that's only 12.5
; insns per NMRA division


;----------------------------------------------------------------------
; busy-wait delay loop, originally from flasher.asm

implement_busywait_delay macro tclock

; we always leave each loop when its counter has reached 0, so on
; entry we assume it's already 0.  For the loops which want to
; count all the way (256 iterations) that's good.

	; set a bit which says how fast the led
	; should flash and count down from 2^(that bit)
 if tclock >= 3915
  local fast=1
  local medium_cycles=198000
 else
  local fast=0
  local medium_cycles=768
 endif
			; now 1/(medium_period in ms)
			; = tclock / (medium_cycles*4)
	movlw		50 * tclock / medium_cycles ; 200ms
	movwf		delay_countslow, 0

 local delayslow_loop
 local delaymedium_loop
delayslow_loop

delaymedium_loop

 if fast
 local delayfast_loop
delayfast_loop
	decfsz		delay_countfast, 1, 0	; 1 cycle
	goto		delayfast_loop		; 2 cycles (skipped or not)
; exited delayfast_loop				; total: 3 * 256 = 768 cycles
 endif


	decfsz		delay_countmedium, 1, 0	; 1 cycle
	goto		delaymedium_loop	; 2 cycles (skipped or not)
; exited delaymedium_loop			; total: ~198000 cycles
						; or     ~768 cycles (no fast)

	decfsz		delay_countslow, 1, 0
	goto		delayslow_loop
; exited delayslow_loop

	endm

define_busywait_delay macro
delay
	implement_busywait_delay clock
	return

	endm
