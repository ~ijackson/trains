; -*- fundamental -*-

; This program:
;   * copies input signals from input pins to output pins, as shown in
;     the `TEST-LEFT' and `TEST-RIGHT' PIC pinout diagrams;
;   * flashes PER-PIC-LED (pin 21) and FLASHER (pin 2) in the
;     pattern [high-low] with 50% duty cycle with period approx 660ms
;     (assuming 20MHz clock input).
;   * leaves all other pins set to their default states (usually Z).

	include		/usr/share/gputils/header/p18f458.inc
	include		onecopybit.inc

	radix		dec

ACCSFR		equ		0x0f00

COUNTINNER	equ		0x00
COUNTOUTER	equ		0x02

 if mclock>=20000
OUTEREXP		equ		7	; 2^7 * 2.6ms = 332ms
 else
 if mclock==1000
OUTEREXP		equ		2	; 2^2 * 52ms = 208ms
 endif
 endif

; we always leave each loop when its counter has reached 0, so on
; entry we assume it's already 0.  For the loops which want to
; count all the way (256 iterations) that's good.

	code

start
	bcf		TRISA, 0, 0	; enable flasher pin output
	bcf		TRISD, 2, 0	; enable per-pic led output
	movlw		0x07 		; turn off A-to-D so we get
	movwf		ADCON1, 0	;  digital inputs on RA0-3 (AN0-3)
	bcf		TRISD, 4, 0	; enable output D
	movlw		0x1f		; enable outputs E,F,G
	movwf		TRISC, 0	;  (RC7,6,5)

loop
	btg		LATA, 0, 0	; toggle flasher pin output
	btg		LATD, 2, 0	; toggle per-pic led output

	; set a bit which says how fast the led
	; should flash and count down from 2^(that bit)
	bsf		COUNTOUTER, OUTEREXP, 0
delayouter_loop

delayinner_loop

	copybiti	PORTB, 5, TRISA, 0x06		; C enable
	copybit		PORTB, 4, LATA,  0x06		; C data
	copybiti	PORTB, 3, TRISA, 0x18		; B enable
	copybit		PORTB, 2, LATA,  0x18		; B data
	copybiti	PORTB, 1, TRISA, 0x20		; A enable
	copybiti	PORTB, 1, TRISE, 0x01		; A enable
	copybit		PORTB, 0, LATA,  0x20		; A data
	copybit		PORTB, 0, LATE,  0x01		; A data

	copybiti	PORTA, 6, LATD,  0x10		; D
	copybiti	PORTC, 0, LATC,  0x80		; E
	copybiti	PORTC, 1, LATC,  0x40		; F
	copybiti	PORTC, 2, LATC,  0x20		; G
						; 12 x copybit @6 = 48cy

	decfsz		COUNTINNER, 1, 0	; 1 cycle
	goto		delayinner_loop		; 2 cycles (skipped or not)
; exited delayinner_loop			; total: 51cy * 256 = 13056cy

						; each cycle 0.2us
						; so each inner loop is ~2.6ms

	decfsz		COUNTOUTER, 1, 0
	goto		delayouter_loop
; exited delayouter_loop

	goto		loop

	end
