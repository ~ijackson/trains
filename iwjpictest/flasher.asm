; -*- fundamental -*-

; This program:
;   * flashes PER-PIC-LED (pin 21) with the pattern
;      [ L-L-Z L-H-Z L-H-L-H-Z L-H-L-H-L-H-Z ]
;     where each state lasts approx. 160ms (at 20MHz clock).
;   * leaves all other pins set to their default states (usually Z).

	include		/usr/share/gputils/header/p18f458.inc
	radix		dec

delay_countfast		equ		0x00
delay_countmedium	equ		0x01
delay_countslow		equ		0x02

clock equ mclock
 include clockvaries.inc
	code

start
loop
	call		red
	call		red
	call		black

	call		red
	call		green
	call		black

	call		red
	call		green
	call		red
	call		green
	call		black

	call		red
	call		green
	call		red
	call		green
	call		red
	call		green
	call		black
	goto		loop

red
	bcf		TRISD, 2, 0		; enable flasher output
	bcf		LATD, 2, 0		; set to low
	goto		delay

green
	bcf		TRISD, 2, 0		; enable flasher output
	bsf		LATD, 2, 0		; set to high
	goto		delay

black
	bsf		TRISD, 2, 0		; disable flasher output
	call		delay
	goto		delay

 define_busywait_delay

	end
