; test for globals and res and stuff

		global fixed
		global somewhere

		include		test.inc

		udata_acs
somewhere	res 1	; silly variable which gets incremented

		udata 0x200
fixed		res 16

vectors		code		0
		goto		startup

		code

startup
		call		nothing_much
		goto		startup

		end
