; -*- fundamental -*-

; This program sets the LED Green and then
; responds to instructions on the serial port
;
; state kept
;   E   current entry
;   R   current address for reading  (Al = low byte; Ah = high_nybble)
;   S   current address for setting  (Bl = low byte; Ah = high_nybble)
;   T   top address nybble for next address set
;   B   bitmask for bit operations
;   F   flags: flag name   weight in FT
;              printset    0x80           print every value we write into *S
;
; prompt is NL CR > SPC initially, then SPC after each command
;
;  data entry
;   0-9a-f    hex digit                      E := (E & 0x0f)<<4 | (digit value)
;   =         confirm value                  print "=", E
;   &         set bitmask                    B := E; print "&", B
;
;  address operations
;   .         set setting address (low byte)   S := TE; print R, ",", S
;   ,         set reading address (low byte)   R := TE; print R, ",", S
;   '         set top address nybble           T := E & 0x0f
;   `         set flags                        F := E; print "`", F
;   @         confirm addresses and bitmask    print R, ",", S, "&", B
;   ~         confirm flags and high nybble    print "`", F, "'", T
;
;  predefined addresses and operations
;   A B C D E set setting address to PORTx     S := 0xf80-0xf84, print op
;   H L Z T   set TRIS and LAT rel.to PORT     modify *(S+0x09), *(S+0x92)
;		according to B		        according to B, and print op
;		(T: toggle LAT, clear TRIS)	NB address addition no carry
;
;  byte operations
;   SPC       read and display                 print *R
;   RET, NL   set whole byte                   printset "S", (*S := E)
;   TAB       read from setting address        print "s", *S
;
;  bitwise operations
;   ?         read and mask                    print "?", (*R & B)
;   _         read and mask from setting addr  printset "_", (*S & B)
;   +         bitwise or (set bits)            printset "+", (*S := *S | B)
;   -         bitwise and-not (clear bits)     printset "-", (*S := *S & ~B)
;   ^         bitwise xor (toggle bits)        printset "^", (*S := *S ^ B)
;
; `printset' means print if `printset' flag is set, otherwise just evaluate

	include		/usr/share/gputils/header/p18f458.inc
	radix		dec

clock equ mclock
	include		insn-aliases.inc
	include		clockvaries.inc
;	list

;------------------------------------------------------------
; variable declarations and RAM memory map

r		equ	FSR0L
rh		equ	FSR0H
star_r		equ	INDF0

s		equ	FSR1L
sh		equ	FSR1H
star_s		equ	INDF1

 udata_acs
 res 2 ; skip 0 and 1
perpicled_bit	res 1

sec_etbf udata_acs 0x30
e		res 1
t		res 1
b		res 1
f		res 1

sec_temps udata_acs 0x38
hex_temp	res 1
original_op	res 1
value_temp	res 1

f_printset_bit	equ	7

sec_a5 udata_acs 0x40
test_loc_a5	res 1

sec_5a udata_acs 0x42
test_loc_5a	res 1

 code

;----------------------------------------
serial_literal macro serial_literal_macro_char
;	transmits char through the serial port
; W		undefined	undefined
	mov_lw		serial_literal_macro_char
	rcall		serial_write_char
	endm

start
	rcall		led_green
	rcall		serial_setup

	clr_f		e
	mov_lw		0x80 ; PORTA
	mov_wf		r
	set_f		rh
	mov_wf		s
	set_f		sh
	set_f		t
	mov_lw		(1<<f_printset_bit)
	mov_wf		f

	mov_lw		0x5a
	mov_wf		test_loc_5a
	mov_lw		0xa5
	mov_wf		test_loc_a5

	serial_literal	10
	serial_literal	13
	serial_literal	'>'

command_loop
	serial_literal	' '
	rcall		serial_read_char

	mov_wf		original_op

check_last set 0
checkequal macro check_value, check_label
	local check_ifnot
	add_lw		check_last - check_value
	bra_nz		check_ifnot
	bra		check_label
check_ifnot
check_last set check_value
	endm

checkatleast macro check_minvalue, check_label
				; if it takes, W gets char - check_minvalue
	add_lw		check_last-check_minvalue
	local check_ifnot
	bra_n		check_ifnot
	bra		check_label
check_ifnot
check_last set check_minvalue
	endm

	; data entry
	checkequal	'=',	command_byte_confirm
	checkequal	'&',	command_byte_bitmask_set
	; address operations
	checkequal	'.',	command_address_set_setting
	checkequal	',',	command_address_set_reading
	checkequal	39,	command_address_set_high
	checkequal	'`',	command_flags_set
	checkequal	'@',	command_address_confirm
	checkequal	'~',	command_confirm_flags_highnyb
	; byte operations
	checkequal	' ',	command_byte_read
	checkequal	10,	command_byte_set
	checkequal	13,	command_byte_set
	checkequal	9,	command_byte_read_fromset
	; bit operations
	checkequal	'?',	command_bitwise_read_mask
	checkequal	'_',	command_bitwise_read_mask_fromset
	checkequal	'+',	command_bitwise_or
	checkequal	'-',	command_bitwise_and_not
	checkequal	'^',	command_bitwise_xor

	; TRIS/LAT operations
	checkequal	'H',	command_trislat_h
	checkequal	'L',	command_trislat_l
	checkequal	'Z',	command_trislat_z
	checkequal	'T',	command_trislat_t

	; these checkatleast ranges have to be in descending
	; order because of the way that checkatleast works

	checkatleast	'f'+1, command_ifnot_letterhexdigit
	checkatleast	'a', command_letterhexdigit
command_ifnot_letterhexdigit

	checkatleast	'E'+1, command_ifnot_portaddr
	checkatleast	'A', command_portaddr
command_ifnot_portaddr

	checkatleast	'9'+1, command_ifnot_digit
	checkatleast	'0', command_digit
command_ifnot_digit

command_wrong
	mov_lw		'?'
	rcall		serial_write_char
command_endswitch
	bra		command_loop

;--------------------
; command digits

command_letterhexdigit
	add_lw		10
command_digit
	mov_wf		hex_temp
	swap_fw		e
	and_lw		0xf0
	ior_wfw		hex_temp
	mov_wf		e
	bra		command_loop

;--------------------
; common routines for command loop

command_endswitch_set
	mov_wf		star_s
	bt_f_if0	f, f_printset_bit
	bra		command_endswitch_set_ifnot_print
	mov_wf		value_temp
	mov_fw		original_op
	rcall		serial_write_char
	mov_fw		value_temp
command_endswitch_phex
	rcall		serial_write_hex
command_endswitch_set_ifnot_print
	bra		command_endswitch

;--------------------
; address commands

command_address_set_setting
	mov_ff		t,sh
	mov_ff		e,s
	bra		command_address_confirm

command_address_set_reading
	mov_ff		t,rh
	mov_ff		e,r
	bra		command_address_confirm

command_address_set_high
	mov_ff		e,t
	bra		command_endswitch

command_flags_set
	mov_ff		e,f
	rcall		confirm_flags
	bra		command_endswitch

command_address_confirm
	rcall		confirm_address
	rcall		confirm_bitmask
	bra		command_endswitch

command_portaddr
	add_lw		PORTA & 0x0ff
	mov_wf		s
	set_f		sh
	mov_fw		original_op
	rcall		serial_write_char
	bra		command_endswitch

;--------------------
; trislat commands

command_trislat_h
	rcall		trislat_setup_indf2_lat
	ior_wff		INDF2
command_trislat_nz
	rcall		trislat_setup_indf2_tris
	com_w
	and_wff		INDF2
command_trislat_echo
	mov_fw		original_op
	rcall		serial_write_char
	bra		command_endswitch

command_trislat_z
	rcall		trislat_setup_indf2_tris
	ior_wff		INDF2
	bra		command_trislat_echo

command_trislat_l
	rcall		trislat_setup_indf2_lat
	com_w
	and_wff		INDF2
	bra		command_trislat_nz

command_trislat_t
	rcall		trislat_setup_indf2_lat
	xor_wff		INDF2
	bra		command_trislat_nz

;----------
trislat_setup_indf2_lat
; W		undefined	B
; INDF2*	undefined	relevant LAT
	mov_lw		0x09
	bra		trislat_setup_indf2_any

;----------
trislat_setup_indf2_tris
; W		undefined	B
; INDF2*	undefined	relevant TRIS
	mov_lw		0x12
	bra		trislat_setup_indf2_any

;----------
trislat_setup_indf2_any
; W		REG-PORT	B
; INDF2*	undefined	relevant REG
	add_wfw		s
	mov_wf		FSR2L
	set_f		FSR2H
	mov_fw		b
	return

;----------
confirm_address
;	prints R, ",", S
; W		undefined	undefined
; hex_temp	undefined	undefined
	mov_fw		rh
	rcall		serial_write_hex_digit
	mov_fw		r
	rcall		serial_write_hex
	mov_lw		','
	rcall		serial_write_char
	mov_fw		sh
	rcall		serial_write_hex_digit
	mov_fw		s
	rcall		serial_write_hex
	return

command_confirm_flags_highnyb
	rcall		confirm_flags
	serial_literal	39
	mov_fw		t
	rcall		serial_write_hex_digit
	bra		command_endswitch

;----------
confirm_bitmask
;	prints "&", B
; W		undefined	undefined
; hex_temp	undefined	undefined
	serial_literal	'&'
	mov_fw		b
	rcall		serial_write_hex
	return

;----------
confirm_flags
;	prints "`", F
; W		undefined	undefined
; hex_temp	undefined	undefined
	serial_literal	'`'
	mov_fw		f
	rcall		serial_write_hex
	return

;--------------------
; byte operations

command_byte_confirm
	serial_literal	'='
	mov_fw		e
	bra		command_endswitch_phex

command_byte_bitmask_set
	mov_ff		e, b
	rcall		confirm_bitmask
	bra		command_endswitch

command_byte_read
	mov_fw		star_r
	bra		command_endswitch_phex

command_byte_set
	mov_lw		'S'
	mov_wf		original_op
	mov_fw		e
	bra		command_endswitch_set

command_byte_read_fromset
	serial_literal	's'
	mov_fw		star_s
	bra		command_endswitch_phex

;--------------------
; bit operations

command_bitwise_read_mask
	serial_literal	'?'
	mov_fw		b
	and_wfw		star_r
	bra		command_endswitch_phex

command_bitwise_read_mask_fromset
	serial_literal	'_'
	mov_fw		b
	and_wfw		star_s
	bra		command_endswitch_phex

command_bitwise_or
	mov_fw		b
	ior_wfw		star_s
	bra		command_endswitch_set

command_bitwise_xor
	mov_fw		b
	xor_wfw		star_s
	bra		command_endswitch_set

command_bitwise_and_not
	mov_fw		b
	com_w
	and_wfw		star_s
	bra		command_endswitch_set

;----------------------------------------
led_green
	bc_f		TRISD, perpicled_bit
	bs_f		LATD, perpicled_bit
	return


;----------------------------------------
serial_setup
; W		undefined	undefined
	mov_lw		(1<<TXEN) | serial_brgh | (1<<TRMT)
	mov_wf		TXSTA	; asynch xmit enabled, high baud rate, 8-bit,
	movlw_movwf_spbrg
serial_receive_reset ;from serial_read_if_error
	mov_lw		(1<<SPEN) | (1<<CREN)
	mov_wf		RCSTA	; enable serial port, continuous rx, 8-bit
	return

;----------------------------------------
serial_read_char
; on errors, sets LED red and transmits *
; W		undefined	character read
serial_read_char_loop
	bt_f_if0	PIR1, RCIF
	bra		serial_read_char_loop
	mov_fw		RCSTA
	and_lw		(1<<CREN) | (1<<FERR) | (1<<OERR)
	xor_lw		(1<<CREN)
	bra_nz		serial_read_if_error
	mov_fw		RCREG
	return

serial_read_if_error
	mov_lw		'*'
	rcall		serial_write_char
	rcall		serial_receive_reset
	bra		serial_read_char_loop

 include syncwrite.inc
 end
