;======================================================================

	include		/usr/share/gputils/header/p18f458.inc
	radix		dec
	include		../iwjpictest/insn-aliases.inc

  code
;----------------------------------------
serial_write_char
; W		character	undefined
serial_write_char_loop
	bt_f_if0	PIR1, TXIF
	bra		serial_write_char_loop
	mov_wf		TXREG
	return

;======================================================================
  include ../iwjpictest/syncwrite.inc
  end
