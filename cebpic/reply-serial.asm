; pin 21 (per-pic-led, RD2/PSP2/C1IN) states: high H = green, low L = red, float Z = black

        include         /usr/share/gputils/header/p18f458.inc
	radix		dec

clock equ mclock
	include		../iwjpictest/clockvaries.inc

	extern	led_green
	extern	led_red
	extern	led_black

	org	0
	goto	initialise_serial

; interrupt routine call - this needs to be at 000018h (low priority interrupt)

	org	000018h
	goto	interrupt_check


	code

initialise_serial

; globally enable interrupts - p77
	bsf	RCON,7,0	; enable priority levels
	bsf	INTCON,7,0	; enable high-priority interrupts
	bsf	INTCON,6,0	; enable low-priority interrupts

; initial config - TXSTA register p181
        bcf     TXSTA,6,0	; p181, set 8-bit mode
  	bsf	TXSTA,5,0	; transmit enable
	bcf	TXSTA,4,0	; asynchronous mode
	bsc_txsta_brgh		; set high or low baud rate
	
; initial config - RCSTA register p182
        bsf	RCSTA,7,0	; serial port enable (p182)
	bsf	RCSTA,6,0	; 8-bit reception
	bsf	RCSTA,4,0	; enable continuous receive

; set SPBRG to get correct baud rate
	movlw_movwf_spbrg

; interrupt set-up for serial receive
	bsf	PIE1,5,0	; enable USART receive interrupt (p85)
	bsf	IPR1,5,0	; set to low-priority interrupt


; LED pin (21) initialisation
        bcf     TRISE,4,0       ; turn off PSPMODE (Data Sheet p100/101)


; timer0 initial config
	morse_t0setup mclock, (1<<TMR0ON), -1, -1

main
	call 	led_green
        call    waiting
	call 	led_black
        call    waiting
	goto	main


waiting
        bcf     INTCON,2,0      ; clear timer0 interrupt bit (p109)
        clrf    TMR0H,0         ; p107 set high bit of timer0 to 0 (buffered,
                                ; so only actually set when write to tmr0l occurs)
        clrf    TMR0L,0         ; set low bit o timer0 - timer now set to 0000h
loop
        btfss   INTCON,2,0      ; check whethr tiomer0 interrupt has been set -
                                ; skip next instruction if so
        bra     loop
        return




interrupt_check
	btfss	PIR1,5,0	; check whether serial receive interrupt bit set
	retfie			; return from interrupt to main if spurious interrupt
	call	led_red
	clrf	WREG		; clear working register
	movff	RCREG,WREG 	; read data out of serial receive buffer -> WREG
	incf	WREG		; increment WREG
	movff	WREG,TXREG	; write data out of WREG -> serial transmit buffer
	retfie


	end
