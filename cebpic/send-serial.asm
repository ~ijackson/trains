; pin 21 (per-pic-led, RD2/PSP2/C1IN) states: high H = green, low L = red, float Z = black

        include         /usr/share/gputils/header/p18f458.inc
	radix		dec

clock equ mclock
	include		../iwjpictest/clockvaries.inc

	code
start
; initial config
        bcf     TXSTA,6,0	; p181, set 8-bit mode
  	bsf	TXSTA,5,0	; transmit enable
	bcf	TXSTA,4,0	; asynchronous mode
	bsc_txsta_brgh		; set high or low baud rate
        bsf	RCSTA,7,0	; serial port enable (p182)

; set SPBRG to get correct baud rate
	movlw_movwf_spbrg

	call	initialiseascii

main
	movff	00h,TXREG	; move 00h to transmit buffer (ascii s)
	call	waitfortsr
	movff	01h,TXREG	; move 01h to transmit buffer (ascii q)
	call	waitfortsr
	movff	02h,TXREG	; move 02h to transmit buffer (ascii o)
	call	waitfortsr
	movff	02h,TXREG	; move 02h to transmit buffer (ascii o)
	call	waitfortsr
	movff	03h,TXREG	; move 03h to transmit buffer (ascii k)
	call	waitfortsr
	movff	04h,TXREG	; move 04h to transmit buffer (ascii SPACE)
	call	waitfortsr
	goto	main

	
waitfortsr
	btfss	TXSTA,1,0	; check whether TRMT is set (i.e. TSR is empty)
	bra 	waitfortsr
	return


initialiseascii	; set up ascii letter bytes 

; there must be a faster way to do this...

	clrf	00h,0	; clear data registers
	clrf	01h,0
	clrf	02h,0
	clrf	03h,0
	clrf	04h,0
	
	bsf	00h,6,0	; set 00h to be ascii s
	bsf	00h,5,0	
	bsf	00h,4,0
	bsf	00h,1,0
	bsf	00h,0,0
	
	bsf	01h,6,0	; set 01h to be ascii q
	bsf	01h,5,0	
	bsf	01h,4,0
	bsf	01h,0,0

	bsf	02h,6,0	; set 01h to be ascii o
	bsf	02h,5,0
	bsf	02h,3,0
	bsf	02h,2,0
	bsf	02h,1,0
	bsf	02h,0,0

	bsf	03h,6,0	; set 03h to be ascii k
	bsf	03h,5,0
	bsf	03h,3,0
	bsf	03h,1,0
	bsf	03h,0,0

	bsf	04h,5,0	; set 04h to be ascii _

	return

	end
