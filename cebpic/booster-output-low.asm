


        include         /usr/share/gputils/header/p18f458.inc

	code

; initialise booster direction
        bcf     TRISC,0,0       ; make pin 0 (booster direction) output
	bcf     PORTC,0,0       ; set low initially

		
; set booster pwm high
        bcf     TRISC,1,0       ; make pin 1 (booster pwm) output
        bsf     PORTC,1,0       ; booster pwm high

loop
	goto loop

	end
