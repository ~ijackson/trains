; pin 21 (per-pic-led, RD2/PSP2/C1IN) states: high H = green, low L = red, float Z = black

        include         /usr/share/gputils/header/p18f458.inc
	radix		dec

clock equ mclock
	include		../iwjpictest/clockvaries.inc

	code
start
; pin initial config
	bcf	TRISE,4,0	; turn off PSPMODE (Data Sheet p100/101)
; timer initial config
	morse_t0setup mclock, (1<<TMR0ON), -1, -1
; actually do stuff
	call 	green
	call 	waiting
	call 	black
	call 	waiting
	call 	red
	call 	waiting
	call 	black
	call 	waiting
	goto	start


green
	bcf	TRISD,2,0	; make pin RD2 an output (DS100)
	bsf	LATD,2,0	; set pin RD2 H (green)
	return

black
	bsf	TRISD,2,0	; make pin RD2 an input (i.e. set Z, black) (DS100)
	return

red
	bcf	TRISD,2,0	; make pin RD2 an output (DS100)
	bcf	LATD,2,0	; set pin RD2 L (red)
	return

waiting
	bcf	INTCON,2,0	; clear timer0 interrupt bit (p109)
	clrf	TMR0H,0		; p107 set high bit of timer0 to 0 (buffered, 
				; so only actually set when write to tmr0l occurs)
	clrf	TMR0L,0		; set low bit o timer0 - timer now set to 0000h
loop
	btfss	INTCON,2,0	; check whethr tiomer0 interrupt has been set -
				; skip next instruction if so
	bra	loop
	return

	end
