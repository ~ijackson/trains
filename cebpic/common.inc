;**********************************************************************
; boilerplate.inc
;  Include this at the top of each file.
;  Does the following things:
;   includes the PIC18F458 definitions file (register and bit names)
;   switches to decimal by default
;   defines various useful macros

	include         /usr/share/gputils/header/p18f458.inc
        radix           dec 

;==============================
; RAM memory usage

; Access bank locations
WREG2		equ	0x000
WREG3		equ	0x001
WREG4		equ	0x002
TESTFLASH       equ     04h     ; test LED flash pattern


;==============================
; Program (flash) memory regions

; Actual code starts at 0 with the reset and interrupt vectors and
; then the program proper.

;--------------------
; Area filled in by morse-auto.asm (see morse-auto.asm-gen for details)
morse_messages_start	equ	0x4000
morse_messages_end	equ	0x4400

;----------------------------------------
; ifbit1(REGISTER,BITNUMBER)
;       executes the next instruction but only if bit BITNUMBER
;       in REGISTER (which must be in the access bank) is set
ifbit1 macro REGISTER, BITNUMBER
        btfsc   REGISTER, BITNUMBER, 0
        endm

;----------------------------------------
; ifbit0(REGISTER,BITNUMBER)
;       executes the next instruction but only if bit BITNUMBER
;       in REGISTER (which must be in the access bank) is clear
ifbit0 macro REGISTER, BITNUMBER
        btfss   REGISTER, BITNUMBER, 0
        endm

