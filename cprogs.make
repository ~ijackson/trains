OPTIMISE= -O2
WERROR= -Werror
CFLAGS=	$(CPPFLAGS) -D_GNU_SOURCE \
	-Wall -Wwrite-strings -Wpointer-arith -Wmissing-declarations \
	-Wnested-externs -Wmissing-prototypes -Wstrict-prototypes \
	$(WERROR) \
	-g $(OPTIMISE)
CPPFLAGS=
LINK=		$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $+ $(LIBS)

LEX=flex
BISON=bison

FLEXCFLAGS=	-Wno-unused -Wno-missing-declarations -Wno-missing-prototypes

%.o:	%.c $(AUTOINCS)
	$(CC) $(CFLAGS) $(SPECIAL_CFLAGS_$*) -MMD -o $@ -c $<

%.c:	%.y
	$(BISON) -o $@ $<

ETAGS_SRCS += $(wildcard *.[ch])

TAGS:	$(ETAGS_SRCS)
	etags $^
