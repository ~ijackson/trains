#!/usr/bin/perl -n
# This script snarfs the BZX84C models from
#  http://www.diodes.com/spicemodels/index.php?language=en
# which is checked in here as diodes.com-spice.txt, and turns
# them into what is now known here as bzx55.mod.
#
# Justification: Farnell catalogue claims that the BZX84C (SMD)
# are equivalents for the `popular' BZX55.

BEGIN { print "*BZX55 - GENERATED FILE DO NOT EDIT*\n" or die $! }
s/\r?\n/\n/;
next unless m/^\*SRC=BZX84C\d+V?\d+\;/..m/.ENDS/;
s/^(\.subckt)\sdi_bzx84(c\w*)\s/$1 bzx55\-\L$2 /i;
print or die $!;
