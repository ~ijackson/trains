#!/usr/bin/perl -w

use strict;

our($qty) = 50;
our(@e12) = qw(10 12 15 18 22 27 33 39 47 56 68 82);
our(@ex24) = qw(11 13 16 20 24 30 36 43 51 62 75 91);
our(@e24)= sort @e12, @ex24;
our(@list) = ([ \@e12, [qw(xrx)] ],
	      [ \@e24, [qw(xxr xx0r xkx xxk xx0k)] ],
	      [ [qw(10)], [qw(xmx)] ]);

sub addm ($$) {
    my ($values,$decades) = @_;
    my ($decade,$value);
    
    foreach $decade (@$decades) {
	foreach $value (@$values) {
	    $_= $decade;
	    s/x/ substr($value,0,1) /e;
	    s/x/ substr($value,1,1) /e;
	    s/0$//;
	    printf "\tstd %s \t%d=\n", $_, $qty or die $!;
	}
    }
}

sub main () {
    my ($e);
    print "Resistors\n" or die $!;
    foreach $e (@list) {
	addm($e->[0], $e->[1]);
    }
}

main();
