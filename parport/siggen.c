/**/

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/ioctl.h>
#include <linux/ppdev.h>

#include "lib.h"

static void badusage(void) {
  fprintf(stderr,
	  "usage: siggen [m<mult>] item...\n"
	  "items:\n"
	  "  k<value>   PPWCONTROL value\n"
	  "  t<value>   PPWDATA    value\n"
	  "  w<value>   usleep(value)\n"
	  "options:\n"
	  "  n<value>   do whole list n times\n"
	  "  m<value>   do every item m times\n");
  exit(12);
}	  

int main(int argc, const char *const *argv) {
  int ii, io=0, m=1, n=-1, loops_count, loops_n, loops_incr;
  unsigned long ul;
  unsigned char v;
  const char *a;

  argc--;
  argv++;
  while (argc>=1 && (a=argv[0])) {
    switch (a[0]) {
    case 'm': m= number(a+1); break;
    case 'n': n= number(a+1); break;
    default: goto loop_break_options;
    }
    argc--;
    argv++;
  }
loop_break_options:
  if (argc<1) badusage();

  if (n==-1) {
    loops_n= 1;
    loops_incr= 0;
  } else {
    loops_n= n * argc * m;
    loops_incr= 1;
  }
  for (ii=0, loops_count=0;
       loops_count < loops_n;
       ii++, ii%=(argc*m), loops_count += loops_incr) {
    a= argv[ii/m];
    switch (a[0]) {
    case 'k': io= PPWCONTROL; break;
    case 't': io= PPWDATA;    break;
    case 'w': io= -1;         break;
    default: badusage();
    }
    ul= number(a+1);
    if (io==-1) {
      usleep_gettod(ul);
    } else {
      v= ul;
      doioctl(io, &v, v);
    }
  }
  return 0;
}
