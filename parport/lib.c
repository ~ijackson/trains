/**/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

#include <sys/ioctl.h>

#include "lib.h"

int fd;

void doioctl(int ioctlnum, void *vp, unsigned long vpv) {
  int r;
  errno=0;
  r= ioctl(fd, ioctlnum, vp);
  if (r) {
    fprintf(stderr,"ioctl #%d 0x%lx gave %d %s\n",
	    ioctlnum, vpv, r, strerror(errno));
    exit(127);
  }
}

unsigned long number(const char *a) {
  unsigned long v;
  char *ep;

  v= strtoul(a,&ep,0);
  if (*ep || ep==a) {
    fprintf(stderr,"bad number `%s'\n",a);
    exit(127);
  }
  return v;
}

static void gettod(struct timeval *now_r) {
  int r;
  r= gettimeofday(now_r,0);
  if (r) { perror("gettimeofday failed"); exit(127); }
}  

void usleep_gettod(unsigned long us) {
  struct timeval start, end, now;
  ldiv_t ld;

  gettod(&start);

  ld= ldiv(start.tv_usec + us, 1000000);
  end.tv_sec= start.tv_sec + ld.quot;
  end.tv_usec= ld.rem;

  do {
    gettod(&now);
  } while (timercmp(&now,&end,<));
}
