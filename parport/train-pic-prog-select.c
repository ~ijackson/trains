/**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <linux/ppdev.h>

#include "lib.h"

#define COLUMNS 7
#define ROWS 8

static const void badusage(const char *what) {
  fprintf(stderr,"bad usage: %s\n"
	  "usage: train-pic-prog-select -i0|-i1"
	  " [-p/dev/parport] picno|range...\n"
	  "ranges are picno-picno (inclusive)\n"
	  "-i0 means PPWDATA bit is 0 for selected pics; -i1 means 1\n"
	  " (default: value of TRAIN_PPWDATA_SEL_BIT env. var)\n"
	  "-pstdin means use fd 0; -pstdin-noclaim uses fd 0 and"
	  " doesn't PPCLAIM\n",
	  what);
  exit(126);
}

typedef unsigned char yesno;

const char *parport= "/dev/parport0";

static int yesfor1bit= -1; /* same as bit needed for `yes' :-) */

static union {
  yesno rect[COLUMNS][ROWS];
  yesno lin[COLUMNS*ROWS];
} yesses;

static void wpause(int ioctlnum, unsigned char value) {
  printf(" %s%02x",
	 ioctlnum == PPWCONTROL ? "C" :
	 ioctlnum == PPWDATA ? "" : 0,
	 value);
  doioctl(ioctlnum, &value, value);
  usleep(5000);
}

int main(int argc, const char *const *argv) {
  long first, last;
  int row, col, v, nselected;
  char *ep;
  const char *arg;
  
  if (!*argv++) badusage("need argv[0]");
  while ((arg= *argv) && arg[0]=='-') {
    argv++;
    if (arg[1]=='p') {
      parport= arg+2;
    } else if (arg[1]=='i') {
      yesfor1bit= atoi(arg+2);
    } else {
      badusage("unknown option");
    }
  }
  if (!(yesfor1bit==0 || yesfor1bit==1)) badusage("need proper -i value");

  if (!*argv) badusage("need to specify pics");

  while ((arg= *argv++)) {
    first= last= strtol(arg,&ep,10);
    if (ep==arg) badusage("syntactically incorrect pic number");
    if (*ep == '-') {
      last= strtol((arg=ep+1),&ep,10);
      if (ep==arg) badusage("syntactically incorrect range");
    }
    if (*ep) badusage("syntactically incorrect picno or range");
    if (first<0 || last>COLUMNS*ROWS-1 || first>last)
      badusage("picno or range out of permissible range");
    for (; first<=last; first++) {
      if (yesses.lin[first]) badusage("one pic specified more than once");
      yesses.lin[first]= 1;
    }
  }
  if (!strcmp(parport,"stdin") || !strcmp(parport,"stdin-noclaim")) {
    fd= 0;
  } else {
    fd= open(parport, O_RDWR);
    if (fd<0) { perror(parport); exit(-1); }
  }

  setvbuf(stdout,0,_IONBF,0);
  
  for (nselected=0, row=0; row<ROWS; row++)
    for (col=0; col<COLUMNS; col++)
      if (yesses.rect[col][row])
	nselected++;

  if (!strcmp(parport,"stdin-noclaim")) {
    printf("Not claiming.\n");
  } else {
    printf("Claiming %s...\n", parport);
    doioctl(PPCLAIM,0,0);
  }

  printf("Selecting %d/%d; bytes:", nselected, ROWS*COLUMNS);
  wpause(PPWCONTROL, 0x02 /* !ENGAGE_VPP, !PC, !PDW */);

  for (row=0; row<ROWS; row++) {
    v= 0;
    for (col=0; col<COLUMNS; col++)
      v |= yesses.rect[col][row]==yesfor1bit ? 0 : (1<<col);
    wpause(PPWDATA, v | 0x80);
    wpause(PPWDATA, v); /* SEL_CLK  ~|_  shifts */
  }
  wpause(PPWDATA, yesfor1bit ? 0xff : 0x80); /* SEL_CLK  _|~  deploys */
  putchar('\n');
  return 0;
}
