/**/

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/ppdev.h>

#include "lib.h"

static void badusage(void) {
  fprintf(stderr,
	  "usage: readlots <&parport\n"
	  "options: none\n");
}	  

int main(int argc, const char *const *argv) {
  unsigned char v;
  struct timeval tv;
  int r;

  argc--;
  argv++;
  if (argc>=1) badusage();

  for (;;) {
    r= gettimeofday(&tv,0); assert(!r);
    doioctl(PPRSTATUS, &v, 0);
    printf("%10lu.%06lu %02x\n", tv.tv_sec, tv.tv_usec, v);
  }
}
