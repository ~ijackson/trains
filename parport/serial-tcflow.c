#include <termios.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>

int main(int argc, const char *const *argv) {
  int op, r, v=0, *vp=0, v0;
  
  switch (argv[1][0]) {
  case 'N': op= TCOOFF; break;
  case 'Y': op= TCOON;  break;
  case 'n': op= TCIOFF; break;
  case 'y': op= TCION;  break;
  case 'g': op= TIOCMGET; vp=&v; break;
  case 's': op= TIOCMSET; vp=&v; v=atoi(argv[1]+1); break;
  case 'C': op= TIOCMBIC; vp=&v; v=atoi(argv[1]+1); break;
  case 'S': op= TIOCMBIS; vp=&v; v=atoi(argv[1]+1); break;
  default: abort();
  }
  errno= 0;
  v0= v;
  if (!vp) {
    v= 0;
    r= tcflow(3,op);
  } else {
    r= ioctl(3,op,vp);
  }
  fprintf(stderr,"v0=0x%02x r=%d v=0x%02x e=%s\n",v0,r,v,strerror(errno));
  return 0;
}
